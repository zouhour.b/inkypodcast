﻿1
00:00:00,000 --> 00:00:05,160
Quand j'ai vu comment l'image
ressortait sur le téléphone,

2
00:00:05,160 --> 00:00:10,960
ça m'a surpris. 
L'ambiance s'est amplifiée.

3
00:00:10,960 --> 00:00:13,040
C'était assez hypnotique.

4
00:00:13,040 --> 00:00:15,960
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

5
00:00:15,960 --> 00:00:18,080
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

6
00:00:18,080 --> 00:00:19,200
qui criait au milieu
de l'avenue Habib Bourguiba.

7
00:01:09,360 --> 00:01:11,400
14 janvier 2011.

8
00:01:11,400 --> 00:01:15,080
L'UGTT a annoncé la grève générale

9
00:01:15,080 --> 00:01:17,640
et a appelé à une manifestation
au départ de la place Mohamed Ali.

10
00:01:17,640 --> 00:01:20,040
Des milliers de Tunisien·nes
ont répondu à l'appel.

11
00:01:20,040 --> 00:01:23,880
Dont Imed, 43 ans,
qui travaille dans la publicité.

12
00:01:23,880 --> 00:01:27,000
Quand le gouvernement
a annoncé un couvre-feu pour 17h,

13
00:01:27,000 --> 00:01:29,000
il s'est retrouvé chez son ami Zein

14
00:01:29,000 --> 00:01:31,000
dans un appartement situé
sur l'avenue Habib Bourguiba,

15
00:01:31,000 --> 00:01:32,400
au-dessus de l'Univers

16
00:01:32,400 --> 00:01:35,200
un bar emblématique
dans le milieu militant de gauche.

17
00:01:35,200 --> 00:01:38,040
À la nuit tombée,
il n'y avait plus aucun bruit

18
00:01:38,040 --> 00:01:40,720
ni dans les rues,
ni dans les maisons.

19
00:01:40,720 --> 00:01:43,880
Imed entend alors des cris
qui rompent ce silence.

20
00:01:43,880 --> 00:01:45,400
Abdennaceur Laouini

21
00:01:45,400 --> 00:01:47,760
un avocat connu
dans le milieu militant,

22
00:01:47,760 --> 00:01:49,360
crie à perdre haleine :

23
00:01:49,360 --> 00:01:52,080
"Ben Ali s'est enfui".

24
00:01:52,080 --> 00:01:55,120
Imed descend et le rejoint,

25
00:01:55,120 --> 00:01:59,200
en plein couvre-feu,
dans une avenue quadrillée par les policiers.

26
00:01:59,200 --> 00:02:01,920
Il sort son téléphone
et enregistre une vidéo

27
00:02:01,920 --> 00:02:04,280
qui s'inscrira
dans la mémoire collective tunisienne

28
00:02:04,280 --> 00:02:07,840
comme un symbole de la victoire
des peuples sur la dictature.

29
00:02:07,840 --> 00:02:09,480
Pendant un an,

30
00:02:09,480 --> 00:02:12,320
en préparation de la commémoration
des 10 ans de la révolution,

31
00:02:12,320 --> 00:02:15,120
Inkyfada va raconter les histoires
de quelques personnes parmi des milliers

32
00:02:15,120 --> 00:02:17,480
ayant vécu cette journée historique.

33
00:02:19,400 --> 00:02:22,080
Tu étais où le 14 janvier ?

34
00:02:22,080 --> 00:02:26,000
Épisode 1 : Ben Ali hrab

35
00:02:31,809 --> 00:02:34,129
14 janvier.

36
00:02:34,129 --> 00:02:36,969
C’était un vendredi, mais on avait 
l'impression d'être un dimanche.

37
00:02:36,969 --> 00:02:38,969
Il y avait quelque chose de spécial

38
00:02:38,969 --> 00:02:41,129
avec l'appel à la grève générale.

39
00:02:41,129 --> 00:02:43,609
Je me suis habillé et suis sorti
pour chercher un taxi.

40
00:02:43,609 --> 00:02:45,609
Il n'y en avait pas,
j'ai attendu dans la rue.

41
00:02:45,609 --> 00:02:49,049
Quelqu'un d'autre attendait
à côté de moi.

42
00:02:49,049 --> 00:02:50,369
“Tu attends un taxi ?"

43
00:02:50,369 --> 00:02:52,809
"S'il y en a un qui arrive,
on le partage ?"

44
00:02:52,809 --> 00:02:53,849
il m'a dit ok.

45
00:02:53,849 --> 00:02:54,969
Je lui ai demandé où il allait,

46
00:02:54,969 --> 00:02:56,489
il n'a pas voulu répondre.

47
00:02:56,489 --> 00:02:58,489
J'ai senti qu'il avait peur.

48
00:02:58,489 --> 00:03:02,209
Ça raconte ce que les Tunisien·nes
vivaient à l’époque.

49
00:03:02,209 --> 00:03:04,449
Les gens vivaient dans la peur.

50
00:03:17,889 --> 00:03:18,889
On est descendus.

51
00:03:18,889 --> 00:03:22,769
C'était différent cette fois.

52
00:03:22,769 --> 00:03:25,609
Parmi les gens autour de toi,

53
00:03:25,609 --> 00:03:27,769
il y a beaucoup de visages inconnus.

54
00:03:27,769 --> 00:03:32,000
Mais ce n'était pas des policiers infiltrés
comme d'habitude.

55
00:03:32,009 --> 00:03:39,720
Ça avait l’air cool.
On se sentait dans un parc familial.

56
00:03:40,929 --> 00:03:43,689
J’ai retrouvé Zein au milieu de la foule.

57
00:03:43,689 --> 00:03:46,049
J'avais besoin d'eau, j'avais soif.

58
00:03:46,049 --> 00:03:51,969
Ça faisait environ trois heures
que j’étais là.

59
00:03:53,209 --> 00:03:57,129
Zein est un vieil ami.

60
00:03:57,129 --> 00:03:59,529
On se connaît depuis longtemps.

61
00:03:59,529 --> 00:04:02,809
Il habite dans le centre-ville,
au-dessus de l'Univers.

62
00:04:02,809 --> 00:04:04,809
On y reviendra après.

63
00:04:04,809 --> 00:04:07,289
L'objectif était de trouver
une bouteille d'eau.

64
00:04:07,289 --> 00:04:09,009
Tout était fermé.

65
00:04:09,009 --> 00:04:13,320
Sauf "Bouabenna",
un bar-resto rue de Marseille.

66
00:04:13,320 --> 00:04:13,329
Sauf "Bouabenna",
un bar-resto rue de Marseille.
On est montés.

67
00:04:13,329 --> 00:04:14,609
On est montés.

68
00:04:14,609 --> 00:04:18,449
Comme on n'a pas trouvé d'eau,
on a pris de la bière.

69
00:04:18,449 --> 00:04:21,169
Ça compense.

70
00:04:21,169 --> 00:04:25,929
Il n'y avait rien à manger,
le gérant nous a servi ses propre pâtes.

71
00:04:27,129 --> 00:04:29,569
Après quelques bières,
on est redescendus.

72
00:04:37,649 --> 00:04:39,649
Quand on est sortis du bar,
c’était le bordel.

73
00:04:39,649 --> 00:04:43,809
L'objectif, c'était de rejoindre l'appartement
au-dessus de l'Univers.

74
00:04:43,809 --> 00:04:46,369
On a dû faire un énorme détour.

75
00:04:46,369 --> 00:04:48,849
Tout était bloqué
sur l'avenue Habib Bourguiba.

76
00:04:48,849 --> 00:04:55,969
Pour faire 200 mètres,
entre rue de Marseille et l’Univers,

77
00:04:55,969 --> 00:04:57,169
c’était impossible.

78
00:04:57,169 --> 00:05:01,929
il fallait faire des détours et se cacher,

79
00:05:01,929 --> 00:05:06,089
pour éviter les policiers
qui passaient avec leurs matraques.

80
00:05:06,089 --> 00:05:09,420
L'ambiance était dingue.

81
00:05:09,420 --> 00:05:11,824
On avait l'impression d’être en pleine guerre.

82
00:05:26,440 --> 00:05:28,960
C’était le chaos avec la fumée blanche
des lacrymogènes partout,

83
00:05:28,960 --> 00:05:32,920
les gens couraient dans tous les sens,
tout le monde s'est dispersé.

84
00:05:32,920 --> 00:05:35,520
Tout était blanc, embrouillé.

85
00:05:35,520 --> 00:05:40,170
Et dans ce brouillard,
tu vois des silhouettes

86
00:05:40,176 --> 00:05:41,520
mais tu ne distingues qu’au dernier moment

87
00:05:41,520 --> 00:05:43,520
si ce sont des gens
ou s'ils portent des casques.

88
00:05:48,720 --> 00:05:53,340
On a fait tout ce trajet
pour finalement arriver chez Zein.

89
00:05:53,344 --> 00:05:56,010
La télévision était allumée.

90
00:05:56,016 --> 00:05:58,510
Ils ont annoncé un couvre-feu pour 17h.

91
00:05:58,512 --> 00:06:01,640
Donc on était obligés de rester sur place.

92
00:06:02,620 --> 00:06:03,860
Interdiction de sortir.

93
00:06:03,860 --> 00:06:07,900
Tout était vide.
Il n’y avait que des policiers et des militaires.

94
00:06:07,900 --> 00:06:11,300
Je suis resté chez Zein, avec Houria et Jalel,

95
00:06:11,300 --> 00:06:12,940
sur l’avenue Habib Bourguiba.

96
00:06:12,940 --> 00:06:18,260
On avait une vue sur ce qui se passait,

97
00:06:18,260 --> 00:06:21,660
avec les flics, les gaz lacrymogènes…

98
00:06:21,660 --> 00:06:25,260
Il ne restait presque plus personne.
Tout le monde avait disparu.

99
00:06:25,260 --> 00:06:28,720
Là, à la télé on annonce que…
Comment il s’appelle déjà ?

100
00:06:28,740 --> 00:06:33,700
Ben Ali est monté dans un avion
et est parti.

101
00:06:33,700 --> 00:06:36,660
Vers la Libye, la France…
Chacun sa version.

102
00:06:36,660 --> 00:06:40,620
“Le président Zine el Abidine Ben Ali
a quitté le pouvoir.

103
00:06:40,620 --> 00:06:45,100
Le premier ministre Mohamed Ghannouchi
devrait assurer l’intérim.

104
00:06:45,100 --> 00:06:50,460
Ben Ali a quitté la Tunisie pour Malte,
sous protection libyenne vers 17h.

105
00:06:50,460 --> 00:06:56,300
Notre source avait indiqué
qu’il pourrait se rendre en France”.

106
00:06:57,340 --> 00:07:04,340
C’était un moment très spécial.

107
00:07:04,340 --> 00:07:12,100
On a vécu avec cette présence, cette photo,
pendant 23 ans.

108
00:07:12,100 --> 00:07:14,460
Il y a eu une rupture à ce moment-là.

109
00:07:14,460 --> 00:07:18,700
Le symbole du pouvoir était parti.

110
00:07:18,700 --> 00:07:25,300
J’imagine que tout le monde a eu peur.

111
00:07:25,300 --> 00:07:28,220
Même celles et ceux
qui voulaient un changement

112
00:07:28,220 --> 00:07:32,340
ou prendre sa place,

113
00:07:32,340 --> 00:07:33,740
ne s'y attendaient pas.

114
00:07:33,740 --> 00:07:36,420
Ça a pris tout le monde de court.

115
00:07:36,420 --> 00:07:45,460
Normalement, dans ces circonstances,
les gens devraient réagir,

116
00:07:45,460 --> 00:07:49,060
sortir dans la rue, klaxonner,
exprimer leur joie.

117
00:07:49,060 --> 00:07:52,140
Mais ils n’y avait rien de tout ça,
c’était le vide total.

118
00:07:52,140 --> 00:07:57,140
Rien… à part quelques chats.
Et des policiers bien sûr.

119
00:07:57,140 --> 00:07:59,140
Je suis sorti à la fenêtre
pour fumer une cigarette.

120
00:07:59,140 --> 00:08:02,540
Les policiers sont passés et m’ont insulté,

121
00:08:02,540 --> 00:08:05,180
ils m’ont demandé de fermer la fenêtre.

122
00:08:05,180 --> 00:08:09,700
Leur langage habituel.

123
00:08:09,700 --> 00:08:12,220
inutile de rentrer dans les détails.

124
00:08:12,220 --> 00:08:15,540
Et à ce moment-là,

125
00:08:15,540 --> 00:08:17,180
j’entends quelqu’un crier dans la rue.

126
00:08:18,980 --> 00:08:21,180
Alors que l'avenue Bourguiba était vide,

127
00:08:21,180 --> 00:08:26,740
il n'y avait des policiers, dans la nuit,
ni voiture ni personne.

128
00:08:26,740 --> 00:08:33,620
Vous n’avez pas idée de l’écho
que ça produisait.

129
00:08:33,620 --> 00:08:36,740
En l'entendant,
je me suis mis à rire

130
00:08:36,740 --> 00:08:38,500
avant de voir qui c'était.

131
00:08:38,500 --> 00:08:42,060
D'ailleurs, j'ai dit à Zein :
“ça y est, les lèche-bottes sont de sortie”.

132
00:08:42,060 --> 00:08:47,420
Parce qu'ils étaient déjà sortis
avec des voitures de location,

133
00:08:47,420 --> 00:08:56,940
pour faire leur propagande
avec des photos de Ben Ali et des youyous.

134
00:08:56,940 --> 00:08:58,940
Donc j’entends le type crier :

135
00:08:58,940 --> 00:09:03,740
“Ben Ali s’est enfui".

136
00:09:03,740 --> 00:09:07,820
Ce n’était pas très audible.
Ça se voyait qu’il était ivre en plus.

137
00:09:07,820 --> 00:09:10,020
J'ai appelé Zein :

138
00:09:10,020 --> 00:09:14,220
“Regarde, c’est le premier
à avoir retourné sa veste”.

139
00:09:16,100 --> 00:09:20,660
Quand il s’est approché,
j’ai reconnu Abdennaceur Laouini.

140
00:09:20,660 --> 00:09:22,660
On se connaît depuis la fac.

141
00:09:22,660 --> 00:09:27,820
il était actif au sein de l’UGET
(syndicat étudiant de gauche).

142
00:09:27,820 --> 00:09:34,420
Au milieu de cette désolation,
ça m’a fait plaisir de voir un visage familier.

143
00:09:34,420 --> 00:09:39,860
Il est sorti pour exprimer sa joie
à sa manière.

144
00:09:39,860 --> 00:09:45,060
Du coup je l’ai rejoint,
on s’est embrassés, on était contents.

145
00:09:45,060 --> 00:09:49,340
Je ne vous cache pas
que j’étais moi-même surpris.

146
00:09:49,340 --> 00:09:53,900
On était vraiment seuls,
au milieu de l’avenue Habib Bourguiba, vide.

147
00:09:53,900 --> 00:09:56,820
Il n’y avait que des policiers,
dans le noir.

148
00:09:56,820 --> 00:10:05,820
C’était une expérience unique.

149
00:10:05,820 --> 00:10:11,740
Ça m’a fait rire,
la situation était absurde.

150
00:10:11,740 --> 00:10:16,780
Où sont les gens ?

151
00:10:16,780 --> 00:10:26,820
Imaginez la soirée du siècle,

152
00:10:26,820 --> 00:10:30,500
les gens se battraient pour avoir une place

153
00:10:30,500 --> 00:10:34,540
mais au final,
il n’y a qu’un seul spectateur.

154
00:10:34,540 --> 00:10:37,170
C'est ça qui était absurde.

155
00:10:37,175 --> 00:10:42,820
À un moment, je me retourne
et je vois Zein et sa mère Houria

156
00:10:42,820 --> 00:10:45,820
à l’entrée de l’immeuble.

157
00:10:45,820 --> 00:10:48,020
"Trois personnes sont sorties sur l’avenue
Habib Bourguiba, vide."

158
00:10:48,025 --> 00:10:50,340
"Regarde par là-bas."

159
00:10:50,340 --> 00:10:51,740
“Le criminel s’est enfui”

160
00:10:51,740 --> 00:10:53,140
“Ben Ali s’est enfui”

161
00:10:53,140 --> 00:10:55,260
"Vive la Tunisie libre”

162
00:10:55,860 --> 00:10:57,820
“Comme il est courageux !”

163
00:11:04,140 --> 00:11:05,860
"Écoute, il y a quelqu’un dans la rue,

164
00:11:05,860 --> 00:11:10,980
heureux, il raconte comment les gens ont été torturés.

165
00:11:10,980 --> 00:11:13,180
Tu n’as pas idée,

166
00:11:13,180 --> 00:11:17,500
il m’a donné la chair
de poule sans le savoir. Écoute !”

167
00:11:18,660 --> 00:11:22,500
"Où sont les voitures et les youyous
achetés par Ben Ali hier ?”

168
00:11:22,500 --> 00:11:27,180
“Où sont les voitures de location ?”

169
00:11:32,800 --> 00:11:37,200
C’était l’idée de Houria. Je ne sais pas
pourquoi elle m’a dit de filmer.

170
00:11:37,200 --> 00:11:43,360
Mon téléphone n’a pas une caméra
de bonne qualité.

171
00:11:43,360 --> 00:11:51,720
Mais il fallait enregistrer ce moment, en vidéo.

172
00:11:51,720 --> 00:11:53,840
“On s’est libéré·es. Ben Ali s’est enfui.

173
00:11:53,840 --> 00:11:59,760
Il s’est enfui. Ben Ali s’est enfui.
Ben Ali s’est enfui.

174
00:11:59,760 --> 00:12:04,120
La Tunisie sans Ben Ali.
Le criminel s’est enfui.

175
00:12:04,120 --> 00:12:10,000
Le criminel a été destitué.
Le peuple tunisien a destitué Ben Ali.”

176
00:12:11,960 --> 00:12:17,360
Quand j’ai vu comment l’image
ressortait sur mon téléphone,

177
00:12:17,360 --> 00:12:20,850
sur l'écran de ce Nokia,
que je tenais dans ma main.

178
00:12:20,850 --> 00:12:25,440
L’ambiance s’est amplifiée.

179
00:12:25,440 --> 00:12:27,520
C’était hypnotique.

180
00:12:27,520 --> 00:12:30,150
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

181
00:12:30,150 --> 00:12:32,270
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

182
00:12:32,270 --> 00:12:34,270
qui criait au milieu
de l'avenue Habib Bourguiba.

183
00:12:35,360 --> 00:12:38,550
Vers la fin de la vidéo, alors qu’il disait
“Ben Ali s’est enfui”,

184
00:12:38,550 --> 00:12:41,200
j’ai rigolé, la situation m'a fait rire.

185
00:12:41,200 --> 00:12:44,520
Mais c’était courageux, impressionnant.

186
00:12:44,520 --> 00:12:46,840
Il faut toujours que quelqu’un se lance en premier,

187
00:12:46,840 --> 00:12:51,080
mais bizarrement, personne n’est sorti après.

188
00:12:51,080 --> 00:12:57,160
Alors que des milliers de personnes
étaient sur l’avenue

189
00:12:57,160 --> 00:13:01,360
et étaient restées bloquées comme moi.

190
00:13:01,360 --> 00:13:04,360
Certaines ont dormi sur des toits d’immeuble,

191
00:13:04,360 --> 00:13:07,200
d’autres se sont cachées chez des inconnu·es,

192
00:13:07,200 --> 00:13:10,640
il y avait plein de monde

193
00:13:10,640 --> 00:13:13,200
et tout le monde savait que Ben Ali s’était enfui.

194
00:13:13,200 --> 00:13:15,880
Il y a même eu une deuxième vidéo

195
00:13:15,880 --> 00:13:26,200
de cette fille coincée dans un centre
d’appel avec d’autres personnes.

196
00:13:26,200 --> 00:13:30,320
Plein de gens regardaient par la fenêtre,
bloqué·es sur l’avenue Habib Bourguiba,

197
00:13:30,320 --> 00:13:32,425
mais ils ont eu peur de sortir.

198
00:13:34,960 --> 00:13:35,960
“Félicitations !”

199
00:13:50,840 --> 00:13:52,760
“Fermez toutes les fenêtres !”

200
00:13:52,760 --> 00:13:56,600
“Ils nous disent de fermer la fenêtre, allez”.

201
00:14:05,720 --> 00:14:10,600
Après avoir filmé, les policiers sont arrivés
pour nous dire de partir.

202
00:14:10,600 --> 00:14:16,800
Finalement, Abdennaceur est rentré
et je suis retourné chez Zein.

203
00:14:16,800 --> 00:14:22,920
Je n’avais pas de connexion ce soir-là.

204
00:14:22,920 --> 00:14:30,880
Donc j’ai dû attendre le lendemain matin
pour la mettre en ligne depuis chez moi.

205
00:14:37,760 --> 00:14:43,160
Je me suis réveillé le matin, tout était bloqué.

206
00:14:43,160 --> 00:14:46,840
Je devais sortir de l’avenue Habib Bourguiba
mais elle était interdite d’accès.

207
00:14:46,840 --> 00:14:52,520
C’était absurde, un policier m'a dit que
je n’avais pas le droit de sortir de l’avenue.

208
00:14:54,240 --> 00:14:58,600
“Pourquoi ? Je veux rentrer !”

209
00:14:58,600 --> 00:15:05,720
Donc il fallait improviser.

210
00:15:05,720 --> 00:15:12,720
Tu sens que tu es une cible mobile.
Mais ça s’est bien passé finalement.

211
00:15:23,720 --> 00:15:31,400
L’objectif n’était pas de faire de la pub
ou que la vidéo devienne virale.

212
00:15:31,400 --> 00:15:35,280
C’était juste la vidéo qu’il fallait partager.

213
00:15:35,280 --> 00:15:44,520
Après quelques jours,
je me suis rendue compte de l’ampleur.

214
00:15:44,520 --> 00:15:50,040
La vidéo était diffusée en continu
sur Nessma TV, Al Jazeera, d’autres médias...

215
00:15:50,040 --> 00:15:52,640
C’était incroyable.

216
00:15:52,640 --> 00:15:58,240
On voyait Rached Ghannouchi pleurer dessus
et les gens qui réagissaient !

217
00:15:58,240 --> 00:16:00,520
Ça m’a vraiment étonné.

218
00:16:00,520 --> 00:16:04,920
Est-ce que les gens ont aimé la vidéo

219
00:16:04,920 --> 00:16:09,880
parce qu’ils avaient eu peur
de sortir ce soir-là ?

220
00:16:09,880 --> 00:16:14,600
Le 14 janvier, quand ils ont dit
que Ben Ali s’était enfui,

221
00:16:14,600 --> 00:16:22,000
c’était l’occasion à ne pas rater pour célébrer.

222
00:16:22,000 --> 00:16:26,225
C’était le rêve caché de beaucoup de Tunisien·nes.

223
00:16:26,225 --> 00:16:28,480
qu'ils et elles soient sortis manifester ou pas.

224
00:16:28,480 --> 00:16:33,320
Tout le monde voulait un changement
mais quand il arrive,

225
00:16:33,320 --> 00:16:36,130
tout le monde se cache. C’est bizarre.

226
00:16:36,130 --> 00:16:40,120
Le 14 janvier,

227
00:16:40,120 --> 00:16:46,680
les gens étaient sortis le matin,
ils étaient beaucoup plus nombreux que les policiers.

228
00:16:46,680 --> 00:16:51,760
Je ne sais pas pourquoi ils ont eu peur après,
alors qu’ils ne devaient plus avoir peur.

229
00:16:51,760 --> 00:17:01,760
Ils ont repris les mêmes réflexes, d’attendre devant
la télévision pour voir ce qui va se passer.

230
00:17:01,760 --> 00:17:07,160
Et c’est la télévision qui te dit ce qui se passe.
C’est décevant.

231
00:17:12,040 --> 00:17:20,680
Pendant les jours qui ont suivi,
avec l’absence des policiers,

232
00:17:20,680 --> 00:17:25,470
les gens se sont organisés,

233
00:17:25,475 --> 00:17:32,170
ils ont commencé à gérer de manière autonome,

234
00:17:32,170 --> 00:17:32,175
ils ont commencé à gérer de manière autonome,
à s’organiser entre eux, tenir la garde pendant la nuit.

235
00:17:32,175 --> 00:17:38,600
à s’organiser entre eux, tenir la garde pendant la nuit.

236
00:17:38,600 --> 00:17:43,040
Il y avait beaucoup de solidarité, de partage,

237
00:17:43,040 --> 00:17:53,880
comme avant dans les quartiers. Les gens
qui s’entraident, qui cuisinent et partagent,

238
00:17:53,880 --> 00:17:58,720
c’était magnifique. Ça pouvait être le début
de quelque chose, d’un mouvement.

239
00:17:58,720 --> 00:18:05,950
Les gens contrôlaient même les policiers,
ils les arrêtaient et les fouillaient.

240
00:18:05,950 --> 00:18:10,900
Mais ça n’a pas duré longtemps.

241
00:18:10,900 --> 00:18:17,800
Petit à petit, les médias ont eu les gens à l’usure,

242
00:18:17,800 --> 00:18:22,160
avec les rumeurs et fausses alertes.

243
00:18:22,160 --> 00:18:27,480
Ils voulaient faire peur aux gens.
Cette situation de solidarité ne devait pas durer.

244
00:18:27,480 --> 00:18:30,880
Il fallait que les gens reprennent leur place,
rentrent chez eux, devant leur télévision...

245
00:18:30,880 --> 00:18:34,040
qu’ils retournent à leur routine.

246
00:18:34,040 --> 00:18:38,240
Ils ont fait ce qu’il fallait.
Le résultat est là.

247
00:18:38,240 --> 00:18:42,880
Les gens sortaient manifester
parce qu’ils ne pouvaient pas se payer une baguette.

248
00:18:42,880 --> 00:18:46,160
Maintenant, ils sont dans une situation pire

249
00:18:46,160 --> 00:18:49,600
mais continuent à travailler, à se taire
et à rester cloîtrés chez eux.

250
00:19:17,000 --> 00:19:18,970
Tu étais où le 14 janvier ?

251
00:19:18,973 --> 00:19:20,970
Épisode 1 : Ben Ali hrab

252
00:19:20,970 --> 00:19:24,420
Production Inkyfada

253
00:19:24,420 --> 00:19:24,426
Production Inkyfada
Réalisation, montage et mixage :

254
00:19:24,426 --> 00:19:27,580
Réalisation, montage et mixage :

255
00:19:27,580 --> 00:19:27,586
Réalisation, montage et mixage :
Monia Ben Hamadi, Hazar Abidi,
Bochra Triki, Yassine Kawana

256
00:19:27,586 --> 00:19:33,560
Monia Ben Hamadi, Hazar Abidi,
Bochra Triki, Yassine Kawana

257
00:19:33,560 --> 00:19:34,650
Captations :

258
00:19:34,653 --> 00:19:36,266
Yassine Kawana

259
00:19:36,266 --> 00:19:38,260
Musique et design sonore :

260
00:19:38,260 --> 00:19:40,360
Oussema Gaidi

261
00:19:40,360 --> 00:19:41,600
Voix off :

262
00:19:41,600 --> 00:19:42,840
Bochra Triki

263
00:19:42,840 --> 00:19:44,480
Sous-titrage :

264
00:19:44,480 --> 00:19:48,640
Monia Ben Hamadi, Hazar Abidi,
Haïfa Mzalouat

265
00:19:48,640 --> 00:19:50,200
Écoute collective et feedbacks :

266
00:19:50,200 --> 00:19:55,480
Yasmin Houamed, Chayma Mehdi,
Haïfa Mzalouat, Khookha McQueer

267
00:19:55,480 --> 00:19:56,720
Illustrations :

268
00:19:56,720 --> 00:19:58,450
Marwen Ben Mustapha

269
00:19:58,453 --> 00:20:02,053
Avec la collaboration
de toute l'équipe d'Inkyfada
