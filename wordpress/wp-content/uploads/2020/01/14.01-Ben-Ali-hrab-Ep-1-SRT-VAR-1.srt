﻿1
00:00:00,120 --> 00:00:05,280
عندما لاحظت كيف بدت الصورة على شاشة الهاتف

2
00:00:05,280 --> 00:00:11,080
تفاجأت، بدا المشهد مهيبا

3
00:00:11,080 --> 00:00:13,160
ومخدِّرا كذلك

4
00:00:13,160 --> 00:00:16,080
وجدتُني مبهورا بالشاشة والشخصية في الصورة

5
00:00:16,080 --> 00:00:20,600
شخصية وحيدة : عبد الناصر العويني يهتف في شارع الحبيب بورڨيبة

6
00:01:09,480 --> 00:01:11,520
14 جانفي 2011

7
00:01:11,520 --> 00:01:15,200
مر يومان منذ أن نادى الاتحاد العام التونسي للشغل لإضراب عام

8
00:01:15,200 --> 00:01:17,760
ومظاهرة في بطحاء محمد علي

9
00:01:17,760 --> 00:01:20,160
استجاب الآلاف من التونسيين لهذا النداء

10
00:01:20,160 --> 00:01:24,000
ومن بينهم عماد
43 عاما

11
00:01:24,000 --> 00:01:27,120
ويشتغل في مجال الإشهار
عند الخامسة مساء،

12
00:01:27,120 --> 00:01:29,120
عندما أعلنت الحكومة حظر التجوال
وجد عماد نفسه في منزل صديقه زين

13
00:01:29,120 --> 00:01:32,520
شقة في شارع الحبيب بورڨيبة في الطابق العلوي للـ"أونفير"

14
00:01:32,520 --> 00:01:35,320
مقهى-حانة له تاريخ يساري مهم

15
00:01:35,320 --> 00:01:38,160
في تلك الليلة، كان  الهدوء يخيم على الشارع

16
00:01:38,160 --> 00:01:40,840
وعلى المباني المجاورة أيضاً

17
00:01:40,840 --> 00:01:44,000
يسمع عماد صوت هتاف مزّق الصمت المطبق

18
00:01:44,000 --> 00:01:45,520
عبد الناصر العويني،

19
00:01:45,520 --> 00:01:47,880
محام معروف في الأوساط النضالية

20
00:01:47,880 --> 00:01:49,480
يهتف بأعلى صوته

21
00:01:49,480 --> 00:01:52,200
"بن علي هرب"

22
00:01:52,200 --> 00:01:55,240
ينزل عماد من الشقة ليلحق بعبد الناصر

23
00:01:55,240 --> 00:01:59,320
خلال حظر التجوال
ربما في أكثر شارع به رجال شرطة في ذلك الوقت

24
00:01:59,320 --> 00:02:04,400
يمد عماد هاتفه ليصور شريط فيديو سينقش في ذاكرة التونسيين

25
00:02:04,400 --> 00:02:07,960
ويصبح رمزا لانتصار الشعوب على الدكتاتورية

26
00:02:07,960 --> 00:02:12,440
في انتظار الذكرى العاشرة للثورة، ولمدة عام كامل،

27
00:02:12,440 --> 00:02:17,600
تنشر إنكفاضة قصص البعض من آلاف
الذين عاشوا هذا اليوم التاريخي

28
00:02:19,520 --> 00:02:22,200
أين كنت يوم 14 جانفي؟

29
00:02:22,200 --> 00:02:26,120
الحلقة الأولى
"بن علي هرب"

30
00:02:31,920 --> 00:02:34,240
14 جانفي ...

31
00:02:34,240 --> 00:02:37,080
كان يوم جمعة لكنك تصحى من النوم بإحساس "يوم أحد" (عطلة)

32
00:02:37,080 --> 00:02:39,080
وبحصول شيء مميز

33
00:02:39,080 --> 00:02:41,240
كان هناك نداء لإضراب عام

34
00:02:41,240 --> 00:02:43,720
استيقظت. ارتديت ملابسي.
بحثت عن سيارة أجرة

35
00:02:43,720 --> 00:02:45,720
لم أجد واحدة مكثت منتظرا في الشارع

36
00:02:45,720 --> 00:02:49,160
عندها لمحت رجلا يبدو أنه ينتظر مثلي

37
00:02:49,160 --> 00:02:50,480
"أنت أيضا تبحث عن تاكسي؟"

38
00:02:50,480 --> 00:02:52,920
عندما نجد واحدة، هل نستقلها معا؟"

39
00:02:52,920 --> 00:02:53,960
وافق

40
00:02:53,960 --> 00:02:55,080
سألته عن وجهته ...

41
00:02:55,080 --> 00:02:58,600
فرفض الإجابة أظنه شعر بالخوف

42
00:02:58,600 --> 00:03:02,320
وهذا يلخص ما كان يعيشه التونسيون في تلك الفترة

43
00:03:02,320 --> 00:03:04,560
كانوا يعيشون في خوف

44
00:03:18,000 --> 00:03:19,000
ذهبنا هناك

45
00:03:19,000 --> 00:03:22,880
كان هناك شيء مختلف تلك المرة

46
00:03:22,880 --> 00:03:25,720
عندما تنظر إلى الوجوه التي تحيط بك
52
00:03:25,600 --> 00:03:27,760
تجد أناسا لا تعرفهم

47
00:03:27,880 --> 00:03:32,120
ولكن ... لم يكونوا رجال شرطة مندسين كالعادة
54
00:03:32,000 --> 00:03:39,720
كان المشهد رائعا
حتى أنك تحس نفسك في حديقة عائلية

48
00:03:41,040 --> 00:03:43,800
وجدت "زين" في ذلك الحشد

49
00:03:43,800 --> 00:03:46,160
كنت عندها أبحث عن قارورة ماء لأني شعرت بالعطش

50
00:03:46,160 --> 00:03:52,080
إذ مرّ على وجودي هناك 3 ساعات ربما

51
00:03:53,320 --> 00:03:57,240
"زين" هو صديق قديم

52
00:03:57,240 --> 00:03:59,640
أعرفه منذ وقت طويل

53
00:03:59,640 --> 00:04:02,920
ويقيم في وسط البلد فوق "الاونفير"

54
00:04:02,920 --> 00:04:04,920
سنعود لهذا التفصيل لاحقا

55
00:04:04,920 --> 00:04:07,400
كان الهدف إذن أن نجد قارورة ماء

56
00:04:07,400 --> 00:04:09,120
كل المحلات كانت مغلقة

57
00:04:09,120 --> 00:04:13,440
عدا "بوعبانة" في "شارع مرسيليا"
"بوعبانة" هو مطعم وحانة

58
00:04:13,440 --> 00:04:14,720
ذهبنا

59
00:04:14,720 --> 00:04:18,560
بما أننا لم نجد ماء، تناولنا البيرة

60
00:04:18,560 --> 00:04:21,280
كتعويض ..

61
00:04:21,280 --> 00:04:26,040
لم نجد حتى ما نأكله
لو لم يطعمنا "الهاشمي" المقرونة خاصّته

62
00:04:27,240 --> 00:04:29,680
احتسينا القليل من البيرة
ونزلنا مجددا

63
00:04:37,760 --> 00:04:39,760
عندما نزلنا من "بوعبانة" وجدنا الشارع في حالة فوضى

64
00:04:39,760 --> 00:04:43,920
فكان هدفنا أن نصل إلى البيت فوق "الأونفير"

65
00:04:43,920 --> 00:04:46,480
وبالمناسبة، قمنا بلفة طويلة في الشوارع

66
00:04:46,480 --> 00:04:48,960
لأن الفوضى عمت شارع الحبيب بورڨيبة

67
00:04:48,960 --> 00:04:56,080
أن تقطع مسافة 200 متر ... أو أقل
بين "نهج مرسيليا" و "الأونفير"

68
00:04:56,080 --> 00:04:57,280
كان الأمر مستحيلا

69
00:04:57,280 --> 00:05:02,040
كان علينا أن نمر بالشوارع المجاورة

70
00:05:02,040 --> 00:05:06,200
ونتوقف مرات أخرى لنختبئ
من سيارات الشرطة ومن رجال الشرطة وهراواتهم

71
00:05:06,200 --> 00:05:09,520
جوّ يشوبه الجنون

72
00:05:09,520 --> 00:05:11,920
وكأنك في حالة حرب

73
00:05:26,560 --> 00:05:29,080
الغاز المسيل للدموع كان يملأ المكان
والجميع يهرول في كل إتجاه

74
00:05:29,080 --> 00:05:33,040
وتم تفريق الحشد الكبير الذي تركناه
تم تفريقهم جميعا

75
00:05:33,040 --> 00:05:35,640
لم يكن هناك شيء، عدا ضباب كثيف ومتظاهرين هاربين

76
00:05:35,640 --> 00:05:40,280
في وسط الضباب تتراءى الأجسام

77
00:05:40,280 --> 00:05:43,640
ثم يتسنى لك تمييزها إن كانت لمدنيين
أم لأعوان يرتدون خوذات

78
00:05:48,840 --> 00:05:53,440
قطعنا تلك المسافة
ووصلنا أخيرا لمنزل "زين"

79
00:05:53,440 --> 00:05:56,120
وهناك، كان ثمة تلفاز

80
00:05:56,120 --> 00:05:58,600
بعد قليل، أعلنوا عن حظر التجوال عند الخامسة

81
00:05:58,600 --> 00:06:01,760
ومن هناك، وجوب البقاء على عين المكان

82
00:06:02,720 --> 00:06:03,960
لا يسمح لك بالخروج …

83
00:06:03,960 --> 00:06:08,000
في الخارج، كان المكان خالٍ
إلا من الشرطة والجيش

84
00:06:08,000 --> 00:06:11,400
مكثت مع "زين"
و "حورية" و "جلال"

85
00:06:11,400 --> 00:06:13,040
في شارع الحبيب بورڨيبة

86
00:06:13,040 --> 00:06:18,360
هناك مشهد شامل عمّا يجري

87
00:06:18,360 --> 00:06:21,760
عن رجال الشرطة و الغاز المسيل للدموع

88
00:06:21,760 --> 00:06:25,360
اختفى الجميع تقريبا

89
00:06:25,360 --> 00:06:28,840
وفي التلفاز أعلنوا أن
"ما إسمه؟"

90
00:06:28,840 --> 00:06:33,800
بن علي استقل طائرة وغادر
شيء من هذا القبيل

91
00:06:33,800 --> 00:06:36,760
إلى ليبيا أو فرنسا
كل شخص يسرد أخبارا مختلفة

92
00:06:36,760 --> 00:06:40,720
تخلى الرئيس زين العابدين بن علي عن السلطة

93
00:06:40,720 --> 00:06:45,200
بمقتضى تسوية يتسلم بموجبها
الوزير الأول محمد الغنوشي السلطة مؤقتا

94
00:06:45,200 --> 00:06:50,560
بن علي غادر تونس إلى مالطا بحماية ليبية
في الساعة الخامسة بالتوقيت المحلي

95
00:06:50,560 --> 00:06:56,400
وكما ذكر موفدنا منذ قليل
بأنه قد يكون ذهب إلى فرنسا ...

96
00:06:57,440 --> 00:07:04,440
في تلك اللحظة، تحس أن شيئا مميزا حصل

97
00:07:04,440 --> 00:07:12,200
الصورة دامت لـ23 عاما

98
00:07:12,200 --> 00:07:14,560
في تلك اللحظة صار قطع مع الماضي

99
00:07:14,560 --> 00:07:18,800
رمز السلطة غادر …

100
00:07:18,800 --> 00:07:25,400
أعتقد أن الجميع أحس بالخوف

101
00:07:25,400 --> 00:07:28,320
أنا متأكد من ذلك
حتى من كان يرغب بالتغيير

102
00:07:28,320 --> 00:07:32,440
وحتى من يرغب بالسلطة مكانه

103
00:07:32,440 --> 00:07:33,840
لم يتصور أحد حصول ذلك

104
00:07:33,840 --> 00:07:36,520
فتفاجأ الجميع على ما أعتقد

105
00:07:36,520 --> 00:07:45,560
لأنه من المفروض أن يكون هناك رد للفعل
خاصة من قبل الناس الذين كانوا في الشارع يومها في وسط البلد
في المظاهرة

106
00:07:45,560 --> 00:07:49,160
من المفروض أن يُسمع صوت أبواق السيارات
وأن يخرج الناس للشارع فرحا

107
00:07:49,160 --> 00:07:52,240
لكنه لم يحصل أي شيء من هذا
المكان كان فارغا تماما

108
00:07:52,240 --> 00:07:57,240
عدا بعض القطط
ورجال الشرطة طبعا

109
00:07:57,240 --> 00:07:59,240
اتجهت إلى النافذة
لتدخين سيجارة

110
00:07:59,240 --> 00:08:02,640
وكان رجال الشرطة يوجهون الشتائم

111
00:08:02,640 --> 00:08:05,280
ويجبرون الجميع على إغلاق النوافذ

112
00:08:05,280 --> 00:08:09,800
"أغلقوا النوافذ وإلا..."

113
00:08:09,800 --> 00:08:12,320
لا داعي للمزيد من التفاصيل
جميعنا نعرف كلام البوليس

114
00:08:12,320 --> 00:08:15,640
وفي الأثناء،

115
00:08:15,640 --> 00:08:17,280
سمعت شخصا يهتف في الشارع

116
00:08:19,080 --> 00:08:21,280
كان الشارع فارغا إلا من البوليس

117
00:08:21,280 --> 00:08:26,840
في الليل، دون سيارات أو ناس
إذا ما صرخ شخص هناك

118
00:08:26,840 --> 00:08:33,720
بإمكانك أن تتصور الصدى الذي قد تكوّن في ذلك الفراغ وذلك الجو

119
00:08:33,720 --> 00:08:36,840
ضحكت كردة فعل

120
00:08:36,840 --> 00:08:38,600
قبل أن أعرف من هو

121
00:08:38,600 --> 00:08:42,160
علاوة على ذلك، قلت لزين "ها قد بدأ لاعقو الأحذية بالظهور"

122
00:08:42,160 --> 00:08:47,520
خاصة مع تاريخنا مع السيارات المؤجرة

123
00:08:47,520 --> 00:08:57,040
تصورت أنهم نفس الأشخاص هكذا هي البروباغندا
تخرج السيارات المؤجرة وصور بن علي وما إلى ذلك

124
00:08:57,040 --> 00:08:59,040
إذن سمعت أحدهم يهتف في الشارع

125
00:08:59,040 --> 00:09:03,840
"بن علي هرب"

126
00:09:03,840 --> 00:09:07,920
وكلام آخر غير مفهوم
وكان يبدو ثملا أيضا

127
00:09:07,920 --> 00:09:10,120
ناديت زين وقلت له

128
00:09:10,120 --> 00:09:14,320
"هاهو أول واحد يتظاهر بالثورية"

129
00:09:16,200 --> 00:09:20,760
لكن عندما اقترب عرفت من يكون
عبد الناصر العويني

130
00:09:20,760 --> 00:09:22,760
أعرفه منذ أيام الجامعة

131
00:09:22,760 --> 00:09:27,920
وكان ناشطا في الاتحاد العام لطلبة تونس

132
00:09:27,920 --> 00:09:34,520
سُررت بوجوده وسط ذلك الفراغ الذي عمَّ الحبيب بورڨيبة
خاصة أنه وجه مألوف

133
00:09:34,520 --> 00:09:39,960
فقد خرج رغم كل شيء للتعبير عن فرحه على طريقته الخاصة

134
00:09:39,960 --> 00:09:45,160
وكانت ردة فعلي بأن لحقت به
وتبادلنا العناق

135
00:09:45,160 --> 00:09:49,440
تفاجأت فعلا

136
00:09:49,440 --> 00:09:54,000
كنا وحيدين في شارع الحبيب بورڨيبة

137
00:09:54,000 --> 00:09:56,920
الشارع فارغ تماما إلا من الشرطة

138
00:09:56,920 --> 00:10:05,920
كانت لحظة فريدة
أظنها تجربة فريدة

139
00:10:05,920 --> 00:10:11,840
عبثية أيضا

140
00:10:11,840 --> 00:10:16,880
أين ذهب الناس ؟

141
00:10:16,880 --> 00:10:26,920
قصدي بأنه كان من الممكن
أن تكون أجمل سهرة قد نعيشها كحفل موسيقي

142
00:10:26,920 --> 00:10:30,600
كان من المفترض أن يحضر العديد من الناس

143
00:10:30,600 --> 00:10:34,640
لكن في الأخير، لم يحضر سوى شخص واحد

144
00:10:34,640 --> 00:10:37,280
هذا ما أقصده بالعبثية

145
00:10:37,280 --> 00:10:42,920
في وقت ما، اِلتفت فوجدت زين وحورية

146
00:10:42,920 --> 00:10:45,920
أمام باب المبنى

147
00:10:45,920 --> 00:10:48,120
هناك 3 أشخاص في شارع الحبيب بورڨيبة

148
00:10:48,120 --> 00:10:50,440
"أنظري هناك."

149
00:10:50,440 --> 00:10:51,840
“المجرم هرب”

150
00:10:51,840 --> 00:10:53,240
بن علي هرب

151
00:10:53,240 --> 00:10:55,360
تحيا تونس حرة

152
00:10:55,960 --> 00:10:57,920
يا لشجاعته

153
00:11:04,240 --> 00:11:05,960
إسمعي، هناك شخص ما في الشارع

154
00:11:05,960 --> 00:11:11,080
هو سعيد ... ويحكي كيف عُذّب الناس ...

155
00:11:11,080 --> 00:11:13,280
أنت لا تدركين

156
00:11:13,280 --> 00:11:17,600
كم أشعر بالقشعريرة ... أنصتي ...

157
00:11:18,760 --> 00:11:22,600
أين السيارات والزغاريد التي دفع ثمنها بن علي أمس ؟

158
00:11:22,600 --> 00:11:27,280
أين سيارات التأجير ؟

159
00:11:32,920 --> 00:11:37,320
حورية كانت صاحبة الفكرة
لا أدري لماذا طلبت مني تصويره

160
00:11:37,320 --> 00:11:43,480
لم تكن كاميرا الهاتف ذات جودة

161
00:11:43,480 --> 00:11:51,840
لكن المشهد كان يستحق التصوير
فسجلت الفيديو

162
00:11:51,840 --> 00:11:53,960
تحررنا

163
00:11:53,960 --> 00:11:59,880
بن علي هرب
بن علي هرب

164
00:11:59,880 --> 00:12:04,240
تونس دون بن علي
المجرم هرب

165
00:12:04,240 --> 00:12:10,120
الشعب خلع بن علي

166
00:12:12,080 --> 00:12:17,480
لكن المشهد كان يستحق التصوير
فسجلت الفيديو

167
00:12:17,480 --> 00:12:20,960
عندما لاحظت كيف بدت الصورة على شاشة الهاتف
تفاجأت بما رأيت

168
00:12:20,960 --> 00:12:25,560
بدا المشهد مهيبا

169
00:12:25,560 --> 00:12:27,640
ومخدِّرا كذلك

170
00:12:27,640 --> 00:12:30,240
وجدتُني مبهورا بالشاشة والشخصية في الصورة

171
00:12:30,240 --> 00:12:34,360
شخصية وحيدة : عبد الناصر العويني يهتف في شارع الحبيب بورڨيبة

172
00:12:35,480 --> 00:12:38,640
في آخر الفيديو عندما كان يهتف "بن علي هرب"

173
00:12:38,640 --> 00:12:41,320
ضحكت

174
00:12:41,320 --> 00:12:44,640
لكنه كان شجاعا جدا

175
00:12:44,640 --> 00:12:46,960
في مثل هذه المواقف، هناك دائما من يبادر

176
00:12:46,960 --> 00:12:51,200
لكن الغريب هو أنه لم يلتحق أحد بنا

177
00:12:51,200 --> 00:12:57,280
كان هناك الآلاف من الناس ممن وجدوا أنفسهم في نفس حالتي

178
00:12:57,280 --> 00:13:01,480
محاصرين في شارع الحبيب بورڨيبة

179
00:13:01,480 --> 00:13:04,480
سمعت قصصا عديدة عن أشخاص قضوا ليلتهم فوق الأسطح

180
00:13:04,480 --> 00:13:07,320
وفي منازل غرباء

181
00:13:07,320 --> 00:13:13,320
كلهم يعلمون بأن بن علي قد فرّ

182
00:13:13,320 --> 00:13:16,000
والدليل في الفيديو الثاني

183
00:13:16,000 --> 00:13:26,320
صورته فتاة من فوق "الأونفير"
وجدت نفسها عالقة مع العديد

184
00:13:26,320 --> 00:13:30,440
العديد كانوا يشاهدون من النوافذ

185
00:13:30,440 --> 00:13:32,520
لكن شعروا بالخوف ولم يخرجوا

186
00:13:35,080 --> 00:13:36,080
مبروك

187
00:13:50,960 --> 00:13:52,880
أغلقوا كل النوافذ !

188
00:13:52,880 --> 00:13:56,720
طلبوا منّا غلق النوافذ، لنفعل ذلك !

189
00:14:05,840 --> 00:14:10,720
عندما انتهيت من تصوير الفيديو
قام رجال الشرطة بتفريقنا

190
00:14:10,720 --> 00:14:16,920
عبد الناصر يقيم في منزل قريب
عاد إلى بيته وأنا عدت إلى بيت زين

191
00:14:16,920 --> 00:14:23,040
لم يكن بإمكاني أن ألج إلى أنترنت
لتحميل الفيديو وقتها

192
00:14:23,040 --> 00:14:31,000
وكان علي أن أنتظر الغد للقيام بذلك

193
00:14:37,880 --> 00:14:43,280
في الصباح الموالي كان الشارع مغلقا

194
00:14:43,280 --> 00:14:46,960
كان عليّ المغادرة، لكن شارع الحبيب بورڨيبة كان ممنوعا من الدخول

195
00:14:46,960 --> 00:14:52,640
وتتواصل العبثية عندما منعني شرطي من المغادرة

196
00:14:54,360 --> 00:14:58,720
"لماذا؟ أريد أن أعود إلى بيتي!"

197
00:14:58,720 --> 00:15:05,840
كان عليّ أن أرتجل حتى أعود لمنزلي

198
00:15:05,840 --> 00:15:12,840
وانتابني إحساس بأنني "هدف متنقل"
في الأخير، كان كل شيء على ما يرام

199
00:15:23,840 --> 00:15:31,520
لم يكن الهدف هو "انتشار" الفيديو

200
00:15:31,520 --> 00:15:35,400
لكنه الفيديو الذي وجب نشره وقتها

201
00:15:35,400 --> 00:15:44,640
بعد أيام، إكتشفت بأن الفيديو كان يبث على مدار الساعة

202
00:15:44,640 --> 00:15:50,160
في "نسمة" مثلا والجزيرة، وغيرها من القنوات

203
00:15:50,160 --> 00:15:52,760
أمر مثير للدهشة

204
00:15:52,760 --> 00:15:58,360
تلاعبوا بها وراشد الغنوشي يبكي بعد أن شاهد الفيديو

205
00:15:58,360 --> 00:16:00,640
شخصيا ذهلت من ردة فعل الناس

206
00:16:00,640 --> 00:16:10,000
هل أحب الناس الشريط لأنهم خافوا من الخروج ليلتها ؟

207
00:16:10,000 --> 00:16:14,720
يوم 14 جانفي عندما أعلنوا عن فرار بن علي
كانت المناسبة التي لم يجدر بنا تفويتها للاحتفال

208
00:16:14,720 --> 00:16:22,120
لكن المؤكد أن فرار بن علي هي المناسبة التي لم يجدر بنا تفويتها للاحتفال

209
00:16:22,120 --> 00:16:26,320
فراره كان الحلم الخفي للتونسيين

210
00:16:26,320 --> 00:16:28,600
سواء خرجوا في مظاهرات أم لا

211
00:16:28,600 --> 00:16:33,440
الكل كان يرغب في التغيير

212
00:16:33,440 --> 00:16:36,240
ولكن عندما حصل التغيير اختبأ الجميع
أمر غريب

213
00:16:36,240 --> 00:16:40,240
يوم 14 جانفي،

214
00:16:40,240 --> 00:16:46,800
كان الناس في الشارع يتظاهرون
كانوا أكثر من رجال الشرطة

215
00:16:46,800 --> 00:16:51,880
لهذا لم أفهم سبب خوفهم في الأخير
لأنه فعلا، ليس هناك من سبب يدعو إلى الخوف

216
00:16:51,880 --> 00:17:01,880
عادوا فجأة إلى جهاز التلفاز
لمعرفة ما سيحصل

217
00:17:01,880 --> 00:17:07,280
والتلفاز يخبرهم بما سيحصل
وهو أمر مخيب للآمال

218
00:17:12,160 --> 00:17:20,800
بعد أيام، وخاصة مع اختفاء الشرطة

219
00:17:20,800 --> 00:17:32,280
بدأ الجميع بالتنظم الذاتي

220
00:17:32,280 --> 00:17:38,720
ولحماية عائلاتهم ومنازلهم وممتلكاتهم

221
00:17:38,720 --> 00:17:43,160
كان هناك تضامن وتشاركية

222
00:17:43,160 --> 00:17:54,000
يذكّر بالماضي
والتضامن في الأحياء والتقاسم بين الجيران

223
00:17:54,000 --> 00:17:58,840
كان أمرا رائعا
كان ربما بداية لشيء أروع

224
00:17:58,840 --> 00:18:06,040
خاصة عندما بدأ الناس بمراقبة البوليس
وأقاموا المعابر وفتشوا سياراتهم

225
00:18:06,040 --> 00:18:11,000
لكن ذلك لم يستمر أكثر من بضعة أيام

226
00:18:11,000 --> 00:18:17,920
وكان ذلك بسبب وسائل الإعلام خاصة

227
00:18:17,920 --> 00:18:22,280
أرهقوا الناس بالإشاعات والأخبار الزائفة

228
00:18:22,280 --> 00:18:27,600
أرهبوا الناس
بالنسبة لهم، من غير الممكن أن يتواصل التضامن دون خوف

229
00:18:27,600 --> 00:18:31,000
من المهم أن يعود كل واحد إلى مكانه الطبيعي أمام التلفاز

230
00:18:31,000 --> 00:18:34,160
وإلى الروتين اليومي

231
00:18:34,160 --> 00:18:38,360
قاموا بما عليهم فعله
والنتيجة واضحة للعيان

232
00:18:38,360 --> 00:18:43,000
في الماضي تظاهر الناس بسبب عدم قدرتهم على شراء رغيف من الخبز

233
00:18:43,000 --> 00:18:46,280
في الحاضر، هم غير قادرين على شراء ربع رغيف

234
00:18:46,280 --> 00:18:49,720
ورغم ذلك يعملون
في صمت …

235
00:19:17,120 --> 00:19:19,080
أين كنت يوم 14 جانفي؟

236
00:19:19,080 --> 00:19:21,080
الحلقة الأولى
"بن علي هرب"

237
00:19:21,080 --> 00:19:24,520
إنتاج إنكفاضة

238
00:19:24,520 --> 00:19:27,680
إخراج مونتاج ميكساج :

239
00:19:27,680 --> 00:19:33,680
منية بن حمادي، هزار عبيدي، بشرى تريكي، ياسين كعوانة

240
00:19:33,680 --> 00:19:34,760
تسجيل :

241
00:19:34,760 --> 00:19:36,360
ياسين كعوانة

242
00:19:36,360 --> 00:19:38,360
موسيقى وتصميم صوتي :

243
00:19:38,360 --> 00:19:40,480
أسامة ڨايدي

244
00:19:40,480 --> 00:19:41,720
تعليق صوتي

245
00:19:41,720 --> 00:19:42,960
بشرى تريكي

246
00:19:42,960 --> 00:19:44,600
ترجمة :
259
00:19:44,480 --> 00:19:48,640
منية بن حمادي، هزار عبيدي، هيفاء مزلوط

247
00:19:48,760 --> 00:19:50,320
سماع جماعي و نصائح :

248
00:19:50,320 --> 00:19:55,600
ياسمين حوامد، شيماء مهدي، هيفاء مزلوط و خوخة ماكوير

249
00:19:55,600 --> 00:19:56,840
رسوم :

250
00:19:56,840 --> 00:19:58,560
مروان بن مصطفى

251
00:19:58,560 --> 00:20:02,160
بالتعاون مع كامل فريق إنكفاضة
