﻿1
00:00:00,160 --> 00:00:05,320
Quand j'ai vu comment l'image
ressortait sur le téléphone,

2
00:00:05,320 --> 00:00:11,120
ça m'a surpris.
L'ambiance s'est amplifiée.

3
00:00:11,120 --> 00:00:13,200
C'était assez hypnotique.

4
00:00:13,200 --> 00:00:16,120
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

5
00:00:16,120 --> 00:00:18,240
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

6
00:00:18,240 --> 00:00:20,480
qui criait au milieu
de l'avenue Habib Bourguiba.

7
00:01:09,520 --> 00:01:11,560
14 janvier 2011.

8
00:01:11,560 --> 00:01:15,240
L'UGTT a annoncé la grève générale

9
00:01:15,240 --> 00:01:17,800
et a appelé à une manifestation
au départ de la place Mohamed Ali.

10
00:01:17,800 --> 00:01:20,200
Des milliers de Tunisien·nes
ont répondu à l'appel.

11
00:01:20,200 --> 00:01:24,040
Dont Imed, 43 ans,
qui travaille dans la publicité.

12
00:01:24,040 --> 00:01:27,160
Quand le gouvernement
a annoncé un couvre-feu pour 17h,

13
00:01:27,160 --> 00:01:29,160
il s'est retrouvé chez son ami Zein

14
00:01:29,160 --> 00:01:31,160
dans un appartement situé
sur l'avenue Habib Bourguiba,

15
00:01:31,160 --> 00:01:32,560
au-dessus de l'Univers

16
00:01:32,560 --> 00:01:35,360
un bar emblématique
dans le milieu militant de gauche.

17
00:01:35,360 --> 00:01:38,200
À la nuit tombée,
il n'y avait plus aucun bruit

18
00:01:38,200 --> 00:01:40,880
ni dans les rues,
ni dans les maisons.

19
00:01:40,880 --> 00:01:44,040
Imed entend alors des cris
qui rompent ce silence.

20
00:01:44,040 --> 00:01:45,560
Abdennaceur Laouini

21
00:01:45,560 --> 00:01:47,920
un avocat connu
dans le milieu militant,

22
00:01:47,920 --> 00:01:49,520
crie à perdre haleine :

23
00:01:49,520 --> 00:01:52,240
"Ben Ali s'est enfui".

24
00:01:52,240 --> 00:01:55,280
Imed descend et le rejoint,

25
00:01:55,280 --> 00:01:59,360
en plein couvre-feu,
dans une avenue quadrillée par les policiers.

26
00:01:59,360 --> 00:02:02,080
Il sort son téléphone
et enregistre une vidéo

27
00:02:02,080 --> 00:02:04,440
qui s'inscrira
dans la mémoire collective tunisienne

28
00:02:04,440 --> 00:02:08,000
comme un symbole de la victoire
des peuples sur la dictature.

29
00:02:08,000 --> 00:02:09,640
Pendant un an,

30
00:02:09,640 --> 00:02:12,480
en préparation de la commémoration
des 10 ans de la révolution,

31
00:02:12,480 --> 00:02:15,280
Inkyfada va raconter les histoires
de quelques personnes parmi des milliers

32
00:02:15,280 --> 00:02:17,640
ayant vécu cette journée historique.

33
00:02:19,560 --> 00:02:22,240
Tu étais où le 14 janvier ?

34
00:02:22,240 --> 00:02:26,160
Épisode 1 : Ben Ali hrab

35
00:02:31,960 --> 00:02:34,280
14 janvier.

36
00:02:34,280 --> 00:02:37,120
C’était un vendredi, mais on avait
l'impression d'être un dimanche.

37
00:02:37,120 --> 00:02:39,120
Il y avait quelque chose de spécial

38
00:02:39,120 --> 00:02:41,280
avec l'appel à la grève générale.

39
00:02:41,280 --> 00:02:43,760
Je me suis habillé et suis sorti
pour chercher un taxi.

40
00:02:43,760 --> 00:02:45,760
Il n'y en avait pas,
j'ai attendu dans la rue.

41
00:02:45,760 --> 00:02:49,200
Quelqu'un d'autre attendait
à côté de moi.

42
00:02:49,200 --> 00:02:50,520
“Tu attends un taxi ?"

43
00:02:50,520 --> 00:02:52,960
"S'il y en a un qui arrive,
on le partage ?"

44
00:02:52,960 --> 00:02:54,000
il m'a dit ok.

45
00:02:54,000 --> 00:02:55,120
Je lui ai demandé où il allait,

46
00:02:55,120 --> 00:02:56,640
il n'a pas voulu répondre.

47
00:02:56,640 --> 00:02:58,640
J'ai senti qu'il avait peur.

48
00:02:58,640 --> 00:03:02,360
Ça raconte ce que les Tunisien·nes
vivaient à l’époque.

49
00:03:02,360 --> 00:03:04,600
Les gens vivaient dans la peur.

50
00:03:18,040 --> 00:03:19,040
On est descendus.

51
00:03:19,040 --> 00:03:22,920
C'était différent cette fois.

52
00:03:22,920 --> 00:03:25,760
Parmi les gens autour de toi,

53
00:03:25,760 --> 00:03:27,920
il y a beaucoup de visages inconnus.

54
00:03:27,920 --> 00:03:32,160
Mais ce n'était pas des policiers infiltrés
comme d'habitude.

55
00:03:32,160 --> 00:03:39,880
Ça avait l’air cool.
On se sentait dans un parc familial.

56
00:03:41,080 --> 00:03:43,840
J’ai retrouvé Zein au milieu de la foule.

57
00:03:43,840 --> 00:03:46,200
J'avais besoin d'eau, j'avais soif.

58
00:03:46,200 --> 00:03:52,120
Ça faisait environ trois heures
que j’étais là.

59
00:03:53,360 --> 00:03:57,280
Zein est un vieil ami.

60
00:03:57,280 --> 00:03:59,680
On se connaît depuis longtemps.

61
00:03:59,680 --> 00:04:02,960
Il habite dans le centre-ville,
au-dessus de l'Univers.

62
00:04:02,960 --> 00:04:04,960
On y reviendra après.

63
00:04:04,960 --> 00:04:07,440
L'objectif était de trouver
une bouteille d'eau.

64
00:04:07,440 --> 00:04:09,160
Tout était fermé.

65
00:04:09,160 --> 00:04:13,480
Sauf "Bouabenna",
un bar-resto rue de Marseille.

66
00:04:13,480 --> 00:04:14,760
On est montés.

67
00:04:14,760 --> 00:04:18,600
Comme on n'a pas trouvé d'eau,
on a pris de la bière.

68
00:04:18,600 --> 00:04:21,320
Ça compense.

69
00:04:21,320 --> 00:04:26,080
Il n'y avait rien à manger,
le gérant nous a servi ses propre pâtes.

70
00:04:27,280 --> 00:04:29,720
Après quelques bières,
on est redescendus.

71
00:04:37,800 --> 00:04:39,800
Quand on est sortis du bar,
c’était le bordel.

72
00:04:39,800 --> 00:04:43,960
L'objectif, c'était de rejoindre l'appartement
au-dessus de l'Univers.

73
00:04:43,960 --> 00:04:46,520
On a dû faire un énorme détour.

74
00:04:46,520 --> 00:04:49,000
Tout était bloqué
sur l'avenue Habib Bourguiba.

75
00:04:49,000 --> 00:04:56,120
Pour faire 200 mètres,
entre rue de Marseille et l’Univers,

76
00:04:56,120 --> 00:04:57,320
c’était impossible.

77
00:04:57,320 --> 00:05:02,080
il fallait faire des détours et se cacher,

78
00:05:02,080 --> 00:05:06,240
pour éviter les policiers
qui passaient avec leurs matraques.

79
00:05:06,240 --> 00:05:09,560
L'ambiance était dingue.

80
00:05:09,560 --> 00:05:11,960
On avait l'impression d’être en pleine guerre.

81
00:05:26,600 --> 00:05:29,120
C’était le chaos avec la fumée blanche
des lacrymogènes partout,

82
00:05:29,120 --> 00:05:33,080
les gens couraient dans tous les sens,
tout le monde s'est dispersé.

83
00:05:33,080 --> 00:05:35,680
Tout était blanc, embrouillé.

84
00:05:35,680 --> 00:05:40,320
Et dans ce brouillard,
tu vois des silhouettes

85
00:05:40,320 --> 00:05:41,680
mais tu ne distingues qu’au dernier moment

86
00:05:41,680 --> 00:05:43,680
si ce sont des gens
ou s'ils portent des casques.

87
00:05:48,880 --> 00:05:53,480
On a fait tout ce trajet
pour finalement arriver chez Zein.

88
00:05:53,480 --> 00:05:56,160
La télévision était allumée.

89
00:05:56,160 --> 00:05:58,640
Ils ont annoncé un couvre-feu pour 17h.

90
00:05:58,640 --> 00:06:01,800
Donc on était obligés de rester sur place.

91
00:06:02,760 --> 00:06:04,000
Interdiction de sortir.

92
00:06:04,000 --> 00:06:08,040
Tout était vide.
Il n’y avait que des policiers et des militaires.

93
00:06:08,040 --> 00:06:11,440
Je suis resté chez Zein, avec Houria et Jalel,

94
00:06:11,440 --> 00:06:13,080
sur l’avenue Habib Bourguiba.

95
00:06:13,080 --> 00:06:18,400
On avait une vue sur ce qui se passait,

96
00:06:18,400 --> 00:06:21,800
avec les flics, les gaz lacrymogènes…

97
00:06:21,800 --> 00:06:25,400
Il ne restait presque plus personne.
Tout le monde avait disparu.

98
00:06:25,400 --> 00:06:28,880
Là, à la télé on annonce que…
Comment il s’appelle déjà ?

99
00:06:28,880 --> 00:06:33,840
Ben Ali est monté dans un avion
et est parti.

100
00:06:33,840 --> 00:06:36,800
Vers la Libye, la France…
Chacun sa version.

101
00:06:36,800 --> 00:06:40,760
“Le président Zine el Abidine Ben Ali
a quitté le pouvoir.

102
00:06:40,760 --> 00:06:45,240
Le premier ministre Mohamed Ghannouchi
devrait assurer l’intérim.

103
00:06:45,240 --> 00:06:50,600
Ben Ali a quitté la Tunisie pour Malte,
sous protection libyenne vers 17h.

104
00:06:50,600 --> 00:06:56,440
Notre source avait indiqué
qu’il pourrait se rendre en France”.

105
00:06:57,480 --> 00:07:04,480
C’était un moment très spécial.

106
00:07:04,480 --> 00:07:12,240
On a vécu avec cette présence, cette photo,
pendant 23 ans.

107
00:07:12,240 --> 00:07:14,600
Il y a eu une rupture à ce moment-là.

108
00:07:14,600 --> 00:07:18,840
Le symbole du pouvoir était parti.

109
00:07:18,840 --> 00:07:25,440
J’imagine que tout le monde a eu peur.

110
00:07:25,440 --> 00:07:28,360
Même celles et ceux
qui voulaient un changement

111
00:07:28,360 --> 00:07:32,480
ou prendre sa place,

112
00:07:32,480 --> 00:07:33,880
ne s'y attendaient pas.

113
00:07:33,880 --> 00:07:36,560
Ça a pris tout le monde de court.

114
00:07:36,560 --> 00:07:45,600
Normalement, dans ces circonstances,
les gens devraient réagir,

115
00:07:45,600 --> 00:07:49,200
sortir dans la rue, klaxonner,
exprimer leur joie.

116
00:07:49,200 --> 00:07:52,280
Mais ils n’y avait rien de tout ça,
c’était le vide total.

117
00:07:52,280 --> 00:07:57,280
Rien… à part quelques chats.
Et des policiers bien sûr.

118
00:07:57,280 --> 00:07:59,280
Je suis sorti à la fenêtre
pour fumer une cigarette.

119
00:07:59,280 --> 00:08:02,680
Les policiers sont passés et m’ont insulté,

120
00:08:02,680 --> 00:08:05,320
ils m’ont demandé de fermer la fenêtre.

121
00:08:05,320 --> 00:08:09,840
Leur langage habituel.

122
00:08:09,840 --> 00:08:12,360
inutile de rentrer dans les détails.

123
00:08:12,360 --> 00:08:15,680
Et à ce moment-là,

124
00:08:15,680 --> 00:08:17,320
j’entends quelqu’un crier dans la rue.

125
00:08:19,120 --> 00:08:21,320
Alors que l'avenue Bourguiba était vide,

126
00:08:21,320 --> 00:08:26,880
il n'y avait des policiers, dans la nuit,
ni voiture ni personne.

127
00:08:26,880 --> 00:08:33,760
Vous n’avez pas idée de l’écho
que ça produisait.

128
00:08:33,760 --> 00:08:36,880
En l'entendant,
je me suis mis à rire

129
00:08:36,880 --> 00:08:38,640
avant de voir qui c'était.

130
00:08:38,640 --> 00:08:42,200
D'ailleurs, j'ai dit à Zein :
“ça y est, les lèche-bottes sont de sortie”.

131
00:08:42,200 --> 00:08:47,560
Parce qu'ils étaient déjà sortis
avec des voitures de location,

132
00:08:47,560 --> 00:08:57,080
pour faire leur propagande
avec des photos de Ben Ali et des youyous.

133
00:08:57,080 --> 00:08:59,080
Donc j’entends le type crier :

134
00:08:59,080 --> 00:09:03,880
“Ben Ali s’est enfui".

135
00:09:03,880 --> 00:09:07,960
Ce n’était pas très audible.
Ça se voyait qu’il était ivre en plus.

136
00:09:07,960 --> 00:09:10,160
J'ai appelé Zein :

137
00:09:10,160 --> 00:09:14,360
“Regarde, c’est le premier
à avoir retourné sa veste”.

138
00:09:16,240 --> 00:09:20,800
Quand il s’est approché,
j’ai reconnu Abdennaceur Laouini.

139
00:09:20,800 --> 00:09:22,800
On se connaît depuis la fac.

140
00:09:22,800 --> 00:09:27,960
il était actif au sein de l’UGET
(syndicat étudiant de gauche).

141
00:09:27,960 --> 00:09:34,560
Au milieu de cette désolation,
ça m’a fait plaisir de voir un visage familier.

142
00:09:34,560 --> 00:09:40,000
Il est sorti pour exprimer sa joie
à sa manière.

143
00:09:40,000 --> 00:09:45,200
Du coup je l’ai rejoint,
on s’est embrassés, on était contents.

144
00:09:45,200 --> 00:09:49,480
Je ne vous cache pas
que j’étais moi-même surpris.

145
00:09:49,480 --> 00:09:54,040
On était vraiment seuls,
au milieu de l’avenue Habib Bourguiba, vide.

146
00:09:54,040 --> 00:09:56,960
Il n’y avait que des policiers,
dans le noir.

147
00:09:56,960 --> 00:10:05,960
C’était une expérience unique.

148
00:10:05,960 --> 00:10:11,880
Ça m’a fait rire,
la situation était absurde.

149
00:10:11,880 --> 00:10:16,920
Où sont les gens ?

150
00:10:16,920 --> 00:10:26,960
Imaginez la soirée du siècle,

151
00:10:26,960 --> 00:10:30,640
les gens se battraient pour avoir une place

152
00:10:30,640 --> 00:10:34,680
mais au final,
il n’y a qu’un seul spectateur.

153
00:10:34,680 --> 00:10:37,320
C'est ça qui était absurde.

154
00:10:37,320 --> 00:10:42,960
À un moment, je me retourne
et je vois Zein et sa mère Houria

155
00:10:42,960 --> 00:10:45,960
à l’entrée de l’immeuble.

156
00:10:45,960 --> 00:10:48,160
"Trois personnes sont sorties sur l’avenue
Habib Bourguiba, vide."

157
00:10:48,160 --> 00:10:50,480
"Regarde par là-bas."

158
00:10:50,480 --> 00:10:51,880
“Le criminel s’est enfui”

159
00:10:51,880 --> 00:10:53,280
“Ben Ali s’est enfui”

160
00:10:53,280 --> 00:10:55,400
"Vive la Tunisie libre”

161
00:10:56,000 --> 00:10:57,960
“Comme il est courageux !”

162
00:11:04,280 --> 00:11:06,000
"Écoute, il y a quelqu’un dans la rue,

163
00:11:06,000 --> 00:11:11,120
heureux, il raconte comment les gens ont été torturés.

164
00:11:11,120 --> 00:11:13,320
Tu n’as pas idée,

165
00:11:13,320 --> 00:11:17,640
il m’a donné la chair
de poule sans le savoir. Écoute !”

166
00:11:18,800 --> 00:11:22,640
"Où sont les voitures et les youyous
achetés par Ben Ali hier ?”

167
00:11:22,640 --> 00:11:27,320
“Où sont les voitures de location ?”

168
00:11:32,960 --> 00:11:37,360
C’était l’idée de Houria. Je ne sais pas
pourquoi elle m’a dit de filmer.

169
00:11:37,360 --> 00:11:43,520
Mon téléphone n’a pas une caméra
de bonne qualité.

170
00:11:43,520 --> 00:11:51,880
Mais il fallait enregistrer ce moment, en vidéo.

171
00:11:51,880 --> 00:11:54,000
“On s’est libéré·es. Ben Ali s’est enfui.

172
00:11:54,000 --> 00:11:59,920
Il s’est enfui. Ben Ali s’est enfui.
Ben Ali s’est enfui.

173
00:11:59,920 --> 00:12:04,280
La Tunisie sans Ben Ali.
Le criminel s’est enfui.

174
00:12:04,280 --> 00:12:10,160
Le criminel a été destitué.
Le peuple tunisien a destitué Ben Ali.”

175
00:12:12,120 --> 00:12:17,520
Quand j’ai vu comment l’image
ressortait sur mon téléphone,

176
00:12:17,520 --> 00:12:21,000
sur l'écran de ce Nokia,
que je tenais dans ma main.

177
00:12:21,000 --> 00:12:25,600
L’ambiance s’est amplifiée.

178
00:12:25,600 --> 00:12:27,680
C’était hypnotique.

179
00:12:27,680 --> 00:12:30,280
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

180
00:12:30,280 --> 00:12:32,400
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

181
00:12:32,400 --> 00:12:34,400
qui criait au milieu
de l'avenue Habib Bourguiba.

182
00:12:35,520 --> 00:12:38,680
Vers la fin de la vidéo, alors qu’il disait
“Ben Ali s’est enfui”,

183
00:12:38,680 --> 00:12:41,360
j’ai rigolé, la situation m'a fait rire.

184
00:12:41,360 --> 00:12:44,680
Mais c’était courageux, impressionnant.

185
00:12:44,680 --> 00:12:47,000
Il faut toujours que quelqu’un se lance en premier,

186
00:12:47,000 --> 00:12:51,240
mais bizarrement, personne n’est sorti après.

187
00:12:51,240 --> 00:12:57,320
Alors que des milliers de personnes
étaient sur l’avenue

188
00:12:57,320 --> 00:13:01,520
et étaient restées bloquées comme moi.

189
00:13:01,520 --> 00:13:04,520
Certaines ont dormi sur des toits d’immeuble,

190
00:13:04,520 --> 00:13:07,360
d’autres se sont cachées chez des inconnu·es,

191
00:13:07,360 --> 00:13:10,800
il y avait plein de monde

192
00:13:10,800 --> 00:13:13,360
et tout le monde savait que Ben Ali s’était enfui.

193
00:13:13,360 --> 00:13:16,040
Il y a même eu une deuxième vidéo

194
00:13:16,040 --> 00:13:26,360
de cette fille coincée dans un centre
d’appel avec d’autres personnes.

195
00:13:26,360 --> 00:13:30,480
Plein de gens regardaient par la fenêtre,
bloqué·es sur l’avenue Habib Bourguiba,

196
00:13:30,480 --> 00:13:32,560
mais ils ont eu peur de sortir.

197
00:13:35,120 --> 00:13:36,120
“Félicitations !”

198
00:13:51,000 --> 00:13:52,920
“Fermez toutes les fenêtres !”

199
00:13:52,920 --> 00:13:56,760
“Ils nous disent de fermer la fenêtre, allez”.

200
00:14:05,880 --> 00:14:10,760
Après avoir filmé, les policiers sont arrivés
pour nous dire de partir.

201
00:14:10,760 --> 00:14:16,960
Finalement, Abdennaceur est rentré
et je suis retourné chez Zein.

202
00:14:16,960 --> 00:14:23,080
Je n’avais pas de connexion ce soir-là.

203
00:14:23,080 --> 00:14:31,040
Donc j’ai dû attendre le lendemain matin
pour la mettre en ligne depuis chez moi.

204
00:14:37,920 --> 00:14:43,320
Je me suis réveillé le matin, tout était bloqué.

205
00:14:43,320 --> 00:14:47,000
Je devais sortir de l’avenue Habib Bourguiba
mais elle était interdite d’accès.

206
00:14:47,000 --> 00:14:52,680
C’était absurde, un policier m'a dit que
je n’avais pas le droit de sortir de l’avenue.

207
00:14:54,400 --> 00:14:58,760
“Pourquoi ? Je veux rentrer !”

208
00:14:58,760 --> 00:15:05,880
Donc il fallait improviser.

209
00:15:05,880 --> 00:15:12,880
Tu sens que tu es une cible mobile.
Mais ça s’est bien passé finalement.

210
00:15:23,880 --> 00:15:31,560
L’objectif n’était pas de faire de la pub
ou que la vidéo devienne virale.

211
00:15:31,560 --> 00:15:35,440
C’était juste la vidéo qu’il fallait partager.

212
00:15:35,440 --> 00:15:44,680
Après quelques jours,
je me suis rendue compte de l’ampleur.

213
00:15:44,680 --> 00:15:50,200
La vidéo était diffusée en continu
sur Nessma TV, Al Jazeera, d’autres médias...

214
00:15:50,200 --> 00:15:52,800
C’était incroyable.

215
00:15:52,800 --> 00:15:58,400
On voyait Rached Ghannouchi pleurer dessus
et les gens qui réagissaient !

216
00:15:58,400 --> 00:16:00,680
Ça m’a vraiment étonné.

217
00:16:00,680 --> 00:16:05,080
Est-ce que les gens ont aimé la vidéo

218
00:16:05,080 --> 00:16:10,040
parce qu’ils avaient eu peur
de sortir ce soir-là ?

219
00:16:10,040 --> 00:16:14,760
Le 14 janvier, quand ils ont dit
que Ben Ali s’était enfui,

220
00:16:14,760 --> 00:16:22,160
c’était l’occasion à ne pas rater pour célébrer.

221
00:16:22,160 --> 00:16:26,360
C’était le rêve caché de beaucoup de Tunisien·nes.

222
00:16:26,360 --> 00:16:28,640
qu'ils et elles soient sortis manifester ou pas.

223
00:16:28,640 --> 00:16:33,480
Tout le monde voulait un changement
mais quand il arrive,

224
00:16:33,480 --> 00:16:36,280
tout le monde se cache. C’est bizarre.

225
00:16:36,280 --> 00:16:40,280
Le 14 janvier,

226
00:16:40,280 --> 00:16:46,840
les gens étaient sortis le matin,
ils étaient beaucoup plus nombreux que les policiers.

227
00:16:46,840 --> 00:16:51,920
Je ne sais pas pourquoi ils ont eu peur après,
alors qu’ils ne devaient plus avoir peur.

228
00:16:51,920 --> 00:17:01,920
Ils ont repris les mêmes réflexes, d’attendre devant
la télévision pour voir ce qui va se passer.

229
00:17:01,920 --> 00:17:07,320
Et c’est la télévision qui te dit ce qui se passe.
C’est décevant.

230
00:17:12,200 --> 00:17:20,840
Pendant les jours qui ont suivi,
avec l’absence des policiers,

231
00:17:20,840 --> 00:17:25,600
les gens se sont organisés,

232
00:17:25,600 --> 00:17:32,320
ils ont commencé à gérer de manière autonome,

233
00:17:32,320 --> 00:17:38,760
à s’organiser entre eux, tenir la garde pendant la nuit.

234
00:17:38,760 --> 00:17:43,200
Il y avait beaucoup de solidarité, de partage,

235
00:17:43,200 --> 00:17:54,040
comme avant dans les quartiers. Les gens
qui s’entraident, qui cuisinent et partagent,

236
00:17:54,040 --> 00:17:58,880
c’était magnifique. Ça pouvait être le début
de quelque chose, d’un mouvement.

237
00:17:58,880 --> 00:18:06,080
Les gens contrôlaient même les policiers,
ils les arrêtaient et les fouillaient.

238
00:18:06,080 --> 00:18:11,040
Mais ça n’a pas duré longtemps.

239
00:18:11,040 --> 00:18:17,960
Petit à petit, les médias ont eu les gens à l’usure,

240
00:18:17,960 --> 00:18:22,320
avec les rumeurs et fausses alertes.

241
00:18:22,320 --> 00:18:27,640
Ils voulaient faire peur aux gens.
Cette situation de solidarité ne devait pas durer.

242
00:18:27,640 --> 00:18:31,040
Il fallait que les gens reprennent leur place,
rentrent chez eux, devant leur télévision...

243
00:18:31,040 --> 00:18:34,200
qu’ils retournent à leur routine.

244
00:18:34,200 --> 00:18:38,400
Ils ont fait ce qu’il fallait.
Le résultat est là.

245
00:18:38,400 --> 00:18:43,040
Les gens sortaient manifester
parce qu’ils ne pouvaient pas se payer une baguette.

246
00:18:43,040 --> 00:18:46,320
Maintenant, ils sont dans une situation pire

247
00:18:46,320 --> 00:18:49,760
mais continuent à travailler, à se taire
et à rester cloîtrés chez eux.

248
00:19:17,160 --> 00:19:19,120
Tu étais où le 14 janvier ?

249
00:19:19,120 --> 00:19:21,120
Épisode 1 : Ben Ali hrab

250
00:19:21,120 --> 00:19:24,560
Production Inkyfada

251
00:19:24,560 --> 00:19:27,720
Réalisation, montage et mixage :

252
00:19:27,720 --> 00:19:33,720
Monia Ben Hamadi, Hazar Abidi,
Bochra Triki, Yassine Kawana

253
00:19:33,720 --> 00:19:34,800
Captations :

254
00:19:34,800 --> 00:19:36,400
Yassine Kawana

255
00:19:36,400 --> 00:19:38,400
Musique et design sonore :

256
00:19:38,400 --> 00:19:40,520
Oussema Gaidi

257
00:19:40,520 --> 00:19:41,760
Voix off :

258
00:19:41,760 --> 00:19:43,000
Bochra Triki

259
00:19:43,000 --> 00:19:44,640
Sous-titrage :

260
00:19:44,640 --> 00:19:48,800
Monia Ben Hamadi, Hazar Abidi,
Haïfa Mzalouat

261
00:19:48,800 --> 00:19:50,360
Écoute collective et feedbacks :

262
00:19:50,360 --> 00:19:55,640
Yasmin Houamed, Chayma Mehdi,
Haïfa Mzalouat, Khookha McQueer

263
00:19:55,640 --> 00:19:56,880
Illustrations :

264
00:19:56,880 --> 00:19:58,600
Marwen Ben Mustapha

265
00:19:58,600 --> 00:20:02,200
Avec la collaboration
de toute l'équipe d'Inkyfada

