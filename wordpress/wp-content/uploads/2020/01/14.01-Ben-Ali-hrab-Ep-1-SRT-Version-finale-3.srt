﻿1
00:00:00,000 --> 00:00:05,160
Quand j'ai vu comment l'image
ressortait sur le téléphone,

2
00:00:05,160 --> 00:00:10,960
ça m'a surpris. 
L'ambiance s'est amplifiée.

3
00:00:10,960 --> 00:00:13,040
C'était assez hypnotique.

4
00:00:13,040 --> 00:00:15,960
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

5
00:00:15,960 --> 00:00:18,080
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

6
00:00:18,080 --> 00:00:20,320
qui criait au milieu
de l'avenue Habib Bourguiba.

7
00:01:09,360 --> 00:01:11,400
14 janvier 2011.

8
00:01:11,400 --> 00:01:15,080
L'UGTT a annoncé la grève générale

9
00:01:15,080 --> 00:01:17,640
et a appelé à une manifestation
au départ de la place Mohamed Ali.

10
00:01:17,640 --> 00:01:20,040
Des milliers de Tunisien·nes
ont répondu à l'appel.

11
00:01:20,040 --> 00:01:23,880
Dont Imed, 43 ans,
qui travaille dans la publicité.

12
00:01:23,880 --> 00:01:27,000
Quand le gouvernement
a annoncé un couvre-feu pour 17h,

13
00:01:27,000 --> 00:01:29,000
il s'est retrouvé chez son ami Zein

14
00:01:29,000 --> 00:01:31,000
dans un appartement situé
sur l'avenue Habib Bourguiba,

15
00:01:31,000 --> 00:01:32,400
au-dessus de l'Univers

16
00:01:32,400 --> 00:01:35,200
un bar emblématique
dans le milieu militant de gauche.

17
00:01:35,200 --> 00:01:38,040
À la nuit tombée,
il n'y avait plus aucun bruit

18
00:01:38,040 --> 00:01:40,720
ni dans les rues,
ni dans les maisons.

19
00:01:40,720 --> 00:01:43,880
Imed entend alors des cris
qui rompent ce silence.

20
00:01:43,880 --> 00:01:45,400
Abdennaceur Laouini

21
00:01:45,400 --> 00:01:47,760
un avocat connu
dans le milieu militant,

22
00:01:47,760 --> 00:01:49,360
crie à perdre haleine :

23
00:01:49,360 --> 00:01:52,080
"Ben Ali s'est enfui".

24
00:01:52,080 --> 00:01:55,120
Imed descend et le rejoint,

25
00:01:55,120 --> 00:01:59,200
en plein couvre-feu,
dans une avenue quadrillée par les policiers.

26
00:01:59,200 --> 00:02:01,920
Il sort son téléphone
et enregistre une vidéo

27
00:02:01,920 --> 00:02:04,280
qui s'inscrira
dans la mémoire collective tunisienne

28
00:02:04,280 --> 00:02:07,840
comme un symbole de la victoire
des peuples sur la dictature.

29
00:02:07,840 --> 00:02:09,480
Pendant un an,

30
00:02:09,480 --> 00:02:12,320
en préparation de la commémoration
des 10 ans de la révolution,

31
00:02:12,320 --> 00:02:15,120
Inkyfada va raconter les histoires
de quelques personnes parmi des milliers

32
00:02:15,120 --> 00:02:17,480
ayant vécu cette journée historique.

33
00:02:19,400 --> 00:02:22,080
Tu étais où le 14 janvier ?

34
00:02:22,080 --> 00:02:26,000
Épisode 1 : Ben Ali hrab

35
00:02:31,800 --> 00:02:34,120
14 janvier.

36
00:02:34,120 --> 00:02:36,960
C’était un vendredi, mais on avait 
l'impression d'être un dimanche.

37
00:02:36,960 --> 00:02:38,960
Il y avait quelque chose de spécial

38
00:02:38,960 --> 00:02:41,120
avec l'appel à la grève générale.

39
00:02:41,120 --> 00:02:43,600
Je me suis habillé et suis sorti
pour chercher un taxi.

40
00:02:43,600 --> 00:02:45,600
Il n'y en avait pas,
j'ai attendu dans la rue.

41
00:02:45,600 --> 00:02:49,040
Quelqu'un d'autre attendait
à côté de moi.

42
00:02:49,040 --> 00:02:50,360
“Tu attends un taxi ?"

43
00:02:50,360 --> 00:02:52,800
"S'il y en a un qui arrive,
on le partage ?"

44
00:02:52,800 --> 00:02:53,840
il m'a dit ok.

45
00:02:53,840 --> 00:02:54,960
Je lui ai demandé où il allait,

46
00:02:54,960 --> 00:02:56,480
il n'a pas voulu répondre.

47
00:02:56,480 --> 00:02:58,480
J'ai senti qu'il avait peur.

48
00:02:58,480 --> 00:03:02,200
Ça raconte ce que les Tunisien·nes
vivaient à l’époque.

49
00:03:02,200 --> 00:03:04,440
Les gens vivaient dans la peur.

50
00:03:17,880 --> 00:03:18,880
On est descendus.

51
00:03:18,880 --> 00:03:22,760
C'était différent cette fois.

52
00:03:22,760 --> 00:03:25,600
Parmi les gens autour de toi,

53
00:03:25,600 --> 00:03:27,760
il y a beaucoup de visages inconnus.

54
00:03:27,760 --> 00:03:32,000
Mais ce n'était pas des policiers infiltrés
comme d'habitude.

55
00:03:32,000 --> 00:03:39,720
Ça avait l’air cool.
On se sentait dans un parc familial.

56
00:03:40,920 --> 00:03:43,680
J’ai retrouvé Zein au milieu de la foule.

57
00:03:43,680 --> 00:03:46,040
J'avais besoin d'eau, j'avais soif.

58
00:03:46,040 --> 00:03:51,960
Ça faisait environ trois heures
que j’étais là.

59
00:03:53,200 --> 00:03:57,120
Zein est un vieil ami.

60
00:03:57,120 --> 00:03:59,520
On se connaît depuis longtemps.

61
00:03:59,520 --> 00:04:02,800
Il habite dans le centre-ville,
au-dessus de l'Univers.

62
00:04:02,800 --> 00:04:04,800
On y reviendra après.

63
00:04:04,800 --> 00:04:07,280
L'objectif était de trouver
une bouteille d'eau.

64
00:04:07,280 --> 00:04:09,000
Tout était fermé.

65
00:04:09,000 --> 00:04:13,320
Sauf "Bouabenna",
un bar-resto rue de Marseille.

66
00:04:13,320 --> 00:04:14,600
On est montés.

67
00:04:14,600 --> 00:04:18,440
Comme on n'a pas trouvé d'eau,
on a pris de la bière.

68
00:04:18,440 --> 00:04:21,160
Ça compense.

69
00:04:21,160 --> 00:04:25,920
Il n'y avait rien à manger,
le gérant nous a servi ses propre pâtes.

70
00:04:27,120 --> 00:04:29,560
Après quelques bières,
on est redescendus.

71
00:04:37,640 --> 00:04:39,640
Quand on est sortis du bar,
c’était le bordel.

72
00:04:39,640 --> 00:04:43,800
L'objectif, c'était de rejoindre l'appartement
au-dessus de l'Univers.

73
00:04:43,800 --> 00:04:46,360
On a dû faire un énorme détour.

74
00:04:46,360 --> 00:04:48,840
Tout était bloqué
sur l'avenue Habib Bourguiba.

75
00:04:48,840 --> 00:04:55,960
Pour faire 200 mètres,
entre rue de Marseille et l’Univers,

76
00:04:55,960 --> 00:04:57,160
c’était impossible.

77
00:04:57,160 --> 00:05:01,920
il fallait faire des détours et se cacher,

78
00:05:01,920 --> 00:05:06,080
pour éviter les policiers
qui passaient avec leurs matraques.

79
00:05:06,080 --> 00:05:09,400
L'ambiance était dingue.

80
00:05:09,400 --> 00:05:11,800
On avait l'impression d’être en pleine guerre.

81
00:05:26,440 --> 00:05:28,960
C’était le chaos avec la fumée blanche
des lacrymogènes partout,

82
00:05:28,960 --> 00:05:32,920
les gens couraient dans tous les sens,
tout le monde s'est dispersé.

83
00:05:32,920 --> 00:05:35,520
Tout était blanc, embrouillé.

84
00:05:35,520 --> 00:05:40,160
Et dans ce brouillard,
tu vois des silhouettes

85
00:05:40,160 --> 00:05:41,520
mais tu ne distingues qu’au dernier moment

86
00:05:41,520 --> 00:05:43,520
si ce sont des gens
ou s'ils portent des casques.

87
00:05:48,720 --> 00:05:53,320
On a fait tout ce trajet
pour finalement arriver chez Zein.

88
00:05:53,320 --> 00:05:56,000
La télévision était allumée.

89
00:05:56,000 --> 00:05:58,480
Ils ont annoncé un couvre-feu pour 17h.

90
00:05:58,480 --> 00:06:01,640
Donc on était obligés de rester sur place.

91
00:06:02,600 --> 00:06:03,840
Interdiction de sortir.

92
00:06:03,840 --> 00:06:07,880
Tout était vide.
Il n’y avait que des policiers et des militaires.

93
00:06:07,880 --> 00:06:11,280
Je suis resté chez Zein, avec Houria et Jalel,

94
00:06:11,280 --> 00:06:12,920
sur l’avenue Habib Bourguiba.

95
00:06:12,920 --> 00:06:18,240
On avait une vue sur ce qui se passait,

96
00:06:18,240 --> 00:06:21,640
avec les flics, les gaz lacrymogènes…

97
00:06:21,640 --> 00:06:25,240
Il ne restait presque plus personne.
Tout le monde avait disparu.

98
00:06:25,240 --> 00:06:28,720
Là, à la télé on annonce que…
Comment il s’appelle déjà ?

99
00:06:28,720 --> 00:06:33,680
Ben Ali est monté dans un avion
et est parti.

100
00:06:33,680 --> 00:06:36,640
Vers la Libye, la France…
Chacun sa version.

101
00:06:36,640 --> 00:06:40,600
“Le président Zine el Abidine Ben Ali
a quitté le pouvoir.

102
00:06:40,600 --> 00:06:45,080
Le premier ministre Mohamed Ghannouchi
devrait assurer l’intérim.

103
00:06:45,080 --> 00:06:50,440
Ben Ali a quitté la Tunisie pour Malte,
sous protection libyenne vers 17h.

104
00:06:50,440 --> 00:06:56,280
Notre source avait indiqué
qu’il pourrait se rendre en France”.

105
00:06:57,320 --> 00:07:04,320
C’était un moment très spécial.

106
00:07:04,320 --> 00:07:12,080
On a vécu avec cette présence, cette photo,
pendant 23 ans.

107
00:07:12,080 --> 00:07:14,440
Il y a eu une rupture à ce moment-là.

108
00:07:14,440 --> 00:07:18,680
Le symbole du pouvoir était parti.

109
00:07:18,680 --> 00:07:25,280
J’imagine que tout le monde a eu peur.

110
00:07:25,280 --> 00:07:28,200
Même celles et ceux
qui voulaient un changement

111
00:07:28,200 --> 00:07:32,320
ou prendre sa place,

112
00:07:32,320 --> 00:07:33,720
ne s'y attendaient pas.

113
00:07:33,720 --> 00:07:36,400
Ça a pris tout le monde de court.

114
00:07:36,400 --> 00:07:45,440
Normalement, dans ces circonstances,
les gens devraient réagir,

115
00:07:45,440 --> 00:07:49,040
sortir dans la rue, klaxonner,
exprimer leur joie.

116
00:07:49,040 --> 00:07:52,120
Mais ils n’y avait rien de tout ça,
c’était le vide total.

117
00:07:52,120 --> 00:07:57,120
Rien… à part quelques chats.
Et des policiers bien sûr.

118
00:07:57,120 --> 00:07:59,120
Je suis sorti à la fenêtre
pour fumer une cigarette.

119
00:07:59,120 --> 00:08:02,520
Les policiers sont passés et m’ont insulté,

120
00:08:02,520 --> 00:08:05,160
ils m’ont demandé de fermer la fenêtre.

121
00:08:05,160 --> 00:08:09,680
Leur langage habituel.

122
00:08:09,680 --> 00:08:12,200
inutile de rentrer dans les détails.

123
00:08:12,200 --> 00:08:15,520
Et à ce moment-là,

124
00:08:15,520 --> 00:08:17,160
j’entends quelqu’un crier dans la rue.

125
00:08:18,960 --> 00:08:21,160
Alors que l'avenue Bourguiba était vide,

126
00:08:21,160 --> 00:08:26,720
il n'y avait des policiers, dans la nuit,
ni voiture ni personne.

127
00:08:26,720 --> 00:08:33,600
Vous n’avez pas idée de l’écho
que ça produisait.

128
00:08:33,600 --> 00:08:36,720
En l'entendant,
je me suis mis à rire

129
00:08:36,720 --> 00:08:38,480
avant de voir qui c'était.

130
00:08:38,480 --> 00:08:42,040
D'ailleurs, j'ai dit à Zein :
“ça y est, les lèche-bottes sont de sortie”.

131
00:08:42,040 --> 00:08:47,400
Parce qu'ils étaient déjà sortis
avec des voitures de location,

132
00:08:47,400 --> 00:08:56,920
pour faire leur propagande
avec des photos de Ben Ali et des youyous.

133
00:08:56,920 --> 00:08:58,920
Donc j’entends le type crier :

134
00:08:58,920 --> 00:09:03,720
“Ben Ali s’est enfui".

135
00:09:03,720 --> 00:09:07,800
Ce n’était pas très audible.
Ça se voyait qu’il était ivre en plus.

136
00:09:07,800 --> 00:09:10,000
J'ai appelé Zein :

137
00:09:10,000 --> 00:09:14,200
“Regarde, c’est le premier
à avoir retourné sa veste”.

138
00:09:16,080 --> 00:09:20,640
Quand il s’est approché,
j’ai reconnu Abdennaceur Laouini.

139
00:09:20,640 --> 00:09:22,640
On se connaît depuis la fac.

140
00:09:22,640 --> 00:09:27,800
il était actif au sein de l’UGET
(syndicat étudiant de gauche).

141
00:09:27,800 --> 00:09:34,400
Au milieu de cette désolation,
ça m’a fait plaisir de voir un visage familier.

142
00:09:34,400 --> 00:09:39,840
Il est sorti pour exprimer sa joie
à sa manière.

143
00:09:39,840 --> 00:09:45,040
Du coup je l’ai rejoint,
on s’est embrassés, on était contents.

144
00:09:45,040 --> 00:09:49,320
Je ne vous cache pas
que j’étais moi-même surpris.

145
00:09:49,320 --> 00:09:53,880
On était vraiment seuls,
au milieu de l’avenue Habib Bourguiba, vide.

146
00:09:53,880 --> 00:09:56,800
Il n’y avait que des policiers,
dans le noir.

147
00:09:56,800 --> 00:10:05,800
C’était une expérience unique.

148
00:10:05,800 --> 00:10:11,720
Ça m’a fait rire,
la situation était absurde.

149
00:10:11,720 --> 00:10:16,760
Où sont les gens ?

150
00:10:16,760 --> 00:10:26,800
Imaginez la soirée du siècle,

151
00:10:26,800 --> 00:10:30,480
les gens se battraient pour avoir une place

152
00:10:30,480 --> 00:10:34,520
mais au final,
il n’y a qu’un seul spectateur.

153
00:10:34,520 --> 00:10:37,160
C'est ça qui était absurde.

154
00:10:37,160 --> 00:10:42,800
À un moment, je me retourne
et je vois Zein et sa mère Houria

155
00:10:42,800 --> 00:10:45,800
à l’entrée de l’immeuble.

156
00:10:45,800 --> 00:10:48,000
"Trois personnes sont sorties sur l’avenue
Habib Bourguiba, vide."

157
00:10:48,000 --> 00:10:50,320
"Regarde par là-bas."

158
00:10:50,320 --> 00:10:51,720
“Le criminel s’est enfui”

159
00:10:51,720 --> 00:10:53,120
“Ben Ali s’est enfui”

160
00:10:53,120 --> 00:10:55,240
"Vive la Tunisie libre”

161
00:10:55,840 --> 00:10:57,800
“Comme il est courageux !”

162
00:11:04,120 --> 00:11:05,840
"Écoute, il y a quelqu’un dans la rue,

163
00:11:05,840 --> 00:11:10,960
heureux, il raconte comment les gens ont été torturés.

164
00:11:10,960 --> 00:11:13,160
Tu n’as pas idée,

165
00:11:13,160 --> 00:11:17,480
il m’a donné la chair
de poule sans le savoir. Écoute !”

166
00:11:18,640 --> 00:11:22,480
"Où sont les voitures et les youyous
achetés par Ben Ali hier ?”

167
00:11:22,480 --> 00:11:27,160
“Où sont les voitures de location ?”

168
00:11:32,800 --> 00:11:37,200
C’était l’idée de Houria. Je ne sais pas
pourquoi elle m’a dit de filmer.

169
00:11:37,200 --> 00:11:43,360
Mon téléphone n’a pas une caméra
de bonne qualité.

170
00:11:43,360 --> 00:11:51,720
Mais il fallait enregistrer ce moment, en vidéo.

171
00:11:51,720 --> 00:11:53,840
“On s’est libéré·es. Ben Ali s’est enfui.

172
00:11:53,840 --> 00:11:59,760
Il s’est enfui. Ben Ali s’est enfui.
Ben Ali s’est enfui.

173
00:11:59,760 --> 00:12:04,120
La Tunisie sans Ben Ali.
Le criminel s’est enfui.

174
00:12:04,120 --> 00:12:10,000
Le criminel a été destitué.
Le peuple tunisien a destitué Ben Ali.”

175
00:12:11,960 --> 00:12:17,360
Quand j’ai vu comment l’image
ressortait sur mon téléphone,

176
00:12:17,360 --> 00:12:20,840
sur l'écran de ce Nokia,
que je tenais dans ma main.

177
00:12:20,840 --> 00:12:25,440
L’ambiance s’est amplifiée.

178
00:12:25,440 --> 00:12:27,520
C’était hypnotique.

179
00:12:27,520 --> 00:12:30,120
J'étais bloqué sur le téléphone,
sur le sujet du téléphone,

180
00:12:30,120 --> 00:12:32,240
Il n'y en avait qu'un,
c'était Abdennaceur Laouini

181
00:12:32,240 --> 00:12:34,240
qui criait au milieu
de l'avenue Habib Bourguiba.

182
00:12:35,360 --> 00:12:38,520
Vers la fin de la vidéo, alors qu’il disait
“Ben Ali s’est enfui”,

183
00:12:38,520 --> 00:12:41,200
j’ai rigolé, la situation m'a fait rire.

184
00:12:41,200 --> 00:12:44,520
Mais c’était courageux, impressionnant.

185
00:12:44,520 --> 00:12:46,840
Il faut toujours que quelqu’un se lance en premier,

186
00:12:46,840 --> 00:12:51,080
mais bizarrement, personne n’est sorti après.

187
00:12:51,080 --> 00:12:57,160
Alors que des milliers de personnes
étaient sur l’avenue

188
00:12:57,160 --> 00:13:01,360
et étaient restées bloquées comme moi.

189
00:13:01,360 --> 00:13:04,360
Certaines ont dormi sur des toits d’immeuble,

190
00:13:04,360 --> 00:13:07,200
d’autres se sont cachées chez des inconnu·es,

191
00:13:07,200 --> 00:13:10,640
il y avait plein de monde

192
00:13:10,640 --> 00:13:13,200
et tout le monde savait que Ben Ali s’était enfui.

193
00:13:13,200 --> 00:13:15,880
Il y a même eu une deuxième vidéo

194
00:13:15,880 --> 00:13:26,200
de cette fille coincée dans un centre
d’appel avec d’autres personnes.

195
00:13:26,200 --> 00:13:30,320
Plein de gens regardaient par la fenêtre,
bloqué·es sur l’avenue Habib Bourguiba,

196
00:13:30,320 --> 00:13:32,400
mais ils ont eu peur de sortir.

197
00:13:34,960 --> 00:13:35,960
“Félicitations !”

198
00:13:50,840 --> 00:13:52,760
“Fermez toutes les fenêtres !”

199
00:13:52,760 --> 00:13:56,600
“Ils nous disent de fermer la fenêtre, allez”.

200
00:14:05,720 --> 00:14:10,600
Après avoir filmé, les policiers sont arrivés
pour nous dire de partir.

201
00:14:10,600 --> 00:14:16,800
Finalement, Abdennaceur est rentré
et je suis retourné chez Zein.

202
00:14:16,800 --> 00:14:22,920
Je n’avais pas de connexion ce soir-là.

203
00:14:22,920 --> 00:14:30,880
Donc j’ai dû attendre le lendemain matin
pour la mettre en ligne depuis chez moi.

204
00:14:37,760 --> 00:14:43,160
Je me suis réveillé le matin, tout était bloqué.

205
00:14:43,160 --> 00:14:46,840
Je devais sortir de l’avenue Habib Bourguiba
mais elle était interdite d’accès.

206
00:14:46,840 --> 00:14:52,520
C’était absurde, un policier m'a dit que
je n’avais pas le droit de sortir de l’avenue.

207
00:14:54,240 --> 00:14:58,600
“Pourquoi ? Je veux rentrer !”

208
00:14:58,600 --> 00:15:05,720
Donc il fallait improviser.

209
00:15:05,720 --> 00:15:12,720
Tu sens que tu es une cible mobile.
Mais ça s’est bien passé finalement.

210
00:15:23,720 --> 00:15:31,400
L’objectif n’était pas de faire de la pub
ou que la vidéo devienne virale.

211
00:15:31,400 --> 00:15:35,280
C’était juste la vidéo qu’il fallait partager.

212
00:15:35,280 --> 00:15:44,520
Après quelques jours,
je me suis rendue compte de l’ampleur.

213
00:15:44,520 --> 00:15:50,040
La vidéo était diffusée en continu
sur Nessma TV, Al Jazeera, d’autres médias...

214
00:15:50,040 --> 00:15:52,640
C’était incroyable.

215
00:15:52,640 --> 00:15:58,240
On voyait Rached Ghannouchi pleurer dessus
et les gens qui réagissaient !

216
00:15:58,240 --> 00:16:00,520
Ça m’a vraiment étonné.

217
00:16:00,520 --> 00:16:04,920
Est-ce que les gens ont aimé la vidéo

218
00:16:04,920 --> 00:16:09,880
parce qu’ils avaient eu peur
de sortir ce soir-là ?

219
00:16:09,880 --> 00:16:14,600
Le 14 janvier, quand ils ont dit
que Ben Ali s’était enfui,

220
00:16:14,600 --> 00:16:22,000
c’était l’occasion à ne pas rater pour célébrer.

221
00:16:22,000 --> 00:16:26,200
C’était le rêve caché de beaucoup de Tunisien·nes.

222
00:16:26,200 --> 00:16:28,480
qu'ils et elles soient sortis manifester ou pas.

223
00:16:28,480 --> 00:16:33,320
Tout le monde voulait un changement
mais quand il arrive,

224
00:16:33,320 --> 00:16:36,120
tout le monde se cache. C’est bizarre.

225
00:16:36,120 --> 00:16:40,120
Le 14 janvier,

226
00:16:40,120 --> 00:16:46,680
les gens étaient sortis le matin,
ils étaient beaucoup plus nombreux que les policiers.

227
00:16:46,680 --> 00:16:51,760
Je ne sais pas pourquoi ils ont eu peur après,
alors qu’ils ne devaient plus avoir peur.

228
00:16:51,760 --> 00:17:01,760
Ils ont repris les mêmes réflexes, d’attendre devant
la télévision pour voir ce qui va se passer.

229
00:17:01,760 --> 00:17:07,160
Et c’est la télévision qui te dit ce qui se passe.
C’est décevant.

230
00:17:12,040 --> 00:17:20,680
Pendant les jours qui ont suivi,
avec l’absence des policiers,

231
00:17:20,680 --> 00:17:25,440
les gens se sont organisés,

232
00:17:25,440 --> 00:17:32,160
ils ont commencé à gérer de manière autonome,

233
00:17:32,160 --> 00:17:38,600
à s’organiser entre eux, tenir la garde pendant la nuit.

234
00:17:38,600 --> 00:17:43,040
Il y avait beaucoup de solidarité, de partage,

235
00:17:43,040 --> 00:17:53,880
comme avant dans les quartiers. Les gens
qui s’entraident, qui cuisinent et partagent,

236
00:17:53,880 --> 00:17:58,720
c’était magnifique. Ça pouvait être le début
de quelque chose, d’un mouvement.

237
00:17:58,720 --> 00:18:05,920
Les gens contrôlaient même les policiers,
ils les arrêtaient et les fouillaient.

238
00:18:05,920 --> 00:18:10,880
Mais ça n’a pas duré longtemps.

239
00:18:10,880 --> 00:18:17,800
Petit à petit, les médias ont eu les gens à l’usure,

240
00:18:17,800 --> 00:18:22,160
avec les rumeurs et fausses alertes.

241
00:18:22,160 --> 00:18:27,480
Ils voulaient faire peur aux gens.
Cette situation de solidarité ne devait pas durer.

242
00:18:27,480 --> 00:18:30,880
Il fallait que les gens reprennent leur place,
rentrent chez eux, devant leur télévision...

243
00:18:30,880 --> 00:18:34,040
qu’ils retournent à leur routine.

244
00:18:34,040 --> 00:18:38,240
Ils ont fait ce qu’il fallait.
Le résultat est là.

245
00:18:38,240 --> 00:18:42,880
Les gens sortaient manifester
parce qu’ils ne pouvaient pas se payer une baguette.

246
00:18:42,880 --> 00:18:46,160
Maintenant, ils sont dans une situation pire

247
00:18:46,160 --> 00:18:49,600
mais continuent à travailler, à se taire
et à rester cloîtrés chez eux.

248
00:19:17,000 --> 00:19:18,960
Tu étais où le 14 janvier ?

249
00:19:18,960 --> 00:19:20,960
Épisode 1 : Ben Ali hrab

250
00:19:20,960 --> 00:19:24,400
Production Inkyfada

251
00:19:24,400 --> 00:19:27,560
Réalisation, montage et mixage :

252
00:19:27,560 --> 00:19:33,560
Monia Ben Hamadi, Hazar Abidi,
Bochra Triki, Yassine Kawana

253
00:19:33,560 --> 00:19:34,640
Captations :

254
00:19:34,640 --> 00:19:36,240
Yassine Kawana

255
00:19:36,240 --> 00:19:38,240
Musique et design sonore :

256
00:19:38,240 --> 00:19:40,360
Oussema Gaidi

257
00:19:40,360 --> 00:19:41,600
Voix off :

258
00:19:41,600 --> 00:19:42,840
Bochra Triki

259
00:19:42,840 --> 00:19:44,480
Sous-titrage :

260
00:19:44,480 --> 00:19:48,640
Monia Ben Hamadi, Hazar Abidi,
Haïfa Mzalouat

261
00:19:48,640 --> 00:19:50,200
Écoute collective et feedbacks :

262
00:19:50,200 --> 00:19:55,480
Yasmin Houamed, Chayma Mehdi,
Haïfa Mzalouat, Khookha McQueer

263
00:19:55,480 --> 00:19:56,720
Illustrations :

264
00:19:56,720 --> 00:19:58,440
Marwen Ben Mustapha

265
00:19:58,440 --> 00:20:02,040
Avec la collaboration
de toute l'équipe d'Inkyfada
