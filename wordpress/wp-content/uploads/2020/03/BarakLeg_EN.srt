0
00:00:00.530 --> 00:00:00.161


1
00:00:00.161 --> 00:00:05.15
If you don’t beg in Libya,

2
00:00:06.229 --> 00:00:08.629
you’ll die of hunger.

3
00:00:09.761 --> 00:00:15.744
You need to go to the market<br />to transport vegetables.

4
00:00:15.744 --> 00:00:19.526
They give you 1 dinar, 2 dinars.<br />What good is that?

5
00:00:19.526 --> 00:00:23.86
It’s not even one Tunisian dinar.

6
00:00:25.367 --> 00:00:31.811
Then at the end of the day,<br />we keep the onions and spoiled tomatoes.

7
00:00:31.811 --> 00:00:36.027
We take them home<br />so we can survive.
