0
00:00:00.00 --> 00:00:00.484


1
00:00:00.484 --> 00:00:03.978
You don’t see anything, everything is black.

2
00:00:04.625 --> 00:00:09.767
And there’s an endless wall.

3
00:00:10.054 --> 00:00:11.729
You pay for the trip,

4
00:00:11.729 --> 00:00:17.518
and you don’t even have<br />a small mat to sleep on.

5
00:00:18.504 --> 00:00:22.6
We slept on the floor.

6
00:00:22.6 --> 00:00:27.876
If someone had described<br />the Libyan situation to me,

7
00:00:27.876 --> 00:00:30.273
I would have preferred to die.

8
00:00:30.273 --> 00:00:32.914
I would've stayed in the Gambia

9
00:00:32.914 --> 00:00:35.763
or Casamance, and not left the country.

