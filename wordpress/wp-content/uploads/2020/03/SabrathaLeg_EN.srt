0
00:00:00.00 --> 00:00:00.55

1
00:00:00.559 --> 00:00:03.612
We left Libya.

2
00:00:03.612 --> 00:00:08.128
We voyaged until Friday morning,

3
00:00:09.138 --> 00:00:11.36
and then there was no more fuel.

4
00:00:11.36 --> 00:00:15.548
We passed the night like this.<br />Our mothers cried.

5
00:00:15.548 --> 00:00:23.857
There were many girls,<br />at least 40.

6
00:00:23.857 --> 00:00:27.269
The girls were screaming.

7
00:00:27.269 --> 00:00:29.848
The men were crying.

8
00:00:29.848 --> 00:00:32.474
Without sleeping, without drinking.

9
00:00:32.474 --> 00:00:37.12
Some spent three days<br />without sitting

10
00:00:32.474 --> 00:00:40.012
because there wasn’t space.

11
00:00:40.012 --> 00:00:41.952
Without sleep, without anything. 