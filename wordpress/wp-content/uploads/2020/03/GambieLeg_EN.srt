0
00:00:00.530 --> 00:00:01.501


1
00:00:01.636 --> 00:00:03.062
Us, we were born in war,

2
00:00:03.062 --> 00:00:06.794
we grew up in war, we went to school in war.

3
00:00:07.098 --> 00:00:09.978
My father was assassinated in Casamance

4
00:00:09.978 --> 00:00:12.071
during the civil war.

5
00:00:12.723 --> 00:00:18.082
I moved to the Gambia<br />with my mother and younger brother.
