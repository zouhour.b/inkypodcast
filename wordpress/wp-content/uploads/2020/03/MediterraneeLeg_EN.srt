0
00:00:00.00 --> 00:00:00.042


1
00:00:00.042 --> 00:00:04.136
We were so tired that<br />we could not see death!

2
00:00:04.303 --> 00:00:08.453
We prefer to die in the Mediterranean<br />and to leave that camp.

3
00:00:08.452 --> 00:00:11.244
We could no longer turn back.

4
00:00:11.244 --> 00:00:15.775
Now, the only goal is<br />to cross the Mediterranean.

5
00:00:15.775 --> 00:00:19.421
Once in Libya,<br />we just want to cross the Mediterranean.

6
00:00:19.421 --> 00:00:22.683
It is the only way to peace,

7
00:00:22.683 --> 00:00:26.796
to be considered a human being.

8
00:00:26.796 --> 00:00:32.581
It is the only way to be<br />a whole person.