﻿1
00:00:04,800 --> 00:00:06,320
INKYSTORIES

2
00:00:06,320 --> 00:00:07,480
une série d'articles,

3
00:00:07,480 --> 00:00:09,600
reportages et enquêtes d'Inkyfada,

4
00:00:09,660 --> 00:00:11,120
au format Podcast.

5
00:00:18,020 --> 00:00:19,360
Hamrouni et Aziz,

6
00:00:19,360 --> 00:00:21,640
visages de travailleuse et travailleur du sexe

7
00:00:23,900 --> 00:00:25,940
lu et écrit par :

8
00:00:26,000 --> 00:00:27,500
Hortense Lac

9
00:00:46,440 --> 00:00:48,320
Hamrouni et Aziz exercent le même métier

10
00:00:48,320 --> 00:00:49,720
sans avoir les mêmes droits.

11
00:00:49,940 --> 00:00:51,520
Elle est travailleuse du sexe légale,

12
00:00:51,520 --> 00:00:53,220
tandis qu’il travaille clandestinement.

13
00:00:54,620 --> 00:00:56,180
Entre leurs rapports à la police

14
00:00:56,180 --> 00:00:57,920
ou le simple regard du voisinage,

15
00:00:57,920 --> 00:00:59,920
leurs quotidiens ne se ressemblent en rien.

16
00:00:59,920 --> 00:01:02,440
Le soir, c’est dans la rue,

17
00:01:02,440 --> 00:01:04,240
les cafés ou même dans les parcs

18
00:01:04,320 --> 00:01:05,860
qu’Aziz propose ses services.

19
00:01:06,440 --> 00:01:08,200
Le travailleur du sexe clandestin

20
00:01:08,220 --> 00:01:09,380
essaie de rester discret

21
00:01:09,440 --> 00:01:11,520
pour ne pas attirer l’attention des forces de l'ordre,

22
00:01:11,520 --> 00:01:13,980
tout en étant visible par les potentiels clients.

23
00:01:15,220 --> 00:01:16,940
À l’inverse, c’est en plein jour,

24
00:01:17,060 --> 00:01:19,000
dans une impasse de la médina de Tunis

25
00:01:19,080 --> 00:01:20,800
et à la vue de toutes et tous,

26
00:01:21,080 --> 00:01:22,640
qu’Hamrouni travaille.

27
00:01:22,800 --> 00:01:23,980
Protégée par une porte,

28
00:01:24,200 --> 00:01:26,780
Abdallah Guech reste un haut lieu de travail du sexe

29
00:01:26,780 --> 00:01:28,060
encadré par l’État.

30
00:01:28,540 --> 00:01:29,680
Depuis presque neuf ans,

31
00:01:29,680 --> 00:01:32,120
c’est dans cette impasse qu’elle propose ses services.

32
00:01:38,500 --> 00:01:39,360
PREMIERS PAS

33
00:01:45,980 --> 00:01:47,440
Originaire du Grand Tunis,

34
00:01:47,440 --> 00:01:49,580
Hamrouni commence par vendre des briquets dans la rue,

35
00:01:49,640 --> 00:01:51,600
pour subvenir aux besoins de ses trois enfants.

36
00:01:52,020 --> 00:01:53,160
Il y a bientôt neuf ans,

37
00:01:53,160 --> 00:01:54,620
elle apprend qu’une de ses filles,

38
00:01:54,620 --> 00:01:55,900
usagère de drogues injectables,

39
00:01:55,960 --> 00:01:57,560
a contracté le VIH.

40
00:02:00,880 --> 00:02:02,300
Entre les médicaments et les soins,

41
00:02:02,300 --> 00:02:05,180
Hamrouni ne se sent plus capable d’assumer les dépenses de sa famille.

42
00:02:05,740 --> 00:02:07,180
Sans connaître personne là-bas,

43
00:02:07,360 --> 00:02:10,200
l’idée lui vient d’aller frapper à la porte de l’impasse Abdallah Guech

44
00:02:10,200 --> 00:02:12,200
pour se faire embaucher et “gagner plus d’argent”.

45
00:02:15,900 --> 00:02:18,640
La mère de famille se rend donc à la municipalité de Tunis.

46
00:02:19,060 --> 00:02:20,200
Il y a une Police des mœurs

47
00:02:20,240 --> 00:02:23,180
spécialisée dans les femmes qui veulent être travailleuses du sexe.

48
00:02:23,300 --> 00:02:26,140
Là-bas, elle assure être convaincue de sa décision

49
00:02:26,200 --> 00:02:28,460
et affirme ne pas être engagée dans une relation,

50
00:02:28,520 --> 00:02:31,580
condition nécessaire à l’obtention d’une place à Abdallah Guech.

51
00:02:32,360 --> 00:02:33,740
Si une femme veut se marier,

52
00:02:33,740 --> 00:02:35,440
elle fait la demande à la Police des mœurs

53
00:02:35,440 --> 00:02:36,640
et elle arrête de travailler.

54
00:02:37,540 --> 00:02:38,580
Dès son arrivée,

55
00:02:38,600 --> 00:02:40,340
la tenancière de l’appartement qu’elle occupe

56
00:02:40,340 --> 00:02:41,480
lui prête 2.000 dinars

57
00:02:41,540 --> 00:02:42,400
pour l’encourager.

58
00:02:42,960 --> 00:02:43,920
Neuf ans après,

59
00:02:43,920 --> 00:02:45,920
Hamrouni continue de rembourser cette dette

60
00:02:45,920 --> 00:02:48,300
en lui versant quelques dizaines de dinars chaque mois.

61
00:02:51,580 --> 00:02:52,900
Contrairement à Hamrouni,

62
00:02:52,900 --> 00:02:55,260
Aziz s’est tourné vers le travail du sexe par hasard.

63
00:02:55,840 --> 00:02:57,300
Vers 13 ou 14 ans,

64
00:02:57,340 --> 00:02:59,160
son chemin croise celui d’un homme,

65
00:02:59,160 --> 00:02:59,860
un étranger.

66
00:03:00,360 --> 00:03:02,460
"Je crois que c’était dans la rue, ou sur la plage."

67
00:03:03,540 --> 00:03:05,460
Plus de dix ans ont passé depuis cette rencontre

68
00:03:05,640 --> 00:03:08,040
et certains détails se sont estompés avec le temps.

69
00:03:08,740 --> 00:03:11,920
Il se rappelle que cet homme lui avait proposé de venir "jouer au docteur".

70
00:03:14,300 --> 00:03:16,040
L’adolescent accepte de l’accompagner,

71
00:03:16,040 --> 00:03:17,800
n’ayant pas saisi le sous-entendu.

72
00:03:18,900 --> 00:03:20,560
"Au départ, je n’ai rien cherché…"

73
00:03:22,340 --> 00:03:23,360
Peu de temps après,

74
00:03:23,360 --> 00:03:26,100
le jeune homme commence à se faire payer contre des faveurs sexuelles.

75
00:03:26,580 --> 00:03:27,720
"Lorsque tu prends cette route,

76
00:03:27,720 --> 00:03:28,960
tu banalises ces choses.

77
00:03:29,240 --> 00:03:30,800
Tu te dis : ce n’est qu’un cul

78
00:03:30,800 --> 00:03:32,800
ce n’est qu’un corps, ce n’est qu’une bouche…"

79
00:03:33,600 --> 00:03:37,100
Il devient rapidement dépendant de l’argent et des cadeaux que ses partenaires lui donnent,

80
00:03:37,140 --> 00:03:39,100
multiplie les sorties et dépense sans compter.

81
00:03:39,560 --> 00:03:42,120
"C’était un moyen d’avoir son Walkman dernier cri, par exemple.

82
00:03:42,120 --> 00:03:43,420
Ça peut sembler futile,

83
00:03:43,420 --> 00:03:45,860
mais dans les yeux de quelqu’un qui n’a même pas atteint les 18 ans,

84
00:03:45,860 --> 00:03:47,860
c’est quelque chose".

85
00:03:52,220 --> 00:03:53,460
VIE QUOTIDIENNE

86
00:03:59,060 --> 00:04:00,200
À Abdallah Guech,

87
00:04:00,200 --> 00:04:03,940
Hamrouni partage un petit appartement avec plusieurs collègues travailleuses du sexe.

88
00:04:04,740 --> 00:04:06,760
Toute l’impasse est organisée de cette manière,

89
00:04:06,880 --> 00:04:09,620
chaque tenancière possédant quelques chambres dans les logements.

90
00:04:10,540 --> 00:04:11,880
Le prix des passes est fixe.

91
00:04:11,880 --> 00:04:13,880
“8 dinars 500”, affirme la jeune femme.

92
00:04:15,240 --> 00:04:18,540
Cette somme est ensuite répartie entre la matrone et les travailleuses.

93
00:04:19,060 --> 00:04:21,440
Sur une passe, l'employée gagne 3 dinars,

94
00:04:21,700 --> 00:04:23,600
qui sont parfois complétés par quelques pièces

95
00:04:23,600 --> 00:04:25,020
laissées en supplément par les clients.

96
00:04:27,020 --> 00:04:28,160
Concernant leurs droits,

97
00:04:28,280 --> 00:04:31,020
les travailleuses du sexe des maisons closes ne travaillent jamais le vendredi

98
00:04:31,280 --> 00:04:32,720
jour de fermeture de l’impasse.

99
00:04:33,440 --> 00:04:36,720
Un·e médecin leur rend visite deux fois par semaine pour les examiner.

100
00:04:37,540 --> 00:04:40,500
Le personnel médical leur délivre également un arrêt de travail,

101
00:04:40,500 --> 00:04:41,720
lors de leurs menstruations.

102
00:04:42,500 --> 00:04:44,500
Le soir, une fois les portes de l’impasse fermées,

103
00:04:44,680 --> 00:04:47,860
la plupart d’entre elles continuent à exercer leur profession illégalement.

104
00:04:48,640 --> 00:04:49,560
À ce moment-là,

105
00:04:49,560 --> 00:04:51,400
elles ne se soumettent plus à aucune règle

106
00:04:51,400 --> 00:04:53,860
et fixent leurs prix suivant les clients et leurs demandes.

107
00:04:55,720 --> 00:04:57,440
Aziz n’a jamais eu accès à tout ça,

108
00:04:57,500 --> 00:04:59,180
n’exerçant pas dans un cadre légal.

109
00:04:59,720 --> 00:05:02,440
À l’époque où il s'adonnait régulièrement au travail du sexe,

110
00:05:02,840 --> 00:05:04,200
il fixait le prix de ses passes

111
00:05:04,200 --> 00:05:05,800
entre 200 et 300 dinars.

112
00:05:06,600 --> 00:05:08,920
Ses clients, majoritairement étrangers,

113
00:05:09,260 --> 00:05:11,000
pouvaient dépenser des centaines de dinars

114
00:05:11,000 --> 00:05:12,520
pour passer un peu de temps avec lui,

115
00:05:12,760 --> 00:05:14,660
parfois même uniquement le temps d’un dîner.

116
00:05:15,660 --> 00:05:17,740
"C’était plus de l’escort, de la compagnie."

117
00:05:18,120 --> 00:05:20,220
Le jeune homme ne parle jamais d’argent avec ses clients.

118
00:05:21,660 --> 00:05:23,660
"Il ne faut pas leur montrer que tu es là pour ça,

119
00:05:23,660 --> 00:05:24,900
qu’ils payent pour un service,

120
00:05:25,540 --> 00:05:27,660
je préfère que ce soit plus spontané de leur part."

121
00:05:29,660 --> 00:05:32,280
Depuis quelques années, il ne travaille presque plus dans le sexe,

122
00:05:32,280 --> 00:05:33,440
"c’est devenu ennuyeux".

123
00:05:34,280 --> 00:05:35,880
"J’avais peur que ça me consomme.

124
00:05:35,880 --> 00:05:37,700
C’est l’excitation qui a diminué,

125
00:05:37,700 --> 00:05:39,700
peut-être qu’avec l’âge, le besoin a changé."

126
00:05:42,340 --> 00:05:45,880
Financièrement, il a trouvé d’autres moyens de gagner sa vie, en parallèle de ses études,

127
00:05:46,380 --> 00:05:49,320
il consacre beaucoup de temps à la vente d’accessoires sur internet.

128
00:05:49,760 --> 00:05:52,280
C’est d’ailleurs cette passion qui lui a permis de camoufler à ses parents

129
00:05:52,280 --> 00:05:54,280
ses rentrées d’argent depuis son adolescence.

130
00:05:54,800 --> 00:05:56,740
Il pense que sa famille ne se doute de rien,

131
00:05:56,740 --> 00:05:59,240
hormis sa mère, qui ne serait pas dupe de ses activités.

132
00:06:05,840 --> 00:06:06,680
LES CLIENTS

133
00:06:11,900 --> 00:06:14,100
Une fois les portes de l’impasse Abdallah Guech franchies,

134
00:06:14,360 --> 00:06:16,320
le client se retrouve face à plusieurs femmes

135
00:06:16,320 --> 00:06:18,980
et choisit celle avec qui il souhaite avoir un rapport sexuel.

136
00:06:19,640 --> 00:06:21,440
La travailleuse du sexe garde le dernier mot

137
00:06:21,580 --> 00:06:23,920
et peut refuser si l’homme ne lui inspire pas confiance

138
00:06:23,920 --> 00:06:25,920
ou semble présenter des signes d'infection.

139
00:06:26,760 --> 00:06:28,420
"Ah les femmes qui travaillent en maison close

140
00:06:28,680 --> 00:06:30,420
avant d’avoir un rapport sexuel

141
00:06:30,420 --> 00:06:31,960
elles voient s’il y a un truc qui cloche."

142
00:06:34,220 --> 00:06:36,900
Avec le temps, certains clients deviennent des habitués.

143
00:06:37,240 --> 00:06:39,780
Il leur arrive d’appeler Hamrouni avant de venir

144
00:06:39,780 --> 00:06:41,500
pour qu’elle prenne une douche et se prépare.

145
00:06:42,740 --> 00:06:44,720
Mais souvent, ils ne sont que de passage.

146
00:06:45,460 --> 00:06:46,620
En discutant avec eux,

147
00:06:46,620 --> 00:06:49,960
Hamrouni apprend qu'ils viennent principalement à Tunis pour des raisons professionnelles

148
00:06:49,960 --> 00:06:51,960
et, une fois leurs affaires bouclées,

149
00:06:51,960 --> 00:06:53,140
ils passent par la maison close.

150
00:06:53,960 --> 00:06:55,480
Pour le travail du sexe clandestin,

151
00:06:55,480 --> 00:06:57,060
le rapport aux clients est différent.

152
00:06:57,180 --> 00:07:00,320
Si les nouveaux venus ne font appel à Aziz que pour un acte sexuel,

153
00:07:00,560 --> 00:07:03,400
les habitués de longue date paient également pour des services d’escort.

154
00:07:05,200 --> 00:07:07,300
"Les coins les plus connus c’est les coins touristiques,

155
00:07:07,720 --> 00:07:08,820
il y a des coins encore plus chauds

156
00:07:08,940 --> 00:07:10,300
le soir par exemple comme

157
00:07:10,300 --> 00:07:12,500
le jardin d’amour, le jardin du Passage

158
00:07:12,500 --> 00:07:13,340
le Belvédère".

159
00:07:13,400 --> 00:07:14,300
Mais dans ces lieux,

160
00:07:14,300 --> 00:07:15,800
les risques d’agression augmentent

161
00:07:15,800 --> 00:07:18,080
avec la présence des forces de l'ordre et du voisinage.

162
00:07:23,900 --> 00:07:24,900
LES RISQUES

163
00:07:30,840 --> 00:07:33,080
L’impasse Abdallah Guech est connue des forces de l’ordre.

164
00:07:33,480 --> 00:07:35,320
Elles ont d’ailleurs protégé la maison close

165
00:07:35,320 --> 00:07:37,780
lors des tentatives de fermeture après la révolution.

166
00:07:38,620 --> 00:07:39,400
Depuis,

167
00:07:39,400 --> 00:07:41,500
une porte ferme l’impasse chaque soir

168
00:07:41,500 --> 00:07:43,700
pour éviter que n’importe qui ne s’y aventure.

169
00:07:44,240 --> 00:07:45,240
En plus de ça,

170
00:07:45,240 --> 00:07:48,580
plusieurs personnes du quartier aident les travailleuses du sexe dans leur quotidien

171
00:07:48,820 --> 00:07:50,820
et surveillent les allées et venues des environs.

172
00:07:52,520 --> 00:07:53,740
En cas de débordement,

173
00:07:53,740 --> 00:07:55,740
les forces de l'ordre ne sont jamais très loin.

174
00:08:02,520 --> 00:08:03,780
D’un point de vue sanitaire,

175
00:08:03,780 --> 00:08:07,840
les travailleuses de la maison close sont suivies deux fois par semaine par un·e médecin.

176
00:08:08,480 --> 00:08:10,380
Il ou elle leur donne quelques préservatifs,

177
00:08:10,640 --> 00:08:12,980
insistant sur leur importance pour éviter les infections,

178
00:08:13,180 --> 00:08:15,540
les maladies et les grossesses non désirées.

179
00:08:16,260 --> 00:08:17,540
Mais, ce n'est pas suffisant.

180
00:08:17,540 --> 00:08:19,540
Hamrouni doit souvent piocher dans ses revenus

181
00:08:19,540 --> 00:08:21,540
pour en acheter d’autres.

182
00:08:21,540 --> 00:08:23,480
Face à l’insistance de certains clients

183
00:08:23,480 --> 00:08:25,220
et pour quelques dinars supplémentaires

184
00:08:25,420 --> 00:08:27,140
il lui arrive de ne pas se protéger.

185
00:08:28,300 --> 00:08:30,960
Aziz, lui, ne mettait pas du tout de préservatif,

186
00:08:31,220 --> 00:08:33,660
c’était excitant. Tu prenais le risque de jouer.

187
00:08:34,340 --> 00:08:35,140
À 23 ans,

188
00:08:35,140 --> 00:08:38,260
il se rend dans un bureau de l'association ATL MST Sida

189
00:08:38,440 --> 00:08:39,420
pour se faire dépister.

190
00:08:39,880 --> 00:08:42,680
Il reste stupéfait de voir qu’il n’a pas contracté le virus.

191
00:08:43,180 --> 00:08:44,360
Je n’arrivais pas à comprendre !

192
00:08:44,460 --> 00:08:46,680
"Même dans l’association, même eux ils étaient bouche bée".

193
00:08:49,060 --> 00:08:50,240
Désormais plus prudent,

194
00:08:50,240 --> 00:08:52,380
il met un préservatif avant chaque rapport.

195
00:08:56,220 --> 00:08:58,820
L’un des autres risques pour un travailleur du sexe clandestin

196
00:08:58,820 --> 00:09:00,640
est de se faire arrêter par la police.

197
00:09:01,220 --> 00:09:03,680
Aziz a toujours été très prévoyant par rapport à ça.

198
00:09:05,420 --> 00:09:06,840
"Je ne passais pas par les hôtels

199
00:09:07,000 --> 00:09:08,840
Ailleurs, mais pas les hôtels.

200
00:09:09,060 --> 00:09:10,840
Chez des amis, chez le client...

201
00:09:12,720 --> 00:09:15,640
Si un travailleur du sexe est vu en compagnie d’un homme d’un certain âge

202
00:09:16,060 --> 00:09:18,040
les flics devinent, ils ne sont pas stupides,

203
00:09:18,360 --> 00:09:20,860
C’est des loups. Ils veulent leur part du gâteau.

204
00:09:21,320 --> 00:09:22,660
Pour eux c’est une opportunité.

205
00:09:22,800 --> 00:09:25,100
ou ils te baisent, ou tu achètes leur silence".

206
00:09:31,400 --> 00:09:34,060
Malgré ses précautions et ses contacts au sein de la police,

207
00:09:34,060 --> 00:09:36,060
Aziz n’a pas échappé aux arrestations.

208
00:09:37,180 --> 00:09:38,320
Avant la révolution,

209
00:09:38,320 --> 00:09:39,380
à Monastir,

210
00:09:39,600 --> 00:09:41,600
alors qu’il attendait un client à l’aéroport,

211
00:09:41,960 --> 00:09:43,600
les forces de l'ordre l’interpellent,

212
00:09:43,700 --> 00:09:45,280
le questionnent sur sa présence ici,

213
00:09:45,580 --> 00:09:47,280
avant de l’emmener au commissariat.

214
00:09:47,560 --> 00:09:49,280
"J’ai passé toute la nuit avec les regards,

215
00:09:49,740 --> 00:09:50,840
tous les regards sur moi,

216
00:09:50,840 --> 00:09:51,900
Les moqueries…

217
00:09:52,600 --> 00:09:54,000
‘Oh la pédale de Tunis’..."

218
00:10:00,320 --> 00:10:01,040
LE FUTUR

219
00:10:08,420 --> 00:10:10,080
La difficulté d’exercer ce métier,

220
00:10:10,300 --> 00:10:11,860
légalement ou illégalement

221
00:10:12,180 --> 00:10:14,160
pousse beaucoup de travailleurs et travailleuses du sexe

222
00:10:14,160 --> 00:10:16,160
à changer de métier ou du moins à diminuer leur activité.

223
00:10:18,160 --> 00:10:19,400
Aziz, par exemple,

224
00:10:19,400 --> 00:10:20,660
ne croit pas en son avenir

225
00:10:20,980 --> 00:10:22,300
professionnel ou sentimental.

226
00:10:23,380 --> 00:10:25,340
Il est persuadé qu’il terminera sa vie seul.

227
00:10:26,360 --> 00:10:28,080
"Ça ne marche pas, c’est perdu d’avance

228
00:10:28,480 --> 00:10:30,600
Peut-être que j’ai abusé, j’ai trop exagéré".

229
00:10:32,160 --> 00:10:34,860
Il se sent discriminé du fait de son orientation sexuelle,

230
00:10:35,200 --> 00:10:36,860
en plus de son ancien travail.

231
00:10:37,540 --> 00:10:39,500
Même s’il a désormais quitté ce milieu-là,

232
00:10:39,500 --> 00:10:41,960
il garde un rôle d’entremetteur entre les clients

233
00:10:41,960 --> 00:10:44,140
et ses ami·es travailleurs et travailleuses du sexe.

234
00:10:45,540 --> 00:10:46,920
"On a toujours un pied dans le milieu,

235
00:10:46,920 --> 00:10:48,340
on garde toujours ses contacts.

236
00:10:48,340 --> 00:10:50,340
On sait jamais, ça peut servir".

237
00:10:51,300 --> 00:10:52,200
Pour Hamrouni,

238
00:10:52,200 --> 00:10:54,560
la priorité est désormais de s’occuper de son fils

239
00:10:54,560 --> 00:10:55,360
encore écolier.

240
00:10:55,920 --> 00:10:57,840
Sa fille atteinte du VIH

241
00:10:57,840 --> 00:10:59,180
est décédée en 2016,

242
00:10:59,440 --> 00:11:02,220
tandis que sa cadette est en prison pour consommation de drogues.

243
00:11:03,000 --> 00:11:05,240
La mère de famille souhaite désormais changer de métier

244
00:11:05,600 --> 00:11:08,600
mais craint de ne pas gagner aussi bien sa vie sans le travail du sexe.

245
00:11:09,720 --> 00:11:12,480
Avec l’aide de l’association ATL MST Sida,

246
00:11:12,740 --> 00:11:14,000
elle apprend à faire du crochet,

247
00:11:14,200 --> 00:11:15,420
une technique de tissage

248
00:11:15,440 --> 00:11:17,700
pour éventuellement se professionnaliser dans cette voie.

249
00:11:18,200 --> 00:11:19,220
En plus de ça,

250
00:11:19,460 --> 00:11:21,460
l’organisation lui verse une petite somme d’argent

251
00:11:21,800 --> 00:11:23,460
pour qu'elle ait d'autres sources de revenues.

252
00:11:33,060 --> 00:11:36,040
Peu de statistiques existent sur le sujet du travail du sexe légal.

253
00:11:36,480 --> 00:11:37,160
À ce jour,

254
00:11:37,160 --> 00:11:40,280
seules quelques associations travaillant avec les femmes des maisons closes,

255
00:11:40,520 --> 00:11:42,280
essaient de mesurer le phénomène.

256
00:11:42,760 --> 00:11:43,940
Pour le Bureau des Mœurs,

257
00:11:43,940 --> 00:11:45,340
elles étaient en 2018

258
00:11:45,340 --> 00:11:47,740
près de 70 à exercer ce métier dans le pays.

259
00:11:50,400 --> 00:11:52,760
Hamrouni a elle-même choisi son prénom d’emprunt.

260
00:11:53,220 --> 00:11:55,520
Celui de Aziz a également été modifié,

261
00:11:55,880 --> 00:11:57,520
par souci d’anonymat.

262
00:12:00,180 --> 00:12:02,960
Cet article a été lu en respectant une grammaire sensible au genre.

263
00:12:05,100 --> 00:12:06,680
Retrouvez l'intégralité de l'article

264
00:12:06,680 --> 00:12:10,140
"Hamrouni et Aziz, visages de travailleuse et travailleur du sexe"

265
00:12:10,280 --> 00:12:12,140
sur Inkyfada.com

266
00:12:15,680 --> 00:12:17,280
Article audio produit par :

267
00:12:17,280 --> 00:12:18,960
Inkyfada Podcast

268
00:12:19,180 --> 00:12:20,280
Réalisation :

269
00:12:20,520 --> 00:12:22,460
Monia Ben Hammadi et Bochra Triki

270
00:12:23,020 --> 00:12:24,000
Article et voix :

271
00:12:24,000 --> 00:12:24,800
Hortense Lac

272
00:12:25,320 --> 00:12:26,540
Montage et Mixage :

273
00:12:26,780 --> 00:12:28,100
Yassine Kawana

274
00:12:28,280 --> 00:12:29,040
Musique :

275
00:12:29,180 --> 00:12:30,300
Omar Aloulou

276
00:12:30,620 --> 00:12:31,580
Enregistrement :

277
00:12:31,680 --> 00:12:32,640
Bochra Triki
