﻿1
00:00:04,130 --> 00:00:05,420
Inkystories

2
00:00:05,420 --> 00:00:08,880
Une séries d'articles, enquêtes et reportages d'Inkyfada

3
00:00:08,880 --> 00:00:10,880
au format podcast

4
00:00:14,880 --> 00:00:20,840
Cléo, une militante transgenre réfugiée en Tunisie

5
00:00:23,820 --> 00:00:27,370
Article lu et écrit par Haïfa Mzalouat

6
00:00:44,750 --> 00:00:50,350
Originaire du Bénin, Cléo* a quitté son pays en raison de sa transsexualité et de son militantisme.

7
00:00:50,350 --> 00:00:52,710
Elle vit en Tunisie depuis quelques années

8
00:00:52,710 --> 00:00:54,710
et a récemment obtenu le statut de réfugiée.

9
00:00:54,970 --> 00:00:57,110
Elle nous raconte son combat et son parcours.

10
00:01:03,820 --> 00:01:05,640
Cléo est une femme grande et élégante.

11
00:01:05,640 --> 00:01:09,330
Souriante, elle se présente en quelques mots, d’un ton calme et posé.

12
00:01:09,330 --> 00:01:12,400
“Je m’appelle Cléo, j’ai 28 ans, et je suis Béninoise.

13
00:01:12,400 --> 00:01:14,260
J’entame ma quatrième année en Tunisie,

14
00:01:14,260 --> 00:01:16,260
et j’ai obtenu le statut de réfugié il y a 6 mois,

15
00:01:16,260 --> 00:01:18,260
car je suis une personne transgenre."

16
00:01:23,280 --> 00:01:27,020
Un départ précipité d'Abidjan

17
00:01:31,020 --> 00:01:34,310
Par bribes, Cléo commence à raconter son histoire.

18
00:01:34,310 --> 00:01:40,080
Depuis son plus jeune âge, elle ne se sent pas en adéquation
avec l’étiquette de garçon qui lui a été assignée à la naissance.

19
00:01:40,310 --> 00:01:43,060
Enfant, elle refuse de “s’habiller en garçon”.

20
00:01:43,060 --> 00:01:45,640
Lors de son adolescence, qu’elle passe en Côte d’Ivoire,

21
00:01:45,640 --> 00:01:48,350
elle est perçue comme “un ado beaucoup trop efféminé”.

22
00:01:48,350 --> 00:01:52,620
Son père, chez qui elle vivait alors, critique constamment son comportement.

23
00:01:53,330 --> 00:01:57,730
Dans son lycée, exclusivement masculin, la situation est également difficile.

24
00:01:59,730 --> 00:02:04,130
“Les gens ne savaient pas trop si j’étais un garçon ou une fille car j’avais une apparence assez androgyne”,

25
00:02:04,130 --> 00:02:08,750
Je n’avais pas d’amis, à part deux ou trois garçons homosexuels, et j’ai subi beaucoup de rejet”

26
00:02:09,910 --> 00:02:11,640
Cléo se fait également harceler.

27
00:02:11,730 --> 00:02:14,620
Un jour, la quasi-totalité des élèves de l’internat

28
00:02:14,620 --> 00:02:17,510
ont attendu qu’elle et un de ses amis entrent dans l’établissement.

29
00:02:18,440 --> 00:02:23,680
“Ils nous ont accueillis en tapant sur les murs et sur les tables en scandant ‘Pédés! Pédés!’”

30
00:02:23,680 --> 00:02:27,680
En dépit de ces moqueries, Cléo commence à défendre les droits des personnes LGBT

31
00:02:27,680 --> 00:02:31,730
(Lesbiennes, Gays, Bisexuel.le.s et Transgenres) au sein de son établissement scolaire

32
00:02:31,730 --> 00:02:34,440
en improvisant des réunions pour discuter de la question.

33
00:02:35,600 --> 00:02:38,170
J’ai révélé que j’étais transgenre pendant mon année de terminale,

34
00:02:38,170 --> 00:02:39,550
et j’ai trouvé quelques soutiens.

35
00:02:39,550 --> 00:02:42,710
Cela a même poussé d’autres élèves à révéler leur homosexualité”.

36
00:02:43,860 --> 00:02:49,330
En apprenant cela, son père la chasse de chez lui, et aucun membre de sa famille ne veut l’accueillir.

37
00:02:49,680 --> 00:02:53,910
Un “grand ami” l’héberge pendant près d’un an et l’aide à passer son bac, tant bien que mal.

38
00:02:55,910 --> 00:03:01,240
“À la fin de l’année, ma mère a décidé qu’il valait peut-être mieux pour moi que je la rejoigne au Bénin,
pour qu’elle puisse me protéger”.

39
00:03:01,820 --> 00:03:03,510
Elle quitte Abidjan en 2010.

40
00:03:09,510 --> 00:03:11,510
Un coming-out "mémorable"

41
00:03:29,510 --> 00:03:33,640
Une fois chez sa mère, Cléo fait un coming-out qu’elle qualifie de “mémorable”.

42
00:03:33,860 --> 00:03:37,730
Elle se souvient qu’un soir, sa mère est venue dans sa chambre et lui a déclaré:

43
00:03:38,350 --> 00:03:40,480
“Écoute, j’aimerais savoir si tu es gay.

44
00:03:40,480 --> 00:03:43,820
Si tu l’es, je préfère que tu me le dises plutôt que de l’apprendre de l’extérieur,

45
00:03:43,820 --> 00:03:45,820
et je saurais comment te protéger”.

46
00:03:45,950 --> 00:03:48,440
Sur le coup, Cléo n’ose pas lui dire la vérité.

47
00:03:49,110 --> 00:03:51,420
Je m’étais préparée à ce qu’elle me rejette, elle aussi.

48
00:03:51,420 --> 00:03:54,170
Mais j’ai finalement décidé de lui parler pour être fixée une bonne fois pour toutes”.

49
00:03:55,420 --> 00:03:59,680
Elle rejoint donc sa mère une demi-heure plus tard et lui dit que non, elle n’est pas “homosexuelle”.

50
00:04:00,040 --> 00:04:02,660
“En fait, j’aime les garçons et je me sens mieux en robe”.

51
00:04:03,860 --> 00:04:07,240
C’est avec une certaine émotion que la jeune femme répète la réponse de sa mère:

52
00:04:07,730 --> 00:04:10,310
Moi aussi, j’aime les garçons et j’aime porter des robes, tu sais”.

53
00:04:10,310 --> 00:04:12,840
“Mon coming-out est passé aussi simplement que cela”,

54
00:04:12,840 --> 00:04:17,200
“Je me suis sentie bien et très soulagée, même s’il y a eu un petit malaise dans les semaines qui ont suivi.

55
00:04:17,200 --> 00:04:20,310
On ne savait pas trop comment se regarder ni comment en parler…”

56
00:04:20,710 --> 00:04:24,260
C’est sa mère qui fait encore une fois le premier pas en lui disant, avec douceur,

57
00:04:24,260 --> 00:04:26,260
qu’elle peut “s’habiller en fille” si elle le désire.

58
00:04:27,640 --> 00:04:31,730
“On en a parlé ensemble, et elle m’a dit qu’elle se doutait que j’étais transgenre depuis ma petite enfance.

59
00:04:32,130 --> 00:04:36,480
Elle m’a par exemple raconté que je n’aimais pas m’habiller en garçon, qu’elle me faisait des tresses…”

60
00:04:42,480 --> 00:04:44,480
Une vie de militante au Bénin

61
00:05:02,710 --> 00:05:05,860
Grâce à sa mère, qui est l’aînée d’une “famille assez connue et noble”,

62
00:05:05,860 --> 00:05:07,860
Cléo est plus ou moins protégée.

63
00:05:07,860 --> 00:05:10,480
“J’ai pu rester au Bénin assez longtemps sans vraiment craindre quoi que ce soit,

64
00:05:10,480 --> 00:05:12,480
car j’étais en quelque sorte ‘intouchable’”.

65
00:05:13,020 --> 00:05:17,680
Cléo commence alors à militer pour les droits LGBT, au sein de petites associations clandestines.

66
00:05:18,130 --> 00:05:23,420
Comme en Tunisie, “certaines d’entre elles sont enregistrées auprès de la préfecture mais pour autre chose que la défense de ces droits”

67
00:05:23,420 --> 00:05:26,130
“La prévention contre le Sida ou les MST, par exemple”.

68
00:05:27,240 --> 00:05:30,040
Un jour, Cléo rencontre la conseillère de l’ambassadeur de France

69
00:05:30,350 --> 00:05:32,440
qui l’aide à créer, au sein de l’ambassade,

70
00:05:32,440 --> 00:05:35,950
une plate-forme visant à accueillir les associations de lutte contre l’homophobie.

71
00:05:36,220 --> 00:05:38,970
Cette structure permet aux personnes homosexuelles ou transgenres

72
00:05:38,970 --> 00:05:42,480
de se retrouver et de discuter de potentielles actions à mener sur le terrain.

73
00:05:44,480 --> 00:05:48,930
Les militants partent ainsi à la rencontre de dignitaires religieux, de ministres et de journalistes béninois

74
00:05:48,930 --> 00:05:51,510
afin de débattre de la situation des droits LGBT,

75
00:05:51,510 --> 00:05:53,510
sujet que Cléo qualifie de “tabou”.

76
00:05:54,800 --> 00:05:57,330
Ils organisent des débats, des journées portes ouvertes…

77
00:05:57,600 --> 00:06:00,880
“Malheureusement, le soutien de l’ambassade de France n’a pas joué en notre faveur,

78
00:06:00,880 --> 00:06:05,020
cela a renforcé l’idée que les droits LGBT étaient quelque chose d’importé et d’imposé par l’Europe”

79
00:06:06,350 --> 00:06:09,730
A travers toutes ces actions, Cléo devient une militante de premier plan.

80
00:06:09,730 --> 00:06:14,100
Elle parle à visage découvert, et répond aux questions que les gens se posent sur la transsexualité.

81
00:06:14,100 --> 00:06:18,750
Militer lui fait du bien
et lui donne vraiment l’impression de “faire quelque chose” pour sa communauté.

82
00:06:20,150 --> 00:06:24,350
“Le fait d’avoir la protection et le soutien inconditionnels de ma mère m’a poussée à parler publiquement

83
00:06:24,700 --> 00:06:30,150
“Elle-même a essayé de créer une association de parents d’enfants transgenres et homosexuels,
mais malheureusement, cela a fait un gros flop!”.

84
00:06:31,350 --> 00:06:34,250
Mais être transsexuelle au Bénin est une situation délicate.

85
00:06:34,250 --> 00:06:37,250
Cléo reçoit des menaces et craint d’être dénoncée à la police,

86
00:06:37,250 --> 00:06:40,100
ce qui lui ferait subir le même sort que d’autres personnes transgenres

87
00:06:40,100 --> 00:06:42,100
“qui sont déshabillées et filmées en public,

88
00:06:42,100 --> 00:06:44,650
parfois par des journalistes pour une rubrique dite ‘à scandale’”.

89
00:06:45,350 --> 00:06:48,500
La militante a également été victime de violences physiques et psychologiques.

90
00:06:48,500 --> 00:06:53,350
Un de ses oncles maternels l’a séquestrée dans une église pendant 3 jours, sans boire, ni manger.

91
00:06:53,350 --> 00:06:57,100
Ce jeûne obligatoire avait pour but de “l’exorciser de son mal profond”.

92
00:06:57,100 --> 00:07:01,150
Elle ne peut sortir que grâce aux protestations de sa mère, forte de son rôle d’aînée.

93
00:07:01,550 --> 00:07:05,700
Quelques semaines plus tard, elle est de nouveau enfermée, au domicile de son oncle cette fois.

94
00:07:06,350 --> 00:07:10,650
“Je ne pouvais plus aller à l’université, et ils avaient confisqué mon ordinateur et mon téléphone.

95
00:07:10,650 --> 00:07:14,900
Parfois mon oncle ou un pasteur venaient me parler pour me dire que ce ce que je faisais était contre-nature,

96
00:07:14,900 --> 00:07:18,000
que je ne pouvais pas faire ça à ma mère qui n’avait pas d’autre garçon”.

97
00:07:18,450 --> 00:07:20,850
Cléo reste enfermée pendant presque un mois.

98
00:07:21,450 --> 00:07:24,000
La jeune femme craint de plus en plus pour sa sécurité.

99
00:07:24,000 --> 00:07:27,800
“Un jour, j’ai reçu une convocation de la police m’accusant d’attentat à la pudeur.

100
00:07:28,200 --> 00:07:30,450
Cela a vraiment été la goutte d’eau qui a fait déborder le vase”.

101
00:07:31,400 --> 00:07:34,550
Ce délit est passible d’une amende et de plusieurs années de prison.

102
00:07:35,500 --> 00:07:39,050
A ce moment-là, sa mère estime que le Bénin n’était plus un endroit sûr pour sa fille

103
00:07:39,050 --> 00:07:41,850
et que la notoriété de sa famille ne suffira plus à la protéger. Elles décident de partir.

104
00:07:49,150 --> 00:07:50,450
La fuite

105
00:08:07,950 --> 00:08:10,000
Une nuit, sans que personne ne le sache,

106
00:08:10,000 --> 00:08:12,000
Cléo et sa mère quittent précipitamment le pays.

107
00:08:13,200 --> 00:08:16,750
Tout le trajet s’effectuera en bus car il leur est impossible de partir en avion.

108
00:08:16,750 --> 00:08:20,350
Son père travaillant “dans le trafic aérien” aurait pu en être informé,

109
00:08:20,600 --> 00:08:22,950
Il aurait été immédiatement au courant de mon départ.

110
00:08:22,950 --> 00:08:28,000
Et il m’avait prévenu que s’il me retrouvait, il me “buterait” pour m’empêcher de jeter la honte sur la famille”.

111
00:08:28,400 --> 00:08:33,200
Les deux femmes traversent ainsi le Togo, le Ghana et arrivent en Côte d’Ivoire où elles s’installent provisoirement.

112
00:08:33,600 --> 00:08:36,300
Le trajet ne dure que deux jours, mais Cléo est à bout.

113
00:08:36,750 --> 00:08:38,300
J’avais peur, j’étais stressée”,

114
00:08:38,300 --> 00:08:41,450
"Dans la précipitation, je n’ai pu prendre quasiment aucune affaire.

115
00:08:42,100 --> 00:08:46,250
Je ne suis pas matérialiste mais j’avais vraiment l’impression de tout laisser derrière moi".

116
00:08:47,500 --> 00:08:50,250
Malgré tout, certaines anecdotes la font sourire.

117
00:08:50,250 --> 00:08:55,350
Comme ce douanier, à la frontière entre le Bénin et le Togo qui n’arrive pas à déterminer le genre de Cléo.

118
00:08:55,350 --> 00:09:00,250
“Il était totalement déboussolé, car il avait l’impression que j’étais une femme,
mais mon passeport indiquait que j’étais un homme.

119
00:09:00,600 --> 00:09:03,650
Il m’a gardée pendant une demi-heure, a demandé l’avis de tous ses collègues,

120
00:09:03,650 --> 00:09:06,400
et a fini par me laisser passer en m’appelant ‘Ma copine-copain !,

121
00:09:06,400 --> 00:09:08,400
Je suis toujours en contact avec lui d’ailleurs”.

122
00:09:09,300 --> 00:09:13,300
Une fois à Abidjan, les deux femmes cherchent un pays où elles pourraient se réinstaller,

123
00:09:13,350 --> 00:09:15,200
et où Cléo pourrait entamer ses études de mode.

124
00:09:15,350 --> 00:09:17,250
Les pays frontaliers sont vite exclus.

125
00:09:17,900 --> 00:09:21,350
“En Afrique de l’Ouest, la répression contre les transgenres est partout la même,

126
00:09:21,500 --> 00:09:22,900
je ne me serais pas sentie à l’abri.”

127
00:09:24,000 --> 00:09:26,850
Cléo dépose donc sa candidature pour plusieurs écoles de mode

128
00:09:26,850 --> 00:09:29,600
“situées le plus loin possible” de sa région d’origine.

129
00:09:29,950 --> 00:09:31,800
La vie en France étant trop chère,

130
00:09:31,800 --> 00:09:37,350
elle finit par se tourner vers le Maghreb car on lui a raconté
qu’une communauté homosexuelle noire commençait à s’y développer.

131
00:09:37,900 --> 00:09:41,850
Le Maroc, l’Algérie ou la Tunisie semblent ainsi être de bonnes alternatives.

132
00:09:41,850 --> 00:09:44,250
“Une école tunisoise a fini par me répondre,

133
00:09:44,250 --> 00:09:47,400
j’ai fait mon visa en une semaine et je suis partie pour Tunis début 2012”.

134
00:09:52,900 --> 00:09:54,950
Des débuts compliqués en Tunisie

135
00:10:13,000 --> 00:10:16,550
Pour Cléo, son installation en Tunisie est un moyen de commencer une nouvelle vie.

136
00:10:16,550 --> 00:10:18,200
“Mais j’ai très vite déchanté.

137
00:10:18,200 --> 00:10:23,100
Dès mon arrivée, j’ai dû signer un papier
attestant qu’il m’était impossible de travailler sur le territoire tunisien,

138
00:10:23,100 --> 00:10:25,100
contrairement à ce que m’avait affirmé l’école”.

139
00:10:27,100 --> 00:10:32,400
Malgré tout, la jeune femme décide de rester,
se disant qu’elle finira par trouver un moyen de subvenir à ses besoins.

140
00:10:33,700 --> 00:10:37,900
Pendant deux ans et demi, Cléo vit donc principalement sur ses économies et celles de sa mère.

141
00:10:37,900 --> 00:10:40,400
“Entre le prix de l’école, les transports, la nourriture,

142
00:10:40,400 --> 00:10:43,850
j’ai fini par être à court et ma situation est devenue vraiment précaire”.

143
00:10:43,850 --> 00:10:47,650
A l’expiration de son visa, elle se retrouve également en situation irrégulière

144
00:10:47,650 --> 00:10:50,300
et elle craint d’être expulsée vers le Bénin où elle n’a aucun soutien,

145
00:10:50,300 --> 00:10:52,300
sa mère ayant quitté le pays.

146
00:10:52,300 --> 00:10:55,800
Un seul contrôle de police suffirait à la renvoyer vers son pays d’origine.

147
00:10:56,250 --> 00:11:00,650
La jeune femme se sent démunie et décrit cette période comme “extrêmement difficile et angoissante”.

148
00:11:01,250 --> 00:11:06,550
En plus de ces difficultés financières et administratives, Cléo est victime de nombreuses discriminations.

149
00:11:07,450 --> 00:11:11,650
"La Tunisie est un très beau pays, je me sens assez bien intégrée, j’ai des amis tunisiens…

150
00:11:11,650 --> 00:11:15,150
Mais je vois aussi que les gens peuvent avoir une image (péjorative) des femmes noires,

151
00:11:15,150 --> 00:11:18,650
Le harcèlement sexuel est très présent et je ne m’étais pas préparée à vivre ça".

152
00:11:18,650 --> 00:11:22,550
La jeune femme évite de rentrer tard et préfère les taxis aux transports en commun.

153
00:11:22,550 --> 00:11:24,550
Mais la violence ne s'arrête pas à la rue.

154
00:11:24,550 --> 00:11:30,750
Pour sa transition, Cléo doit prendre un traitement hormonal, ce qui la met face à l'agressivité de certains médecins tunisiens.

155
00:11:31,400 --> 00:11:35,650
“Quand ils n’étaient pas moralisateurs, ils étaient très dédaigneux, voire violents dans leurs propos.

156
00:11:35,650 --> 00:11:39,400
L’un d’eux m’a dit d’aller me faire soigner et m’a traitée de folle… enfin de fou”.

157
00:11:41,400 --> 00:11:46,150
D’autres acceptent de lui prescrire les médicaments mais ne veulent pas la suivre et lui disent de se débrouiller,

158
00:11:46,700 --> 00:11:50,800
“C'est assez dangereux lorsqu’on prend un tel traitement. Ils ne prenaient même pas la peine d’adapter les dosages!”

159
00:11:51,800 --> 00:11:55,950
Avec tous ces problèmes, l’idée d’une demande d’asile lui est suggérée par une amie tunisienne.

160
00:11:55,950 --> 00:11:59,000
“Elle avait simplement entendu parler d’une procédure pour les personnes comme moi.

161
00:11:59,000 --> 00:12:02,650
Au début, on ne savait pas du tout qui contacter, ni comment ça se passait.

162
00:12:02,650 --> 00:12:07,400
Grâce aux informations trouvées sur Internet, j’ai fini par faire une demande de visa touristique pour la France

163
00:12:07,400 --> 00:12:10,350
en espérant qu’une fois sur place, je pourrais demander l’asile”.

164
00:12:10,350 --> 00:12:12,350
Mais le visa lui est refusé.

165
00:12:12,750 --> 00:12:15,250
Cléo reste donc en Tunisie et poursuit ses recherches.

166
00:12:15,850 --> 00:12:20,500
Elle finit par obtenir le contact de la “Maison du Droit des Migrations”, dans le courant de l’année 2013.

167
00:12:21,150 --> 00:12:24,150
Cette association l’aide à établir un dossier auprès du HCR

168
00:12:24,150 --> 00:12:26,150
(Haut-Commissariat aux Réfugiés des Nations Unies),

169
00:12:26,650 --> 00:12:30,300
seule organisation apte à fournir un statut de demandeur d'asile en Tunisie.

170
00:12:30,800 --> 00:12:32,300
Elle l'obtient en 2015.

171
00:12:38,300 --> 00:12:45,650
En effet, bien que la Tunisie ait ratifié la Convention générale du 28 juillet 1951
relative au statut des réfugiés,

172
00:12:45,650 --> 00:12:48,400
impliquant la délivrance de titre de séjour aux réfugiés

173
00:12:48,400 --> 00:12:51,750
et la reconnaissance de leurs droits au travail, à la santé et à l’éducation,

174
00:12:52,150 --> 00:12:54,000
il n’existe pas de loi protectrice,

175
00:12:54,000 --> 00:12:56,500
ni d’instance nationale pour étudier les demandes d’asile.

176
00:12:57,000 --> 00:12:59,500
En attendant la naissance d’une telle institution,

177
00:12:59,500 --> 00:13:02,900
le HCR, en partenariat avec le Croissant Rouge tunisien,

178
00:13:02,900 --> 00:13:06,850
étudie les demandes et donne le statut de réfugié selon le droit international,

179
00:13:06,850 --> 00:13:09,550
ce qui assure un minimum de protection à ces demandeurs d’asile.

180
00:13:15,000 --> 00:13:16,950
De nouvelles perspectives

181
00:13:34,700 --> 00:13:37,050
Depuis l’obtention de ce statut, Cléo respire.

182
00:13:37,250 --> 00:13:39,050
“La pression est redescendue”,

183
00:13:39,050 --> 00:13:43,100
Pour elle, être considérée comme une réfugiée constitue “la promesse d’un avenir”.

184
00:13:44,200 --> 00:13:47,750
“Je reçois une aide financière du HCR, qui me permet de vivre décemment.

185
00:13:47,750 --> 00:13:51,500
Ils me donnent des lots pharmaceutiques, des couvertures, de la nourriture…”

186
00:13:52,550 --> 00:13:56,000
Pour Cléo, ce nouveau statut lui apporte de “la stabilité”.

187
00:13:56,000 --> 00:13:58,000
Elle n’est plus en situation irrégulière

188
00:13:58,000 --> 00:14:01,850
et ses pénalités, qui s’élevaient à 1500 dinars, ont pu être annulées.

189
00:14:02,250 --> 00:14:04,750
La jeune femme est également suivie par une psychologue

190
00:14:04,750 --> 00:14:07,750
qui l’aide à dépasser le traumatisme des persécutions qu’elle a subies.

191
00:14:09,750 --> 00:14:13,650
“Je sens que j'avance, même s’il y a certaines zones sombres que je n’arrive toujours pas à évoquer”

192
00:14:15,950 --> 00:14:21,100
Cette nouvelle stabilité a permis à Cléo d’ouvrir son propre atelier de couture pendant l'été 2015,

193
00:14:21,100 --> 00:14:23,100
un projet qui lui tient à coeur depuis longtemps.

194
00:14:25,100 --> 00:14:27,100
Mais les débuts sont difficiles.

195
00:14:27,100 --> 00:14:32,000
“Pendant les deux premiers mois, j’ai été exploitée par une dame qui me payait 50 dinars pour des robes de mariée,

196
00:14:32,000 --> 00:14:35,500
je n’avais aucun moyen de subsistance et c’était le seul travail que l’on me proposait”.

197
00:14:37,050 --> 00:14:42,050
Elle finit par arrêter, sur les conseils d'associations qui se mettent à soutenir financièrement son projet.

198
00:14:42,550 --> 00:14:46,300
“A présent, je réalise des robes en partenariat avec trois maisons de couture tunisiennes,

199
00:14:46,300 --> 00:14:48,000
et j’ai aussi organisé un défilé.

200
00:14:48,000 --> 00:14:50,000
J’espère bientôt me faire connaître dans le milieu”.

201
00:14:52,000 --> 00:14:53,750
Pour l’instant, elle accepte toutes les commandes

202
00:14:53,750 --> 00:14:57,250
mais compte se spécialiser avec une formation, dans la haute couture et la broderie.

203
00:15:02,450 --> 00:15:04,500
"Parfois, je regrette un peu"

204
00:15:22,100 --> 00:15:25,450
Cléo avoue qu’elle n’aurait jamais imaginé suivre un tel parcours.

205
00:15:25,450 --> 00:15:27,450
“Quand j’y réfléchis, je me dis que j’avais un avenir tout tracé,

206
00:15:27,450 --> 00:15:31,700
et que si je n’avais pas milité, rien de tout cela ne serait arrivé”.

207
00:15:32,800 --> 00:15:37,300
Avec le recul, la jeune femme porte un regard assez critique sur son passé de militante.

208
00:15:38,000 --> 00:15:41,450
Selon elle, son combat n’a pas eu d’impact sur les droits LGBT au Bénin.

209
00:15:41,450 --> 00:15:46,750
“Il aurait pu en avoir si le fait que des personnes transgenres se fassent déshabiller en public gênait déjà les gens.

210
00:15:46,750 --> 00:15:49,800
Aujourd’hui, ce n’est pas le cas. et personne ne parle de ça”.

211
00:15:53,050 --> 00:16:00,300
L’ancienne militante est donc plutôt pessimiste quant à l’amélioration des droits des personnes transgenres
en Afrique de l’Ouest, et sur le continent en général.

212
00:16:01,150 --> 00:16:04,350
En Tunisie, la situation des personnes LGBT lui semble meilleure qu’au Bénin,

213
00:16:04,350 --> 00:16:07,700
même si cela tient surtout au fait que le sujet soit très occulté dans la société.

214
00:16:07,700 --> 00:16:09,700
“Cela est déjà mieux que des humiliations publiques.

215
00:16:09,700 --> 00:16:12,600
Le fait d’être caché est au final une sorte de protection”

216
00:16:13,950 --> 00:16:18,250
Elle ajoute que même si le débat concernant les droits des homosexuels commence à se développer,

217
00:16:18,250 --> 00:16:23,900
les personnes transgenres sont encore trop souvent marginalisées et oubliées
dans la lutte pour les droits des personnes LGBT,

218
00:16:24,600 --> 00:16:26,600
que ce soit en Occident ou en Afrique,

219
00:16:26,600 --> 00:16:30,200
il faudra du temps pour qu’elles soient réellement prises en compte et acceptées dans la société.

220
00:16:33,700 --> 00:16:37,200
"Aujourd’hui, je suis fatiguée, j’ai envie de me poser, d’avoir une vie tranquille.

221
00:16:37,200 --> 00:16:39,200
Mais si je dois un jour recommencer à militer,

222
00:16:39,200 --> 00:16:41,550
je me focaliserai sur les droits des personnes transgenres

223
00:16:41,550 --> 00:16:43,550
et non sur la communauté dans son ensemble".

224
00:16:45,100 --> 00:16:47,550
Sur sa situation personnelle, Cléo est amère.

225
00:16:48,950 --> 00:16:52,950
“Je pense que mon combat m’a énormément coûté et parfois, je le regrette un peu.

226
00:16:52,950 --> 00:16:57,200
Ma famille est divisée à cause de ça, et ma mère reçoit encore des menaces

227
00:16:57,200 --> 00:17:00,800
Un de mes oncles a même menacé de la brûler si elle ne lui avouait pas où j’étais”.

228
00:17:03,300 --> 00:17:08,800
La jeune femme se sent coupable de voir sa mère continuer à subir des pressions,
mais elle ne voit pas comment remédier à la situation.

229
00:17:09,450 --> 00:17:14,150
Elle a déjà pensé à la faire venir s’installer en Tunisie, mais cette possibilité a vite été écartée.

230
00:17:14,600 --> 00:17:16,150
"Toute sa vie est là-bas”

231
00:17:16,150 --> 00:17:18,150
“À 58 ans, avec sa santé fragile,

232
00:17:18,150 --> 00:17:20,150
elle n’a pas la force de tout recommencer.

233
00:17:20,700 --> 00:17:25,000
Parfois, je me dis que si je n’étais pas ce que je suis, la vie serait beaucoup plus simple”.

234
00:17:27,000 --> 00:17:29,000
Cléo marque une pause avant de reprendre.

235
00:17:29,000 --> 00:17:32,550
Elle tient à ajouter que malgré tout, son combat n’était pas “une bêtise”.

236
00:17:32,850 --> 00:17:35,150
"J’avais besoin que tout cela sorte et c’est sorti.

237
00:17:35,150 --> 00:17:38,350
Mais si c’était à refaire, avec tout ce que je sais aujourd’hui…

238
00:17:38,750 --> 00:17:40,600
je pense que je ne le referais pas.

239
00:17:40,600 --> 00:17:42,600
A cause de ça, je ne peux plus revenir chez moi".

240
00:17:44,600 --> 00:17:48,500
Le seul moyen de retourner dans son pays d’origine serait d’être protégée par une autre nationalité,

241
00:17:48,500 --> 00:17:50,500
mais elle ne se fait pas d’illusions

242
00:17:50,950 --> 00:17:54,300
"lorsque tu fuis ton pays, tu ne peux pas y retourner”.

243
00:17:56,750 --> 00:17:59,950
Enfin, vis-à-vis de sa famille qui l’a rejetée et menacée,

244
00:17:59,950 --> 00:18:02,950
Cléo déclare calmement qu’elle ne leur en veut pas plus que ça.

245
00:18:03,650 --> 00:18:05,550
“Ils font leur vie, je fais la mienne.

246
00:18:05,550 --> 00:18:08,900
J’ai d’ailleurs pris le nom de ma grand-mère maternelle pour réellement couper les ponts.

247
00:18:08,900 --> 00:18:10,900
On a simplement pris des chemins différents”

248
00:18:12,900 --> 00:18:15,800
La jeune femme ne connaît pas encore vraiment ses projets sur le long terme.*

249
00:18:16,150 --> 00:18:19,650
le HCR considère que la Tunisie ne constitue pas un pays stable pour elle

250
00:18:19,650 --> 00:18:22,350
car elle pourrait subir “des discriminations raciales ou sexuelles”.

251
00:18:22,350 --> 00:18:25,350
Cléo est donc inscrite dans une procédure de réinstallation,

252
00:18:25,350 --> 00:18:28,400
et elle vient tout juste d’apprendre qu’elle devrait partir pour l’Europe très prochainement.

253
00:18:29,000 --> 00:18:31,500
Elle ne sait pas encore où elle s’établira précisément,

254
00:18:31,500 --> 00:18:33,500
mais elle devrait bénéficier d’une carte de séjour,

255
00:18:33,500 --> 00:18:35,500
d’un logement et d’une protection.

256
00:18:35,500 --> 00:18:37,500
Une nouvelle promesse d’avenir.

257
00:18:42,300 --> 00:18:48,850
Retrouvez l'intégralité de l'article sur inkyfada.com

258
00:18:51,100 --> 00:18:53,450
Article et voix : Haïfa Mzalouat

259
00:18:53,450 --> 00:18:56,450
Enregistrement : Haïfa Mzalouat
