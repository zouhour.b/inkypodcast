﻿1
00:00:02,880 --> 00:00:06,220
J'ai reçu un message dimanche après-midi, qui disait :

2
00:00:06,220 --> 00:00:11,730
"Écoute Heythem, une des personnes qu'on a vues hier a eu de la fièvre.

3
00:00:11,730 --> 00:00:16,750
Les autorités sanitaires l'ont testée, le résultat sortira d’ici mercredi."

4
00:00:46,130 --> 00:00:50,090
Alors que des millions de personnes autour du monde vivent en confinement,

5
00:00:50,090 --> 00:00:53,330
il est normal de ressentir de l’inquiétude et de la peur devant l’inconnu.

6
00:00:54,090 --> 00:00:57,420
Quelques jours avant que le gouvernement n’annonce le confinement général,

7
00:00:57,420 --> 00:00:59,020
j’ai ressenti de la peur moi aussi.

8
00:00:59,020 --> 00:01:01,730
J’ai appelé Heythem, un ami.

9
00:01:01,730 --> 00:01:04,180
Cela faisait dix jours qu’il était en quarantaine,

10
00:01:04,180 --> 00:01:07,690
ayant été en contact avec une amie à lui rentrée d’Italie,

11
00:01:07,690 --> 00:01:10,180
déclarée positive au Covid-19.

12
00:01:11,110 --> 00:01:13,110
Je me suis dit : il a plus d’expérience que nous,

13
00:01:13,200 --> 00:01:14,620
peut-être que s’il partage son histoire,

14
00:01:14,620 --> 00:01:18,390
ça nous aidera à mieux comprendre et à atténuer la peur.

15
00:01:19,370 --> 00:01:22,660
Heythem accepte, aussi pour meubler sa journée.

16
00:01:22,970 --> 00:01:24,970
Il nous raconte comment il a vécu l’annonce,

17
00:01:25,370 --> 00:01:27,330
comment il passe ses journées,

18
00:01:27,330 --> 00:01:29,330
comment cette expérience l’impacte,

19
00:01:29,330 --> 00:01:33,280
dans son rapport à l’inconnu et dans ses convictions politiques et idéologiques.

20
00:01:33,370 --> 00:01:35,500
De la quarantaine au confinement,

21
00:01:35,500 --> 00:01:38,040
ou comment une expérience individuelle

22
00:01:38,040 --> 00:01:42,390
peut être l’expérience la plus collective de notre Histoire contemporaine.

23
00:01:50,040 --> 00:01:51,150
Allô ?

24
00:01:53,600 --> 00:01:57,020
Ça fait dix jours que je suis en quarantaine.

25
00:01:57,020 --> 00:02:01,550
Le contact suspect s'est fait le samedi 7 mars.

26
00:02:01,550 --> 00:02:09,690
On s'était retrouvé, une vingtaine d'ami·es, de toutes les nationalités.

27
00:02:10,620 --> 00:02:13,240
Il n'y avait aucune crainte que...

28
00:02:13,240 --> 00:02:17,910
Surtout que la personne porteuse n'avait aucun symptôme,

29
00:02:18,130 --> 00:02:20,800
rien qui aurait pu la faire douter elle-même.

30
00:02:21,730 --> 00:02:26,980
Si vous vous rappelez, début mars, le Corona commençait tout juste en Italie.

31
00:02:26,980 --> 00:02:29,380
Mi-février c'était encore surtout en Chine.

32
00:02:29,380 --> 00:02:35,420
L'inquiétude en Tunisie n'était donc pas très forte.

33
00:02:35,820 --> 00:02:38,000
Moi-même, qui suis un peu hypocondriaque,

34
00:02:38,000 --> 00:02:42,310
j'ai pu faire taire ma petite voix qui me disait qu'il y avait peut-être un risque.

35
00:02:49,730 --> 00:02:51,690
J'ai reçu un message

36
00:02:51,690 --> 00:02:53,690
dimanche après-midi,

37
00:02:53,690 --> 00:03:02,220
qui disait :
"Écoute Heythem, une des personnes qu'on a vues hier a eu de la fièvre.

38
00:03:02,220 --> 00:03:06,930
Les autorités sanitaires l'ont testée,

39
00:03:06,930 --> 00:03:09,240
le résultat sortira d’ici mercredi."

40
00:03:09,240 --> 00:03:12,580
À l'époque, le résultat du test prenait 3 jours pour sortir.

41
00:03:12,580 --> 00:03:17,020
“Donc il vaut mieux que tu ne sortes pas, que tu n'ailles pas travailler, etc."

42
00:03:17,020 --> 00:03:21,600
J'ai passé la nuit à poser des questions, dans un état de panique.

43
00:03:22,040 --> 00:03:24,750
Au début, la peur est physique.

44
00:03:24,750 --> 00:03:27,060
Les poils se hérissent, l’estomac se tord...

45
00:03:28,310 --> 00:03:36,890
Puis j'ai essayé de me rappeler si j'avais eu un contact avec le cas suspect.

46
00:03:36,890 --> 00:03:39,380
À ce moment-là, tu n'es plus sûr de rien.

47
00:03:40,750 --> 00:03:45,290
Personnellement, je m'informe sur le virus depuis décembre,

48
00:03:45,290 --> 00:03:49,200
depuis les premiers cas graves.

49
00:03:49,200 --> 00:03:52,890
Mais quand ça a commencé à nous toucher de près...

50
00:03:52,890 --> 00:03:54,620
c’était une autre histoire.

51
00:03:54,620 --> 00:03:57,690
J’ai recommencé à me documenter :

52
00:03:57,690 --> 00:04:02,350
comment ça se transmet, à partir de quel jour on est porteurs,

53
00:04:02,350 --> 00:04:05,640
est-ce que ça n'arrive qu'avec des symptômes…

54
00:04:06,980 --> 00:04:10,130
Le résultat du test est sorti lundi après-midi.

55
00:04:10,130 --> 00:04:12,130
À partir de mardi,

56
00:04:12,130 --> 00:04:16,840
le ministère de la Santé nous a appelés tous les jours.

57
00:04:16,840 --> 00:04:18,840
Une docteure m'a appelé.

58
00:04:18,840 --> 00:04:20,840
Elle a vérifié que j'étais à la soirée,

59
00:04:20,840 --> 00:04:25,240
elle m'a directement donné les mesures de quarantaine à prendre :

60
00:04:25,240 --> 00:04:27,240
je dois rester seul,

61
00:04:27,240 --> 00:04:30,490
si j'habite avec quelqu'un, on ne doit pas toucher les mêmes choses,

62
00:04:30,490 --> 00:04:35,200
nettoyer les poignées et tout ce qui est en commun,

63
00:04:37,200 --> 00:04:39,200
utiliser du savon, de la Javel,

64
00:04:39,200 --> 00:04:42,260
nettoyer la maison, laver les vêtements...

65
00:04:42,260 --> 00:04:45,330
Les mesures qu'on voit dans le spot publicitaire à la télé.

66
00:04:51,640 --> 00:04:58,800
C'est le ou la même médecin qui s'occupe de toi pendant ta quarantaine.

67
00:04:58,800 --> 00:05:01,510
C'est le 11ème jour aujourd'hui,

68
00:05:02,800 --> 00:05:04,620
- c'est passé tellement vite !-

69
00:05:04,620 --> 00:05:06,620
et c'est la même docteure qui m'appelle

70
00:05:06,620 --> 00:05:10,130
pour faire avec moi le suivi de ma température.

71
00:05:10,130 --> 00:05:13,420
Les premiers jours, elle m'a demandé de prendre un thermomètre

72
00:05:13,420 --> 00:05:18,000
et de prendre ma température au réveil et à midi.

73
00:05:18,000 --> 00:05:23,290
Bon finalement, les deux prises se sont combinées, vu que je me réveille vers 11h.

74
00:05:25,600 --> 00:05:28,400
Elle m'appelle vers 14h.

75
00:05:28,400 --> 00:05:33,600
Elle est super gentille, on est devenus ami·es d'ailleurs.

76
00:05:33,600 --> 00:05:37,060
On papote un peu.

77
00:05:37,060 --> 00:05:40,000
Aujourd'hui elle m'a appelé,

78
00:05:40,090 --> 00:05:42,840
nos échanges se sont raccourcis depuis le temps.

79
00:05:42,840 --> 00:05:46,440
Elle me demande si je vais bien, je lui réponds et c'est tout.

80
00:05:46,750 --> 00:05:52,530
C'est devenu une routine,

81
00:05:52,530 --> 00:05:55,510
je prends ma température au réveil

82
00:05:55,510 --> 00:06:00,310
et le ménage est devenu un passe-temps.

83
00:06:06,310 --> 00:06:08,840
J'habite en colocation, on est deux à la maison.

84
00:06:10,350 --> 00:06:14,440
Il était avec moi ce soir-là, on s'est donc retrouvés ensemble en auto confinement.

85
00:06:15,330 --> 00:06:20,620
Il y a un seau avec de l'eau de javel et du détergent dans les toilettes,

86
00:06:20,620 --> 00:06:24,580
on fait attention à bien nettoyer avec après chaque passage tout ce qui a été touché,

87
00:06:24,580 --> 00:06:30,440
les poignées, la lunettes des wc, le lavabo...

88
00:06:32,710 --> 00:06:36,750
Pour la nourriture, on cuisine ensemble

89
00:06:36,750 --> 00:06:46,090
mais on utilise le gel hydroalcoolique pour toucher la vaisselle,

90
00:06:46,090 --> 00:06:50,530
qu'on lave à la javel aussi.

91
00:06:50,530 --> 00:06:53,380
On est vraiment privilégiés,

92
00:06:53,380 --> 00:06:57,820
parce qu'on a des amis qui habitent au 2ème étage du même immeuble.

93
00:06:57,820 --> 00:07:01,690
Dès que la quarantaine a commencé,

94
00:07:01,690 --> 00:07:07,020
ils se sont chargé de nous faire les courses.

95
00:07:07,020 --> 00:07:13,380
Je fais un virement et ils me ramènent les courses devant la porte.

96
00:07:13,380 --> 00:07:17,330
On se fiche d’eux en leur courant derrière, de loin bien sûr.

97
00:07:18,040 --> 00:07:23,240
Il y a un petit débarras dans l'appartement qu'on n'utilise pas.

98
00:07:23,240 --> 00:07:28,040
Donc pour nos poubelles, on utilise les gros sacs,

99
00:07:28,040 --> 00:07:30,890
on en met deux comme dit dans les mesures de quarantaine

100
00:07:30,890 --> 00:07:35,110
et on les laisse dans le débarras.

101
00:07:35,110 --> 00:07:38,750
En ne sortant pas les poubelles chaque jour

102
00:07:38,750 --> 00:07:43,780
on a mesuré notre production de déchets,

103
00:07:43,780 --> 00:07:47,550
et du coup on essaye d'en produire le minimum.

104
00:07:57,550 --> 00:08:02,930
Au début, l'horloge biologique suivait mon rythme habituel.

105
00:08:02,930 --> 00:08:05,950
Je me réveillais vers 7h, 8h.

106
00:08:06,310 --> 00:08:12,490
Après une semaine, elle a compris que je ne bougeais pas

107
00:08:12,490 --> 00:08:15,020
et maintenant, je veille tard et me réveille tard aussi.

108
00:08:17,020 --> 00:08:20,260
Je me réveille, je prends ma température,

109
00:08:20,620 --> 00:08:23,420
je fais ma toilette, prends mon café,

110
00:08:23,420 --> 00:08:25,420
je fume une clope au balcon.

111
00:08:25,640 --> 00:08:31,460
J'ai passé la première semaine à consulter les articles au sujet du Corona.

112
00:08:33,690 --> 00:08:38,310
Le week-end, je m'ennuyais et j'ai dévié vers les théories du complot.

113
00:08:38,310 --> 00:08:45,420
Finalement, elles étaient nulles donc j'ai laissé tomber.

114
00:08:46,000 --> 00:08:51,780
Je monte sur le toit prendre un peu de soleil aussi.

115
00:08:51,780 --> 00:08:56,180
Le reste se passe en ligne.

116
00:09:00,800 --> 00:09:05,240
J'ai senti que j’étais plus fatigué en confinement que quand je bosse.

117
00:09:05,240 --> 00:09:11,110
Le travail est basé sur des tâches, c'est assez chiant.

118
00:09:12,400 --> 00:09:15,780
J'imagine que ton boulot est chiant aussi !

119
00:09:17,020 --> 00:09:19,420
Ce n’est pas une question d'importance,

120
00:09:19,420 --> 00:09:22,040
mais plutôt comment ton travail réussit à te garder occupé·e.

121
00:09:24,660 --> 00:09:30,750
Là, si tu termines ta tâche, tu n'as plus rien à faire.

122
00:09:30,980 --> 00:09:33,150
Alors que maintenant, j'ai plein de tâches :

123
00:09:33,150 --> 00:09:37,510
nettoyer, prendre du soleil, faire de l'étirement,

124
00:09:37,510 --> 00:09:42,930
lire 7000 articles, voir 8000 vidéos,

125
00:09:42,930 --> 00:09:45,860
faire un podcast avec toi...

126
00:09:45,860 --> 00:09:49,640
Je suis tout le temps en train de faire quelque chose.

127
00:10:02,440 --> 00:10:05,200
Je n'ai rien dit à ma famille.

128
00:10:05,200 --> 00:10:08,400
Je ne voulais pas les inquiéter.

129
00:10:08,400 --> 00:10:21,110
Je leur ai raconté qu'on avait commencé le télétravail dès le premier cas à Tunis.

130
00:10:21,110 --> 00:10:24,040
Ma mère est très responsable.

131
00:10:24,040 --> 00:10:26,040
Elle ne sort pas de la maison,

132
00:10:26,040 --> 00:10:30,840
elle ne fait les courses qu'une fois,

133
00:10:30,840 --> 00:10:32,840
elle fait attention et suit les mesures.

134
00:10:32,840 --> 00:10:37,020
Je ne sais pas trop pour mon père, mais je pense qu'il fait attention.

135
00:10:47,460 --> 00:10:55,690
Une fois, j'ai décidé de passer la serpillière dans la cuisine alors qu'elle était déjà propre.

136
00:10:55,690 --> 00:11:00,800
J'ai vidé un seau d'eau javellisée et j'ai commencé à nettoyer.

137
00:11:00,800 --> 00:11:02,800
L'eau de javel m'a étourdi

138
00:11:02,800 --> 00:11:11,330
et l'effort du ménage était comme du sport en salle vu que je ne fais rien de la journée.

139
00:11:11,330 --> 00:11:15,060
J'ai senti une bouffée de chaleur.

140
00:11:15,060 --> 00:11:19,150
Je me sentais fatigué, je suis allé dans ma chambre, j'ai pris ma température.

141
00:11:19,150 --> 00:11:28,400
Les thermomètres en général indiquent 0.5°C degré de moins que la réalité.

142
00:11:28,400 --> 00:11:33,600
D'habitude, il m'indique 36.3, 36.5.

143
00:11:33,600 --> 00:11:36,980
Quand j'ai pris ma température ce jour-là, ça m’a indiqué 37.

144
00:11:36,980 --> 00:11:39,950
J'ai paniqué et la panique crée de la chaleur en plus,

145
00:11:39,950 --> 00:11:41,950
du coup je me suis senti encore plus fiévreux.

146
00:11:41,950 --> 00:11:48,130
Je me suis allongé, j'ai éteint la lumière et mon téléphone,

147
00:11:48,130 --> 00:11:52,040
j'ai essayé de me calmer pendant 30 minutes.

148
00:11:52,040 --> 00:11:56,840
Puis ça allait mieux.

149
00:11:56,840 --> 00:12:00,580
Le soir même, alors que je commençais à m’endormir,

150
00:12:00,580 --> 00:12:08,750
j'ai senti que je n'arrivais plus à respirer comme d'habitude.

151
00:12:09,150 --> 00:12:16,090
La nuit, j'évite de prendre ma température pour ne pas avoir peur,

152
00:12:16,090 --> 00:12:20,800
parce que personne ne pourra venir m'aider.

153
00:12:20,800 --> 00:12:23,640
Je n'ai pas pris ma température,

154
00:12:23,640 --> 00:12:26,490
mais je me suis rappelé que j'avais des allergies

155
00:12:26,660 --> 00:12:30,490
qui me causent des troubles respiratoires parfois.

156
00:12:30,490 --> 00:12:33,380
J’ai pris un comprimé et je me suis senti mieux.

157
00:12:48,350 --> 00:12:51,240
J’ai senti que mon cas n’était pas urgent,

158
00:12:51,240 --> 00:12:55,820
surtout avec la grande charge qui pèse sur les médecins en ce moment,

159
00:12:55,820 --> 00:12:59,950
et je ne voulais pas en rajouter,

160
00:12:59,950 --> 00:13:09,020
Il faut appeler le 190 si la température dépasse 38.5°C,

161
00:13:09,020 --> 00:13:11,780
s'il y a une toux sèche,

162
00:13:11,780 --> 00:13:16,580
des maux de gorge et des difficultés à respirer.

163
00:13:22,930 --> 00:13:27,110
Si ça avait empiré j'aurais pu appeler.

164
00:13:31,110 --> 00:13:34,220
Aucune conférence de presse sur les cas tunisiens ne m'a fait peur.

165
00:13:34,220 --> 00:13:36,220
Ce qui me fait peur, c'est l'inconnu.

166
00:13:36,220 --> 00:13:42,710
Le président de l'OMS lui-même a peur de l'inconnu, c'est pour dire.

167
00:13:42,710 --> 00:13:47,150
L'inconnu est présent aussi dans les politiques de l'État.

168
00:13:47,780 --> 00:13:52,580
Il y a beaucoup de mesures prises par l'État que je ne comprends pas.

169
00:13:52,580 --> 00:13:55,690
Et quand je ne comprends pas, j’ai peur.

170
00:13:55,950 --> 00:14:01,550
D'habitude je considère que la politique est faite pour contrôler les individus,

171
00:14:01,550 --> 00:14:09,330
donc en imposant le couvre-feu, tout le monde devient plus vigilant.

172
00:14:09,330 --> 00:14:16,090
Ça crée cette angoisse qui fait que les gens comprennent qu'il y a une épidémie.

173
00:14:16,750 --> 00:14:23,020
Je ne suis pas pour que l'État manipule les citoyen·nes, ni physiquement ni psychologiquement.

174
00:14:23,020 --> 00:14:28,580
Mais là finalement, ce n'est pas qu'une question d'État mais de communauté,

175
00:14:28,580 --> 00:14:32,220
même si les décisions reviennent à l'État.

176
00:14:32,220 --> 00:14:36,890
Et puis on est face à un virus, ce n'est pas une guerre ou le terrorisme.

177
00:14:37,600 --> 00:14:41,910
Il est encore tôt pour le pic.

178
00:14:41,910 --> 00:14:44,220
La semaine prochaine sera assez critique.

179
00:14:44,750 --> 00:14:47,110
On ne fait pas assez de tests

180
00:14:47,110 --> 00:14:54,930
C'est vrai que la modélisation et la technologie facilite les choses

181
00:14:54,930 --> 00:14:58,980
mais on ne peut pas prévoir comment se comporte le virus.

182
00:14:58,980 --> 00:15:04,260
Une seule personne, dans un coin isolé du monde peut tout chambouler.

183
00:15:04,260 --> 00:15:10,490
C’est ça qui me fait peur, sinon je me sens plutôt bien.

184
00:15:23,200 --> 00:15:26,530
Honnêtement, je profite bien de la quarantaine.

185
00:15:27,640 --> 00:15:29,910
Je n’ai rien à faire dehors.

186
00:15:29,910 --> 00:15:40,130
La pandémie est tellement puissante qu’elle a enlevé toute envie de sortir.

187
00:15:40,130 --> 00:15:45,020
Hier je disais à mon coloc’, à deux mètres de distance bien sûr,

188
00:15:45,020 --> 00:15:50,310
que le virus nous a manipulé·es psychologiquement.

189
00:15:50,310 --> 00:15:53,600
Pleins de besoins n’existent plus,

190
00:15:53,600 --> 00:15:56,930
surtout le besoin social, de sortir, de voir des gens…

191
00:15:56,930 --> 00:16:02,180
Ces lieux habituels ne me manquent pas.

192
00:16:02,180 --> 00:16:10,350
L’auto-confinement et la pandémie me rappellent ma période étudiante,

193
00:16:10,350 --> 00:16:12,090
pendant les révisions.

194
00:16:12,090 --> 00:16:16,220
On est tellement confiné·es et focalisé·es sur les études

195
00:16:16,220 --> 00:16:19,420
qu’on se dit ensuite qu’on sera plus efficace,

196
00:16:22,440 --> 00:16:27,950
qu’on révisera tous les jours...

197
00:16:27,950 --> 00:16:32,530
ça rappelle le réflexe qu’on a de se promettre
de donner aux pauvres si on réussit son année.

198
00:16:32,890 --> 00:16:39,780
On se dit qu’on va devenir une personne meilleure

199
00:16:39,780 --> 00:16:41,780
mais au final, pas vraiment.

200
00:16:41,780 --> 00:16:43,780
Espérons que cela va nous changer.

201
00:17:03,780 --> 00:17:07,510
Quelques jours après l'enregistrement, l'état annonce le confinement général.

202
00:17:07,510 --> 00:17:11,910
Bureaux fermés, équipe confinée, Inkyfada se retrouve aussi en télétravail,

203
00:17:11,910 --> 00:17:18,480
à revoir, comme beaucoup d'autre, à revoir ses méthodes de travail avec l’évolution de l’épidémie du Covid-19 en Tunisie.

204
00:17:18,480 --> 00:17:24,620
Comment enregistrer, réaliser ou monter à distance ?

205
00:17:24,620 --> 00:17:28,040
Et aussi, de quoi parler lorsque le monde est focalisé sur ce seul sujet ?

206
00:17:28,040 --> 00:17:34,080
Il fallait s’adapter. Contacts à distance, enregistrement avec le téléphone, avec des problèmes de connexion.

207
00:17:34,080 --> 00:17:36,080
De la quarantaine au confinement

208
00:17:36,080 --> 00:17:44,480
Cet épisode bricolé est aussi une manière de raconter les histoires auxquelles beaucoup peuvent s’identifier, avec les moyens du bord.

209
00:17:45,280 --> 00:17:50,930
RÉALISATION : Bochra Triki, Hazar Abidi, Monia Ben Hamadi

210
00:17:50,930 --> 00:17:53,730
ENREGISTREMENT : Hazar Abidi

211
00:17:53,730 --> 00:17:59,020
MUSIQUE, MONTAGE, HABILLAGE SONORE ET MIXAGE : Oussema Gaidi

212
00:17:59,020 --> 00:18:02,220
VOIX OFF : Hazar Abidi

213
00:18:02,440 --> 00:18:07,460
AVEC LA COLLABORATION DE : Yassine Kawana et toute l’équipe d’Inkyfada.
