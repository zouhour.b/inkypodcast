1
00:00:00,160 --> 00:00:05,320
When I saw how the image
was shown on the phone,

2
00:00:05,320 --> 00:00:11,120
I was surprised.
The mood was somehow amplified.

3
00:00:11,120 --> 00:00:13,200
It was rather hypnotic.

4
00:00:13,200 --> 00:00:16,120
I was focused on the phone,
on what was in the phone,

5
00:00:16,120 --> 00:00:18,240
There was one thing happening,
It was Abdennaceur Laouini

6
00:00:18,240 --> 00:00:20,480
shouting in the middle
of Avenue Habib Bourguiba.

7
00:01:09,520 --> 00:01:11,560
January 14, 2011.

8
00:01:11,560 --> 00:01:15,240
The UGTT has announced a general strike,

9
00:01:15,240 --> 00:01:17,800
and called for a protest 
in Mohamed Ali Square.

10
00:01:17,800 --> 00:01:20,200
Thousands of Tunisians
have answered the call

11
00:01:20,200 --> 00:01:24,040
One of them is Imed, 43, 
and works in advertising.

12
00:01:24,040 --> 00:01:27,160
When the government
announced a curfew for 5PM,

13
00:01:27,160 --> 00:01:29,160
He found himself at his friend’s, Zein

14
00:01:29,160 --> 00:01:31,160
it was an apartment located
in Habib Bourguiba Avenue,

15
00:01:31,160 --> 00:01:32,560
on top of L’Univers Café

16
00:01:32,560 --> 00:01:35,360
an emblematic café bar 
with a leftist history.

17
00:01:35,360 --> 00:01:38,200
It was night time,
calm, with no noises

18
00:01:38,200 --> 00:01:40,880
not in streets,
not in houses.

19
00:01:40,880 --> 00:01:44,040
Imed heard some shouting
that broke the silence.

20
00:01:44,040 --> 00:01:45,560
Abdennaceur Laouini,

21
00:01:45,560 --> 00:01:47,920
a well known lawyer
and a political activist,

22
00:01:47,920 --> 00:01:49,520
shouting from his guts:

23
00:01:49,520 --> 00:01:52,240
“Ben Ali has escaped.”

24
00:01:52,240 --> 00:01:55,280
Imed comes down, and joins him,

25
00:01:55,280 --> 00:01:59,360
during the curfew,
in an avenue marked with police presence.

26
00:01:59,360 --> 00:02:02,080
He gets his phone out
and records a video

27
00:02:02,080 --> 00:02:04,440
that would later be inscribed
in the Tunisian collective Memory.

28
00:02:04,440 --> 00:02:08,000
It would become a symbol,
of peoples’ victory over dictatorship.

29
00:02:08,000 --> 00:02:09,640
For a whole year,

30
00:02:09,640 --> 00:02:12,480
And while waiting for
the revolution’s 10th Anniversary,

31
00:02:12,480 --> 00:02:15,280
Inkyfada will tell the stories 
of some of the thousands of people 

32
00:02:15,280 --> 00:02:17,640
who lived that historical day.

33
00:02:19,560 --> 00:02:22,240
Where were you on January 14?

34
00:02:22,240 --> 00:02:26,160
Épisode 1 : Ben Ali Hrab

35
00:02:31,960 --> 00:02:34,280
January 14.

36
00:02:34,280 --> 00:02:37,120
It was a Friday, 
but it felt more like a Sunday.

37
00:02:37,120 --> 00:02:39,120
There was something special

38
00:02:39,120 --> 00:02:41,280
with the call for the General Strike.

39
00:02:41,280 --> 00:02:43,760
I woke up, got dressed,
then went out looking for a taxi.

40
00:02:43,760 --> 00:02:45,760
There wasn’t any,
So I waited in the street.

41
00:02:45,760 --> 00:02:49,200
In the meantime I saw someone
also waiting.

42
00:02:49,200 --> 00:02:50,520
“Are you waiting for taxi?”

43
00:02:50,520 --> 00:02:52,960
“If one comes,
shall we share it?"

44
00:02:52,960 --> 00:02:54,000
He said yes.

45
00:02:54,000 --> 00:02:55,120
I asked him where he was going,

46
00:02:55,120 --> 00:02:56,640
but he didn’t want to answer.

47
00:02:56,640 --> 00:02:58,640
I sensed his fear.

48
00:02:58,640 --> 00:03:02,360
This tells you about what Tunisians
were living back in the days.

49
00:03:02,360 --> 00:03:04,600
They were living in fear.

50
00:03:18,040 --> 00:03:19,040
We got out of the taxi.

51
00:03:19,040 --> 00:03:22,920
There was something different that time.

52
00:03:22,920 --> 00:03:25,760
When you look around you,

53
00:03:25,760 --> 00:03:27,920
there were a lot of unfamiliar faces.

54
00:03:27,920 --> 00:03:32,160
But they weren’t infiltrated cops, 
like usual.

55
00:03:32,160 --> 00:03:39,880
There was a cool atmosphere,
as if you were in a family park.

56
00:03:41,080 --> 00:03:43,840
I found Zein in the crowd,

57
00:03:43,840 --> 00:03:46,200
while I was looking for water, I was thirsty.

58
00:03:46,200 --> 00:03:52,120
It had been three hours
since I was there.

59
00:03:53,360 --> 00:03:57,280
Zein is an old friend of mine.

60
00:03:57,280 --> 00:03:59,680
We’ve known each other for a long time.

61
00:03:59,680 --> 00:04:02,960
He lives in Tunis down town,
above l’Univers Café.

62
00:04:02,960 --> 00:04:04,960
We will go back to this later.

63
00:04:04,960 --> 00:04:07,440
The goal was to find
a bottle of water.

64
00:04:07,440 --> 00:04:09,160
Everything was closed.

65
00:04:09,160 --> 00:04:13,480
Except "Bouabenna",
a restaurant in Rue de Marseille.

66
00:04:13,480 --> 00:04:14,760
We went there.

67
00:04:14,760 --> 00:04:18,600
And since there was no water,
we had a beer.

68
00:04:18,600 --> 00:04:21,320
It does the work.

69
00:04:21,320 --> 00:04:26,080
There was nothing to eat,
so the manager served us his own pasta.

70
00:04:27,280 --> 00:04:29,720
So, we had our beers, 
then we went down.

71
00:04:37,800 --> 00:04:39,800
When we got out of the bar,
there was chaos.

72
00:04:39,800 --> 00:04:43,960
So the goal was to go to the apartment,
above l’Univers Café.

73
00:04:43,960 --> 00:04:46,520
We had to make a detour.

74
00:04:46,520 --> 00:04:49,000
Because the situation was chaotic
in Avenue Habib Bourguiba.

75
00:04:49,000 --> 00:04:56,120
To walk for 200 meters,
between Rue de Marseille and L’Univers,

76
00:04:56,120 --> 00:04:57,320
was impassible.

77
00:04:57,320 --> 00:05:02,080
one had to make detours and hide,

78
00:05:02,080 --> 00:05:06,240
to avoid the cops
walking by with their batons.

79
00:05:06,240 --> 00:05:09,560
The whole thing was crazy.

80
00:05:09,560 --> 00:05:11,960
You feel that you are at war.

81
00:05:26,600 --> 00:05:29,120
It was chaos wrapped in white smoke
with tear-gas bombs everywhere,

82
00:05:29,120 --> 00:05:33,080
people were running in all directions,
and the crowd was dispersing.

83
00:05:33,080 --> 00:05:35,680
Everything was white, wrapped in smoke.

84
00:05:35,680 --> 00:05:40,320
And in through this fog,
you’d see silhouettes

85
00:05:40,320 --> 00:05:41,680
but you take time to distinguish them

86
00:05:41,680 --> 00:05:43,680
whether they are people
or whether they are wearing helmets.

87
00:05:48,880 --> 00:05:53,480
So we walked for a long distance
to finally arrive at Zein’s place.

88
00:05:53,480 --> 00:05:56,160
The TV was on.

89
00:05:56,160 --> 00:05:58,640
They have announced a curfew at 5 PM.

90
00:05:58,640 --> 00:06:01,800
So we were obliged to remain where we were.

91
00:06:02,760 --> 00:06:04,000
There was a ban on going out.

92
00:06:04,000 --> 00:06:08,040
Everything was empty.
Only cops and the military were there.

93
00:06:08,040 --> 00:06:11,440
So I remained at Zein’s, with Houria, and Jalel

94
00:06:11,440 --> 00:06:13,080
on Habib Bourguiba Avenue.

95
00:06:13,080 --> 00:06:18,400
We had an overview on the street,

96
00:06:18,400 --> 00:06:21,800
with the cops, the tear-gas bombs…

97
00:06:21,800 --> 00:06:25,400
There was almost no one there.
Everybody disappeared.

98
00:06:25,400 --> 00:06:28,880
On TV they said that…
What was his name again?

99
00:06:28,880 --> 00:06:33,840
Ben Ali got on a plane
and went away.

100
00:06:33,840 --> 00:06:36,800
To Libya, France …
To each their version.

101
00:06:36,800 --> 00:06:40,760
“President Zine el Abidine Ben Ali
quitted presidence.

102
00:06:40,760 --> 00:06:45,240
Prime Minister Mohamed Ghannouchi
would serve as interim president.

103
00:06:45,240 --> 00:06:50,600
Ben Ali left Tunisia towards Malta,
under Libyan protection at around 5PM.

104
00:06:50,600 --> 00:06:56,440
Our source has indicated
that he might go to France ”.

105
00:06:57,480 --> 00:07:04,480
The moment was very special.

106
00:07:04,480 --> 00:07:12,240
Because there was this presence, this image,
that we lived with for 23 years.

107
00:07:12,240 --> 00:07:14,600
There was a rupture at that moment.

108
00:07:14,600 --> 00:07:18,840
The symbol of power was gone.

109
00:07:18,840 --> 00:07:25,440
I imagine everybody got afraid.

110
00:07:25,440 --> 00:07:28,360
Even those who wanted 
a change

111
00:07:28,360 --> 00:07:32,480
or wanted to take his place,

112
00:07:32,480 --> 00:07:33,880
were not expecting it .

113
00:07:33,880 --> 00:07:36,560
It came as a bit of surprise for everyone.

114
00:07:36,560 --> 00:07:45,600
Usually, in such circumstances,
people should react,

115
00:07:45,600 --> 00:07:49,200
go out to the streets, honk,
express joy.

116
00:07:49,200 --> 00:07:52,280
But there wasn’t any of this,
there was total void.

117
00:07:52,280 --> 00:07:57,280
Nothing, maybe only some cats.
And cops, of course.

118
00:07:57,280 --> 00:07:59,280
I went to the window
to smoke a cigarette.

119
00:07:59,280 --> 00:08:02,680
The cops passing by insulted me,

120
00:08:02,680 --> 00:08:05,320
and asked me to close the window.

121
00:08:05,320 --> 00:08:09,840
using their regular cop language.

122
00:08:09,840 --> 00:08:12,360
No need to go into details.

123
00:08:12,360 --> 00:08:15,680
At that moment,

124
00:08:15,680 --> 00:08:17,320
I heard someone shouting in the street.

125
00:08:19,120 --> 00:08:21,320
While Avenue Habib Bourguiba was totally empty,

126
00:08:21,320 --> 00:08:26,880
There were only some cops, at night,
not a car, not a single person.

127
00:08:26,880 --> 00:08:33,760
So you have no idea about the echo
his shouting entailed.

128
00:08:33,760 --> 00:08:36,880
While listening to him,
I started to laugh 

129
00:08:36,880 --> 00:08:38,640
before seeing what it was.

130
00:08:38,640 --> 00:08:42,200
By the way, I told Zein:
“here we go, the bootlickers are out”.

131
00:08:42,200 --> 00:08:47,560
Because they have already been out
with their rental cars,

132
00:08:47,560 --> 00:08:57,080
to do their propaganda
with their pictures of Ben Ali.

133
00:08:57,080 --> 00:08:59,080
So I hear the guy shouting:

134
00:08:59,080 --> 00:09:03,880
“Ben Ali has escaped”.

135
00:09:03,880 --> 00:09:07,960
It wasn’t very audible.
It was clear that he was drunk as well.

136
00:09:07,960 --> 00:09:10,160
I called Zein:

137
00:09:10,160 --> 00:09:14,360
“Hey, look, here’s the first one
to change sides”.

138
00:09:16,240 --> 00:09:20,800
Then when he came closer,
I recognised him, Abdennaceur Laouini.

139
00:09:20,800 --> 00:09:22,800
We have known each other since university days.

140
00:09:22,800 --> 00:09:27,960
He was a student activist with the UGET
(the leftist student union).

141
00:09:27,960 --> 00:09:34,560
I was happy to see a familiar face,
in that moment of desolation.

142
00:09:34,560 --> 00:09:40,000
He went out to express his joy
in his own way.

143
00:09:40,000 --> 00:09:45,200
So I joined him in the moment,
We hugged, we were happy.

144
00:09:45,200 --> 00:09:49,480
I was surprised myself
I wouldn’t hide it from you.

145
00:09:49,480 --> 00:09:54,040
We were really alone,
in the middle of Habib Bourguiba Avenue, empty.

146
00:09:54,040 --> 00:09:56,960
There were only cops,
and everything was dark.

147
00:09:56,960 --> 00:10:05,960
It was a unique experience.

148
00:10:05,960 --> 00:10:11,880
It made me laugh a little,
the situation felt a little absurd.

149
00:10:11,880 --> 00:10:16,920
Where are the people?

150
00:10:16,920 --> 00:10:26,960
It could have been the best night out in our history,

151
00:10:26,960 --> 00:10:30,640
Like a big concert where people fight for spots

152
00:10:30,640 --> 00:10:34,680
but at the end,
there was only one spectator.

153
00:10:34,680 --> 00:10:37,320
This is where the absurdity lies.

154
00:10:37,320 --> 00:10:42,960
At one moment, I turn back
to see Zein, and Houria, Zein’s mother

155
00:10:42,960 --> 00:10:45,960
at the entry of the building.

156
00:10:45,960 --> 00:10:48,160
“Three people went out 
to Avenue Habib Bourguiba, while empty.”

157
00:10:48,160 --> 00:10:50,480
“Look over there.”

158
00:10:50,480 --> 00:10:51,880
“The criminal has escaped”

159
00:10:51,880 --> 00:10:53,280
“Ben Ali has escaped”

160
00:10:53,280 --> 00:10:55,400
“Long live free Tunisia”

161
00:10:56,000 --> 00:10:57,960
“How courageous he is!”

162
00:11:04,280 --> 00:11:06,000
“Listen, there’s someone in the street,

163
00:11:06,000 --> 00:11:11,120
he’s happy, he’s talking about how people were tortured.

164
00:11:11,120 --> 00:11:13,320
You have no idea,

165
00:11:13,320 --> 00:11:17,640
He gave me goosbumps
without me noticing, Listen! Listen!”

166
00:11:18,800 --> 00:11:22,640
“Where are the rental cars
rented by Ben Ali yesterday?”

167
00:11:22,640 --> 00:11:27,320
“Where are the rental cars?”

168
00:11:32,960 --> 00:11:37,360
It was Houria’s idea. I don’t know
why she told me to videotape it.

169
00:11:37,360 --> 00:11:43,520
My phone does not have
a good quality camera.

170
00:11:43,520 --> 00:11:51,880
But we needed to record that moment, in video.

171
00:11:51,880 --> 00:11:54,000
“We are free. Ben Ali has escaped.

172
00:11:54,000 --> 00:11:59,920
Ben Ali has escaped.Ben Ali has escaped.
Ben Ali has escaped.

173
00:11:59,920 --> 00:12:04,280
Tunisia is without Ben Ali.
Ben Ali, the criminal, has escaped.

174
00:12:04,280 --> 00:12:10,160
The criminal was removed.
The Tunisian People removed Ben Ali.”

175
00:12:12,120 --> 00:12:17,520
When I saw how the image
was shown on the phone,

176
00:12:17,520 --> 00:12:21,000
on the screen of that Nokia,
that I was holding in my hand.

177
00:12:21,000 --> 00:12:25,600
The mood was somehow amplified.

178
00:12:25,600 --> 00:12:27,680
It was rather hypnotic.

179
00:12:27,680 --> 00:12:30,280
I was focused on the phone,
on what was in the phone,

180
00:12:30,280 --> 00:12:32,400
There was one thing happening,
It was Abdennaceur Laouini

181
00:12:32,400 --> 00:12:34,400
shouting in the middle of
Avenue Habib Bourguiba.

182
00:12:35,520 --> 00:12:38,680
By the end of the video, when he was saying
“Ben Ali has escaped,”

183
00:12:38,680 --> 00:12:41,360
I laughed, the situation made me laugh.

184
00:12:41,360 --> 00:12:44,680
But it was really brave, impressive.

185
00:12:44,680 --> 00:12:47,000
There always must be someone who should go first,

186
00:12:47,000 --> 00:12:51,240
but weirdly enough, no one went out afterwards.

187
00:12:51,240 --> 00:12:57,320
While thousands of people
were on the avenue

188
00:12:57,320 --> 00:13:01,520
and remained trapped there, like myself.

189
00:13:01,520 --> 00:13:04,520
Some slept on the roofs of some buildings,

190
00:13:04,520 --> 00:13:07,360
others hid in strangers’ houses,

191
00:13:07,360 --> 00:13:10,800
there was a lot of people

192
00:13:10,800 --> 00:13:13,360
and everybody knew that Ben Ali has escaped.

193
00:13:13,360 --> 00:13:16,040
The proof is in the second video

194
00:13:16,040 --> 00:13:26,360
of the girl trapped in a call center
with other people.

195
00:13:26,360 --> 00:13:30,480
Many people were looking from their windows,
Trapped on Avenue Habib Bourguiba,

196
00:13:30,480 --> 00:13:32,560
but they were afraid to go out.

197
00:13:35,120 --> 00:13:36,120
“Congratulations!”

198
00:13:51,000 --> 00:13:52,920
“Close all windows!”

199
00:13:52,920 --> 00:13:56,760
“He said to close all windows, we should obey.”

200
00:14:05,880 --> 00:14:10,760
After having filmed, cops arrived screaming
telling us to go home.

201
00:14:10,760 --> 00:14:16,960
Finally, Abdennaceur went home
and I went back to Zein’s.

202
00:14:16,960 --> 00:14:23,080
I had no internet that night.

203
00:14:23,080 --> 00:14:31,040
So I had to wait for the following morning
to upload it from home.

204
00:14:37,920 --> 00:14:43,320
I woke up in the morning, everything was closed.

205
00:14:43,320 --> 00:14:47,000
I was supposed to walk through Habib Bourguiba Avenue
but it was barred.

206
00:14:47,000 --> 00:14:52,680
It was absurd, a policeman told me
that I didn’t have the right to be there.

207
00:14:54,400 --> 00:14:58,760
“Why? I want to go home!”

208
00:14:58,760 --> 00:15:05,880
So I had to improvise how to go home.

209
00:15:05,880 --> 00:15:12,880
You feel like a walking target.
But everything went well ultimately.

210
00:15:23,880 --> 00:15:31,560
The goal wasn’t to do advertising
so that the video goes viral.

211
00:15:31,560 --> 00:15:35,440
It was just the video that needed to be shared.

212
00:15:35,440 --> 00:15:44,680
A couple of days later,
I discovered the magnitude of my video.

213
00:15:44,680 --> 00:15:50,200
It was continuously broadcasted
on Nessma TV, Al Jazeera, and other media

214
00:15:50,200 --> 00:15:52,800
It was unbelievable.

215
00:15:52,800 --> 00:15:58,400
They showed Rached Ghannouchi crying over it
and people reacting to it!

216
00:15:58,400 --> 00:16:00,680
I was really surprised.

217
00:16:00,680 --> 00:16:05,080
Did people like the video

218
00:16:05,080 --> 00:16:10,040
because they were afraid
to go out that night?

219
00:16:10,040 --> 00:16:14,760
What I know for sure is that
the fact that Ben Ali has escaped,

220
00:16:14,760 --> 00:16:22,160
was an occasion not to miss, to celebrate.

221
00:16:22,160 --> 00:16:26,360
It was the hidden dream of many Tunisians.

222
00:16:26,360 --> 00:16:28,640
whether they were out protesting or not.

223
00:16:28,640 --> 00:16:33,480
Everybody wanted a change
but when change happens,

224
00:16:33,480 --> 00:16:36,280
everybody hides. That’s weird.

225
00:16:36,280 --> 00:16:40,280
On January 14, the media said that

226
00:16:40,280 --> 00:16:46,840
people were out since the morning,
and that they were many more in numbers than the cops.

227
00:16:46,840 --> 00:16:51,920
I don’t know why they were afraid later,
while they shouldn’t have been so.

228
00:16:51,920 --> 00:17:01,920
They went back to their reflexes, to wait for the TV
to know what is happening.

229
00:17:01,920 --> 00:17:07,320
So that the TV tells you what’s happening.
That’s disappointing.

230
00:17:12,200 --> 00:17:20,840
During the following days,
with the absence of cops,

231
00:17:20,840 --> 00:17:25,600
people organised,

232
00:17:25,600 --> 00:17:32,320
and started to manage the situation, autonomously organising,

233
00:17:32,320 --> 00:17:38,760
to watch  tenir neighbourhoods over night

234
00:17:38,760 --> 00:17:43,200
There was a lot of solidarity, of sharing,

235
00:17:43,200 --> 00:17:54,040
just like before in traditional neighbourhoods.
People helping each other, cooking and sharing

236
00:17:54,040 --> 00:17:58,880
It was beautiful. It could have been the beginning
of something, of a movement.

237
00:17:58,880 --> 00:18:06,080
People were even checking the cops,
stopping their cars and searching them.

238
00:18:06,080 --> 00:18:11,040
But this only lasted for some days.

239
00:18:11,040 --> 00:18:17,960
Little by little, media got into peoples’ heads,

240
00:18:17,960 --> 00:18:22,320
with rumours and fake warnings,

241
00:18:22,320 --> 00:18:27,640
They wanted to scare people.
They needed the situation of solidarity to stop.

242
00:18:27,640 --> 00:18:31,040
It was needed for people to go back to their places,
to go back home, in front of their TVs

243
00:18:31,040 --> 00:18:34,200
to go back to their routine.

244
00:18:34,200 --> 00:18:38,400
They did what they needed to do.
The outcome is here.

245
00:18:38,400 --> 00:18:43,040
People that used to protest
because they couldn’t buy a piece of bread.

246
00:18:43,040 --> 00:18:46,320
now are in a worse situation

247
00:18:46,320 --> 00:18:49,760
and yet they continue to work, to keep quiet, 
confined to their homes.

248
00:19:17,160 --> 00:19:19,120
Where were you on January 14?

249
00:19:19,120 --> 00:19:21,120
Épisode 1: Ben Ali Hrab

250
00:19:21,120 --> 00:19:24,560
Production Inkyfada

251
00:19:24,560 --> 00:19:27,720
Directed, edited et mixed by:

252
00:19:27,720 --> 00:19:33,720
Monia Ben Hamadi, Hazar Abidi,
Bochra Triki, Yassine Kawana

253
00:19:33,720 --> 00:19:34,800
Recording: 

254
00:19:34,800 --> 00:19:36,400
Yassine Kawana

255
00:19:36,400 --> 00:19:38,400
Music and Sound Design:

256
00:19:38,400 --> 00:19:40,520
Oussema Gaidi

257
00:19:40,520 --> 00:19:41,760
Voix off :

258
00:19:41,760 --> 00:19:43,000
Bochra Triki

259
00:19:43,000 --> 00:19:44,640
Subtitles:

260
00:19:44,640 --> 00:19:48,800
Shams Radhouani Abdi

261
00:19:48,800 --> 00:19:50,360
Group listening and feedback:

262
00:19:50,360 --> 00:19:55,640
Yasmin Houamed, Chayma Mehdi,
Haïfa Mzalouat, Khookha McQueer

263
00:19:55,640 --> 00:19:56,880
Illustrations:

264
00:19:56,880 --> 00:19:58,600
Marwen Ben Mustapha

265
00:19:58,600 --> 00:20:02,200
In collaboration with Inkyfada team

