﻿1
00:00:02,960 --> 00:00:03,720
وصلتني رسالة

2
00:00:04,380 --> 00:00:05,720
بعد زوال يوم الأحد

3
00:00:06,160 --> 00:00:07,440
...إسمع هيثم"

4
00:00:07,440 --> 00:00:11,760
واحد/ة ممن التقيناهم/ن أمس تـ/يشكو من الحمى

5
00:00:11,760 --> 00:00:14,260
لقد أجرت له/ـا السلطات الصحية  اختبارا

6
00:00:14,260 --> 00:00:16,680
".تكون نتيجته جاهزة يوم الاربعاء

7
00:00:46,140 --> 00:00:49,320
،في حين يقبع الملايين حول العالم في الحجر الصحي

8
00:00:50,020 --> 00:00:52,820
.طبيعي هو الإحساس بالخوف والعجز أمام المجهول

9
00:00:54,200 --> 00:00:56,740
،أيام قبل إعلان الحكومة عن الحجر الصحي

10
00:00:57,100 --> 00:00:58,440
.أنا أيضا أحسست بالخوف

11
00:00:58,760 --> 00:01:00,660
.هاتفت صديقا يدعى هيثم

12
00:01:01,440 --> 00:01:03,920
،كان في الحجر الصحي لمدة 10 أيام

13
00:01:04,280 --> 00:01:06,380
بعد أن كان على اتصال بصديقة

14
00:01:06,380 --> 00:01:07,480
،عائدة من إيطاليا

15
00:01:07,480 --> 00:01:10,180
.تم تشخيصها فيما بعد، كحاملة للفيروس

16
00:01:11,140 --> 00:01:12,380
لديه تجربة في المسألة

17
00:01:13,040 --> 00:01:14,380
،ربما لو حدثنا عن تجربته

18
00:01:14,760 --> 00:01:18,340
...نتمكن من الفهم ويتقلص الإحساس بالخوف و بالعجز

19
00:01:19,380 --> 00:01:20,340
...قبل هيثم ذلك

20
00:01:20,800 --> 00:01:22,340
.حتى يتمكن من ملء وقته، كما يقول

21
00:01:23,060 --> 00:01:24,580
،حدثنا كيف تلقى الخبر

22
00:01:25,100 --> 00:01:26,580
،كيف يقضي أيامه

23
00:01:27,100 --> 00:01:28,860
وكيف أثرت فيه التجربة

24
00:01:29,060 --> 00:01:30,420
في علاقته بالمجهول

25
00:01:30,580 --> 00:01:32,660
...وفي قناعاته السياسية والفكرية

26
00:01:33,500 --> 00:01:35,060
"من الحجر إلى الحظر"

27
00:01:35,360 --> 00:01:37,620
أو كيف يمكن لتجربة فردية

28
00:01:37,900 --> 00:01:41,940
.أن تصبح أكثر تجربة جماعية في التاريخ المعاصر

29
00:01:50,160 --> 00:01:50,700
...الووو

30
00:01:53,800 --> 00:01:56,640
أمضيت 10 أيام في الحجر الصحي

31
00:01:57,060 --> 00:01:59,280
الاتصال المشتبه به

32
00:01:59,280 --> 00:02:01,280
كان يوم السبت 7 مارس

33
00:02:01,560 --> 00:02:02,740
كنا تقريبا

34
00:02:03,140 --> 00:02:06,440
عشرين صديق وصديقة

35
00:02:06,860 --> 00:02:09,280
.من مختلف الجنسيات

36
00:02:10,380 --> 00:02:12,980
...لم يكن هناك تخوف من

37
00:02:12,980 --> 00:02:15,240
خاصة وأن المصابة

38
00:02:15,360 --> 00:02:17,240
لم تكن تحمل أي أعراض

39
00:02:17,240 --> 00:02:20,380
.لا شيء كان يدعوها  للشك هي أيضا

40
00:02:20,760 --> 00:02:22,380
،ان كنتم تذكرون

41
00:02:22,380 --> 00:02:26,980
في أوائل شهر مارس، بدأت كورونا في إيطاليا

42
00:02:26,980 --> 00:02:28,980
.في منتصف فيفري كانت لا تزال في الصين

43
00:02:29,460 --> 00:02:34,640
.القلق لم يكن كبيرا في تونس

44
00:02:36,060 --> 00:02:38,280
حتى انا الذي كنت حذرا

45
00:02:38,280 --> 00:02:42,700
...استطعت اسكات صوت داخلي يقول أنه يوجد خطر

46
00:02:49,840 --> 00:02:51,200
وصلتني رسالة

47
00:02:51,920 --> 00:02:53,700
يوم الأحد بعد الزوال

48
00:02:53,940 --> 00:02:59,640
اسمع هيثم ... واحدة ممن التقيتهم/ن البارحة"

49
00:03:00,240 --> 00:03:01,960
.لديها حمى

50
00:03:03,180 --> 00:03:06,500
أجرت لها السلطات الصحية اختبارا

51
00:03:06,500 --> 00:03:09,140
".ستكون النتائج جاهزة يوم الاربعاء

52
00:03:09,960 --> 00:03:12,360
.عندها، كانت النتائج تستوجب ثلاثة أيام

53
00:03:12,560 --> 00:03:16,480
"من المستحسن ألا تخرج وألا تذهب للعمل"

54
00:03:16,880 --> 00:03:19,780
قضيت الليل في طرح أسئلة

55
00:03:19,780 --> 00:03:21,240
.وفي حالة هلع

56
00:03:23,620 --> 00:03:26,920
.شعرت بقشعريرة … صرت أتصبب عرقا و أعاني آلاما في البطن

57
00:03:31,060 --> 00:03:36,680
...وطفقت أتذكر إذا كنت قد تواصلت مع الحالة المصابة

58
00:03:36,940 --> 00:03:39,140
في مثل هذا الوقت، لن تكون/ي واثقا/ـة من شيء

59
00:03:39,140 --> 00:03:40,740
!فتعيد البحث

60
00:03:41,000 --> 00:03:45,060
شخصيا كنت قد جمعت معلومات عن الفيروس منذ ديسمبر

61
00:03:45,060 --> 00:03:48,700
تاريخ تسجيل أولى الحالات

62
00:03:49,180 --> 00:03:52,440
،لكن عندما أصبح الأمر يخصني

63
00:03:52,720 --> 00:03:54,440
.صارت قصة أخرى

64
00:03:54,440 --> 00:03:55,720
...بدأت بالبحث

65
00:03:55,720 --> 00:03:57,520
كيف تنتقل العدوى؟

66
00:03:58,660 --> 00:04:00,540
إبتداء من أي يوم تبدأ العدوى؟

67
00:04:00,540 --> 00:04:02,180
ألا يظهر ذلك إلا مع الأعراض؟

68
00:04:03,920 --> 00:04:05,800
...ذلك النوع من الهلع

69
00:04:06,920 --> 00:04:09,840
كانت النتيجة جاهزة ظهيرة يوم الاثنين

70
00:04:10,180 --> 00:04:12,220
،ومنذ يوم الثلاثاء

71
00:04:12,420 --> 00:04:16,660
.كانت وزارة الصحة تهاتفنا يوميا

72
00:04:16,660 --> 00:04:18,340
،اتصلت بي طبيبة

73
00:04:18,600 --> 00:04:20,740
تأكدت أني كنت موجودا في السهرة

74
00:04:20,740 --> 00:04:24,840
أعطتني تدابير الحجر الصحي التي يجب اتخاذها

75
00:04:24,840 --> 00:04:26,620
أن أبقى وحيدا

76
00:04:26,620 --> 00:04:28,340
،إذا كنت أقطن مع أحدهم

77
00:04:28,340 --> 00:04:30,200
ألا نشترك في الأكل

78
00:04:30,200 --> 00:04:32,400
وننظف أقفال الأبواب

79
00:04:32,400 --> 00:04:34,400
وكل شيء مشترك

80
00:04:35,140 --> 00:04:39,360
نستعمل ماء الكلور والصابون

81
00:04:39,360 --> 00:04:41,840
...وننظف البيت والملابس

82
00:04:42,260 --> 00:04:45,360
.فس التدابير التي نراها خلال الحملة المتلفزة

83
00:04:51,540 --> 00:04:53,720
،حسب ما فهمت

84
00:04:53,720 --> 00:04:58,700
.نفس الطبيب أو الطبيبة يـ/تهتم بك في الحجر الصحي

85
00:04:58,700 --> 00:05:01,280
...اليوم هو اليوم الحادي عشر

86
00:05:02,660 --> 00:05:04,380
!يمر الوقت بسرعة شديدة

87
00:05:04,620 --> 00:05:06,100
تهاتفني نفس الطبيبة

88
00:05:06,260 --> 00:05:09,840
.لتتابع معي درجات حرارتي

89
00:05:10,260 --> 00:05:12,940
في الأيام الأولى طلبت مني اقتناء آلة قياس حرارة

90
00:05:13,280 --> 00:05:15,160
وأن أقوم بقيس حرارتي

91
00:05:15,160 --> 00:05:17,440
.عند الاستيقاظ وفي الظهيرة

92
00:05:17,920 --> 00:05:20,320
.وفي الأخير، تم إدماج العمليتين

93
00:05:20,500 --> 00:05:22,660
بما أني أستيقظ في حدود  الساعة 10:30 أو 11

94
00:05:25,540 --> 00:05:28,080
فصارت تهاتفني في حدود الساعة 2 بعد الزوال

95
00:05:28,420 --> 00:05:30,080
.وظلت تتابع حالتي

96
00:05:30,320 --> 00:05:33,120
هي لطيفة جدا وصرنا أصدقاء

97
00:05:33,580 --> 00:05:36,740
...نتجاذب أطراف الحديث قليلا

98
00:05:37,180 --> 00:05:38,740
هاتفتني اليوم

99
00:05:39,600 --> 00:05:42,680
صارت مكالماتنا مقتضبة

100
00:05:42,680 --> 00:05:46,160
تسأل إن كنت بخير فأجيبها

101
00:05:50,460 --> 00:05:51,520
."صار "روتينا

102
00:05:51,520 --> 00:05:55,140
اقيس حرارتي عند الاستيقاظ

103
00:05:55,980 --> 00:06:00,000
!"والتنظيف أضحى "هواية

104
00:06:06,220 --> 00:06:07,340
.أتقاسم الايجار مع شخص آخر

105
00:06:07,340 --> 00:06:08,420
.نحن 2 في المنزل

106
00:06:10,420 --> 00:06:12,000
،كان معي ذاك المساء

107
00:06:12,000 --> 00:06:14,640
.فوجدنا أنفسنا معا في حجر ذاتي

108
00:06:16,240 --> 00:06:20,140
في المرحاض يوجد سطل  ماء الكلور ومنظفات

109
00:06:20,480 --> 00:06:24,280
نعمد إلى التنظيف به بعد كل استعمال

110
00:06:24,580 --> 00:06:26,800
،الأقفال

111
00:06:26,800 --> 00:06:29,060
،و أغطية المراحيض

112
00:06:29,060 --> 00:06:29,920
...والاحواض

113
00:06:30,680 --> 00:06:32,460
...هذا من الناحية الصحية

114
00:06:32,740 --> 00:06:34,760
،بالنسبة للطعام

115
00:06:34,760 --> 00:06:36,760
،نطهو سويا

116
00:06:37,740 --> 00:06:42,040
نستخدم المطهر لنمسك الأواني

117
00:06:42,720 --> 00:06:45,860
.لكل منا أوانيه الخاصة

118
00:06:46,100 --> 00:06:49,940
.التي نغسلها بماء الكلور

119
00:06:50,560 --> 00:06:53,280
!نحن حقا محظوظان

120
00:06:53,280 --> 00:06:57,440
.لنا أصدقاء يسكنون الطابق الثاني من نفس العمارة

121
00:06:57,920 --> 00:07:01,540
،منذ أن بدأ الحجر الصحي

122
00:07:01,860 --> 00:07:06,560
.تكفلوا بتوفير حاجياتنا

123
00:07:07,100 --> 00:07:10,040
،أحول لهم المال

124
00:07:10,040 --> 00:07:12,920
،فيجلبون حاجياتنا أمام باب البيت

125
00:07:13,280 --> 00:07:16,900
...ونتبادل التحية من بعيد طبعا

126
00:07:18,020 --> 00:07:20,900
هناك غرفة تخزين صغيرة

127
00:07:20,900 --> 00:07:22,900
.وغير مستخدمة في الشقة

128
00:07:24,020 --> 00:07:26,120
،نستخدم أكياس القمامة الكبيرة

129
00:07:26,420 --> 00:07:30,500
نضعها داخل كيسين كما ذكرت تدابير الحجر الصحي

130
00:07:30,940 --> 00:07:34,980
.ونتركها في غرفة التخزين

131
00:07:35,260 --> 00:07:38,460
،كوننا لا نقوم بإخراج القمامة كل يوم

132
00:07:38,460 --> 00:07:43,560
،جعلنا نعيد  النظر في كمية إنتاج النفايات لدينا

133
00:07:43,560 --> 00:07:46,360
!فأصبحنا ننتج أقل كمية ممكنة

134
00:07:57,600 --> 00:08:02,260
.في البداية، اتبعت الساعة البيولوجية نسقي المعتاد

135
00:08:02,560 --> 00:08:05,780
أستيقظ حوالي الساعة 7 أو 8 صباحًا

136
00:08:06,300 --> 00:08:12,020
بعد أسبوع، أدركت الساعة البيولوجية أنني لا أقوم بعمل يذكر

137
00:08:12,520 --> 00:08:14,020
!والآن أنام وأستيقظ متأخرًا

138
00:08:15,440 --> 00:08:17,120
...لا يهم متى أستقظ ومتى أنام

139
00:08:17,120 --> 00:08:18,620
،أستيقظ، أقيس درجة حرارتي

140
00:08:20,620 --> 00:08:23,240
،أغتسل، أتناول القهوة

141
00:08:23,660 --> 00:08:25,240
.وادخن سيجارة في الشرفة

142
00:08:25,760 --> 00:08:27,240
قضيت الأسبوع الأول

143
00:08:27,240 --> 00:08:31,080
.في قراءة مقالات ومشاهدة فيديوهات حول كورونا

144
00:08:33,660 --> 00:08:38,080
في عطلة نهاية الأسبوع، شعرت بالملل لدرجة أني اتجهت نحو نظريات المؤامرة

145
00:08:38,080 --> 00:08:40,080
"هل قاموا بتصنيع الفيروس؟"

146
00:08:42,080 --> 00:08:44,920
...لكنها  كانت عقيمة

147
00:08:45,860 --> 00:08:48,720
اصعد أيضًا على السطح

148
00:08:49,140 --> 00:08:51,340
.للتمتع بحمام شمس

149
00:08:51,920 --> 00:08:56,140
.وباقي اليوم أمضيه على شبكة الإنترنت

150
00:08:56,140 --> 00:08:59,540
!عدا أني أقوم بأشياء كثيرة في اليوم

151
00:09:00,820 --> 00:09:05,040
.أشعر أنني متعب في الحجر أكثر مما كنت عليه عندما كنت أعمل

152
00:09:05,440 --> 00:09:07,560
،يعتمد العمل على مهام

153
00:09:09,000 --> 00:09:10,740
!إنه ممل للغاية

154
00:09:12,400 --> 00:09:15,380
!أتصور عملك مملا أيضا

155
00:09:16,960 --> 00:09:18,720
،ليست مسألة أهمية

156
00:09:18,720 --> 00:09:24,380
.ولكن بالأحرى مدى قدرة عملك على أن يبقيك مشغولا/ـة

157
00:09:24,720 --> 00:09:28,140
يعتمد العمل على مهام

158
00:09:28,140 --> 00:09:30,320
.إذا أنهيت مهامك، ليس لديك ما تفعل/ين/ه

159
00:09:30,880 --> 00:09:33,080
:بينما لدي الآن الكثير من المهام

160
00:09:33,080 --> 00:09:34,220
،التنظيف

161
00:09:34,220 --> 00:09:35,580
،حمام الشمس

162
00:09:35,580 --> 00:09:39,160
،الرياضة والاستلقاء

163
00:09:39,160 --> 00:09:41,160
،قراءة 7000 مقالة

164
00:09:41,160 --> 00:09:42,620
،مشاهدة 8000 فيديو

165
00:09:43,060 --> 00:09:45,660
...إجراء بودكاست معك

166
00:09:45,940 --> 00:09:49,200
...طوال الوقت، لدي شيء أفعله

167
00:10:02,820 --> 00:10:05,020
... لم أقل أي شيء لعائلتي

168
00:10:05,600 --> 00:10:07,860
.لا أريد أن أُقلقهم

169
00:10:08,220 --> 00:10:15,240
أخبرتهم أننا نعمل عن بعد

170
00:10:15,240 --> 00:10:20,260
.منذ تسجيل أول حالة في تونس

171
00:10:21,320 --> 00:10:23,800
.والدتي مسؤولة للغاية

172
00:10:24,100 --> 00:10:25,800
لا تخرج من المنزل

173
00:10:28,200 --> 00:10:30,700
.ولا تذهب للتسوق سوى مرة واحدة

174
00:10:30,700 --> 00:10:32,700
.تولي حرصا وتتبع التدابير

175
00:10:32,700 --> 00:10:34,700
بالنسبة لوالدي لا أعرف

176
00:10:35,160 --> 00:10:36,440
!ولكن أعتقد أنه حريص أيضا

177
00:10:47,520 --> 00:10:48,440
،ذات مرة

178
00:10:48,980 --> 00:10:53,060
لا أعرف لماذا؟

179
00:10:53,620 --> 00:11:00,280
قررت تمرير الممسحة في المطبخ رغم أنه كان نظيفا

180
00:11:00,600 --> 00:11:02,680
أشعرني ماء الكلور بالدوار

181
00:11:02,680 --> 00:11:06,060
المجهود المبذول في المنزل

182
00:11:06,060 --> 00:11:09,360
!بمثابة رفع الأثقال

183
00:11:09,360 --> 00:11:11,220
بما أنني لا أفعل شيئًا طوال اليوم

184
00:11:12,620 --> 00:11:14,880
شعرت بلفحة من الحرارة الشديدة

185
00:11:14,880 --> 00:11:16,880
،شعرت بالتعب

186
00:11:16,880 --> 00:11:18,880
.قست درجة الحرارة

187
00:11:18,880 --> 00:11:22,380
تشير موازين الحرارة بشكل عام

188
00:11:22,980 --> 00:11:28,280
.لى 0.5 درجة مئوية أقل من الواقع

189
00:11:28,660 --> 00:11:33,340
...في العادة تكون 36.3 ، 36.5

190
00:11:33,640 --> 00:11:36,100
.عندما قست درجة حرارتي في ذلك اليوم، كانت 37

191
00:11:37,100 --> 00:11:38,100
شعرت بالذعر

192
00:11:38,100 --> 00:11:40,100
،والذعر بطبعه يخلق المزيد من الحرارة

193
00:11:40,100 --> 00:11:42,100
فشعرت بالحمى

194
00:11:42,100 --> 00:11:44,980
....استلقيت

195
00:11:44,980 --> 00:11:47,320
أقفلت هاتفي وأطفأت الأضواء

196
00:11:49,320 --> 00:11:55,200
.بعد 30 دقيقة … تحسنت

197
00:11:55,200 --> 00:11:56,680
...كان ذلك في الظهيرة

198
00:11:56,680 --> 00:11:59,940
،في نفس الليلة، عندما ذهبت إلى النوم مع الواحدة صباحا

199
00:12:00,600 --> 00:12:08,640
تمددت على السرير وشعرت أنني لا أستطيع التنفس كالمعتاد

200
00:12:09,380 --> 00:12:10,920
.ذعرت طبعا

201
00:12:10,920 --> 00:12:14,360
في الليل أتجنب قياس درجة حرارتي

202
00:12:14,360 --> 00:12:15,980
،حتى لا أخاف

203
00:12:15,980 --> 00:12:20,520
لأنه لا يمكن لأحد أن يأتي لمساعدتي، خاصة بالنسبة للطوارئ

204
00:12:20,520 --> 00:12:23,340
،لم أقم بقياس درجة حرارتي

205
00:12:23,340 --> 00:12:25,840
حاولت أن أفهم ما يحصل

206
00:12:26,300 --> 00:12:28,660
لكنني تذكرت أن لدي حساسية

207
00:12:30,320 --> 00:12:33,160
.تناولت حبوبا وشعرت بتحسن

208
00:12:33,460 --> 00:12:35,640
!كان هذا أكثر يوم مروع بالنسبة لي

209
00:12:48,340 --> 00:12:51,000
،شعرت أن حالتي لم تكن مستعجلة

210
00:12:51,300 --> 00:12:53,580
خاصة في  ظل العبء الكبير على عاتق الأطباء

211
00:12:53,580 --> 00:12:59,540
ولم أرغب في مضاعفته

212
00:13:00,020 --> 00:13:08,720
،يجب عليك الاتصال برقم 190 إذا تجاوزت درجة الحرارة 38.5

213
00:13:09,140 --> 00:13:11,260
،إذا كان هناك سعال جاف

214
00:13:11,260 --> 00:13:15,940
.التهاب الحلق وصعوبة في التنفس

215
00:13:16,760 --> 00:13:19,220
...انتظرت قليلا

216
00:13:22,580 --> 00:13:26,600
.لو ساءت الامور، كنت اتصلت

217
00:13:30,620 --> 00:13:33,820
.لم يخفني أي مؤتمر صحفي بشأن الحالات في  تونس

218
00:13:34,240 --> 00:13:35,820
.ما يخيفني هو المجهول

219
00:13:36,200 --> 00:13:39,580
…رئيس منظمة الصحة العالمية نفسه يخشى المجهول

220
00:13:39,580 --> 00:13:42,320
!لن أكون أشد شجاعة منه

221
00:13:42,820 --> 00:13:46,520
.المجهول موجود أيضًا في سياسات الدولة

222
00:13:47,900 --> 00:13:52,240
...هناك العديد من الإجراءات التي اتخذتها الدولة التي لا أفهمها

223
00:13:52,240 --> 00:13:53,780
وعندما لا أتمكن من الفهم، ينتابني الخوف

224
00:13:53,780 --> 00:13:55,780
،التفسير بالبنسبة لي

225
00:13:55,780 --> 00:14:00,660
السياسة وُجدت للسيطرة على الأفراد وترهيبهم/ن

226
00:14:00,660 --> 00:14:06,540
لذلك بفرض حظر تجول، يصير الجميع أشد يقظة

227
00:14:06,540 --> 00:14:15,520
خلق حالة القلق يجعل الناس يدركون أن هناك وباء

228
00:14:16,640 --> 00:14:22,920
.أنا لست مع أن تتلاعب الدولة بالمواطنين، لا جسديا ولا نفسيا

229
00:14:22,920 --> 00:14:28,280
ولكن في النهاية، المسألة لا تتعلق فقط بالدولة بل بالمجتمع برمته

230
00:14:28,540 --> 00:14:32,000
حتى لو تم اتخاذ القرارات من طرف الدولة

231
00:14:32,000 --> 00:14:34,240
،نحن نواجه فيروسًا

232
00:14:34,240 --> 00:14:36,760
!ليست حربًا أو إرهابا... أو ثورة

233
00:14:37,460 --> 00:14:41,600
لا يزال الوقت مبكرا لبلوغ الذروة

234
00:14:41,600 --> 00:14:43,920
.سيكون الأسبوع القادم حاسما للغاية

235
00:14:44,740 --> 00:14:46,720
لأننا لا نجري اختبارات كافية

236
00:14:47,300 --> 00:14:54,100
صحيح أن التكنولوجيا تجعل الأمور أسهل

237
00:14:54,100 --> 00:14:57,780
.لكن لا يمكننا التكهن بكيفية تصرف الفيروس

238
00:14:57,780 --> 00:15:03,780
يمكن لشخص واحد في زاوية نائية من العالم قلب كل شيء رأساً على عقب

239
00:15:04,320 --> 00:15:07,260
،هذا ما يخيفني

240
00:15:07,260 --> 00:15:10,380
.عدا ذلك، أشعر أنني بحالة جيدة

241
00:15:23,180 --> 00:15:26,420
.بصراحة، أنا أستمتع بالحجر الصحي

242
00:15:27,560 --> 00:15:29,040
.ليس لدي ما أفعله بالخارج

243
00:15:29,040 --> 00:15:33,140
الوباء قوي لدرجة

244
00:15:33,460 --> 00:15:39,880
.أنه أزال كل الرغبة في الخروج

245
00:15:40,260 --> 00:15:42,940
قلت أمس لشريكي في السكن

246
00:15:42,940 --> 00:15:44,940
!على بعد مترين بالطبع

247
00:15:45,080 --> 00:15:50,020
أن الفيروس تلاعب بنا نفسيا

248
00:15:50,300 --> 00:15:53,120
...عديد الاحتياجات لم تعد موجودة

249
00:15:53,120 --> 00:15:56,780
...خاصة الحاجة الاجتماعية، للخروج، لرؤية الناس

250
00:15:56,960 --> 00:16:01,760
!فعلا! لا أفتقد الأماكن المعتادة

251
00:16:02,240 --> 00:16:05,460
الحجر الذاتي والوباء

252
00:16:05,900 --> 00:16:10,080
...كأيام الدراسة بالنسبة لي

253
00:16:10,080 --> 00:16:14,700
نكون مضغوطين للغاية ويكون تركيزنا منصبا على المراجعة

254
00:16:14,920 --> 00:16:19,160
!فنقول اننا سنمارس الرياضة إثرها

255
00:16:19,160 --> 00:16:25,520
!بعد الإمتحانات، سأفعل كذا

256
00:16:25,520 --> 00:16:27,920
...سأنظم طريقة المراجعة

257
00:16:27,920 --> 00:16:32,080
يذكرني أيضا بنذورنا لله بإعطاء فقير دينارا إن نجحنا تلك السنة

258
00:16:32,980 --> 00:16:36,080
:نقول لأنفسنا

259
00:16:36,080 --> 00:16:39,500
!سنصبح أشخاصا أفضل

260
00:16:39,500 --> 00:16:40,940
...هذا ليس حقيقيا

261
00:16:41,340 --> 00:16:42,940
...آمل أن نتغير بعد هذا الوضع

262
00:16:56,860 --> 00:16:58,740
"م الحجر للحظر"

263
00:16:59,380 --> 00:17:02,180
إنتاج إنكفاضة بودكاست

264
00:17:04,080 --> 00:17:07,280
أيام قليلة بعد التسجيل، أعلنت الحكومة عن الحجر العام

265
00:17:07,700 --> 00:17:11,720
.وفي إنكفاضة، مكاتب مغلقة، وفريق يعمل عن بعد

266
00:17:11,880 --> 00:17:15,700
على غرار ما فعله الآخرون، تم تنظيم أساليب العمل من جديد

267
00:17:15,700 --> 00:17:17,880
.مع تطور الوباء في تونس

268
00:17:18,840 --> 00:17:20,040
كيف نسجل؟

269
00:17:20,480 --> 00:17:21,860
كيف نقوم بالإخراج؟

270
00:17:22,080 --> 00:17:27,620
أي بودكاست نقوم به في وقت لا يشغل الناس موضوع عدا الكورونا؟

271
00:17:28,320 --> 00:17:31,220
...لكننا تأقلمنا

272
00:17:31,220 --> 00:17:33,220
.مع مشاكل تغطية الانترنت

273
00:17:34,440 --> 00:17:36,060
"م الحجر للحظر"

274
00:17:36,060 --> 00:17:39,280
،بودكاست تم إنتاجه في ظل الإمكانيات المتوفرة

275
00:17:39,540 --> 00:17:43,880
.أنتاجناه ليقص قصة يمكن أن يجد الكثير أنفسهم من خلالها

276
00:17:45,420 --> 00:17:50,340
إخراج: بشرى تريكي ، منية بن حمادي وهزار عبيدي

277
00:17:51,080 --> 00:17:53,580
تسجيل: هزار عبيدي

278
00:17:54,040 --> 00:17:58,980
موسيقى، مونتاج، ميكساج وتصميم صوتي: أسامة قايدي

279
00:17:59,560 --> 00:18:02,180
تعليق صوتي: هزار عبيدي

280
00:18:02,180 --> 00:18:04,180
بمشاركة: ياسين كعوانة وكامل فريق إنكفاضة
