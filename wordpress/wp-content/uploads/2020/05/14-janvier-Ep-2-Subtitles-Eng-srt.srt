
1
00:00:02,500 --> 00:00:04,770
When I arrived to Bab el Khadra street,

2
00:00:04,770 --> 00:00:06,480
I was faced with a sea of people

3
00:00:07,280 --> 00:00:09,420
I asked what was happening,

4
00:00:09,420 --> 00:00:11,860
someone told me, “It’s Helmi’s funeral.”

5
00:00:11,860 --> 00:00:12,890
- “Who’s Helmi ?”

6
00:00:12,890 --> 00:00:15,250
- “The one who was killed yesterday.”

7
00:00:15,980 --> 00:00:17,720
He was 23,

8
00:00:17,720 --> 00:00:20,260
exactly like Ben Ali’s reign.

9
00:00:20,260 --> 00:00:22,610
What a coincidence.

10
00:01:27,740 --> 00:01:30,650
January 14, 2011.

11
00:01:30,650 --> 00:01:33,310
Many people feared what was to come,

12
00:01:33,310 --> 00:01:35,370
looking at these unprecedented events.

13
00:01:35,370 --> 00:01:36,940
Hamideddine is one of them,

14
00:01:36,940 --> 00:01:39,500
50 years old, a photographer.

15
00:01:40,450 --> 00:01:42,820
As he says, he was never an activist,

16
00:01:42,820 --> 00:01:44,440
used to taking pictures of Sidi Bou Saïd,

17
00:01:44,440 --> 00:01:46,360
and the medina, or pictures of tennis games.

18
00:01:47,120 --> 00:01:50,050
But he found the courage to go to Habib Bourguiba Avenue

19
00:01:50,050 --> 00:01:51,440
to take pictures.

20
00:01:53,680 --> 00:01:54,780
From the rooftops,

21
00:01:54,780 --> 00:01:56,750
he takes pictures of the protests, the police,

22
00:01:56,750 --> 00:01:59,040
the military, the people, in the avenues 

23
00:01:59,040 --> 00:02:00,620
and the smaller streets,

24
00:02:01,560 --> 00:02:03,880
until he finds himself in Bab El Khadra,

25
00:02:03,880 --> 00:02:06,040
where he is faced with Helmi’s funeral,

26
00:02:06,040 --> 00:02:07,420
23 years old,

27
00:02:07,420 --> 00:02:10,490
killed in Passage, on January 13.

28
00:02:12,290 --> 00:02:14,040
The crowd heads towards the Avenue;

29
00:02:14,040 --> 00:02:15,500
Hamideddine captures the moment.

30
00:02:16,340 --> 00:02:18,430
Cries of “God is great” and “Dégage (Get out!)” intertwine

31
00:02:18,430 --> 00:02:20,830
with other slogans against the system.

32
00:02:21,850 --> 00:02:25,030
The funerals procession reaches Avenue Habib Bourguiba,

33
00:02:25,030 --> 00:02:28,930
and abruptly, the repression ramps up. 

34
00:02:31,100 --> 00:02:32,010
In his own words,

35
00:02:32,010 --> 00:02:35,570
Hamideddine Bouali describes the pictures he took that day.

36
00:02:35,570 --> 00:02:37,570
Pictures that are inscribed in our memory.

37
00:02:40,730 --> 00:02:42,100
Over the course of a year,

38
00:02:42,100 --> 00:02:44,590
in anticipation of the Revolution’s tenth anniversary,

39
00:02:44,590 --> 00:02:46,020
Inkyfada will tell the stories 

40
00:02:46,020 --> 00:02:47,500
of some people, among thousands,

41
00:02:47,500 --> 00:02:49,760
who lived this historical day.

42
00:02:52,990 --> 00:02:55,720
Where were you on January 14?

43
00:02:56,980 --> 00:03:01,040
Episode 2 : The View from Above

44
00:03:14,620 --> 00:03:16,980
I didn’t really start my day on January 14,

45
00:03:16,980 --> 00:03:19,580
because I hadn’t slept.

46
00:03:19,580 --> 00:03:22,490
After the “January 13 speech,”

47
00:03:22,490 --> 00:03:26,370
I knew that there was going to be something special.

48
00:03:26,370 --> 00:03:28,160
I remember I wrote on Facebook

49
00:03:28,160 --> 00:03:32,220
that Ben Ali had promised to change many things,

50
00:03:32,220 --> 00:03:36,380
to organize elections. So let’s give him a chance.

51
00:03:37,370 --> 00:03:39,300
Added to that, many had died

52
00:03:39,300 --> 00:03:42,480
It had to stop, so we should take his word.

53
00:03:42,480 --> 00:03:45,200
I was not an activist, I always say it,

54
00:03:45,200 --> 00:03:47,280
and I am here as a witness.

55
00:03:47,280 --> 00:03:51,310
I have neither the strength nor the courage to be a revolutionary,

56
00:03:51,310 --> 00:03:54,680
but I had to at least take some pictures.

57
00:03:58,440 --> 00:04:01,920
So I spent the night thinking.

58
00:04:13,180 --> 00:04:15,510
The following day, I was on the metro,

59
00:04:15,510 --> 00:04:19,580
at 10 AM, it wasn’t crowded. 

60
00:04:20,860 --> 00:04:22,860
Everything was as normal in Tunis

61
00:04:22,860 --> 00:04:25,630
except for the presence of the military.

62
00:04:25,630 --> 00:04:26,870
There were many people

63
00:04:26,870 --> 00:04:29,560
but no one had flags or signs,

64
00:04:29,560 --> 00:04:31,380
there was nothing special.

65
00:04:31,380 --> 00:04:35,350
It looked like a Saturday morning.

66
00:04:35,350 --> 00:04:36,680
Nothing out of the ordinary.

67
00:04:37,100 --> 00:04:39,380
I started taking pictures in Passage

68
00:04:39,380 --> 00:04:41,920
of a soldier, directly,

69
00:04:41,920 --> 00:04:47,340
because I felt I had a purpose

70
00:04:47,340 --> 00:04:48,660
and also, because I had free reign.

71
00:04:48,660 --> 00:04:50,740
Which I didn’t feel the days before.

72
00:04:50,740 --> 00:04:55,550
I used to go out with a camera in my backpack, filled with fear,

73
00:04:55,550 --> 00:05:02,140
I didn’t have any experience in taking pictures of protests

74
00:05:02,140 --> 00:05:04,630
and I was fearful, like most Tunisians.

75
00:05:04,630 --> 00:05:08,720
You cannot become a photojournalist overnight,

76
00:05:08,720 --> 00:05:12,820
especially in Tunisia where everything is locked up, sealed.

77
00:05:12,820 --> 00:05:18,510
The police know if someone is a photojournalist

78
00:05:22,000 --> 00:05:25,020
I took pictures of the military in front of Habib Thameur garden,

79
00:05:25,020 --> 00:05:28,910
then I took a picture of a soldier,

80
00:05:28,910 --> 00:05:35,590
with a tough face, holding his rifle,

81
00:05:35,590 --> 00:05:37,690
standing in front of him, a little girl 

82
00:05:37,690 --> 00:05:38,860
looking at him with a confused look.

83
00:05:38,860 --> 00:05:41,180
It was the first time that she was witnessing such a scene.

84
00:05:41,180 --> 00:05:42,790
Her mother was calling for her.

85
00:05:42,790 --> 00:05:47,590
The girl must be in her twenties today.

86
00:05:47,590 --> 00:05:49,410
I always ask myself this question:

87
00:05:49,410 --> 00:05:53,670
What has become of all those people I took pictures of?

88
00:06:10,140 --> 00:06:12,740
Afterwards, I went back to Avenue Habib Bourguiba.

89
00:06:12,740 --> 00:06:14,140
I was astonished

90
00:06:14,140 --> 00:06:18,080
because, for those who remember, we didn’t have the right

91
00:06:18,080 --> 00:06:20,780
to use the Ministry of Interior’s sidewalk.

92
00:06:20,780 --> 00:06:22,480
It wasn’t closed like now,

93
00:06:22,480 --> 00:06:24,160
but we used to be very careful

94
00:06:24,160 --> 00:06:26,620
to change sidewalks when we reached the Ministry

95
00:06:26,620 --> 00:06:32,450
That day, however, people were in the police’s face
96
00:06:32,450 --> 00:06:34,780
which was impossible before that time.

97
00:06:34,780 --> 00:06:36,910
We used to even be afraid of traffic cops,

98
00:06:36,910 --> 00:06:38,790
let alone the BOP (street police).

99
00:06:38,790 --> 00:06:41,620
Even the Ministry’s building was terrifying,

100
00:06:41,620 --> 00:06:44,580
the architect knew what they were doing.

101
00:06:44,580 --> 00:06:48,670
The walls are rough and the windows are very small.

102
00:06:48,670 --> 00:06:51,580
You can feel repression from the very façade,

103
00:06:51,580 --> 00:06:54,050
the stones are yelling

104
00:06:54,050 --> 00:06:56,340
as if they are holding truncheons, ready to attack.

105
00:07:00,520 --> 00:07:02,480
I reached the Ministry,

106
00:07:02,480 --> 00:07:05,110
an took a picture of a young woman holding a flag,

107
00:07:05,110 --> 00:07:06,410
she was smiling, very beautiful.

108
00:07:06,410 --> 00:07:08,510
as if coming out of a May 68 protest.

109
00:07:08,510 --> 00:07:11,760
I teach techniques and history of photography

110
00:07:11,760 --> 00:07:13,100
and there’s a girl in a picture taken in May 68

111
00:07:13,100 --> 00:07:14,360
that looks a lot like her.

112
00:07:14,360 --> 00:07:17,430
When you cover these types of events,

113
00:07:17,430 --> 00:07:19,550
you keep these pictures in mind,

114
00:07:19,550 --> 00:07:22,440
like the picture of the man standing in front of tanks.

115
00:07:22,440 --> 00:07:25,080
Unconsciously, we want to reproduce those pictures

116
00:07:25,080 --> 00:07:26,880
because they are iconic,

117
00:07:26,880 --> 00:07:28,440
and because they marked history.

118
00:07:28,440 --> 00:07:33,200
It’s the dream of every photographer to take a picture that becomes iconic.

119
00:07:35,720 --> 00:07:37,360
Later on, I realized that in order to take pictures 

120
00:07:37,360 --> 00:07:39,200
of such a big crowd,

121
00:07:39,200 --> 00:07:41,200
I needed a higher view.

122
00:07:46,620 --> 00:07:50,580
I entered the building facing the Ministry of Interior.

123
00:07:52,200 --> 00:07:53,360
I climbed the stairs,

124
00:07:53,780 --> 00:07:55,420
the door leading to the rooftop was open,

125
00:07:55,420 --> 00:07:56,840
as if by chance.

126
00:08:01,000 --> 00:08:01,760
On the rooftop,

127
00:08:01,760 --> 00:08:04,750
I discover that a dozen other people had the same idea.

128
00:08:04,750 --> 00:08:06,100
Obviously, I took pictures,

129
00:08:06,720 --> 00:08:09,440
including one of a man holding a sign saying:

130
00:08:09,440 --> 00:08:11,440
“Ben Ali Out.”

131
00:08:12,910 --> 00:08:16,660
The closest person to him was 10 meters away.

132
00:08:16,660 --> 00:08:18,230
People stayed away from him.

133
00:08:18,230 --> 00:08:19,110
They were afraid.

134
00:08:19,110 --> 00:08:21,220
If someone wanted to target and kill him,

135
00:08:21,220 --> 00:08:22,640
they would’ve done so easily.

136
00:08:22,640 --> 00:08:24,030
So people kept away from him

137
00:08:24,030 --> 00:08:27,470
because it was the first sign

138
00:08:27,470 --> 00:08:30,140
where Ben Ali was referred to by name.

139
00:08:30,140 --> 00:08:30,960
“OUT.”

140
00:08:30,960 --> 00:08:33,420
It was incredibly brave

141
00:08:33,420 --> 00:08:36,280
because he didn’t know what would happen later.

142
00:08:40,300 --> 00:08:42,050
From the building I was in,

143
00:08:42,050 --> 00:08:45,180
I took pictures of people around the fountain.

144
00:08:45,970 --> 00:08:50,760
They were far from the crowds, looking, without understanding.

145
00:08:50,760 --> 00:08:53,170
The people there did not know

146
00:08:53,170 --> 00:08:57,640
what their act, their presence, and their slogans

147
00:08:58,220 --> 00:09:00,520
would produce.

148
00:09:02,520 --> 00:09:04,290
No one knew.

149
00:09:08,720 --> 00:09:10,350
I had a Lumix,

150
00:09:10,350 --> 00:09:15,790
a small tourist camera to take souvenir pictures.

151
00:09:15,790 --> 00:09:17,080
My battery was running out,

152
00:09:17,080 --> 00:09:18,630
the memory card was getting full,

153
00:09:18,630 --> 00:09:21,600
so I told myself that maybe it’s time to go.

154
00:09:22,920 --> 00:09:27,960
When your camera tells you “you have 50 shots left,”

155
00:09:28,280 --> 00:09:29,310
you better keep them

156
00:09:29,310 --> 00:09:31,830
because you don’t know what you might stumble upon when going home.

157
00:09:31,830 --> 00:09:34,150
You can find a far more important story

158
00:09:34,150 --> 00:09:38,860
so you better keep that space empty.

159
00:09:39,620 --> 00:09:40,910
I left the building.

160
00:09:41,490 --> 00:09:43,580
I took a picture of a poster of Ben Ali,

161
00:09:43,580 --> 00:09:46,020
still hung in front of the Cathedral,

162
00:09:46,020 --> 00:09:49,820
with a military tank in the foreground.

163
00:09:49,820 --> 00:09:52,750
It was my last picture of Ben Ali.

164
00:09:57,420 --> 00:09:59,970
When I arrived to Bab El Khadra street

165
00:09:59,970 --> 00:10:02,480
I was faced with a sea of people.

166
00:10:06,320 --> 00:10:08,980
I asked what was happening,

167
00:10:09,580 --> 00:10:12,160
someone told me, “It’s Helmi’s funeral.”

168
00:10:12,160 --> 00:10:13,040
- “Who’s Helmi?”

169
00:10:13,040 --> 00:10:15,040
- “The one who was killed yesterday.”

170
00:10:20,220 --> 00:10:23,680
I saw people on a balcony,

171
00:10:24,700 --> 00:10:28,920
in front of the street where the funeral’s procession would pass.

172
00:10:28,920 --> 00:10:30,920
I asked if I could join them,

173
00:10:30,920 --> 00:10:33,000
they agreed and I went up.

174
00:10:33,000 --> 00:10:34,680
The street was crowded, not a single spot was empty.

175
00:10:34,680 --> 00:10:37,580
People were holding pictures of Helmi.

176
00:10:37,580 --> 00:10:39,280
He was 23,

177
00:10:39,280 --> 00:10:41,910
exactly like Ben Ali’s reign.

178
00:10:41,910 --> 00:10:43,880
What a coincidence.

179
00:10:44,260 --> 00:10:46,340
At that moment, Ben Ali was still in the palace.

180
00:10:46,340 --> 00:10:48,590
It was maybe 2 PM.

181
00:10:53,180 --> 00:10:55,630
I was standing on the balcony taking pictures

182
00:10:55,630 --> 00:10:58,270
when someone shouted, “That one is a cop!” 

183
00:10:59,070 --> 00:11:00,370
People started to shout.

184
00:11:00,370 --> 00:11:03,840
The young man who let me in asked me where I came from,

185
00:11:03,840 --> 00:11:05,260
if I was a journalist.

186
00:11:05,260 --> 00:11:06,120
I said no.

187
00:11:06,120 --> 00:11:07,380
-“So what are you doing here ?”

188
00:11:07,380 --> 00:11:09,900
I answered that I’m just passionate about photography.

189
00:11:09,900 --> 00:11:11,500
People were still shouting from the other side,

190
00:11:11,500 --> 00:11:15,840
others were throwing stones at the balcony.

191
00:11:15,840 --> 00:11:17,740
The young man started to get scared.

192
00:11:18,390 --> 00:11:19,840
Then he asked me to leave.

193
00:11:23,960 --> 00:11:27,020
Those seconds seemed like an eternity.

194
00:11:33,320 --> 00:11:36,200
Then, suddenly, there we could only hear “Allahu Akbar.”

195
00:11:36,200 --> 00:11:37,900
The funeral had begun.

196
00:11:40,940 --> 00:11:42,300
People were no longer paying attention to me.

197
00:11:42,300 --> 00:11:46,320
The funeral procession started to move.

198
00:11:46,320 --> 00:11:47,880
So I managed to take some pictures

199
00:11:47,880 --> 00:11:50,680
that every photographer dreams of taking.

200
00:11:57,100 --> 00:11:58,940
I can’t tell you enough.

201
00:11:58,940 --> 00:12:00,630
They were extremely difficult to take,

202
00:12:00,630 --> 00:12:02,870
among the most difficult pictures I ever took.

203
00:12:02,870 --> 00:12:05,220
Before, I used to take pictures of Sidi Bou Saïd’s doors,

204
00:12:05,220 --> 00:12:09,660
animals in Belvédère Zoo, tennis tournaments …

205
00:12:09,660 --> 00:12:11,570
I wasn’t used to covering the news,

206
00:12:11,570 --> 00:12:14,780
and then, overnight, it was head news.

207
00:12:14,780 --> 00:12:17,260
It’s not like I started slowly.

208
00:12:17,260 --> 00:12:20,140
Overnight, I was taking pictures of the Ministry of Interior,

209
00:12:20,140 --> 00:12:23,500
“Game over,” “Get out,”

210
00:12:23,500 --> 00:12:25,200
The military…

211
00:12:25,200 --> 00:12:26,580
and a funeral.

212
00:12:27,160 --> 00:12:28,870
You can’t take pictures of a funeral,

213
00:12:28,870 --> 00:12:30,400
in that mood, without being moved.

214
00:12:30,400 --> 00:12:32,260
I cried while I took those pictures.

215
00:12:32,260 --> 00:12:33,800
It still overwhelms me when I think about it.

216
00:12:38,740 --> 00:12:44,200
Some members of the photography club there on January 14

217
00:12:44,200 --> 00:12:46,180
were so deeply moved

218
00:12:46,180 --> 00:12:48,700
that they forgot to take pictures.

219
00:12:50,360 --> 00:12:52,240
It’s not easy

220
00:12:52,240 --> 00:12:54,370
to step back and take pictures.

221
00:12:54,370 --> 00:12:55,800
If it was a different event,

222
00:12:55,800 --> 00:12:59,230
protests in Hong Kong for example,

223
00:12:59,230 --> 00:13:01,230
it wouldn’t have been the same.

224
00:13:01,230 --> 00:13:04,780
At that moment I was taking pictures of Alia, Selma, Fathi, Salah,

225
00:13:04,780 --> 00:13:06,300
Tunisians.

226
00:13:06,300 --> 00:13:08,070
It personally impacts me.

227
00:13:08,070 --> 00:13:13,480
It’s not someone else’s reality, it’s my reality.

228
00:13:25,140 --> 00:13:29,240
When they were going to put the coffin in the truck,

229
00:13:29,820 --> 00:13:31,880
some people decided to carry it on their shoulders and walk,

230
00:13:31,880 --> 00:13:33,510
I heard them from above.

231
00:13:33,510 --> 00:13:40,320
But I think my camera memory was full.

232
00:13:40,320 --> 00:13:42,880
I could have deleted some pictures taken earlier that day

233
00:13:42,880 --> 00:13:44,580
and follow the funeral,

234
00:13:44,580 --> 00:13:46,600
but I didn’t think that they would walk to the Avenue,

235
00:13:46,600 --> 00:13:48,110
where everything would blow up.

236
00:13:48,110 --> 00:13:49,690
Nobody could have expected that.

237
00:13:49,690 --> 00:13:51,540
In fact, no funeral has ever passed by the Avenue.

238
00:13:51,540 --> 00:13:55,140
Neither Essebsi’s, nor Brahmi’s, nor Chokri Belaïd’s.

239
00:13:55,140 --> 00:13:57,140
It was something absolutely prohibited,

240
00:13:57,140 --> 00:13:58,690
I don’t know why.

241
00:14:11,100 --> 00:14:14,220
The funeral passed by Rome Street

242
00:14:14,220 --> 00:14:17,140
before reaching Habib Bourguiba Avenue

243
00:14:17,140 --> 00:14:20,910
to turn to Carthage Avenue.

244
00:14:20,910 --> 00:14:24,040
Since they were shouting “Allahu Akbar,”

245
00:14:24,040 --> 00:14:25,620
the cops didn’t understand what was happening,

246
00:14:25,620 --> 00:14:27,040
they started hitting people.

247
00:14:27,040 --> 00:14:30,870
Things started to go downhill at this moment.

248
00:14:30,870 --> 00:14:33,140
It must have been 2:30 PM or 3 PM.

249
00:14:35,880 --> 00:14:38,360
I went back home on foot.

250
00:14:41,600 --> 00:14:44,300
I had left that morning to take pictures,

251
00:14:44,300 --> 00:14:48,110
then the events took over.

252
00:14:48,110 --> 00:14:49,920
I didn’t decide anything.

253
00:14:49,920 --> 00:14:52,400
I went back home to Bardo.

254
00:14:52,400 --> 00:14:54,310
I looked at my pictures,

255
00:14:54,660 --> 00:14:56,470
but I only published them the following day

256
00:14:57,280 --> 00:14:58,840
because I was afraid.

257
00:14:59,420 --> 00:15:03,200
Publishing pictures of someone who was just killed,

258
00:15:03,200 --> 00:15:06,840
I was really afraid, and I didn’t have protection.

259
00:15:06,840 --> 00:15:08,310
I wasn’t a journalist,

260
00:15:08,310 --> 00:15:11,550
my page was personal,

261
00:15:12,140 --> 00:15:16,570
if I published them, I’d instantly have problems.

262
00:15:16,840 --> 00:15:19,440
Nothing was certain at that time.

263
00:15:23,680 --> 00:15:26,240
People were stuck in the Avenue, if you remember,

264
00:15:26,240 --> 00:15:28,240
in the buildings.

265
00:15:28,460 --> 00:15:32,640
At the Carlton, there were 20 people per room.

266
00:15:33,180 --> 00:15:36,380
Fortunately, bless him, the owner of the hotel, Ben Smail

267
00:15:37,020 --> 00:15:40,090
prevented the police from entering.

268
00:15:40,090 --> 00:15:43,150
People took refuge in every building.

269
00:15:43,150 --> 00:15:45,680
There was really an atmosphere of panic.

270
00:15:51,850 --> 00:15:53,390
I’m happy I got to live this at 50

271
00:15:53,390 --> 00:15:54,720
and not at 20.

272
00:15:54,720 --> 00:15:58,700
Because if I were 20, I would have been frivolous,

273
00:15:58,700 --> 00:16:00,700
I would have maybe run away

274
00:16:00,700 --> 00:16:03,280
or I would have slept in the morning

275
00:16:03,280 --> 00:16:05,800
and woken up at 5 PM.

276
00:16:05,800 --> 00:16:09,040
I am happy as well that I got to live it at 50

277
00:16:09,040 --> 00:16:10,990
and not at my current age.

278
00:16:19,040 --> 00:16:20,920
For me, January 14,

279
00:16:20,920 --> 00:16:24,090
was so meaningful.

280
00:16:24,480 --> 00:16:28,350
It coincided with my fear of the fifties.

281
00:16:28,350 --> 00:16:30,000
on the 14th,

282
00:16:30,270 --> 00:16:33,420
I was able to photograph the Interior Ministry at close range,

283
00:16:33,420 --> 00:16:37,530
to photograph a cop from so close

284
00:16:37,530 --> 00:16:39,480
that I could see my reflection in his helmet.

285
00:16:40,890 --> 00:16:43,020
Before that day, this would’ve been impossible,

286
00:16:43,020 --> 00:16:44,190
I would’ve never dared to approach him,

287
00:16:44,190 --> 00:16:46,890
only if he asked for my papers.

288
00:16:53,470 --> 00:16:57,020
On January 18, I almost died,

289
00:16:57,020 --> 00:16:59,690
because I took “la photo de trop”

290
00:17:01,690 --> 00:17:03,660
There’s something that we call “la photo de trop”

291
00:17:04,430 --> 00:17:07,050
for which many photographers have died.

292
00:17:07,530 --> 00:17:09,610
We should know when to stop,

293
00:17:09,610 --> 00:17:12,330
to tell ourselves that what we have is enough,

294
00:17:12,330 --> 00:17:15,470
no need to always want more.

295
00:17:19,630 --> 00:17:21,370
It was in Passage,

296
00:17:21,370 --> 00:17:23,600
during a protest,

297
00:17:24,040 --> 00:17:25,180
I was taking pictures on the Avenue,

298
00:17:25,180 --> 00:17:27,020
when the police ran to the crowds

299
00:17:27,020 --> 00:17:28,590
and fired tear gas.

300
00:17:30,940 --> 00:17:32,440
People ran away

301
00:17:32,440 --> 00:17:35,390
and I went up on a wall,

302
00:17:35,850 --> 00:17:39,160
next to the metro station in Passage.

303
00:17:39,500 --> 00:17:41,000
I took some pictures

304
00:17:41,360 --> 00:17:42,720
then I went down.

305
00:17:43,980 --> 00:17:45,720
In those moments, you must remain vigilant.

306
00:17:46,730 --> 00:17:49,440
The slightest distraction can be fatal.

307
00:17:49,980 --> 00:17:52,700
But when I wasn’t looking,

308
00:17:52,700 --> 00:17:55,560
surrounded by tear gas,

309
00:17:55,560 --> 00:17:59,310
a policeman had his grenade launcher

310
00:17:59,310 --> 00:18:00,990
pointed directly at me.

311
00:18:00,990 --> 00:18:03,340
He blocked his nostrils with tissue paper

312
00:18:03,340 --> 00:18:05,020
in order not to smell the gas.

313
00:18:05,720 --> 00:18:07,950
I took a picture, I turned around, and ran away.

314
00:18:07,950 --> 00:18:12,480
The grenade was a few centimeters away from my head.

315
00:18:33,550 --> 00:18:36,810
What happened was a turn of events.

316
00:18:36,810 --> 00:18:39,420
Of course, it wasn't the first of its kind.,

317
00:18:39,420 --> 00:18:41,800
Like Ben Guerdane, and the bread riots,

318
00:18:41,800 --> 00:18:44,190
or January 26, or the uprising of the mining basin

319
00:18:44,190 --> 00:18:45,470
and many others

320
00:18:45,470 --> 00:18:46,430
that we didn’t hear of 

321
00:18:46,430 --> 00:18:48,000
because they must have been suppressed quickly.

322
00:18:48,000 --> 00:18:52,410
This is what made people seize the first opportunity

323
00:18:52,410 --> 00:18:53,930
to turn things around.

324
00:18:53,930 --> 00:18:56,920
And nothing says that this won't repeat itself.

325
00:18:56,920 --> 00:18:59,980
“Freedom for Tunisians.”

326
00:19:02,030 --> 00:19:04,700
I remember the lawyer’s video

327
00:19:04,700 --> 00:19:07,040
Abdennaceur Laouini.

328
00:19:11,290 --> 00:19:13,500
How can you forget it?

329
00:19:14,080 --> 00:19:16,730
Those moments 

330
00:19:16,730 --> 00:19:20,400
are inscribed in our collective memory.

331
00:19:20,400 --> 00:19:21,950
Fortunately, there are pictures.

332
00:19:22,990 --> 00:19:25,280
Maybe today we forget it,

333
00:19:25,280 --> 00:19:30,480
but at the time we got really upset

334
00:19:30,480 --> 00:19:33,850
because we saw pictures from Kasserine.

335
00:19:34,720 --> 00:19:37,840
Do you remember those people crying at the hospital?

336
00:19:37,840 --> 00:19:39,840
Imagine if these images had not existed.

337
00:19:41,580 --> 00:19:43,310
You see, there have been events before,

338
00:19:43,310 --> 00:19:46,300
in Ben Guerdane, in Redeyef,

339
00:19:46,300 --> 00:19:48,650
there weren't many pictures,

340
00:19:48,650 --> 00:19:50,240
no live streaming,

341
00:19:50,240 --> 00:19:52,010
not everyone had Facebook.

342
00:19:52,170 --> 00:19:54,860
They were images

343
00:19:54,860 --> 00:19:58,250
that made it possible to learn what was happening.

344
00:19:58,250 --> 00:20:01,420
Because the first thing a dictator does

345
00:20:02,560 --> 00:20:06,620
is control the distribution of images.

346
00:20:07,230 --> 00:20:08,620
If he controls images,

347
00:20:08,620 --> 00:20:11,280
he can show whatever he wants,

348
00:20:11,550 --> 00:20:14,560
say that people live very well in his country,

349
00:20:14,680 --> 00:20:16,510
that the economy is booming,

350
00:20:16,510 --> 00:20:17,790
that there is no pollution. 

351
00:20:17,790 --> 00:20:20,600
When we have the possibility to distribute images,

352
00:20:20,600 --> 00:20:24,040
we can show pollution, poverty

353
00:20:24,040 --> 00:20:26,970
inequalities, massacres.

354
00:20:26,970 --> 00:20:30,490
The dictator can no longer keep standing.

355
00:20:36,300 --> 00:20:39,280
I always say that 2011 is a romantic year,

356
00:20:39,280 --> 00:20:41,420
because outstanding things

357
00:20:41,420 --> 00:20:43,040
happened.

358
00:20:43,580 --> 00:20:45,680
For example, in front of the Kasbah,

359
00:20:45,680 --> 00:20:48,540
I took pictures of the smallest protest.

360
00:20:49,100 --> 00:20:52,440
A family, a mother, and her three children,

361
00:20:52,440 --> 00:20:53,740
holding signs,

362
00:20:53,740 --> 00:20:56,740
because the father was sick and had to return from Libya,

363
00:20:56,740 --> 00:21:00,420
the family no longer had any source of income.

364
00:21:01,080 --> 00:21:03,780
Sometimes we forget how far we have come.

365
00:21:03,780 --> 00:21:07,840
Before, in cafes,

366
00:21:07,840 --> 00:21:10,040
we could only speak of football,

367
00:21:10,040 --> 00:21:12,540
just saying that the price of milk

368
00:21:12,540 --> 00:21:14,740
increased by 200 millimes

369
00:21:14,740 --> 00:21:16,220
made people give you suspicious looks.

370
00:21:29,180 --> 00:21:31,820
I was able to have the freedom to post

371
00:21:31,820 --> 00:21:34,760
all of my pictures on my blog. 

372
00:21:35,120 --> 00:21:38,980
After a week, I felt that I was part of the world.

373
00:21:38,980 --> 00:21:42,020
Before that, it was not the case, we were hiding.

374
00:21:42,580 --> 00:21:44,380
Now we can't hide anything.

375
00:21:44,380 --> 00:21:45,960
When there’s an event,

376
00:21:45,960 --> 00:21:47,960
Everybody talks about it.

377
00:21:48,680 --> 00:21:51,050
It is an honor to have lived this moment.

378
00:21:51,050 --> 00:21:55,300
We can tell it as a story to future generations,

379
00:21:55,620 --> 00:21:58,960
like those who lived May 68 in France, for example.

380
00:21:58,960 --> 00:22:00,780
These are events to be experienced.

381
00:22:00,780 --> 00:22:03,040
There are stories that can be told to you,

382
00:22:03,040 --> 00:22:04,280
without you missing much,

383
00:22:04,280 --> 00:22:06,280
but there are events that you have to live through.

384
00:22:09,820 --> 00:22:11,080
We have learned many things.

385
00:22:11,080 --> 00:22:14,400
Before, we used to be afraid of politics.

386
00:22:14,400 --> 00:22:19,480
Political culture in Tunisia is now exceptional.

387
00:22:19,480 --> 00:22:22,180
Tunisians follow the parliament, deputies

388
00:22:22,180 --> 00:22:24,880
speak of immunity, of the Constitution,

389
00:22:24,880 --> 00:22:27,140
we have grown enormously in 10 years,

390
00:22:27,140 --> 00:22:30,500
including children, who hear it all.

391
00:22:30,500 --> 00:22:34,840
Before, we only used to talk about football, Valentine’s Day,

392
00:22:34,840 --> 00:22:37,240
about Aïd, buying sheep and sweets,

393
00:22:37,240 --> 00:22:38,100
and that’s it.

394
00:22:38,100 --> 00:22:40,760
Now, all of that has become an accessory.

395
00:22:40,760 --> 00:22:43,980
Politics are the most important to us now,

396
00:22:44,600 --> 00:22:46,560
because it is what affects our lives the most.

397
00:22:48,560 --> 00:22:51,100
We became adults overnight.

398
00:23:04,180 --> 00:23:06,380
Where were you on January 14?

399
00:23:06,380 --> 00:23:08,980
Episode 2 : The View from Above

400
00:23:09,320 --> 00:23:10,680
Production

401
00:23:10,680 --> 00:23:12,420
Inkyfada

402
00:23:16,980 --> 00:23:20,180
Directed, edited, and mixed by

403
00:23:20,180 --> 00:23:23,380
Bochra Triki, Hazar Abidi

404
00:23:23,380 --> 00:23:26,360
Monia Ben Hamadi, Yassine Kawana

405
00:23:27,600 --> 00:23:28,960
Music

406
00:23:28,960 --> 00:23:29,840
Oussema Gaidi

407
00:23:30,600 --> 00:23:31,980
Voice-over

408
00:23:31,980 --> 00:23:33,400
Hazar Abidi

409
00:23:34,390 --> 00:23:36,820
With the collaboration of the entire Inkyfada team
