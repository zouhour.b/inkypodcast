1
00:00:03,020 --> 00:00:07,950
Cette confrontation... quand il casse mon téléphone et que je le reprends,

2
00:00:07,950 --> 00:00:13,200
pour moi c'est une manière de lui dire : "je résiste, tu ne vas pas m'intimider,

3
00:00:13,200 --> 00:00:17,510
peu importe ce que tu peux me faire".

4
00:00:17,510 --> 00:00:22,710
C'est ça qui m'a marquée, c'était génial de voir les jeunes dans la rue défier la police.

5
00:00:22,710 --> 00:00:25,150
J'ai le sourire en m'en rappelant.

6
00:00:25,150 --> 00:00:27,150
C'est une image que je n'oublierai jamais.

7
00:01:13,150 --> 00:01:14,800
14 Janvier 2011,

8
00:01:14,800 --> 00:01:18,930
Bakhta, militante féministe et syndicaliste, descend à la place Mohamed-Ali.

9
00:01:18,930 --> 00:01:20,930
Elle est avec sa famille et ses ami·es

10
00:01:20,930 --> 00:01:23,730
Ils et elles vont manifester contre le système de Ben Ali.

11
00:01:23,730 --> 00:01:27,460
Avec la violence policière et la foule, ils et elles se perdent de vue.

12
00:01:27,640 --> 00:01:31,680
A partir de ce moment, chacun·e vit un scénario différent.

13
00:01:32,400 --> 00:01:36,660
Bakhta se retrouve dans un appartement et, au même moment, pas très loin d'elle,

14
00:01:36,660 --> 00:01:40,000
sa fille Haïfa se réfugie dans un autre.

15
00:01:40,350 --> 00:01:42,710
Deux scénarios qui commencent de la même manière,

16
00:01:42,710 --> 00:01:46,570
mais l'un d'eux évolue paisiblement, dans des discussions avec les hôtes,

17
00:01:46,570 --> 00:01:50,040
tandis que dans l'autre, on retrouve tous les ingrédients du 14 janvier :

18
00:01:50,040 --> 00:01:52,040
police, matraques, gaz lacrymogènes...

19
00:01:52,350 --> 00:01:57,240
et un mélange de peur et de joie, avec le sentiment de vivre des moments exceptionnels.

20
00:01:57,820 --> 00:02:00,000
Tu étais où le 14 Janvier ?

21
00:02:00,000 --> 00:02:03,150
Épisode 5 : Portes Ouvertes

22
00:02:07,110 --> 00:02:15,060
Ma mère est syndicaliste et membre de l'Association des femmes démocrates.

23
00:02:15,060 --> 00:02:17,060
Mon père était opposant.

24
00:02:17,060 --> 00:02:25,330
On a vécu, particulièrement les 2 ou 3 années qui ont précédé, avec les policiers devant la maison...

25
00:02:25,330 --> 00:02:31,770
Deux policiers, dans leur voiture. Ils pouvaient même m'accompagner quand j'allais prendre un café au centre-ville,

26
00:02:31,770 --> 00:02:33,770
ils me suivaient.

27
00:02:33,770 --> 00:02:37,690
Je ne sais pas qui ils pensaient que j'allais rencontrer.

28
00:02:37,690 --> 00:02:41,600
Je leur ai un jour demandé : "vous n'avez que ça à faire de me suivre ?"

29
00:02:41,860 --> 00:02:47,150
J'avais 14 ans quand j'ai participé à ma première manifestation, j'habitais à Sfax.

30
00:02:47,150 --> 00:02:49,150
C'était en soutien à la Palestine, en 1967.

31
00:02:49,150 --> 00:02:54,790
Depuis, j'ai toujours participé à ces mouvements... avril 1972, etc.

32
00:02:54,790 --> 00:03:00,260
J'étais de toutes les manifestations. Même les plus petites.

33
00:03:00,260 --> 00:03:07,020
Je suis plus pour l'action dans les rues, pas uniquement dans les communiqués.

34
00:03:07,020 --> 00:03:13,020
Les jours qui ont précédé, on allait toujours participer aux tentatives de manifestations.

35
00:03:13,020 --> 00:03:21,680
Le 9 janvier... le 11, celle des artistes et des blogueurs, puis devant l'UGTT.

36
00:03:21,680 --> 00:03:25,240
On sent que ça bouge.

37
00:03:25,460 --> 00:03:29,820
Il n'y a plus seulement des blogueurs ou certaines associations,

38
00:03:29,820 --> 00:03:33,510
mais beaucoup de gens prennent conscience de ce qui se passe.

39
00:03:33,510 --> 00:03:36,750
On attend une manifestation importante à Tunis.

40
00:03:36,750 --> 00:03:40,880
La majorité des manifestations ont été sévèrement réprimées.

41
00:03:40,880 --> 00:03:43,820
Ce n'est pas facile.

42
00:03:43,820 --> 00:03:49,550
Donc le fait que l'UGTT appelle à manifester, c'est très important.

43
00:03:49,550 --> 00:03:54,260
Le départ de Ben Ali est quelque chose de presque inimaginable,

44
00:03:54,260 --> 00:03:58,080
mais on a confiance.

45
00:03:59,200 --> 00:04:02,970
Le 14 janvier, je me réveille à 6h du matin.

46
00:04:02,970 --> 00:04:04,570
J'ai une boule au ventre.

47
00:04:04,570 --> 00:04:10,080
Je suis très enthousiaste mais en même temps j'ai peur de ce qui peut se passer.

48
00:04:10,080 --> 00:04:16,440
Qu'est-ce que la manifestation va donner ? J'ai peur qu'on ne soit pas nombreux·ses.

49
00:04:16,440 --> 00:04:21,730
J'ai une boule au ventre, mais en même temps je me dis : "c'est notre journée".

50
00:04:21,730 --> 00:04:27,730
Je me réveille le matin, Amel, Nada et Linda sont venues à la maison.

51
00:04:27,730 --> 00:04:29,420
On part ensemble dans la même voiture.

52
00:04:29,420 --> 00:04:31,420
Ma mère conduit.

53
00:04:31,420 --> 00:04:36,530
On fait deux voyages parce qu'on n'a pas d'autre voiture à disposition

54
00:04:36,530 --> 00:04:39,280
et on arrive sur la place Mohamed-Ali.

55
00:04:39,910 --> 00:04:44,170
"De mon âme, de mon sang, je t'honore ô martyr"

56
00:04:54,970 --> 00:04:59,110
Il y a, avec moi, presque toute la famille, des ami·es

57
00:04:59,110 --> 00:05:03,420
et beaucoup de gens sont présents dès le matin.

58
00:05:05,510 --> 00:05:10,000
Il y a un espoir d'arriver à quelque chose.

59
00:05:10,000 --> 00:05:13,280
Des personnes que je n'ai pas vues depuis un an ou deux sont là.

60
00:05:13,280 --> 00:05:18,480
On se retrouve sur la place Mohamed-Ali.

61
00:05:18,480 --> 00:05:22,570
On a tous et toutes la même sensation de peur,

62
00:05:22,840 --> 00:05:27,020
mais en même temps, on n'a plus rien à perdre.

63
00:05:27,020 --> 00:05:29,770
Aujourd'hui, il faut qu'il se passe quelque chose.

64
00:05:29,770 --> 00:05:31,770
Ça ne peut pas passer comme ça.

65
00:05:31,770 --> 00:05:38,750
On prie pour qu'il y ait le maximum de monde, pour que ça puisse avoir un impact,

66
00:05:38,750 --> 00:05:43,730
comme la manifestation de Sfax, deux ou trois jours plus tôt.

67
00:05:43,730 --> 00:05:46,930
C'est ce qui a fait que Tunis a bougé.

68
00:05:46,930 --> 00:05:55,110
"Ben Ali et les Trabelsi, un procès populaire..."

69
00:05:56,530 --> 00:06:01,860
Vers 10h, on commence à partir de la place Mohamed-Ali,

70
00:06:01,860 --> 00:06:03,860
on est tous·tes ensemble.

71
00:06:03,860 --> 00:06:09,200
Ma mère a trouvé ses amies de l'Association des femmes démocrates.

72
00:06:09,200 --> 00:06:12,570
On avance, on s'arrête,

73
00:06:12,570 --> 00:06:20,000
puis à un moment, voir les gens se diriger vers le ministère de l'Intérieur, nous encourage.

74
00:06:20,000 --> 00:06:25,730
Ma peur s'envole, je ne me sens pas seule.

75
00:06:25,730 --> 00:06:30,310
Je suis avec mes ami·es, mes compagnon·nes de route et on est nombreux·ses.

76
00:06:30,310 --> 00:06:32,310
On va dans le bon sens.

77
00:06:32,310 --> 00:06:41,680
À ce moment-là, je n'ai aucune idée de ce qui allait arriver.

78
00:06:41,680 --> 00:06:48,220
Mais on est là, on est debout, avec un seul objectif, que Ben Ali s'en aille.

79
00:06:48,220 --> 00:07:04,310
"Le peuple veut la chute de Ben Ali..."

80
00:07:04,880 --> 00:07:07,020
Petit à petit, beaucoup de nos ami·es nous rejoignent.

81
00:07:07,020 --> 00:07:09,600
On se rassemble au niveau du ministère de l'Intérieur.

82
00:07:09,600 --> 00:07:17,770
C'est impressionnant, d'un seul coup, tout le monde crie "dégage".

83
00:07:17,770 --> 00:07:24,260
Je pleure, je ne me suis pas imaginée vivre ce moment.

84
00:07:24,260 --> 00:07:27,420
Ça me paraissait impossible.

85
00:07:27,420 --> 00:07:33,280
À un moment, je ne sens plus mes jambes

86
00:07:33,280 --> 00:07:37,280
parce qu'on a quand même vécu une époque où on surveille toujours nos arrières.

87
00:07:37,280 --> 00:07:41,950
À la moindre parole, au moindre mouvement, on a peur.

88
00:07:41,950 --> 00:07:51,820
Si on a tous·tes ce courage, ce jour-là, c'est parce que tout le monde a la rage.

89
00:07:51,820 --> 00:07:53,820
Pas seulement moi.

90
00:07:53,820 --> 00:08:00,080
Ma colère est particulière, à cause de mon vécu, mais c'est impressionnant pour moi.

91
00:08:03,350 --> 00:08:09,310
À un moment, on voit des personnes intrigantes,

92
00:08:09,310 --> 00:08:11,310
elles ne scandent pas de slogans comme nous.

93
00:08:11,310 --> 00:08:14,370
Il doit être midi ou 13h.

94
00:08:14,370 --> 00:08:17,260
Elles se préparent à quelque chose.

95
00:08:17,260 --> 00:08:24,460
Ce n'était pas remarquable mais après, c'est elles qui ont commencé à nous tabasser.

96
00:08:24,460 --> 00:08:31,660
Le cortège funèbre de celui qui a été tué la veille passe alors par là. Presque devant l'Intérieur.

97
00:08:31,660 --> 00:08:33,110
Juste après, l'ambiance change.

98
00:08:33,280 --> 00:08:37,460
Les policiers commencent à courir, à crier, à frapper. Surtout ceux en civil.

99
00:08:37,770 --> 00:08:42,040
C'est eux qui commencent, c'est eux qui sont très violent.

100
00:08:42,260 --> 00:08:45,820
Et bien sûr, il y a les bombes lacrymogènes, on ne respire plus.

101
00:09:03,510 --> 00:09:09,650
J'entends comme un bruit de balles, ils tirent des bombes lacrymogènes.

102
00:09:09,650 --> 00:09:12,400
Puis une foule de gens arrive dans notre direction.

103
00:09:12,400 --> 00:09:16,580
Une marée humaine.

104
00:09:16,940 --> 00:09:21,470
Je me souviens avoir tenu la main de Nada, pour qu'on ne s'éloigne pas l'une de l'autre.

105
00:09:21,470 --> 00:09:28,180
La foule nous emporte, nos pieds ne touchent presque plus le sol.

106
00:09:29,070 --> 00:09:32,980
Quand les gens se sont mis à courir, je ne voulais pas qu'ils s'écrasent.

107
00:09:32,980 --> 00:09:36,710
J'essaie de ne pas courir, donc je perds tout de suite les autres,

108
00:09:36,710 --> 00:09:40,890
même mes ami·es, on va chacun·e dans une direction. C'est là qu'ils commencent à frapper.

109
00:09:40,890 --> 00:09:42,890
Comme j'ai tout un groupe avec moi,

110
00:09:42,890 --> 00:09:51,910
j'essaie de retourner là où j'étais mais ils deviennent plus violents,

111
00:09:51,910 --> 00:09:56,360
ils nous repoussent vers la rue Kamel Ataturk qui est très étroite.

112
00:09:56,360 --> 00:10:01,600
Ils nous barrent la route avec leurs bombes lacrymogènes, donc on entre dans un immeuble.

113
00:10:07,600 --> 00:10:13,910
Dans ce mouvement de foule, je retrouve Slim Abida

114
00:10:13,910 --> 00:10:18,270
que je n'ai pas vu de la journée, on était chacun·e de son côté.

115
00:10:18,270 --> 00:10:20,800
Quand on a été bousculé·es, il est arrivé à mon niveau

116
00:10:21,030 --> 00:10:25,830
et il nous a tirées Nada et moi vers le mur, pour ne plus être être bousculées.

117
00:10:25,830 --> 00:10:30,090
À un moment, on est repoussé·es à l'intérieur de l'immeuble du bar des journalistes.

118
00:10:30,090 --> 00:10:33,510
Il y a deux portes. Nous entrons par celle de gauche.

119
00:10:33,510 --> 00:10:36,760
On est une dizaine.

120
00:10:36,760 --> 00:10:39,380
Quelqu'un referme la porte de l'immeuble.

121
00:10:39,380 --> 00:10:44,800
Les policiers continuent de frapper, ils ne peuvent pas nous poursuivre à l'intérieur.

122
00:10:44,800 --> 00:10:46,800
On reste là.

123
00:10:48,270 --> 00:10:52,530
On voit un peu les policiers par la fenêtre, qui restent postés à l'extérieur.

124
00:10:52,710 --> 00:10:55,420
Ils veulent accéder à la cage d'escalier, mais ne réussissent pas.

125
00:10:55,550 --> 00:11:00,570
Par contre, ils parviennent à entrer dans le bloc d'en face.

126
00:11:00,570 --> 00:11:05,460
Il paraît qu'à l'intérieur il y a un flic qui leur aurait permis de s'introduire.

127
00:11:05,460 --> 00:11:07,460
C'est peut-être lui qui leur a ouvert la porte.

128
00:11:07,460 --> 00:11:14,080
Mais là où on est, le gardien a fermé la porte et ne les a pas laissés passer.

129
00:11:16,080 --> 00:11:21,420
On étouffe à l'intérieur, on peut à peine respirer.

130
00:11:21,420 --> 00:11:25,020
On est confiant·es, on n'a pas l'impression qu'il y ait des policiers parmi nous.

131
00:11:25,020 --> 00:11:30,040
On se parle, on essaye de se soutenir et de voir comment s'en sortir.

132
00:11:30,040 --> 00:11:35,420
Certain·es montent tout en haut, pour voir ce qui se passe dehors

133
00:11:35,420 --> 00:11:37,420
et nous dire si on peut ressortir ou pas.

134
00:11:37,420 --> 00:11:41,280
Finalement, on reste sur place.

135
00:11:42,000 --> 00:11:45,280
Dans chaque appartement, il y a des gens réfugié·es.
Certain·es ont été accueilli·es au tout début.

136
00:11:45,280 --> 00:11:49,420
Moi et le jeune qui m'accompagne, sommes entré·es dans un des logements.

137
00:11:49,420 --> 00:11:55,330
Nos hôtes nous offrent à boire et nous proposent de nous reposer :
"dans tous les cas, vous ne pouvez pas sortir".

138
00:11:55,330 --> 00:12:03,370
Effectivement, il est trop tard pour repartir et les policiers rôdent encore aux alentours.

139
00:12:03,370 --> 00:12:05,370
Ils ne bougent presque pas.

140
00:12:05,370 --> 00:12:08,170
Ils vont et viennent, ils insultent, ils crient.

141
00:12:14,130 --> 00:12:17,330
Des gens descendent les escaliers,

142
00:12:17,330 --> 00:12:22,310
d'autres entrent dans l'immeuble et les policiers jettent leurs bombes lacrymogènes à l'intérieur.

143
00:12:22,310 --> 00:12:23,910
On étouffe.

144
00:12:23,910 --> 00:12:25,910
On est obligé·es de monter les escaliers.

145
00:12:25,910 --> 00:12:36,040
Arrivé·es au 1er ou au 2ème étage, on trouve une porte ouverte à droite, celle d'un appartement.

146
00:12:36,040 --> 00:12:38,040
Plusieurs personnes s'y introduisent,

147
00:12:38,350 --> 00:12:41,330
donc Slim propose qu'on y aille aussi.

148
00:12:41,330 --> 00:12:43,330
Entretemps, on a perdu tout le monde.

149
00:12:43,330 --> 00:12:48,260
On se retrouve seules avec Nada, accompagnées de Slim Abida.

150
00:12:48,260 --> 00:12:51,860
On doit être une quarantaine dans cet appartement.

151
00:12:51,860 --> 00:12:55,420
Dans l'immeuble, il y a deux ou trois appartements d'habitation,

152
00:12:55,420 --> 00:12:57,770
le bar des journalistes, et le reste, ce sont des bureaux.

153
00:12:57,770 --> 00:13:05,240
On est dans un grand bureau, avec deux grandes pièces et une quarantaine de personnes à l'intérieur.

154
00:13:05,240 --> 00:13:10,530
On s'assoit, Nada, Slim et moi, l'un·e à côté de l'autre.

155
00:13:10,530 --> 00:13:13,290
La première chose que je fais, c’est d'essayer de joindre ma mère.

156
00:13:13,290 --> 00:13:16,490
Nada, de son côté, réussit à joindre mon frère Youssef.

157
00:13:16,490 --> 00:13:24,530
Il dit qu'il nous attend dans la voiture entre la rue du Caire et Central Park, si on réussit à s'échapper de l'immeuble,

158
00:13:25,060 --> 00:13:29,860
mais que dehors c'est le chaos : gaz lacrymogènes, policiers qui frappent...

159
00:13:29,860 --> 00:13:36,440
On entend les déflagrations, mais on n'arrive même pas à voir par la fenêtre ce qui se passe à l'extérieur.

160
00:13:36,570 --> 00:13:38,930
On n'entend que des cris et des hurlements.

161
00:13:43,130 --> 00:13:46,550
La famille qui nous accueille n'est pas politisée.

162
00:13:46,550 --> 00:13:52,200
Les jeunes filles sont tout le temps sur Facebook et ont une idée de ce qui se passe,

163
00:13:52,200 --> 00:13:55,750
mais elles ne restent avec nous, je ne sais pas pourquoi.

164
00:13:55,750 --> 00:14:01,710
Il y a les parents, assez vieux, d'une gentillesse incroyable.

165
00:14:01,710 --> 00:14:08,600
On reste discuter avec le couple. La femme a préparé le dîner.

166
00:14:08,600 --> 00:14:13,490
Ce sont des personnes généreuses.

167
00:14:13,490 --> 00:14:16,110
Je suis souvent allée leur rendre visite depuis.

168
00:14:16,110 --> 00:14:18,820
Malheureusement, la femme est décédée.

169
00:14:31,840 --> 00:14:39,930
Quelques chaises sont disposées face à la porte d'entrée,

170
00:14:39,930 --> 00:14:42,460
on s'assoit là, Nada, Slim et moi.

171
00:14:42,460 --> 00:14:48,020
Je suis au milieu, Nada du côté de la porte et Slim à ma gauche.

172
00:14:48,020 --> 00:14:53,400
J'essaie d'envoyer un message à ma mère pour lui dire :

173
00:14:53,400 --> 00:15:00,200
fais moi signe quand tu reçois mon message, je suis coincée dans tel immeuble.

174
00:15:00,200 --> 00:15:04,550
25 minutes plus tard, la porte de l'appartement vole en éclats.

175
00:15:04,820 --> 00:15:13,890
Une armée de policiers fait irruption, avec des bâtons, en civil ou en uniforme, avec des matraques...

176
00:15:13,890 --> 00:15:16,510
ils insultent, crient.

177
00:15:16,510 --> 00:15:24,600
Un policier tire Nada pour qu'elle se relève et fait tomber mon téléphone à terre.

178
00:15:24,600 --> 00:15:32,200
Je récupère quand même le téléphone. Jusqu'à ce jour, je conserve son 'cadavre'.

179
00:15:32,200 --> 00:15:36,020
Ils nous disent : "levez-vous. Les filles vont sortir et les garçons vont rester".

180
00:15:36,020 --> 00:15:40,290
Ils nous poussent vers la porte, mais je ne veux pas sortir alors que Slim reste.

181
00:15:40,290 --> 00:15:45,970
Je me souviens l'avoir regardé, je ne peux rien dire. Ils ne me laissent pas le temps de lui parler.

182
00:15:45,970 --> 00:15:50,150
Je l'entends juste dire : "Haifa, fais attention à toi".

183
00:15:50,150 --> 00:15:55,440
Je sors à reculons, je ne sais pas ce qui va arriver à Slim,

184
00:15:55,440 --> 00:15:57,440
avec leur sauvagerie.

185
00:15:57,440 --> 00:16:04,420
Le long des escaliers, un policier à chaque marche nous frappe avec sa matraque.

186
00:16:04,420 --> 00:16:07,840
Je ne sais pas comment on va arriver à l'entrée de l'immeuble.

187
00:16:14,110 --> 00:16:22,690
Dès qu'on sort de l'immeuble des policiers lancent des bombes lacrymogènes.

188
00:16:22,690 --> 00:16:29,620
On court. Comme mon frère nous a dit qu'il nous attendait, on espère pouvoir le retrouver.

189
00:16:29,620 --> 00:16:33,970
On se précipite dans la rue du Caire.

190
00:16:33,970 --> 00:16:38,950
Là, on croise un camarade de classe que je n'ai pas vu depuis longtemps.

191
00:16:38,950 --> 00:16:42,770
Il est très grand de taille. Il me demande où je vais. Je lui dis : "au parking, là-bas".

192
00:16:42,770 --> 00:16:47,440
Il nous prend, Nada et moi, chacune d'un côté, et on s'enfuit.

193
00:16:47,440 --> 00:16:53,890
Des pneus brûlent au milieu de la rue et les policiers arrivent derrière nous,

194
00:16:53,890 --> 00:16:56,860
en lançant des bombes lacrymogènes sur les gens.

195
00:16:56,860 --> 00:16:59,840
Les policiers se dirigent vers l'endroit où on doit se aller.

196
00:16:59,840 --> 00:17:03,350
Il faut juste qu'on arrive à la voiture, je ne sais pas comment...

197
00:17:03,350 --> 00:17:05,530
on court comme on peut.

198
00:17:05,530 --> 00:17:10,550
Heureusement, on retrouve mon frère dans le parking, il nous a attendues.

199
00:17:19,490 --> 00:17:23,570
En me retournant, je vois la fumée monter de l'avenue Mohamed V

200
00:17:23,570 --> 00:17:27,570
et la rue en face est pleine de pneus en feu.

201
00:17:27,570 --> 00:17:31,890
Dès que je monte dans la voiture, je demande où sont les parents.

202
00:17:31,890 --> 00:17:35,310
Mon frère répond que ma mère est dans un appartement, chez une famille.

203
00:17:35,310 --> 00:17:41,090
Ça me rassure. Même si, à cette époque, on ne sait jamais sur qui on peut tomber.

204
00:17:41,090 --> 00:17:44,600
Tout le monde n'a pas un problème avec Ben Ali.

205
00:17:44,600 --> 00:17:49,490
Il y a toujours cette paranoïa que les gens soient 'de mèche'

206
00:17:49,490 --> 00:17:57,530
ou qu'ils n'aient aucun problème avec le maintien de Ben Ali et fassent sortir les gens, malgré le danger.

207
00:17:57,530 --> 00:18:04,910
Je ne parviens à joindre ma mère au téléphone, qu'en arrivant à la maison.

208
00:18:04,910 --> 00:18:08,460
Il devait être 16h30 ou 17h.

209
00:18:08,460 --> 00:18:15,170
Quand je l'appelle, elle me dit qu'elle compte passer la nuit là-bas, de ne pas nous inquiéter, c'est une bonne famille.

210
00:18:19,170 --> 00:18:21,090
On reste suivre les informations.

211
00:18:21,090 --> 00:18:29,090
Le couple connaît la situation mais s'étonne du fait que les gens soient sortis dans la rue,
qu'ils n'aient pas peur.

212
00:18:29,090 --> 00:18:32,770
On me dit : "comment tu peux laisser tes enfants, etc.".

213
00:18:32,770 --> 00:18:37,570
Ça commence comme ça, mais finalement il et elle sont récéptif·ves

214
00:18:37,570 --> 00:18:42,730
On continue de discuter et au final on est d'accord.

215
00:18:44,730 --> 00:18:52,460
Avec tout ça, je suis malade et je culpabilise pour Slim.

216
00:18:53,170 --> 00:18:55,750
Je me dis que j'aurais dû rester avec lui.

217
00:18:55,750 --> 00:18:59,890
J'essaie de l'appeler, il est injoignable.

218
00:18:59,890 --> 00:19:05,970
Je commence à angoisser à ce moment-là, à partir de 17h.

219
00:19:05,970 --> 00:19:08,510
Je n'en dors pas.

220
00:19:08,510 --> 00:19:13,930
Je passe la nuit à chercher Slim, on appelle même les hôpitaux.

221
00:19:13,930 --> 00:19:16,950
On appelle aussi ma mère de temps à autres,

222
00:19:16,950 --> 00:19:20,370
elle va bien et est entre de bonnes mains.

223
00:19:20,370 --> 00:19:23,040
Quand j'apprends la fuite de Ben Ali,

224
00:19:23,040 --> 00:19:29,710
je veux en parler avec elle. Elle me répond qu'on pourra en discuter plus tard.

225
00:19:29,710 --> 00:19:36,150
J'aurais voulu qu'elle soit à mes côtés à ce moment-là.

226
00:19:41,090 --> 00:19:46,770
L'information de la fuite de la famille de Ben Ali commence à circuler.

227
00:19:46,770 --> 00:19:50,770
C'est un soulagement.

228
00:19:50,770 --> 00:19:56,290
Pour nous, c'est quelque chose dont on ne peut même pas rêver.

229
00:19:56,290 --> 00:20:05,440
On aurait pu penser que Ben Ali, avec son pouvoir et son régime, allait résister.

230
00:20:06,020 --> 00:20:11,710
C'est une très belle sensation.

231
00:20:11,710 --> 00:20:13,710
On ne dort pas, évidemment.

232
00:20:13,710 --> 00:20:22,820
On entend les avions, les hélicoptères, des gens qui courent... il n'y a pas de répit.

233
00:20:22,820 --> 00:20:27,000
Au milieu de la nuit, ça se calme un peu. Mais on ne sait pas ce qui se passe dehors.

234
00:20:27,000 --> 00:20:31,400
Dans la maison où je suis, il n'y a pas de fenêtre donnant sur la rue

235
00:20:31,400 --> 00:20:34,420
mais le mouvement des policiers se poursuit toute la nuit.

236
00:20:34,420 --> 00:20:35,970
Sauf peut-être à l'aube.

237
00:20:35,970 --> 00:20:41,710
On n'a pas dormi et le couvre-feu doit être levé à 6h. On sort directement.

238
00:20:41,710 --> 00:20:48,550
Dehors, on retrouve un champ de bataille, les traces des jets de pierre et de la violence.

239
00:20:48,550 --> 00:20:53,040
Tout est resté là, les cartouches de gaz lacrymogènes...

240
00:20:53,620 --> 00:21:01,000
Ma mère rentre le matin, elle est contente d'avoir passé la nuit dehors.

241
00:21:01,000 --> 00:21:07,170
Moi je vais chez le procureur pour voir si Slim Abida est encore vivant.

242
00:21:07,170 --> 00:21:09,890
J'y vais avec mon père.

243
00:21:09,890 --> 00:21:16,370
Le procureur dit qu'il n'a aucune information.

244
00:21:16,370 --> 00:21:23,090
Vers 11h30 ou midi, Slim m'appelle pour me dire qu'il a passé la nuit à l'Intérieur et qu'il vient d'en sortir.

245
00:21:49,090 --> 00:21:52,910
Cette journée nous a donné·es beaucoup d'espoir

246
00:21:52,910 --> 00:21:59,750
Mais, par la suite, ça a été avorté, c'est ce qui fait un peu mal.

247
00:21:59,750 --> 00:22:04,770
Malgré tout, il y a un sentiment de délivrance.

248
00:22:04,770 --> 00:22:11,350
Le fait que le régime tombe de cette manière, même s'il n'est pas entièrement tombé, a révélé sa faiblesse.

249
00:22:11,350 --> 00:22:17,800
Si on avait été plus organisé·es, on aurait poursuivi le combat.

250
00:22:17,800 --> 00:22:20,330
Il y avait tout pour réussir, ce jour-là.

251
00:22:20,330 --> 00:22:26,240
Avec ce qui se passait dans les régions... on aurait pu obtenir de vrais changements.

252
00:22:28,820 --> 00:22:37,040
Depuis ses débuts, ce soulèvement avait une importance pour moi,

253
00:22:37,040 --> 00:22:42,200
parce qu'il peut me libérer de cette pression du policier qui me surveille,

254
00:22:42,200 --> 00:22:51,000
qui connaît mon quotidien, en m'écoutant au téléphone, en se postant devant la maison.

255
00:22:51,000 --> 00:22:54,200
Il sait ce que je porte le matin, où je vais...

256
00:22:54,200 --> 00:22:56,200
pareil pour mes parents.

257
00:22:56,200 --> 00:23:00,420
Pendant les trois dernières années, la pression a augmenté,

258
00:23:00,420 --> 00:23:03,660
mais je vis ça depuis ma naissance.

259
00:23:05,660 --> 00:23:11,350
Mon père a souffert pendant des années, dans son travail, etc.

260
00:23:11,350 --> 00:23:21,930
La fuite de Ben Ali va laisser un goût d'inachevé, parce qu'il n'a pas été jugé.

261
00:23:22,110 --> 00:23:25,130
On n'a pas totalement recouvré notre dignité.

262
00:23:28,550 --> 00:23:32,820
Les gens se souviendront qu'un jour,

263
00:23:32,820 --> 00:23:39,440
un peuple est descendu dans la rue pour dire non à une dictature, non à un système.

264
00:23:39,440 --> 00:23:44,770
Si on pouvait refaire le 14 janvier, avec d'autres choses certes...

265
00:23:44,770 --> 00:23:46,770
mais c'est une chance d'avoir vécu cette journée.

266
00:24:00,770 --> 00:24:02,770
Tu étais où le 14 janvier ?

267
00:24:02,770 --> 00:24:05,570
Épisode 5 : Portes ouvertes

268
00:24:05,570 --> 00:24:07,570
Production inkyfada podcasts

269
00:24:10,640 --> 00:24:14,640
Réalisation : Bochra Triki et Monia Ben Hamadi

270
00:24:14,640 --> 00:24:19,220
Montage, habillage sonore et mixage : Oussema Gaidi

271
00:24:19,220 --> 00:24:22,950
avec la collaboration de toute l'équipe d'inkyfada.
