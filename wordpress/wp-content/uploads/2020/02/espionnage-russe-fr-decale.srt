﻿1
00:00:06,840 --> 00:00:08,360
InkyStories

2
00:00:08,360 --> 00:00:10,360
Articles, reportages

3
00:00:10,360 --> 00:00:12,360
et enquêtes sonores

4
00:00:21,520 --> 00:00:24,480
Chroniques d'une opération d'espionnage russe en Tunisie

5
00:00:26,480 --> 00:00:28,480
Par

6
00:00:28,480 --> 00:00:30,480
Monia Ben Hamadi

7
00:00:30,480 --> 00:00:31,680
Lues par

8
00:00:31,680 --> 00:00:33,680
Majd Mastoura

9
00:00:50,240 --> 00:00:52,120
Mai 2015,

10
00:00:52,120 --> 00:00:55,200
vers 18h devant l'église à Bizerte.

11
00:00:55,800 --> 00:00:59,800
Karim attend Samia qui lui a demandé de lui rendre deux registres de la municipalité.

12
00:01:00,400 --> 00:01:02,320
Samia est bien sur les lieux.

13
00:01:02,320 --> 00:01:04,320
“C’est lui Karim, le Russe”

14
00:01:06,320 --> 00:01:09,320
indique-t-elle aux quatre policiers qui l’entourent.

15
00:01:09,320 --> 00:01:14,320
Une opération d’espionnage russe vient de prendre fin.

16
00:01:34,320 --> 00:01:36,800
Quatre employé·es de municipalités différentes,

17
00:01:37,000 --> 00:01:38,760
qui ne se connaissent pas,

18
00:01:38,760 --> 00:01:42,080
ont été condamné·es à des peines de prison ferme allant de 8 à 15 ans.

19
00:01:42,200 --> 00:01:44,720
Ils et elle ne s’imaginaient pas que leur vie allait basculer

20
00:01:44,720 --> 00:01:49,200
au moment où leurs chemins ont croisé celui de Mikhaïl Salikov et Kyamran Rasim Ogly Ragimov

21
00:01:49,200 --> 00:01:50,640
- dit Karim -

22
00:01:50,640 --> 00:01:53,320
diplomates et espions russes.

23
00:01:53,320 --> 00:01:55,320
Pendant cinq années,

24
00:01:55,320 --> 00:01:57,320
de 2010 à 2015,

25
00:01:57,320 --> 00:02:00,760
les deux espions ont réussi à obtenir un nombre incalculable

26
00:02:00,760 --> 00:02:02,760
de documents officiels vierges

27
00:02:02,760 --> 00:02:04,760
ou relatifs à des personnes tierces,

28
00:02:04,760 --> 00:02:08,640
grâce à la collaboration des fonctionnaires tunisien·nes.

29
00:02:09,120 --> 00:02:12,680
Ils et elle leur ont fourni des registres de l’état civil,

30
00:02:12,680 --> 00:02:16,760
des extraits de naissance, de décès, actes de mariage ou de divorce de Tunisien·nes et d’étranger·es.

31
00:02:16,760 --> 00:02:21,680
Ils ont également pu se munir de documents administratifs vierges, livrets de famille vides

32
00:02:21,680 --> 00:02:24,920
ou de feuilles blanches certifiant que “la copie est conforme à l’original”.

33
00:02:25,840 --> 00:02:28,040
Quelques semaines avant la mise en échec de l’opération,

34
00:02:28,040 --> 00:02:32,320
ils parviennent à obtenir un extrait de naissance au nom de Meriem Ben Idriss,

35
00:02:32,320 --> 00:02:34,320
née en 1986.

36
00:02:35,000 --> 00:02:36,960
Il s’avèrera au fil de l’instruction judiciaire

37
00:02:36,960 --> 00:02:39,800
que cette femme n’a jamais existé.

38
00:02:40,080 --> 00:02:42,440
Il s’agissait d'une “légende” civile,

39
00:02:42,440 --> 00:02:46,480
probablement créée pour servir d’identité factice à une “Illegal”,

40
00:02:46,480 --> 00:02:48,480
une “espionne clandestine”,

41
00:02:48,480 --> 00:02:52,560
une technique d’espionnage dont les services de renseignements russes sont friands.

42
00:02:53,200 --> 00:02:54,560
Pour parvenir à leurs fins,

43
00:02:54,560 --> 00:02:57,680
Mikhaïl et Karim ont noué une relation personnelle avec leurs proies,

44
00:02:57,680 --> 00:02:59,480
de manière plus ou moins continue.

45
00:02:59,480 --> 00:03:02,240
Ils s’informent sur les procédures administratives,

46
00:03:02,240 --> 00:03:06,200
entre un café dans un hôtel de Bizerte ou un dîner à La Marsa.

47
00:03:06,200 --> 00:03:08,200
Ils demandaient des documents spécifiques,

48
00:03:08,200 --> 00:03:10,560
recherchaient des personnes en particulier.

49
00:03:11,960 --> 00:03:13,200
Après leur arrestation,

50
00:03:13,200 --> 00:03:16,280
les employé·es ont révélé de nombreuses informations.

51
00:03:16,280 --> 00:03:18,280
Mais quelques zones d’ombre subsistent.

52
00:03:20,920 --> 00:03:24,040
Comment cette opération d’espionnage a été découverte ?

53
00:03:24,040 --> 00:03:31,240
Pourquoi un employé du ministère de la Justice a été auditionné en tant que simple témoin et laissé en liberté ?

54
00:03:32,240 --> 00:03:36,280
Comment l’enregistrement d’une fausse naissance datée de 1986

55
00:03:36,280 --> 00:03:39,120
dans le système informatisé a pu être effectué sans contrôle ?

56
00:03:39,920 --> 00:03:45,680
Pourquoi une employé·e de la municipalité de Bab Souika citée par un des accusé·es n’a pas été entendue ?

57
00:03:45,680 --> 00:03:48,560
Pourquoi certain·es accusé·es ont changé leur version ?

58
00:04:03,160 --> 00:04:04,760
Samia.

59
00:04:04,760 --> 00:04:07,600
Cheffe de service de l'état civil à la municipalité de Bizerte.

60
00:04:07,920 --> 00:04:09,600
Condamnée à 8 ans de prison.

61
00:04:15,600 --> 00:04:18,680
Originaire de Bizerte,

62
00:04:18,680 --> 00:04:22,880
Samia est employée à la municipalité depuis plus de 20 ans.

63
00:04:23,840 --> 00:04:26,120
Au moment de son arrestation, en mai 2015,

64
00:04:26,120 --> 00:04:28,440
elle est responsable du service de l’état civil.

65
00:04:28,440 --> 00:04:30,440
Son travail, elle le prend à coeur.

66
00:04:30,960 --> 00:04:33,720
Une famille très conservatrice qui l’empêchait de s’épanouir,

67
00:04:34,240 --> 00:04:37,320
un mariage traumatisant suivi d’un divorce,

68
00:04:37,960 --> 00:04:42,160
la vie de Samia se limitait essentiellement à son bureau et au foyer familial.

69
00:04:42,160 --> 00:04:45,280
Mais sa vie va bientôt basculer.

70
00:04:47,280 --> 00:04:48,600
Début 2011,

71
00:04:48,600 --> 00:04:52,200
un étranger qui parle français la contacte sur le téléphone fixe de son bureau.

72
00:04:52,880 --> 00:04:55,120
Rien d’étonnant,

73
00:04:55,120 --> 00:04:57,720
il y a beaucoup d’étrangers à Bizerte.

74
00:04:58,240 --> 00:05:00,680
Il s’agit de Mikhaïl.

75
00:05:01,240 --> 00:05:04,720
Il se présente comme étant directeur du centre culturel de Tunis

76
00:05:04,720 --> 00:05:08,400
et souhaite établir une liste des Russes enregistré·es à Bizerte.

77
00:05:10,000 --> 00:05:12,760
Une demande d’accès aux registres des naissances et des décès

78
00:05:12,960 --> 00:05:15,320
est formulée auprès du président de la municipalité.

79
00:05:15,320 --> 00:05:17,320
Ce dernier donne son autorisation.

80
00:05:17,960 --> 00:05:22,360
Depuis, Mikhaïl vient régulièrement au bureau de la cheffe de service

81
00:05:22,360 --> 00:05:26,480
afin de consulter ces registres sans qu'elle ne s'aperçoive de rien.

82
00:05:26,920 --> 00:05:31,200
Il et elle se lient d’amitié et conviennent de se voir en dehors du bureau.

83
00:05:32,160 --> 00:05:35,880
Samia et Mikhaïl se retrouvent une soirée du mois de Ramadan 2011.

84
00:05:35,880 --> 00:05:38,040
Il vient la chercher en voiture.

85
00:05:38,040 --> 00:05:42,280
Elle remarque la plaque d’immatriculation, elle appartient à un corps diplomatique.

86
00:05:45,280 --> 00:05:49,200
Il et elle se posent dans le café d’un hôtel, parlent de traditions et de vie quotidienne.

87
00:05:50,640 --> 00:05:52,640
En rentrant,

88
00:05:52,640 --> 00:05:56,320
Mikhaïl lui tend une enveloppe fermée qui contient de l'argent.

89
00:05:56,320 --> 00:05:58,640
Elle refuse à plusieurs reprises,

90
00:05:58,640 --> 00:06:00,640
c’est ce qu’elle assure devant le juge.

91
00:06:01,360 --> 00:06:04,120
Mais, face à l’insistance de Mikhaïl, elle accepte.

92
00:06:04,720 --> 00:06:06,440
De retour chez elle,

93
00:06:06,440 --> 00:06:08,000
elle ouvre l’enveloppe.

94
00:06:08,000 --> 00:06:10,000
Elle contient 700 dinars.

95
00:06:23,080 --> 00:06:24,000
À une autre occasion,

96
00:06:24,000 --> 00:06:26,280
Samia accompagne Mikhaïl à Tunis,

97
00:06:26,280 --> 00:06:28,800
au cinéma, pour regarder un documentaire russe.

98
00:06:29,040 --> 00:06:30,600
En la raccompagnant,

99
00:06:30,600 --> 00:06:33,320
il lui donne de nouveau une enveloppe qu’elle accepte aussi,

100
00:06:33,320 --> 00:06:35,800
Cette fois, l’enveloppe contient 500 dinars.

101
00:06:37,040 --> 00:06:39,640
Plusieurs mois plus tard, en 2012,

102
00:06:39,640 --> 00:06:41,080
il et elle sortent de nouveau.

103
00:06:41,800 --> 00:06:45,800
Mikhaïl sait qu’elle souhaite visiter la France très prochainement,

104
00:06:45,800 --> 00:06:48,160
il veut l’aider et lui donne 700 euros.

105
00:06:48,160 --> 00:06:51,400
Samia part en voyage en décembre de la même année.

106
00:06:54,320 --> 00:06:55,800
Selon les dépositions de Samia,

107
00:06:55,800 --> 00:07:01,840
rien n’indique que Mikhaïl lui ait demandé de lui procurer des documents spécifiques.

108
00:07:02,560 --> 00:07:04,120
Au milieu de l’année 2013,

109
00:07:04,120 --> 00:07:07,000
il l’informe que sa mission est terminée et qu’il retourne en Russie.

110
00:07:07,000 --> 00:07:08,280
Mais avant de partir,

111
00:07:08,280 --> 00:07:10,280
il lui présente son remplaçant,

112
00:07:10,280 --> 00:07:12,000
en la personne de “Karim Brahim”,

113
00:07:12,000 --> 00:07:14,000
un Russe qui parle arabe.

114
00:07:14,000 --> 00:07:16,000
Avec Karim,

115
00:07:16,000 --> 00:07:18,000
la situation sera tout de même différente…

116
00:07:20,640 --> 00:07:22,600
Il et elle ne se voient jamais à la municipalité

117
00:07:22,600 --> 00:07:25,480
mais plutôt à Tunis.

118
00:07:25,480 --> 00:07:27,880
La première fois, il l’emmène en voiture.

119
00:07:27,880 --> 00:07:31,600
Les fois suivantes, le rituel est toujours le même.

120
00:07:31,600 --> 00:07:35,440
Elle prend le bus jusqu’à l’aéroport de Tunis-Carthage

121
00:07:35,440 --> 00:07:37,080
où l’attend son “ami”.

122
00:07:37,080 --> 00:07:38,360
Il et elle vont déjeuner,

123
00:07:38,360 --> 00:07:41,240
il lui demande des documents qu'elle ramène au rendez-vous suivant.

124
00:07:41,240 --> 00:07:43,240
Karim se montre très généreux avec Samia :

125
00:07:43,240 --> 00:07:45,240
300 dinars pour chaque fête.

126
00:07:45,240 --> 00:07:47,240
400 dinars lors de certaines rencontres,

127
00:07:47,240 --> 00:07:51,920
1000 dinars pour faire une “Omra”, un smartphone, etc.

128
00:07:52,720 --> 00:07:56,760
Samia accepte, mais lorsque Karim lui offre un parfum pour son anniversaire,

129
00:07:56,760 --> 00:08:01,840
elle refuse car offrir des parfums sépare des ami·es et des amoureu·se·x.

130
00:08:02,880 --> 00:08:05,400
Mais elle aussi se montre très généreuse avec son ami russe,

131
00:08:05,960 --> 00:08:08,880
car il ne se limite pas à de simples consultations de registres.

132
00:08:08,880 --> 00:08:11,440
À partir de mars 2014,

133
00:08:11,840 --> 00:08:14,760
Karim obtient de Samia de nombreux documents administratifs

134
00:08:14,760 --> 00:08:16,760
prétextant une étude,

135
00:08:16,760 --> 00:08:18,760
Samia ne s'est doutée de rien :

136
00:08:19,920 --> 00:08:22,880
Elle lui donne des modèles vides d’actes de naissance,

137
00:08:22,880 --> 00:08:24,880
de certificats de décès

138
00:08:24,880 --> 00:08:27,400
ou d’actes de mariage, etc.

139
00:08:29,400 --> 00:08:31,400
Parmi les registres obtenus par Karim,

140
00:08:31,800 --> 00:08:35,000
celui des naissances de 1987.

141
00:08:35,680 --> 00:08:39,640
Il dit à Samia vouloir recueillir des informations concernant un certain Dupont Orion,

142
00:08:39,640 --> 00:08:42,480
né le 11 octobre 1987

143
00:08:42,480 --> 00:08:44,480
et décédé le lendemain.

144
00:08:44,480 --> 00:08:46,840
Le registre dans lequel figure son acte de naissance

145
00:08:46,840 --> 00:08:51,200
a été retrouvé chez Karim, lors de la perquisition de son domicile en mai 2015.

146
00:08:55,200 --> 00:08:59,080
Prendre l’identité d’enfants décédé·es pour permettre à des espion·nes de s’infiltrer ?

147
00:08:59,640 --> 00:09:01,960
Digne d’un roman d’espionnage,

148
00:09:01,960 --> 00:09:06,440
cette hypothèse n’est pas étrangère aux pratiques des renseignements russes.

149
00:09:06,440 --> 00:09:09,440
L’opération “Ghost Stories”

150
00:09:09,440 --> 00:09:13,920
aux États-Unis menée par le FBI contre des agent·es secret·es russes en est un exemple.

151
00:09:14,680 --> 00:09:19,080
Il s’est avéré que plusieurs personnes arrêtées avaient ressuscité des cadavres d’enfants,

152
00:09:19,080 --> 00:09:24,200
du moins sur les registres officiels, afin de s’approprier leur identité.

153
00:09:34,880 --> 00:09:37,440
Est-ce que Samia était au courant des desseins de Karim ?

154
00:09:37,440 --> 00:09:40,200
Elle jure que non.

155
00:09:40,200 --> 00:09:43,920
Mais elle n’explique pas pour autant certains agissements pour le moins troublants.

156
00:09:43,920 --> 00:09:45,920
Sur son agenda personnel,

157
00:09:47,000 --> 00:09:49,480
elle note ce qu’elle doit faire dans les “situations d’urgence” :

158
00:09:51,480 --> 00:09:54,720
des rendez-vous avec Karim fixés en avance tous les trois mois.

159
00:09:55,640 --> 00:09:59,000
par mail, certains messages codés ont une signification précise.

160
00:09:59,000 --> 00:10:00,360
“Comment va Hela ?”

161
00:10:00,360 --> 00:10:02,800
ou “Cela fait longtemps qu’on ne s’est pas vu·es”

162
00:10:02,800 --> 00:10:07,240
signifient qu’il et elle doivent se retrouver le lendemain, devant un restaurant, à une heure précise.

163
00:10:08,480 --> 00:10:12,320
Samia devait retrouver Karim le 15 mai 2015,

164
00:10:12,320 --> 00:10:16,560
pour lui donner une copie certifiée conforme de son passeport, en arabe et en français.

165
00:10:16,560 --> 00:10:18,560
Elle n’a jamais pu aller au rendez-vous,

166
00:10:18,560 --> 00:10:20,560
elle vient de se faire arrêter.

167
00:10:34,120 --> 00:10:35,600
Mourad

168
00:10:35,600 --> 00:10:38,560
Chef de service de l'état civil à Bab Souika

169
00:10:38,560 --> 00:10:40,960
Condamné à 15 ans de prison.

170
00:10:48,560 --> 00:10:52,000
Mourad est employé à la municipalité de Bab Souika depuis 2007.

171
00:10:52,000 --> 00:10:55,200
Après la révolution, il devient responsable du service de l’état civil.

172
00:10:55,200 --> 00:10:57,920
Il rencontre Mikhaïl pour la première fois en juin 2010.

173
00:10:57,920 --> 00:10:59,920
Mais rien de bien particulier,

174
00:10:59,920 --> 00:11:03,360
le Russe est juste venu retirer un document.

175
00:11:04,600 --> 00:11:06,240
En novembre 2010,

176
00:11:06,240 --> 00:11:08,760
Mikhaïl et Mourad prennent un café à La Goulette.

177
00:11:08,760 --> 00:11:12,000
Le Russe demande au Tunisien s’il peut lui apporter une puce de téléphone.

178
00:11:12,000 --> 00:11:14,000
Mourad ne se pose pas de questions et accepte.

179
00:11:14,000 --> 00:11:16,560
Parce que son ami n'avait pas le temps de le faire.

180
00:11:16,560 --> 00:11:20,840
Depuis, les deux hommes se revoient régulièrement, environ une fois par mois.

181
00:11:22,120 --> 00:11:24,120
Entre 2011 et 2012,

182
00:11:24,120 --> 00:11:29,320
Mikhaïl obtient de Mourad plusieurs extraits de naissance de personnes tunisiennes nées en 1986,

183
00:11:29,320 --> 00:11:31,800
ainsi que des informations sur les actes de mariage

184
00:11:31,800 --> 00:11:35,840
ou le type de stylos utilisés pour les inscriptions dans les différents registres.

185
00:11:35,840 --> 00:11:37,320
En échange,

186
00:11:37,320 --> 00:11:40,920
Mourad avoue avoir perçu 600 dinars au total.

187
00:11:42,160 --> 00:11:43,600
En 2013,

188
00:11:43,600 --> 00:11:46,440
Mikhaïl insiste auprès de Mourad pour qu’il fasse son passeport.

189
00:11:46,440 --> 00:11:48,840
Il lui promet de lui trouver un travail à l’étranger.

190
00:11:48,840 --> 00:11:50,840
Mourad hésite.

191
00:11:50,840 --> 00:11:53,760
Sa situation à la municipalité s’est améliorée.

192
00:11:53,760 --> 00:11:57,720
De plus, il compte se marier bientôt et ne pense pas quitter le pays.

193
00:11:57,720 --> 00:12:01,440
Mais Mikhaïl insiste et Mourad finit par accéder à sa demande.

194
00:12:04,480 --> 00:12:06,160
En juin 2013,

195
00:12:06,160 --> 00:12:08,160
Mikhaïl est accompagné par son ami “Karim”.

196
00:12:08,160 --> 00:12:10,160
Mourad lui remet son passeport.

197
00:12:10,160 --> 00:12:13,040
C’est la dernière fois qu’ils se voient,

198
00:12:13,040 --> 00:12:16,800
Mikhaïl disparaît avec le passeport du Tunisien.

199
00:12:20,040 --> 00:12:21,480
Quelques mois plus tard,

200
00:12:21,480 --> 00:12:25,000
Mourad reçoit un message du numéro qu’il avait donné à Mikhaïl

201
00:12:25,000 --> 00:12:27,400
lui proposant un rendez-vous à La Marsa.

202
00:12:27,920 --> 00:12:29,400
Arrivé sur place,

203
00:12:29,400 --> 00:12:32,120
Mourad est surpris de ne pas trouver Mikhaïl, mais Karim,

204
00:12:32,120 --> 00:12:34,360
son ami russe qui parle arabe.

205
00:12:34,360 --> 00:12:37,000
Ce dernier le rassure qu'il récupérera son passeport.

206
00:12:38,360 --> 00:12:41,400
Depuis, Karim et Mourad se voient tous les mois.

207
00:12:41,760 --> 00:12:44,400
Leur relation devient comme celle qu'il entretenait avec Mikhaïl.

208
00:12:44,400 --> 00:12:46,680
Ils se voient à La Marsa ou à La Goulette,

209
00:12:46,680 --> 00:12:49,800
parlent de la vie quotidienne ou de procédures administratives.

210
00:12:49,800 --> 00:12:51,800
Mourad lui procure une autre puce téléphonique.

211
00:12:51,800 --> 00:12:55,360
Karim non plus n’a pas le temps pour ça.

212
00:12:56,360 --> 00:12:58,280
Un jour de janvier 2015,

213
00:12:58,280 --> 00:13:03,280
Karim demande à récupérer un registre de naissances daté de 1986.

214
00:13:03,280 --> 00:13:06,120
Cette fois, Mourad refuse,

215
00:13:06,440 --> 00:13:09,040
mais accepte tout de même les 150 dinars que son ami lui tend.

216
00:13:10,520 --> 00:13:13,040
Mais Karim n’a pas dit son dernier mot.

217
00:13:13,040 --> 00:13:17,760
En mars 2015, il demande à Mourad de s’informer sur une certaine Meriem Ben Idriss,

218
00:13:17,760 --> 00:13:21,200
inscrite dans les registres de Bab Souika depuis 1986.

219
00:13:21,960 --> 00:13:24,880
Cette dernière ne figure pas sur le système informatisé,

220
00:13:24,880 --> 00:13:27,880
un problème auquel Mourad va remédier, avant d’être arrêté par la police.

221
00:13:28,520 --> 00:13:32,440
Meriem Ben Idriss n’a jamais existé.

222
00:13:47,560 --> 00:13:48,880
Ihsen

223
00:13:48,880 --> 00:13:50,880
Employé à la municipalité de l'Aouina.

224
00:13:51,320 --> 00:13:53,080
Condamné à 8 ans de prison

225
00:14:01,080 --> 00:14:02,000
Ihsen

226
00:14:02,000 --> 00:14:04,960
Est employé à la municipalité de la Goulette depuis 2007.

227
00:14:04,960 --> 00:14:09,720
Il est promu en 2010 dans la circonscription de l’Aouina.

228
00:14:12,560 --> 00:14:14,920
Ihsen n’a jamais rencontré Mikhaïl.

229
00:14:14,920 --> 00:14:16,920
Ce n’est qu’en été 2013

230
00:14:16,920 --> 00:14:18,920
qu’il rencontre un “étranger qui parle arabe”,

231
00:14:19,360 --> 00:14:20,920
Karim.

232
00:14:22,040 --> 00:14:28,360
Ce dernier est accompagné d’un ami qui veut enregistrer une naissance au-delà des 10 jours légaux.

233
00:14:28,360 --> 00:14:30,360
Ihsen lui répond qu’il ne peut pas l’aider,

234
00:14:30,360 --> 00:14:32,360
il faut passer par la justice dans ce type de cas.

235
00:14:32,360 --> 00:14:34,360
Karim et son ami s’en vont.

236
00:14:38,360 --> 00:14:39,440
Quelques mois plus tard,

237
00:14:39,440 --> 00:14:42,200
Karim et Ihsen se rencontrent “par hasard” à La Marsa.

238
00:14:42,880 --> 00:14:47,240
Après deux mois, Karim invite Ihsen pour un café.

239
00:14:47,720 --> 00:14:49,240
Avant de rentrer,

240
00:14:49,240 --> 00:14:52,160
Karim insiste pour qu’Ihsen prenne 40 dinars pour le taxi.

241
00:14:52,160 --> 00:14:54,160
Ihsen finit par accepter.

242
00:14:54,800 --> 00:14:56,640
En 2014,

243
00:14:56,640 --> 00:15:00,600
Karim demande à Ihsen des extraits de naissance de plusieurs personnes.

244
00:15:00,600 --> 00:15:02,600
“C’est légal”,

245
00:15:02,600 --> 00:15:04,600
affirme l’agent municipal.

246
00:15:09,160 --> 00:15:12,080
Karim évoque ensuite l’existence d’un ami tunisien qu’il a perdu de vue.

247
00:15:12,080 --> 00:15:15,400
Il sait qu’il est enregistré à l’Aouina,

248
00:15:15,400 --> 00:15:18,520
qu’il est né entre 1980 et 1990

249
00:15:18,520 --> 00:15:20,520
mais n’a pas d’informations supplémentaires.

250
00:15:20,880 --> 00:15:22,840
Tous les jours, pendant une semaine,

251
00:15:22,840 --> 00:15:25,640
Ihsen accepte de le laisser consulter les registres de ces années-là,

252
00:15:25,640 --> 00:15:27,640
“de bonne foi”,

253
00:15:27,640 --> 00:15:30,240
en sa présence.

254
00:15:32,960 --> 00:15:35,800
Dans les premières auditions avec la brigade criminelle,

255
00:15:35,800 --> 00:15:37,800
sans la présence des ses avocat·es,

256
00:15:37,800 --> 00:15:41,560
Ihsen aurait indiqué avoir remis à Karim plusieurs registres datés des années 1980,

257
00:15:41,560 --> 00:15:43,560
dont deux seraient encore en sa possession.

258
00:15:44,280 --> 00:15:47,640
Aucun de ces registres n’a été retrouvé chez Karim.

259
00:15:48,120 --> 00:15:50,120
Devant le juge d’instruction,

260
00:15:50,120 --> 00:15:54,160
Ihsen dément les faits écrits dans les procès-verbaux

261
00:15:54,160 --> 00:15:57,160
et affirme avoir signé sous la torture.

262
00:15:57,160 --> 00:16:02,520
Il assure qu’il n’a accepté que 100 dinars au total de la part de Karim.

263
00:16:14,160 --> 00:16:15,960
Chekib

264
00:16:15,960 --> 00:16:18,720
Agent financier à la municipalité d'El Menzah 9

265
00:16:18,720 --> 00:16:20,720
Condamné à 8 ans de prison

266
00:16:27,840 --> 00:16:29,240
Chekib

267
00:16:29,240 --> 00:16:31,240
était agent financier

268
00:16:31,240 --> 00:16:34,920
puis chef de service de l’état civil à la municipalité d’El Menzah.

269
00:16:34,920 --> 00:16:39,080
Il est président du bureau local “colonie de vacances” de la même ville.

270
00:16:39,080 --> 00:16:41,080
Dans le cadre de cette seconde activité,

271
00:16:41,080 --> 00:16:43,560
il organise un évènement en mai 2013.

272
00:16:43,560 --> 00:16:46,280
C’est là qu’il rencontre un étranger qui parle français,

273
00:16:46,280 --> 00:16:48,280
Mikhaïl.

274
00:16:50,280 --> 00:16:52,440
Le lendemain, le Russe est de retour avec son ami Karim.

275
00:16:52,440 --> 00:16:54,440
Ce dernier se présente comme un chef d’entreprise

276
00:16:54,440 --> 00:16:56,440
qui veut organiser des activités culturelles.

277
00:16:56,440 --> 00:16:59,760
Il soumet l’idée d’une collaboration tuniso-russe à Chakib

278
00:16:59,760 --> 00:17:02,360
mais un an passe sans qu’ils ne se donnent aucune nouvelle.

279
00:17:03,160 --> 00:17:04,680
En mai 2014,

280
00:17:04,680 --> 00:17:06,680
Karim recontacte Chakib par téléphone.

281
00:17:06,680 --> 00:17:09,840
Les deux hommes reparlent de la collaboration sans donner de rendez-vous.

282
00:17:11,120 --> 00:17:12,560
En 2015,

283
00:17:12,560 --> 00:17:15,240
Karim et Chakib se retrouvent autour d’un verre dans un hôtel.

284
00:17:15,680 --> 00:17:18,640
Il demande au Russe ce qu’il en est de cette collaboration

285
00:17:18,960 --> 00:17:22,560
mais Karim est plus intéressé par ses fonctions à la municipalité,

286
00:17:22,560 --> 00:17:27,560
que par la collaboration tunis-russe dans les colonies de vacances.

287
00:17:28,000 --> 00:17:33,440
Il veut savoir comment enregistrer une naissance au-delà du délai légal de 10 jours.

288
00:17:33,840 --> 00:17:39,480
Il demande également s’il existe des statistiques concernant les naissances et les décès au sein de son administration.

289
00:17:39,480 --> 00:17:40,960
Coïncidence,

290
00:17:40,960 --> 00:17:42,960
Chakib avait accès à ce type d’informations,

291
00:17:42,960 --> 00:17:45,520
en préparation du cinquantenaire de la municipalité.

292
00:17:45,520 --> 00:17:48,280
Il promet de remettre le document à Karim.

293
00:17:51,200 --> 00:17:53,840
Lors de son audition en juillet 2015,

294
00:17:53,840 --> 00:17:59,080
la présidente de la municipalité indique n’avoir jamais été tenue au courant de ces préparatifs,

295
00:17:59,080 --> 00:18:03,160
“Le cinquantenaire devrait avoir lieu en 2029 !”

296
00:18:03,160 --> 00:18:06,320
vu qu'elle a été créée en 1979.

297
00:18:10,320 --> 00:18:12,320
En avril 2015,

298
00:18:12,320 --> 00:18:14,840
Chakib et Karim se revoient pour la dernière fois.

299
00:18:14,840 --> 00:18:17,840
Le premier remet à Karim un livret de famille vierge.

300
00:18:17,840 --> 00:18:19,840
Chekib n’y voit pas d’inconvénients.

301
00:18:19,840 --> 00:18:24,400
Par contre, pour les tampons certifiant la conformité d’un document avec l’original, Chakib refuse.

302
00:18:24,400 --> 00:18:29,480
Il doit d’ailleurs partir rapidement, il ne voudrait pas rater l’anniversaire de sa fille.

303
00:18:29,960 --> 00:18:32,000
Karim propose de le raccompagner

304
00:18:32,000 --> 00:18:37,120
et lui offre du chocolat, des pâtes italiennes, ainsi que du foie gras, pour faire plaisir à sa fille.

305
00:18:37,960 --> 00:18:40,400
Chakib n’y voit toujours pas d’inconvénients.

306
00:18:40,400 --> 00:18:43,920
Ils prévoient enfin de se revoir le 18 mai,

307
00:18:43,920 --> 00:18:48,600
mais le Tunisien est arrêté trois jours plus tard.

308
00:18:49,560 --> 00:18:52,160
Pourquoi ces entrevues sont prévues à l’avance ?

309
00:18:53,160 --> 00:18:56,960
Au cas où ils ne parviendraient pas à se joindre par téléphone,

310
00:18:56,960 --> 00:19:02,320
assure Chakib, qui voulait juste organiser un évènement culturel tuniso-russe.

311
00:19:18,320 --> 00:19:20,320
Kaïs

312
00:19:20,320 --> 00:19:21,480
greffier de justice.

313
00:19:21,480 --> 00:19:23,000
Libre.

314
00:19:31,000 --> 00:19:32,360
Kais

315
00:19:32,360 --> 00:19:34,360
travaille au ministère de la Justice.

316
00:19:37,160 --> 00:19:38,800
Un jour de l’année 2014,

317
00:19:38,800 --> 00:19:42,280
il se pose seul à un café à la Marsa pour regarder un match de football

318
00:19:42,280 --> 00:19:45,320
Un homme lui demande s’il peut s’asseoir à côté de lui.

319
00:19:45,320 --> 00:19:48,120
Il s'agit de Karim.

320
00:19:48,120 --> 00:19:50,120
Au fil de la discussion,

321
00:19:50,120 --> 00:19:52,720
Karim dit à Kaïs qu’il l’a déjà vu au ministère de la Justice

322
00:19:52,720 --> 00:19:54,200
et lui demande de l’aider.

323
00:19:54,200 --> 00:19:58,760
Il cherche à savoir quels sont les documents nécessaire pour obtenir la nationalité tunisienne.

324
00:20:00,760 --> 00:20:02,760
Ils se revoient la semaine suivante.

325
00:20:02,760 --> 00:20:04,760
Karim prétend qu’il est d’origine tunisienne

326
00:20:04,760 --> 00:20:06,760
mais qu’il a vécu au Liban, d'où son accent.

327
00:20:07,200 --> 00:20:09,920
Ce jour-là, Kaïs est embêté, il a perdu son téléphone.

328
00:20:09,920 --> 00:20:10,800
Un mois plus tard,

329
00:20:10,800 --> 00:20:14,200
il revoit Karim qui n’est pas venu les mains vides.

330
00:20:14,200 --> 00:20:17,200
Il a un nouveau téléphone à lui offrir.

331
00:20:17,200 --> 00:20:19,680
Au nom de “l’amitié” qui les lie, Kaïs accepte

332
00:20:19,680 --> 00:20:22,080
et lui offre en retour une bouteille d’huile d’olive.

333
00:20:22,360 --> 00:20:25,120
Selon le procès-verbal établi par la brigade criminelle,

334
00:20:25,120 --> 00:20:28,400
La dernière rencontre entre eux a lieu en mars 2015

335
00:20:39,160 --> 00:20:40,960
Mikhaïl et Karim,

336
00:20:40,960 --> 00:20:43,400
espions russes en liberté.

337
00:20:50,640 --> 00:20:52,360
Mikhaïl Salikov

338
00:20:52,360 --> 00:20:55,480
apparaît pour la première fois en 2010, dans la municipalité de Bab Souika,

339
00:20:55,480 --> 00:20:58,120
pour disparaître

340
00:20:58,120 --> 00:21:00,600
au début de l’été 2013.

341
00:21:01,200 --> 00:21:03,520
Il a noué des liens étroits avec Samia et Mourad,

342
00:21:03,520 --> 00:21:06,240
mais n’a vu qu’une seule fois Chakib, en présence de Karim.

343
00:21:06,240 --> 00:21:08,240
Il n’a jamais rencontré Ihsen.

344
00:21:08,240 --> 00:21:10,240
Officiellement,

345
00:21:10,240 --> 00:21:12,240
il est diplomate au sein de l’ambassade de Russie

346
00:21:12,240 --> 00:21:14,240
mais il y a très peu d’informations sur lui.

347
00:21:14,840 --> 00:21:16,240
Avant de disparaître,

348
00:21:16,240 --> 00:21:20,560
Mikhaïl a, à chaque fois, présenté Karim aux personnes qu’il a ciblées.

349
00:21:20,560 --> 00:21:25,880
Avec Samia et Mourad, il a ouvert la voie à son remplaçant pour recueillir les documents qu’ils convoitaient.

350
00:21:26,800 --> 00:21:30,360
Kyamran Rasim Ogly Ragimov

351
00:21:30,360 --> 00:21:32,720
apparaît en 2013.

352
00:21:32,720 --> 00:21:34,720
Contrairement à Mikhaïl,

353
00:21:34,720 --> 00:21:36,720
il parle arabe et ne donne pas son vrai nom.

354
00:21:37,800 --> 00:21:40,120
Il se fait appeler Karim Ibrahim.

355
00:21:40,760 --> 00:21:45,320
Avec Samia et Mourad, il montre son intérêt pour deux identités en particulier :

356
00:21:45,320 --> 00:21:47,320
Meriem Ben Idriss,

357
00:21:47,320 --> 00:21:50,640
une femme enregistrée à Bab Souika mais qui n’a jamais existé,

358
00:21:50,640 --> 00:21:51,960
et Orion Dupont,

359
00:21:51,960 --> 00:21:53,960
un mort-né enregistré à Bizerte.

360
00:22:06,320 --> 00:22:08,320
Le 16 mai 2015,

361
00:22:08,880 --> 00:22:13,200
Samia lui donne rendez-vous pour récupérer deux registres de naissance.

362
00:22:13,880 --> 00:22:17,400
Devant l’église de Bizerte, il attend avec un cartable à la main.

363
00:22:17,800 --> 00:22:20,120
Samia vient, mais n'est pas seule.

364
00:22:20,120 --> 00:22:22,880
Elle est accompagnée de policiers.

365
00:22:47,320 --> 00:22:51,120
Pendant son audition à la brigade criminelle de Tunis,

366
00:22:51,120 --> 00:22:53,840
Karim confirme les agissements énumérés par la police.

367
00:22:54,080 --> 00:22:56,840
Avez-vous demandé à Samia tel document ?

368
00:22:56,840 --> 00:22:58,080
Oui.

369
00:22:58,840 --> 00:23:00,680
Avez-vous donné de l’argent à Mourad ?

370
00:23:00,680 --> 00:23:02,680
Oui.

371
00:23:02,680 --> 00:23:04,800
Aviez-vous en votre possession tel registre ?

372
00:23:04,800 --> 00:23:06,800
Oui.

373
00:23:06,800 --> 00:23:11,000
Mais pour ce qui est des raisons de ces agissements, la réponse sera toujours la même :

374
00:23:11,000 --> 00:23:13,640
“Je refuse de répondre à cette question”.

375
00:23:15,640 --> 00:23:19,840
L’interrogatoire ne dure pas très longtemps, vu qu'il est diplomate.

376
00:23:19,840 --> 00:23:21,840
Avant de le libérer,

377
00:23:21,840 --> 00:23:24,560
la police l’emmène à son domicile pour une perquisition.

378
00:23:24,560 --> 00:23:28,320
Les agents y trouvent plusieurs documents vierges relatifs à l’état civil :

379
00:23:28,320 --> 00:23:33,000
certificats de décès, de naissance, de mariage ou spécifiques aux morts-nés.

380
00:23:33,000 --> 00:23:37,880
Mais Karim ne risque rien, il bénéficie de l’immunité accordée aux diplomates

381
00:23:37,880 --> 00:23:39,880
et disparaît à son tour.

382
00:23:43,880 --> 00:23:46,880
Plusieurs zones d'ombres subsistent dans cette affaire.

383
00:23:46,880 --> 00:23:48,880
D'abord,

384
00:23:48,880 --> 00:23:50,880
Comment le réseau a-t-il été découvert?

385
00:23:52,000 --> 00:23:53,200
Deuxièmement,

386
00:23:53,200 --> 00:23:56,480
Comment Kais a été lavé de tout soupçon aussi rapidement ?

387
00:23:56,480 --> 00:23:58,000
Troisièmement,

388
00:23:58,000 --> 00:24:00,200
Une personne n'a pas été convoquée

389
00:24:00,200 --> 00:24:05,440
alors que son témoignage aurait été capital pour alléger ou confirmer les charges retenues contre Mourad.

390
00:24:05,440 --> 00:24:09,080
Il s’agit d’Amel, une de ses employé·es.

391
00:24:09,080 --> 00:24:10,600
Ensuite,

392
00:24:10,600 --> 00:24:13,160
des dépositions ont évolué plusieurs fois.

393
00:24:13,160 --> 00:24:15,160
Enfin,

394
00:24:15,160 --> 00:24:18,280
Malgré l'extradition de Karim,

395
00:24:18,280 --> 00:24:20,280
et le changement de l'ambassadeur russe,

396
00:24:20,280 --> 00:24:25,680
Les autorités tunisiennes et russes refusent de donner plus de détails sur l'affaire.

397
00:24:26,080 --> 00:24:28,960
Au bureau des légendes, le secret est bien gardé.

398
00:24:43,600 --> 00:24:45,160
Article lu

399
00:24:45,160 --> 00:24:46,760
produit par Inkyfada

400
00:24:46,760 --> 00:24:48,120
Texte et réalisation

401
00:24:48,120 --> 00:24:49,680
Monia Ben Hamadi

402
00:24:49,760 --> 00:24:50,840
Conseils

403
00:24:50,840 --> 00:24:52,840
Bochra Triki et Marwen Ben Mustapha

404
00:24:53,120 --> 00:24:55,480
Traduction et voix: Majd Mastoura

405
00:24:55,480 --> 00:24:57,000
Montage et mixage

406
00:24:57,000 --> 00:24:59,000
Yassine Kawana

407
00:24:59,000 --> 00:25:01,000
Musique : Omar Aloulou
