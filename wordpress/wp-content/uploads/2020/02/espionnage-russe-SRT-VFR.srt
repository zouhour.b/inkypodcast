﻿1
00:00:07,000 --> 00:00:08,520
InkyStories

2
00:00:08,520 --> 00:00:10,520
Articles, reportages

3
00:00:10,520 --> 00:00:12,520
et enquêtes sonores

4
00:00:21,680 --> 00:00:24,640
Chroniques d'une opération d'espionnage russe en Tunisie

5
00:00:26,640 --> 00:00:28,640
Par

6
00:00:28,640 --> 00:00:30,640
Monia Ben Hamadi

7
00:00:30,640 --> 00:00:31,840
Lues par

8
00:00:31,840 --> 00:00:33,840
Majd Mastoura

9
00:00:50,400 --> 00:00:52,280
Mai 2015,

10
00:00:52,280 --> 00:00:55,360
vers 18h devant l'église à Bizerte.

11
00:00:55,960 --> 00:00:59,960
Karim attend Samia qui lui a demandé de lui rendre deux registres de la municipalité.

12
00:01:00,560 --> 00:01:02,480
Samia est bien sur les lieux.

13
00:01:02,480 --> 00:01:04,480
“C’est lui Karim, le Russe”

14
00:01:06,480 --> 00:01:09,480
indique-t-elle aux quatre policiers qui l’entourent.

15
00:01:09,480 --> 00:01:14,480
Une opération d’espionnage russe vient de prendre fin.

16
00:01:34,480 --> 00:01:36,960
Quatre employé·es de municipalités différentes,

17
00:01:37,160 --> 00:01:38,920
qui ne se connaissent pas,

18
00:01:38,920 --> 00:01:42,240
ont été condamné·es à des peines de prison ferme allant de 8 à 15 ans.

19
00:01:42,360 --> 00:01:44,880
Ils et elle ne s’imaginaient pas que leur vie allait basculer

20
00:01:44,880 --> 00:01:49,360
au moment où leurs chemins ont croisé celui de Mikhaïl Salikov et Kyamran Rasim Ogly Ragimov

21
00:01:49,360 --> 00:01:50,800
- dit Karim -

22
00:01:50,800 --> 00:01:53,480
diplomates et espions russes.

23
00:01:53,480 --> 00:01:55,480
Pendant cinq années,

24
00:01:55,480 --> 00:01:57,480
de 2010 à 2015,

25
00:01:57,480 --> 00:02:00,920
les deux espions ont réussi à obtenir un nombre incalculable

26
00:02:00,920 --> 00:02:02,920
de documents officiels vierges

27
00:02:02,920 --> 00:02:04,920
ou relatifs à des personnes tierces,

28
00:02:04,920 --> 00:02:08,800
grâce à la collaboration des fonctionnaires tunisien·nes.

29
00:02:09,280 --> 00:02:12,840
Ils et elle leur ont fourni des registres de l’état civil,

30
00:02:12,840 --> 00:02:16,920
des extraits de naissance, de décès, actes de mariage ou de divorce de Tunisien·nes et d’étranger·es.

31
00:02:16,920 --> 00:02:21,840
Ils ont également pu se munir de documents administratifs vierges, livrets de famille vides

32
00:02:21,840 --> 00:02:25,080
ou de feuilles blanches certifiant que “la copie est conforme à l’original”.

33
00:02:26,000 --> 00:02:28,200
Quelques semaines avant la mise en échec de l’opération,

34
00:02:28,200 --> 00:02:32,480
ils parviennent à obtenir un extrait de naissance au nom de Meriem Ben Idriss,

35
00:02:32,480 --> 00:02:34,480
née en 1986.

36
00:02:35,160 --> 00:02:37,120
Il s’avèrera au fil de l’instruction judiciaire

37
00:02:37,120 --> 00:02:39,960
que cette femme n’a jamais existé.

38
00:02:40,240 --> 00:02:42,600
Il s’agissait d'une “légende” civile,

39
00:02:42,600 --> 00:02:46,640
probablement créée pour servir d’identité factice à une “Illegal”,

40
00:02:46,640 --> 00:02:48,640
une “espionne clandestine”,

41
00:02:48,640 --> 00:02:52,720
une technique d’espionnage dont les services de renseignements russes sont friands.

42
00:02:53,360 --> 00:02:54,720
Pour parvenir à leurs fins,

43
00:02:54,720 --> 00:02:57,840
Mikhaïl et Karim ont noué une relation personnelle avec leurs proies,

44
00:02:57,840 --> 00:02:59,640
de manière plus ou moins continue.

45
00:02:59,640 --> 00:03:02,400
Ils s’informent sur les procédures administratives,

46
00:03:02,400 --> 00:03:06,360
entre un café dans un hôtel de Bizerte ou un dîner à La Marsa.

47
00:03:06,360 --> 00:03:08,360
Ils demandaient des documents spécifiques,

48
00:03:08,360 --> 00:03:10,720
recherchaient des personnes en particulier.

49
00:03:12,120 --> 00:03:13,360
Après leur arrestation,

50
00:03:13,360 --> 00:03:16,440
les employé·es ont révélé de nombreuses informations.

51
00:03:16,440 --> 00:03:18,440
Mais quelques zones d’ombre subsistent.

52
00:03:21,080 --> 00:03:24,200
Comment cette opération d’espionnage a été découverte ?

53
00:03:24,200 --> 00:03:31,400
Pourquoi un employé du ministère de la Justice a été auditionné en tant que simple témoin et laissé en liberté ?

54
00:03:32,400 --> 00:03:36,440
Comment l’enregistrement d’une fausse naissance datée de 1986

55
00:03:36,440 --> 00:03:39,280
dans le système informatisé a pu être effectué sans contrôle ?

56
00:03:40,080 --> 00:03:45,840
Pourquoi une employé·e de la municipalité de Bab Souika citée par un des accusé·es n’a pas été entendue ?

57
00:03:45,840 --> 00:03:48,720
Pourquoi certain·es accusé·es ont changé leur version ?

58
00:04:03,320 --> 00:04:04,920
Samia.

59
00:04:04,920 --> 00:04:07,760
Cheffe de service de l'état civil à la municipalité de Bizerte.

60
00:04:08,080 --> 00:04:09,760
Condamnée à 8 ans de prison.

61
00:04:15,760 --> 00:04:18,840
Originaire de Bizerte,

62
00:04:18,840 --> 00:04:23,040
Samia est employée à la municipalité depuis plus de 20 ans.

63
00:04:24,000 --> 00:04:26,280
Au moment de son arrestation, en mai 2015,

64
00:04:26,280 --> 00:04:28,600
elle est responsable du service de l’état civil.

65
00:04:28,600 --> 00:04:30,600
Son travail, elle le prend à coeur.

66
00:04:31,120 --> 00:04:33,880
Une famille très conservatrice qui l’empêchait de s’épanouir,

67
00:04:34,400 --> 00:04:37,480
un mariage traumatisant suivi d’un divorce,

68
00:04:38,120 --> 00:04:42,320
la vie de Samia se limitait essentiellement à son bureau et au foyer familial.

69
00:04:42,320 --> 00:04:45,440
Mais sa vie va bientôt basculer.

70
00:04:47,440 --> 00:04:48,760
Début 2011,

71
00:04:48,760 --> 00:04:52,360
un étranger qui parle français la contacte sur le téléphone fixe de son bureau.

72
00:04:53,040 --> 00:04:55,280
Rien d’étonnant,

73
00:04:55,280 --> 00:04:57,880
il y a beaucoup d’étrangers à Bizerte.

74
00:04:58,400 --> 00:05:00,840
Il s’agit de Mikhaïl.

75
00:05:01,400 --> 00:05:04,880
Il se présente comme étant directeur du centre culturel de Tunis

76
00:05:04,880 --> 00:05:08,560
et souhaite établir une liste des Russes enregistré·es à Bizerte.

77
00:05:10,160 --> 00:05:12,920
Une demande d’accès aux registres des naissances et des décès

78
00:05:13,120 --> 00:05:15,480
est formulée auprès du président de la municipalité.

79
00:05:15,480 --> 00:05:17,480
Ce dernier donne son autorisation.

80
00:05:18,120 --> 00:05:22,520
Depuis, Mikhaïl vient régulièrement au bureau de la cheffe de service

81
00:05:22,520 --> 00:05:26,640
afin de consulter ces registres sans qu'elle ne s'aperçoive de rien.

82
00:05:27,080 --> 00:05:31,360
Il et elle se lient d’amitié et conviennent de se voir en dehors du bureau.

83
00:05:32,320 --> 00:05:36,040
Samia et Mikhaïl se retrouvent une soirée du mois de Ramadan 2011.

84
00:05:36,040 --> 00:05:38,200
Il vient la chercher en voiture.

85
00:05:38,200 --> 00:05:42,440
Elle remarque la plaque d’immatriculation, elle appartient à un corps diplomatique.

86
00:05:45,440 --> 00:05:49,360
Il et elle se posent dans le café d’un hôtel, parlent de traditions et de vie quotidienne.

87
00:05:50,800 --> 00:05:52,800
En rentrant,

88
00:05:52,800 --> 00:05:56,480
Mikhaïl lui tend une enveloppe fermée qui contient de l'argent.

89
00:05:56,480 --> 00:05:58,800
Elle refuse à plusieurs reprises,

90
00:05:58,800 --> 00:06:00,800
c’est ce qu’elle assure devant le juge.

91
00:06:01,520 --> 00:06:04,280
Mais, face à l’insistance de Mikhaïl, elle accepte.

92
00:06:04,880 --> 00:06:06,600
De retour chez elle,

93
00:06:06,600 --> 00:06:08,160
elle ouvre l’enveloppe.

94
00:06:08,160 --> 00:06:10,160
Elle contient 700 dinars.

95
00:06:23,240 --> 00:06:24,160
À une autre occasion,

96
00:06:24,160 --> 00:06:26,440
Samia accompagne Mikhaïl à Tunis,

97
00:06:26,440 --> 00:06:28,960
au cinéma, pour regarder un documentaire russe.

98
00:06:29,200 --> 00:06:30,760
En la raccompagnant,

99
00:06:30,760 --> 00:06:33,480
il lui donne de nouveau une enveloppe qu’elle accepte aussi,

100
00:06:33,480 --> 00:06:35,960
Cette fois, l’enveloppe contient 500 dinars.

101
00:06:37,200 --> 00:06:39,800
Plusieurs mois plus tard, en 2012,

102
00:06:39,800 --> 00:06:41,240
il et elle sortent de nouveau.

103
00:06:41,960 --> 00:06:45,960
Mikhaïl sait qu’elle souhaite visiter la France très prochainement,

104
00:06:45,960 --> 00:06:48,320
il veut l’aider et lui donne 700 euros.

105
00:06:48,320 --> 00:06:51,560
Samia part en voyage en décembre de la même année.

106
00:06:54,480 --> 00:06:55,960
Selon les dépositions de Samia,

107
00:06:55,960 --> 00:07:02,000
rien n’indique que Mikhaïl lui ait demandé de lui procurer des documents spécifiques.

108
00:07:02,720 --> 00:07:04,280
Au milieu de l’année 2013,

109
00:07:04,280 --> 00:07:07,160
il l’informe que sa mission est terminée et qu’il retourne en Russie.

110
00:07:07,160 --> 00:07:08,440
Mais avant de partir,

111
00:07:08,440 --> 00:07:10,440
il lui présente son remplaçant,

112
00:07:10,440 --> 00:07:12,160
en la personne de “Karim Brahim”,

113
00:07:12,160 --> 00:07:14,160
un Russe qui parle arabe.

114
00:07:14,160 --> 00:07:16,160
Avec Karim,

115
00:07:16,160 --> 00:07:18,160
la situation sera tout de même différente…

116
00:07:20,800 --> 00:07:22,760
Il et elle ne se voient jamais à la municipalité

117
00:07:22,760 --> 00:07:25,640
mais plutôt à Tunis.

118
00:07:25,640 --> 00:07:28,040
La première fois, il l’emmène en voiture.

119
00:07:28,040 --> 00:07:31,760
Les fois suivantes, le rituel est toujours le même.

120
00:07:31,760 --> 00:07:35,600
Elle prend le bus jusqu’à l’aéroport de Tunis-Carthage

121
00:07:35,600 --> 00:07:37,240
où l’attend son “ami”.

122
00:07:37,240 --> 00:07:38,520
Il et elle vont déjeuner,

123
00:07:38,520 --> 00:07:41,400
il lui demande des documents qu'elle ramène au rendez-vous suivant.

124
00:07:41,400 --> 00:07:43,400
Karim se montre très généreux avec Samia :

125
00:07:43,400 --> 00:07:45,400
300 dinars pour chaque fête.

126
00:07:45,400 --> 00:07:47,400
400 dinars lors de certaines rencontres,

127
00:07:47,400 --> 00:07:52,080
1000 dinars pour faire une “Omra”, un smartphone, etc.

128
00:07:52,880 --> 00:07:56,920
Samia accepte, mais lorsque Karim lui offre un parfum pour son anniversaire,

129
00:07:56,920 --> 00:08:02,000
elle refuse car offrir des parfums sépare des ami·es et des amoureu·se·x.

130
00:08:03,040 --> 00:08:05,560
Mais elle aussi se montre très généreuse avec son ami russe,

131
00:08:06,120 --> 00:08:09,040
car il ne se limite pas à de simples consultations de registres.

132
00:08:09,040 --> 00:08:11,600
À partir de mars 2014,

133
00:08:12,000 --> 00:08:14,920
Karim obtient de Samia de nombreux documents administratifs

134
00:08:14,920 --> 00:08:16,920
prétextant une étude,

135
00:08:16,920 --> 00:08:18,920
Samia ne s'est doutée de rien :

136
00:08:20,080 --> 00:08:23,040
Elle lui donne des modèles vides d’actes de naissance,

137
00:08:23,040 --> 00:08:25,040
de certificats de décès

138
00:08:25,040 --> 00:08:27,560
ou d’actes de mariage, etc.

139
00:08:29,560 --> 00:08:31,560
Parmi les registres obtenus par Karim,

140
00:08:31,960 --> 00:08:35,160
celui des naissances de 1987.

141
00:08:35,840 --> 00:08:39,800
Il dit à Samia vouloir recueillir des informations concernant un certain Dupont Orion,

142
00:08:39,800 --> 00:08:42,640
né le 11 octobre 1987

143
00:08:42,640 --> 00:08:44,640
et décédé le lendemain.

144
00:08:44,640 --> 00:08:47,000
Le registre dans lequel figure son acte de naissance

145
00:08:47,000 --> 00:08:51,360
a été retrouvé chez Karim, lors de la perquisition de son domicile en mai 2015.

146
00:08:55,360 --> 00:08:59,240
Prendre l’identité d’enfants décédé·es pour permettre à des espion·nes de s’infiltrer ?

147
00:08:59,800 --> 00:09:02,120
Digne d’un roman d’espionnage,

148
00:09:02,120 --> 00:09:06,600
cette hypothèse n’est pas étrangère aux pratiques des renseignements russes.

149
00:09:06,600 --> 00:09:09,600
L’opération “Ghost Stories”

150
00:09:09,600 --> 00:09:14,080
aux États-Unis menée par le FBI contre des agent·es secret·es russes en est un exemple.

151
00:09:14,840 --> 00:09:19,240
Il s’est avéré que plusieurs personnes arrêtées avaient ressuscité des cadavres d’enfants,

152
00:09:19,240 --> 00:09:24,360
du moins sur les registres officiels, afin de s’approprier leur identité.

153
00:09:35,040 --> 00:09:37,600
Est-ce que Samia était au courant des desseins de Karim ?

154
00:09:37,600 --> 00:09:40,360
Elle jure que non.

155
00:09:40,360 --> 00:09:44,080
Mais elle n’explique pas pour autant certains agissements pour le moins troublants.

156
00:09:44,080 --> 00:09:46,080
Sur son agenda personnel,

157
00:09:47,160 --> 00:09:49,640
elle note ce qu’elle doit faire dans les “situations d’urgence” :

158
00:09:51,640 --> 00:09:54,880
des rendez-vous avec Karim fixés en avance tous les trois mois.

159
00:09:55,800 --> 00:09:59,160
par mail, certains messages codés ont une signification précise.

160
00:09:59,160 --> 00:10:00,520
“Comment va Hela ?”

161
00:10:00,520 --> 00:10:02,960
ou “Cela fait longtemps qu’on ne s’est pas vu·es”

162
00:10:02,960 --> 00:10:07,400
signifient qu’il et elle doivent se retrouver le lendemain, devant un restaurant, à une heure précise.

163
00:10:08,640 --> 00:10:12,480
Samia devait retrouver Karim le 15 mai 2015,

164
00:10:12,480 --> 00:10:16,720
pour lui donner une copie certifiée conforme de son passeport, en arabe et en français.

165
00:10:16,720 --> 00:10:18,720
Elle n’a jamais pu aller au rendez-vous,

166
00:10:18,720 --> 00:10:20,720
elle vient de se faire arrêter.

167
00:10:34,280 --> 00:10:35,760
Mourad

168
00:10:35,760 --> 00:10:38,720
Chef de service de l'état civil à Bab Souika

169
00:10:38,720 --> 00:10:41,120
Condamné à 15 ans de prison.

170
00:10:48,720 --> 00:10:52,160
Mourad est employé à la municipalité de Bab Souika depuis 2007.

171
00:10:52,160 --> 00:10:55,360
Après la révolution, il devient responsable du service de l’état civil.

172
00:10:55,360 --> 00:10:58,080
Il rencontre Mikhaïl pour la première fois en juin 2010.

173
00:10:58,080 --> 00:11:00,080
Mais rien de bien particulier,

174
00:11:00,080 --> 00:11:03,520
le Russe est juste venu retirer un document.

175
00:11:04,760 --> 00:11:06,400
En novembre 2010,

176
00:11:06,400 --> 00:11:08,920
Mikhaïl et Mourad prennent un café à La Goulette.

177
00:11:08,920 --> 00:11:12,160
Le Russe demande au Tunisien s’il peut lui apporter une puce de téléphone.

178
00:11:12,160 --> 00:11:14,160
Mourad ne se pose pas de questions et accepte.

179
00:11:14,160 --> 00:11:16,720
Parce que son ami n'avait pas le temps de le faire.

180
00:11:16,720 --> 00:11:21,000
Depuis, les deux hommes se revoient régulièrement, environ une fois par mois.

181
00:11:22,280 --> 00:11:24,280
Entre 2011 et 2012,

182
00:11:24,280 --> 00:11:29,480
Mikhaïl obtient de Mourad plusieurs extraits de naissance de personnes tunisiennes nées en 1986,

183
00:11:29,480 --> 00:11:31,960
ainsi que des informations sur les actes de mariage

184
00:11:31,960 --> 00:11:36,000
ou le type de stylos utilisés pour les inscriptions dans les différents registres.

185
00:11:36,000 --> 00:11:37,480
En échange,

186
00:11:37,480 --> 00:11:41,080
Mourad avoue avoir perçu 600 dinars au total.

187
00:11:42,320 --> 00:11:43,760
En 2013,

188
00:11:43,760 --> 00:11:46,600
Mikhaïl insiste auprès de Mourad pour qu’il fasse son passeport.

189
00:11:46,600 --> 00:11:49,000
Il lui promet de lui trouver un travail à l’étranger.

190
00:11:49,000 --> 00:11:51,000
Mourad hésite.

191
00:11:51,000 --> 00:11:53,920
Sa situation à la municipalité s’est améliorée.

192
00:11:53,920 --> 00:11:57,880
De plus, il compte se marier bientôt et ne pense pas quitter le pays.

193
00:11:57,880 --> 00:12:01,600
Mais Mikhaïl insiste et Mourad finit par accéder à sa demande.

194
00:12:04,640 --> 00:12:06,320
En juin 2013,

195
00:12:06,320 --> 00:12:08,320
Mikhaïl est accompagné par son ami “Karim”.

196
00:12:08,320 --> 00:12:10,320
Mourad lui remet son passeport.

197
00:12:10,320 --> 00:12:13,200
C’est la dernière fois qu’ils se voient,

198
00:12:13,200 --> 00:12:16,960
Mikhaïl disparaît avec le passeport du Tunisien.

199
00:12:20,200 --> 00:12:21,640
Quelques mois plus tard,

200
00:12:21,640 --> 00:12:25,160
Mourad reçoit un message du numéro qu’il avait donné à Mikhaïl

201
00:12:25,160 --> 00:12:27,560
lui proposant un rendez-vous à La Marsa.

202
00:12:28,080 --> 00:12:29,560
Arrivé sur place,

203
00:12:29,560 --> 00:12:32,280
Mourad est surpris de ne pas trouver Mikhaïl, mais Karim,

204
00:12:32,280 --> 00:12:34,520
son ami russe qui parle arabe.

205
00:12:34,520 --> 00:12:37,160
Ce dernier le rassure qu'il récupérera son passeport.

206
00:12:38,520 --> 00:12:41,560
Depuis, Karim et Mourad se voient tous les mois.

207
00:12:41,920 --> 00:12:44,560
Leur relation devient comme celle qu'il entretenait avec Mikhaïl.

208
00:12:44,560 --> 00:12:46,840
Ils se voient à La Marsa ou à La Goulette,

209
00:12:46,840 --> 00:12:49,960
parlent de la vie quotidienne ou de procédures administratives.

210
00:12:49,960 --> 00:12:51,960
Mourad lui procure une autre puce téléphonique.

211
00:12:51,960 --> 00:12:55,520
Karim non plus n’a pas le temps pour ça.

212
00:12:56,520 --> 00:12:58,440
Un jour de janvier 2015,

213
00:12:58,440 --> 00:13:03,440
Karim demande à récupérer un registre de naissances daté de 1986.

214
00:13:03,440 --> 00:13:06,280
Cette fois, Mourad refuse,

215
00:13:06,600 --> 00:13:09,200
mais accepte tout de même les 150 dinars que son ami lui tend.

216
00:13:10,680 --> 00:13:13,200
Mais Karim n’a pas dit son dernier mot.

217
00:13:13,200 --> 00:13:17,920
En mars 2015, il demande à Mourad de s’informer sur une certaine Meriem Ben Idriss,

218
00:13:17,920 --> 00:13:21,360
inscrite dans les registres de Bab Souika depuis 1986.

219
00:13:22,120 --> 00:13:25,040
Cette dernière ne figure pas sur le système informatisé,

220
00:13:25,040 --> 00:13:28,040
un problème auquel Mourad va remédier, avant d’être arrêté par la police.

221
00:13:28,680 --> 00:13:32,600
Meriem Ben Idriss n’a jamais existé.

222
00:13:47,720 --> 00:13:49,040
Ihsen

223
00:13:49,040 --> 00:13:51,040
Employé à la municipalité de l'Aouina.

224
00:13:51,480 --> 00:13:53,240
Condamné à 8 ans de prison

225
00:14:01,240 --> 00:14:02,160
Ihsen

226
00:14:02,160 --> 00:14:05,120
Est employé à la municipalité de la Goulette depuis 2007.

227
00:14:05,120 --> 00:14:09,880
Il est promu en 2010 dans la circonscription de l’Aouina.

228
00:14:12,720 --> 00:14:15,080
Ihsen n’a jamais rencontré Mikhaïl.

229
00:14:15,080 --> 00:14:17,080
Ce n’est qu’en été 2013

230
00:14:17,080 --> 00:14:19,080
qu’il rencontre un “étranger qui parle arabe”,

231
00:14:19,520 --> 00:14:21,080
Karim.

232
00:14:22,200 --> 00:14:28,520
Ce dernier est accompagné d’un ami qui veut enregistrer une naissance au-delà des 10 jours légaux.

233
00:14:28,520 --> 00:14:30,520
Ihsen lui répond qu’il ne peut pas l’aider,

234
00:14:30,520 --> 00:14:32,520
il faut passer par la justice dans ce type de cas.

235
00:14:32,520 --> 00:14:34,520
Karim et son ami s’en vont.

236
00:14:38,520 --> 00:14:39,600
Quelques mois plus tard,

237
00:14:39,600 --> 00:14:42,360
Karim et Ihsen se rencontrent “par hasard” à La Marsa.

238
00:14:43,040 --> 00:14:47,400
Après deux mois, Karim invite Ihsen pour un café.

239
00:14:47,880 --> 00:14:49,400
Avant de rentrer,

240
00:14:49,400 --> 00:14:52,320
Karim insiste pour qu’Ihsen prenne 40 dinars pour le taxi.

241
00:14:52,320 --> 00:14:54,320
Ihsen finit par accepter.

242
00:14:54,960 --> 00:14:56,800
En 2014,

243
00:14:56,800 --> 00:15:00,760
Karim demande à Ihsen des extraits de naissance de plusieurs personnes.

244
00:15:00,760 --> 00:15:02,760
“C’est légal”,

245
00:15:02,760 --> 00:15:04,760
affirme l’agent municipal.

246
00:15:09,320 --> 00:15:12,240
Karim évoque ensuite l’existence d’un ami tunisien qu’il a perdu de vue.

247
00:15:12,240 --> 00:15:15,560
Il sait qu’il est enregistré à l’Aouina,

248
00:15:15,560 --> 00:15:18,680
qu’il est né entre 1980 et 1990

249
00:15:18,680 --> 00:15:20,680
mais n’a pas d’informations supplémentaires.

250
00:15:21,040 --> 00:15:23,000
Tous les jours, pendant une semaine,

251
00:15:23,000 --> 00:15:25,800
Ihsen accepte de le laisser consulter les registres de ces années-là,

252
00:15:25,800 --> 00:15:27,800
“de bonne foi”,

253
00:15:27,800 --> 00:15:30,400
en sa présence.

254
00:15:33,120 --> 00:15:35,960
Dans les premières auditions avec la brigade criminelle,

255
00:15:35,960 --> 00:15:37,960
sans la présence des ses avocat·es,

256
00:15:37,960 --> 00:15:41,720
Ihsen aurait indiqué avoir remis à Karim plusieurs registres datés des années 1980,

257
00:15:41,720 --> 00:15:43,720
dont deux seraient encore en sa possession.

258
00:15:44,440 --> 00:15:47,800
Aucun de ces registres n’a été retrouvé chez Karim.

259
00:15:48,280 --> 00:15:50,280
Devant le juge d’instruction,

260
00:15:50,280 --> 00:15:54,320
Ihsen dément les faits écrits dans les procès-verbaux

261
00:15:54,320 --> 00:15:57,320
et affirme avoir signé sous la torture.

262
00:15:57,320 --> 00:16:02,680
Il assure qu’il n’a accepté que 100 dinars au total de la part de Karim.

263
00:16:14,320 --> 00:16:16,120
Chekib

264
00:16:16,120 --> 00:16:18,880
Agent financier à la municipalité d'El Menzah 9

265
00:16:18,880 --> 00:16:20,880
Condamné à 8 ans de prison

266
00:16:28,000 --> 00:16:29,400
Chekib

267
00:16:29,400 --> 00:16:31,400
était agent financier

268
00:16:31,400 --> 00:16:35,080
puis chef de service de l’état civil à la municipalité d’El Menzah.

269
00:16:35,080 --> 00:16:39,240
Il est président du bureau local “colonie de vacances” de la même ville.

270
00:16:39,240 --> 00:16:41,240
Dans le cadre de cette seconde activité,

271
00:16:41,240 --> 00:16:43,720
il organise un évènement en mai 2013.

272
00:16:43,720 --> 00:16:46,440
C’est là qu’il rencontre un étranger qui parle français,

273
00:16:46,440 --> 00:16:48,440
Mikhaïl.

274
00:16:50,440 --> 00:16:52,600
Le lendemain, le Russe est de retour avec son ami Karim.

275
00:16:52,600 --> 00:16:54,600
Ce dernier se présente comme un chef d’entreprise

276
00:16:54,600 --> 00:16:56,600
qui veut organiser des activités culturelles.

277
00:16:56,600 --> 00:16:59,920
Il soumet l’idée d’une collaboration tuniso-russe à Chakib

278
00:16:59,920 --> 00:17:02,520
mais un an passe sans qu’ils ne se donnent aucune nouvelle.

279
00:17:03,320 --> 00:17:04,840
En mai 2014,

280
00:17:04,840 --> 00:17:06,840
Karim recontacte Chakib par téléphone.

281
00:17:06,840 --> 00:17:10,000
Les deux hommes reparlent de la collaboration sans donner de rendez-vous.

282
00:17:11,280 --> 00:17:12,720
En 2015,

283
00:17:12,720 --> 00:17:15,400
Karim et Chakib se retrouvent autour d’un verre dans un hôtel.

284
00:17:15,840 --> 00:17:18,800
Il demande au Russe ce qu’il en est de cette collaboration

285
00:17:19,120 --> 00:17:22,720
mais Karim est plus intéressé par ses fonctions à la municipalité,

286
00:17:22,720 --> 00:17:27,720
que par la collaboration tunis-russe dans les colonies de vacances.

287
00:17:28,160 --> 00:17:33,600
Il veut savoir comment enregistrer une naissance au-delà du délai légal de 10 jours.

288
00:17:34,000 --> 00:17:39,640
Il demande également s’il existe des statistiques concernant les naissances et les décès au sein de son administration.

289
00:17:39,640 --> 00:17:41,120
Coïncidence,

290
00:17:41,120 --> 00:17:43,120
Chakib avait accès à ce type d’informations,

291
00:17:43,120 --> 00:17:45,680
en préparation du cinquantenaire de la municipalité.

292
00:17:45,680 --> 00:17:48,440
Il promet de remettre le document à Karim.

293
00:17:51,360 --> 00:17:54,000
Lors de son audition en juillet 2015,

294
00:17:54,000 --> 00:17:59,240
la présidente de la municipalité indique n’avoir jamais été tenue au courant de ces préparatifs,

295
00:17:59,240 --> 00:18:03,320
“Le cinquantenaire devrait avoir lieu en 2029 !”

296
00:18:03,320 --> 00:18:06,480
vu qu'elle a été créée en 1979.

297
00:18:10,480 --> 00:18:12,480
En avril 2015,

298
00:18:12,480 --> 00:18:15,000
Chakib et Karim se revoient pour la dernière fois.

299
00:18:15,000 --> 00:18:18,000
Le premier remet à Karim un livret de famille vierge.

300
00:18:18,000 --> 00:18:20,000
Chekib n’y voit pas d’inconvénients.

301
00:18:20,000 --> 00:18:24,560
Par contre, pour les tampons certifiant la conformité d’un document avec l’original, Chakib refuse.

302
00:18:24,560 --> 00:18:29,640
Il doit d’ailleurs partir rapidement, il ne voudrait pas rater l’anniversaire de sa fille.

303
00:18:30,120 --> 00:18:32,160
Karim propose de le raccompagner

304
00:18:32,160 --> 00:18:37,280
et lui offre du chocolat, des pâtes italiennes, ainsi que du foie gras, pour faire plaisir à sa fille.

305
00:18:38,120 --> 00:18:40,560
Chakib n’y voit toujours pas d’inconvénients.

306
00:18:40,560 --> 00:18:44,080
Ils prévoient enfin de se revoir le 18 mai,

307
00:18:44,080 --> 00:18:48,760
mais le Tunisien est arrêté trois jours plus tard.

308
00:18:49,720 --> 00:18:52,320
Pourquoi ces entrevues sont prévues à l’avance ?

309
00:18:53,320 --> 00:18:57,120
Au cas où ils ne parviendraient pas à se joindre par téléphone,

310
00:18:57,120 --> 00:19:02,480
assure Chakib, qui voulait juste organiser un évènement culturel tuniso-russe.

311
00:19:18,480 --> 00:19:20,480
Kaïs

312
00:19:20,480 --> 00:19:21,640
greffier de justice.

313
00:19:21,640 --> 00:19:23,160
Libre.

314
00:19:31,160 --> 00:19:32,520
Kais

315
00:19:32,520 --> 00:19:34,520
travaille au ministère de la Justice.

316
00:19:37,320 --> 00:19:38,960
Un jour de l’année 2014,

317
00:19:38,960 --> 00:19:42,440
il se pose seul à un café à la Marsa pour regarder un match de football

318
00:19:42,440 --> 00:19:45,480
Un homme lui demande s’il peut s’asseoir à côté de lui.

319
00:19:45,480 --> 00:19:48,280
Il s'agit de Karim.

320
00:19:48,280 --> 00:19:50,280
Au fil de la discussion,

321
00:19:50,280 --> 00:19:52,880
Karim dit à Kaïs qu’il l’a déjà vu au ministère de la Justice

322
00:19:52,880 --> 00:19:54,360
et lui demande de l’aider.

323
00:19:54,360 --> 00:19:58,920
Il cherche à savoir quels sont les documents nécessaire pour obtenir la nationalité tunisienne.

324
00:20:00,920 --> 00:20:02,920
Ils se revoient la semaine suivante.

325
00:20:02,920 --> 00:20:04,920
Karim prétend qu’il est d’origine tunisienne

326
00:20:04,920 --> 00:20:06,920
mais qu’il a vécu au Liban, d'où son accent.

327
00:20:07,360 --> 00:20:10,080
Ce jour-là, Kaïs est embêté, il a perdu son téléphone.

328
00:20:10,080 --> 00:20:10,960
Un mois plus tard,

329
00:20:10,960 --> 00:20:14,360
il revoit Karim qui n’est pas venu les mains vides.

330
00:20:14,360 --> 00:20:17,360
Il a un nouveau téléphone à lui offrir.

331
00:20:17,360 --> 00:20:19,840
Au nom de “l’amitié” qui les lie, Kaïs accepte

332
00:20:19,840 --> 00:20:22,240
et lui offre en retour une bouteille d’huile d’olive.

333
00:20:22,520 --> 00:20:25,280
Selon le procès-verbal établi par la brigade criminelle,

334
00:20:25,280 --> 00:20:28,560
La dernière rencontre entre eux a lieu en mars 2015

335
00:20:39,320 --> 00:20:41,120
Mikhaïl et Karim,

336
00:20:41,120 --> 00:20:43,560
espions russes en liberté.

337
00:20:50,800 --> 00:20:52,520
Mikhaïl Salikov

338
00:20:52,520 --> 00:20:55,640
apparaît pour la première fois en 2010, dans la municipalité de Bab Souika,

339
00:20:55,640 --> 00:20:58,280
pour disparaître

340
00:20:58,280 --> 00:21:00,760
au début de l’été 2013.

341
00:21:01,360 --> 00:21:03,680
Il a noué des liens étroits avec Samia et Mourad,

342
00:21:03,680 --> 00:21:06,400
mais n’a vu qu’une seule fois Chakib, en présence de Karim.

343
00:21:06,400 --> 00:21:08,400
Il n’a jamais rencontré Ihsen.

344
00:21:08,400 --> 00:21:10,400
Officiellement,

345
00:21:10,400 --> 00:21:12,400
il est diplomate au sein de l’ambassade de Russie

346
00:21:12,400 --> 00:21:14,400
mais il y a très peu d’informations sur lui.

347
00:21:15,000 --> 00:21:16,400
Avant de disparaître,

348
00:21:16,400 --> 00:21:20,720
Mikhaïl a, à chaque fois, présenté Karim aux personnes qu’il a ciblées.

349
00:21:20,720 --> 00:21:26,040
Avec Samia et Mourad, il a ouvert la voie à son remplaçant pour recueillir les documents qu’ils convoitaient.

350
00:21:26,960 --> 00:21:30,520
Kyamran Rasim Ogly Ragimov

351
00:21:30,520 --> 00:21:32,880
apparaît en 2013.

352
00:21:32,880 --> 00:21:34,880
Contrairement à Mikhaïl,

353
00:21:34,880 --> 00:21:36,880
il parle arabe et ne donne pas son vrai nom.

354
00:21:37,960 --> 00:21:40,280
Il se fait appeler Karim Ibrahim.

355
00:21:40,920 --> 00:21:45,480
Avec Samia et Mourad, il montre son intérêt pour deux identités en particulier :

356
00:21:45,480 --> 00:21:47,480
Meriem Ben Idriss,

357
00:21:47,480 --> 00:21:50,800
une femme enregistrée à Bab Souika mais qui n’a jamais existé,

358
00:21:50,800 --> 00:21:52,120
et Orion Dupont,

359
00:21:52,120 --> 00:21:54,120
un mort-né enregistré à Bizerte.

360
00:22:06,480 --> 00:22:08,480
Le 16 mai 2015,

361
00:22:09,040 --> 00:22:13,360
Samia lui donne rendez-vous pour récupérer deux registres de naissance.

362
00:22:14,040 --> 00:22:17,560
Devant l’église de Bizerte, il attend avec un cartable à la main.

363
00:22:17,960 --> 00:22:20,280
Samia vient, mais n'est pas seule.

364
00:22:20,280 --> 00:22:23,040
Elle est accompagnée de policiers.

365
00:22:47,480 --> 00:22:51,280
Pendant son audition à la brigade criminelle de Tunis,

366
00:22:51,280 --> 00:22:54,000
Karim confirme les agissements énumérés par la police.

367
00:22:54,240 --> 00:22:57,000
Avez-vous demandé à Samia tel document ?

368
00:22:57,000 --> 00:22:58,240
Oui.

369
00:22:59,000 --> 00:23:00,840
Avez-vous donné de l’argent à Mourad ?

370
00:23:00,840 --> 00:23:02,840
Oui.

371
00:23:02,840 --> 00:23:04,960
Aviez-vous en votre possession tel registre ?

372
00:23:04,960 --> 00:23:06,960
Oui.

373
00:23:06,960 --> 00:23:11,160
Mais pour ce qui est des raisons de ces agissements, la réponse sera toujours la même :

374
00:23:11,160 --> 00:23:13,800
“Je refuse de répondre à cette question”.

375
00:23:15,800 --> 00:23:20,000
L’interrogatoire ne dure pas très longtemps, vu qu'il est diplomate.

376
00:23:20,000 --> 00:23:22,000
Avant de le libérer,

377
00:23:22,000 --> 00:23:24,720
la police l’emmène à son domicile pour une perquisition.

378
00:23:24,720 --> 00:23:28,480
Les agents y trouvent plusieurs documents vierges relatifs à l’état civil :

379
00:23:28,480 --> 00:23:33,160
certificats de décès, de naissance, de mariage ou spécifiques aux morts-nés.

380
00:23:33,160 --> 00:23:38,040
Mais Karim ne risque rien, il bénéficie de l’immunité accordée aux diplomates

381
00:23:38,040 --> 00:23:40,040
et disparaît à son tour.

382
00:23:44,040 --> 00:23:47,040
Plusieurs zones d'ombres subsistent dans cette affaire.

383
00:23:47,040 --> 00:23:49,040
D'abord,

384
00:23:49,040 --> 00:23:51,040
Comment le réseau a-t-il été découvert?

385
00:23:52,160 --> 00:23:53,360
Deuxièmement,

386
00:23:53,360 --> 00:23:56,640
Comment Kais a été lavé de tout soupçon aussi rapidement ?

387
00:23:56,640 --> 00:23:58,160
Troisièmement,

388
00:23:58,160 --> 00:24:00,360
Une personne n'a pas été convoquée

389
00:24:00,360 --> 00:24:05,600
alors que son témoignage aurait été capital pour alléger ou confirmer les charges retenues contre Mourad.

390
00:24:05,600 --> 00:24:09,240
Il s’agit d’Amel, une de ses employé·es.

391
00:24:09,240 --> 00:24:10,760
Ensuite,

392
00:24:10,760 --> 00:24:13,320
des dépositions ont évolué plusieurs fois.

393
00:24:13,320 --> 00:24:15,320
Enfin,

394
00:24:15,320 --> 00:24:18,440
Malgré l'extradition de Karim,

395
00:24:18,440 --> 00:24:20,440
et le changement de l'ambassadeur russe,

396
00:24:20,440 --> 00:24:25,840
Les autorités tunisiennes et russes refusent de donner plus de détails sur l'affaire.

397
00:24:26,240 --> 00:24:29,120
Au bureau des légendes, le secret est bien gardé.

398
00:24:43,760 --> 00:24:45,320
Article lu

399
00:24:45,320 --> 00:24:46,920
produit par Inkyfada

400
00:24:46,920 --> 00:24:48,280
Texte et réalisation

401
00:24:48,280 --> 00:24:49,840
Monia Ben Hamadi

402
00:24:49,920 --> 00:24:51,000
Conseils

403
00:24:51,000 --> 00:24:53,000
Bochra Triki et Marwen Ben Mustapha

404
00:24:53,280 --> 00:24:55,640
Traduction et voix: Majd Mastoura

405
00:24:55,640 --> 00:24:57,160
Montage et mixage

406
00:24:57,160 --> 00:24:59,160
Yassine Kawana

407
00:24:59,160 --> 00:25:01,160
Musique : Omar Aloulou
