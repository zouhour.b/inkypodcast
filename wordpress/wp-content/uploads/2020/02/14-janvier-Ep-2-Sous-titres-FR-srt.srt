﻿1
00:00:02,500 --> 00:00:04,770
En arrivant dans la rue Bab el Khadra,

2
00:00:04,770 --> 00:00:06,480
je découvre une marée humaine.

3
00:00:07,280 --> 00:00:09,420
J’ai demandé ce qui se passait,

4
00:00:09,420 --> 00:00:11,860
on m’a répondu : “C’est l’enterrement de Helmi”.

5
00:00:11,860 --> 00:00:12,890
- “Helmi qui ?”

6
00:00:12,890 --> 00:00:15,250
- “Celui qui a été tué hier.”

7
00:00:15,980 --> 00:00:17,720
Il avait 23 ans,

8
00:00:17,720 --> 00:00:20,260
exactement comme le règne de Ben Ali.

9
00:00:20,260 --> 00:00:22,610
Quelle coïncidence.

10
00:01:27,740 --> 00:01:30,650
14 janvier 2011.

11
00:01:30,650 --> 00:01:33,310
Beaucoup de personnes ont connu la peur et l’incertitude

12
00:01:33,310 --> 00:01:35,370
face à des événements inédits.

13
00:01:35,370 --> 00:01:36,940
Parmi elles, Hamideddine,

14
00:01:36,940 --> 00:01:39,500
50 ans, photographe.

15
00:01:40,450 --> 00:01:42,820
Comme il le dit, il n’a jamais été militant,

16
00:01:42,820 --> 00:01:44,440
il était habitué aux photos de Sidi Bou Saïd,

17
00:01:44,440 --> 00:01:46,360
de la Médina ou des matchs de tennis.

18
00:01:47,120 --> 00:01:50,050
Mais il a trouvé le courage de descendre sur l’avenue Habib Bourguiba

19
00:01:50,050 --> 00:01:51,440
pour prendre des photos.

20
00:01:53,680 --> 00:01:54,780
Sur les toits,

21
00:01:54,780 --> 00:01:56,750
il photographie les manifestations, les policiers

22
00:01:56,750 --> 00:01:59,040
les militaires, les gens, dans les avenues

23
00:01:59,040 --> 00:02:00,620
et les rues adjacentes,

24
00:02:01,560 --> 00:02:03,880
jusqu’à ce qu’il se retrouve à Bab El Khadra,

25
00:02:03,880 --> 00:02:06,040
où il croise le cortège funéraire accompagnant Helmi,

26
00:02:06,040 --> 00:02:07,420
23 ans,

27
00:02:07,420 --> 00:02:10,490
tué le 13 janvier au Passage.

28
00:02:12,290 --> 00:02:14,040
Le cortège se dirige vers l’Avenue

29
00:02:14,040 --> 00:02:15,500
et Hamideddine immortalise l’instant.

30
00:02:16,340 --> 00:02:18,430
Les “takbir” se mélangent aux “dégage”

31
00:02:18,430 --> 00:02:20,830
et autres slogans contre le pouvoir.

32
00:02:21,850 --> 00:02:25,030
Au moment où le cortège arrive avenue Habib Bourguiba,

33
00:02:25,030 --> 00:02:28,930
l’ambiance bascule et la répression se durcit.

34
00:02:31,100 --> 00:02:32,010
Par la parole,

35
00:02:32,010 --> 00:02:35,570
Hamideddine Bouali décrit les images qu’il a prises ce jour-là.

36
00:02:35,570 --> 00:02:37,570
Des images ancrées dans la mémoire.

37
00:02:40,730 --> 00:02:42,100
Pendant un an,

38
00:02:42,100 --> 00:02:44,590
en préparation de la commémoration des dix ans de la Révolution,

39
00:02:44,590 --> 00:02:46,020
Inkyfada va raconter les histoires

40
00:02:46,020 --> 00:02:47,500
de quelques personnes parmi des milliers

41
00:02:47,500 --> 00:02:49,760
qui ont vécu cette journée historique.

42
00:02:52,990 --> 00:02:55,720
Où étais-tu le 14 janvier ?

43
00:02:56,980 --> 00:03:01,040
Épisode 2 : Vues d’en haut

44
00:03:14,620 --> 00:03:16,980
Je n’ai pas commencé ma journée du 14 janvier,

45
00:03:16,980 --> 00:03:19,580
parce que je n’ai pas dormi.

46
00:03:19,580 --> 00:03:22,490
Après le discours du 13 janvier,

47
00:03:22,490 --> 00:03:26,370
j’ai compris qu’il y avait quelque chose de particulier.

48
00:03:26,370 --> 00:03:28,160
J’ai écrit sur Facebook

49
00:03:28,160 --> 00:03:32,220
que Ben Ali avait promis de changer beaucoup de choses,

50
00:03:32,220 --> 00:03:36,380
d'organiser des élections. Autant lui donner sa chance.

51
00:03:37,370 --> 00:03:39,300
En plus, beaucoup de gens sont morts,

52
00:03:39,300 --> 00:03:42,480
il fallait arrêter, le prendre au mot.

53
00:03:42,480 --> 00:03:45,200
Moi je n’ai pas milité, je le dis toujours,

54
00:03:45,200 --> 00:03:47,280
je suis là pour témoigner.

55
00:03:47,280 --> 00:03:51,310
Je n’ai pas la force ni le courage d’être révolutionnaire,

56
00:03:51,310 --> 00:03:54,680
je devais au moins prendre des photos.

57
00:03:58,440 --> 00:04:01,920
Donc j’ai passé une nuit à réfléchir.

58
00:04:13,180 --> 00:04:15,510
Le lendemain, j’ai pris le métro,

59
00:04:15,510 --> 00:04:19,580
à 10h du matin, il n’était pas bondé.

60
00:04:20,860 --> 00:04:22,860
Tout était normal à Tunis

61
00:04:22,860 --> 00:04:25,630
sauf la présence militaire.

62
00:04:25,630 --> 00:04:26,870
Il y avait plein de gens

63
00:04:26,870 --> 00:04:29,560
mais personne n’avait de drapeau ou de pancarte,

64
00:04:29,560 --> 00:04:31,380
il n’y avait rien de spécial,

65
00:04:31,380 --> 00:04:35,350
on aurait dit un samedi.

66
00:04:35,350 --> 00:04:36,680
Rien de particulier.

67
00:04:37,100 --> 00:04:39,380
J’ai commencé à prendre des photos au Passage

68
00:04:39,380 --> 00:04:41,920
dont un soldat à bout portant,

69
00:04:41,920 --> 00:04:47,340
parce que j’ai senti que j’avais une mission

70
00:04:47,340 --> 00:04:48,660
et que j’avais carte blanche.

71
00:04:48,660 --> 00:04:50,740
Ce que je ne ressentais pas les jours précédents.

72
00:04:50,740 --> 00:04:55,550
Je descendais avec mon appareil photo dans mon sac, la peur au ventre,

73
00:04:55,550 --> 00:05:02,140
je n’avais aucune expérience de photos au milieu de manifestants

74
00:05:02,140 --> 00:05:04,630
et j’étais peureux comme la plupart des Tunisien·nes.

75
00:05:04,630 --> 00:05:08,720
On ne peut devenir du jour au lendemain un reporter photographe,

76
00:05:08,720 --> 00:05:12,820
surtout en Tunisie où tout est bouclé, cadenassé.

77
00:05:12,820 --> 00:05:18,510
Les policiers savent si tu es photographe de presse ou pas.

78
00:05:22,000 --> 00:05:25,020
J’ai photographié des militaires devant le jardin Habib Thameur,

79
00:05:25,020 --> 00:05:28,910
ensuite j’ai pris une photo d’un soldat

80
00:05:28,910 --> 00:05:35,590
au visage dur, tenant son fusil,

81
00:05:35,590 --> 00:05:37,690
et en face de lui, une petite fille de dos

82
00:05:37,690 --> 00:05:38,860
qui le regarde sans comprendre.

83
00:05:38,860 --> 00:05:41,180
C’était la première fois qu’elle assistait à ce type de scène.

84
00:05:41,180 --> 00:05:42,790
Et sa mère l’appelait.

85
00:05:42,790 --> 00:05:47,590
La fille doit avoir la vingtaine aujourd’hui.

86
00:05:47,590 --> 00:05:49,410
Je me pose toujours cette question :

87
00:05:49,410 --> 00:05:53,670
que sont devenus les gens que j’ai pris en photo ?

88
00:06:10,140 --> 00:06:12,740
Après, j’ai remonté l’avenue Habib Bourguiba.

89
00:06:12,740 --> 00:06:14,140
J’étais étonné

90
00:06:14,140 --> 00:06:18,080
parce qu’avant, on n’avait pas le droit

91
00:06:18,080 --> 00:06:20,780
d’aller sur le trottoir du ministère de l’Intérieur.

92
00:06:20,780 --> 00:06:22,480
Ce n’était pas fermé comme maintenant,

93
00:06:22,480 --> 00:06:24,160
mais tu comprenais tout seul

94
00:06:24,160 --> 00:06:26,620
qu’il fallait changer de trottoir.

95
00:06:26,620 --> 00:06:32,450
Là, les gens étaient nez-à-nez avec la police,

96
00:06:32,450 --> 00:06:34,780
ce qui était impossible avant.

97
00:06:34,780 --> 00:06:36,910
On avait même peur des agent·es de circulation,

98
00:06:36,910 --> 00:06:38,790
alors ne parlons pas des BOP.

99
00:06:38,790 --> 00:06:41,620
Même le bâtiment du ministère est terrifiant,

100
00:06:41,620 --> 00:06:44,580
l’architecte savait ce qu’il faisait.

101
00:06:44,580 --> 00:06:48,670
Les murs sont rêches avec des fenêtres exiguës.

102
00:06:48,670 --> 00:06:51,580
La façade transpire la répression,

103
00:06:51,580 --> 00:06:54,050
Les pierres hurlent

104
00:06:54,050 --> 00:06:56,340
comme si elles tenaient une matraque pour frapper.

105
00:07:00,520 --> 00:07:02,480
Donc je suis arrivé devant le ministère,

106
00:07:02,480 --> 00:07:05,110
j’ai photographié une jeune femme tenant un drapeau,

107
00:07:05,110 --> 00:07:06,410
souriante, très belle.

108
00:07:06,410 --> 00:07:08,510
Elle rappelait Mai 68.

109
00:07:08,510 --> 00:07:11,760
J’enseigne l’Histoire de la photographie

110
00:07:11,760 --> 00:07:13,100
et il y a une photo prise en Mai 68

111
00:07:13,100 --> 00:07:14,360
qui lui ressemble beaucoup.

112
00:07:14,360 --> 00:07:17,430
Quand on couvre ce type d’événement,

113
00:07:17,430 --> 00:07:19,550
on garde ces photos en tête,

114
00:07:19,550 --> 00:07:22,440
comme l’homme debout devant des tanks.

115
00:07:22,440 --> 00:07:25,080
Inconsciemment, on essaie de reproduire ces photos,

116
00:07:25,080 --> 00:07:26,880
parce qu’elles sont iconiques,

117
00:07:26,880 --> 00:07:28,440
elles sont restées dans l'Histoire.

118
00:07:28,440 --> 00:07:33,200
C’est le rêve de tout photographe de prendre une photo qui deviendra iconique.

119
00:07:35,720 --> 00:07:37,360
Après, je me suis rendu compte

120
00:07:37,360 --> 00:07:39,200
que pour photographier autant de gens,

121
00:07:39,200 --> 00:07:41,200
il me fallait un endroit surélevé.

122
00:07:46,620 --> 00:07:50,580
Je suis rentré dans le bâtiment juste en face du ministère de l’Intérieur.

123
00:07:52,200 --> 00:07:53,360
J’ai grimpé les escaliers,

124
00:07:53,780 --> 00:07:55,420
la porte menant vers le toit était ouverte,

125
00:07:55,420 --> 00:07:56,840
comme par hasard.

126
00:08:01,000 --> 00:08:01,760
Sur le toit,

127
00:08:01,760 --> 00:08:04,750
je découvre qu’une dizaine de personnes ont eu la même idée que moi.

128
00:08:04,750 --> 00:08:06,100
Évidemment, j’ai pris des photos,

129
00:08:06,720 --> 00:08:09,440
dont un homme qui tenait une pancarte où il était écrit

130
00:08:09,440 --> 00:08:11,440
“Ben Ali dehors”.

131
00:08:12,910 --> 00:08:16,660
Il n’y avait personne autour de lui, sur au moins 10 mètres.

132
00:08:16,660 --> 00:08:18,230
Les gens se sont éloignés de lui.

133
00:08:18,230 --> 00:08:19,110
Ils avaient peur.

134
00:08:19,110 --> 00:08:21,220
Si quelqu’un avait voulu le viser et le tuer,

135
00:08:21,220 --> 00:08:22,640
il aurait pu le faire facilement.

136
00:08:22,640 --> 00:08:24,030
Donc les gens s'éloignaient

137
00:08:24,030 --> 00:08:27,470
car c’était la première pancarte

138
00:08:27,470 --> 00:08:30,140
où Ben Ali était désigné nominativement.

139
00:08:30,140 --> 00:08:30,960
“Dehors”.

140
00:08:30,960 --> 00:08:33,420
C’était d’un courage inouï

141
00:08:33,420 --> 00:08:36,280
parce qu’il ne savait pas ce qui allait se passer après.

142
00:08:40,300 --> 00:08:42,050
Du bâtiment où je me trouvais,

143
00:08:42,050 --> 00:08:45,180
j’ai photographié des personnes autour de la fontaine.

144
00:08:45,970 --> 00:08:50,760
Elles restaient à distance, à regarder sans comprendre.

145
00:08:50,760 --> 00:08:53,170
Les gens qui étaient là ne savaient pas

146
00:08:53,170 --> 00:08:57,640
ce que leur geste, leur présence, leurs slogans

147
00:08:58,220 --> 00:09:00,520
allaient produire.

148
00:09:02,520 --> 00:09:04,290
Personne ne savait.

149
00:09:08,720 --> 00:09:10,350
J’avais un Lumix,

150
00:09:10,350 --> 00:09:15,790
un appareil photo de souvenir touristique.

151
00:09:15,790 --> 00:09:17,080
Je n’avais presque plus de batterie,

152
00:09:17,080 --> 00:09:18,630
la carte mémoire était presque pleine,

153
00:09:18,630 --> 00:09:21,600
je me suis dit qu’il était temps de partir.

154
00:09:22,920 --> 00:09:27,960
Quand tu n’as presque plus de mémoire dans ton appareil photo,

155
00:09:28,280 --> 00:09:29,310
il vaut mieux en garder

156
00:09:29,310 --> 00:09:31,830
car tu ne sais jamais sur quoi tu vas tomber en rentrant.

157
00:09:31,830 --> 00:09:34,150
Tu peux trouver un scoop bien plus important

158
00:09:34,150 --> 00:09:38,860
donc autant garder de l'espace.

159
00:09:39,620 --> 00:09:40,910
Je suis donc sorti.

160
00:09:41,490 --> 00:09:43,580
J’ai photographié une affiche de Ben Ali,

161
00:09:43,580 --> 00:09:46,020
encore accrochée devant la Cathédrale,

162
00:09:46,020 --> 00:09:49,820
avec un blindé militaire au premier plan.

163
00:09:49,820 --> 00:09:52,750
C’était la dernière fois que je photographiais Ben Ali.

164
00:09:57,420 --> 00:09:59,970
En arrivant dans le rue Bab El Khadra

165
00:09:59,970 --> 00:10:02,480
je découvre une marée humaine.

166
00:10:06,320 --> 00:10:08,980
J’ai demandé ce qui se passait,

167
00:10:09,580 --> 00:10:12,160
on m’a répondu : “C’est l’enterrement de Helmi”.

168
00:10:12,160 --> 00:10:13,040
- “Helmi qui ?”

169
00:10:13,040 --> 00:10:15,040
- “Celui qui a été tué hier."

170
00:10:20,220 --> 00:10:23,680
J'ai vu des gens sur un balcon,

171
00:10:24,700 --> 00:10:28,920
devant la rue où allait passer le cortège.

172
00:10:28,920 --> 00:10:30,920
Je leur ai demandé si je pouvais les rejoindre,

173
00:10:30,920 --> 00:10:33,000
ils ont accepté et je suis monté.

174
00:10:33,000 --> 00:10:34,680
La rue était noire de monde.

175
00:10:34,680 --> 00:10:37,580
Les gens brandissaient des photos de Helmi.

176
00:10:37,580 --> 00:10:39,280
Il avait 23 ans,

177
00:10:39,280 --> 00:10:41,910
exactement comme le règne de Ben Ali.

178
00:10:41,910 --> 00:10:43,880
Quelle coïncidence.

179
00:10:44,260 --> 00:10:46,340
À ce moment-là, Ben Ali était encore au Palais.

180
00:10:46,340 --> 00:10:48,590
Il devait être 14h.

181
00:10:53,180 --> 00:10:55,630
J’étais donc debout sur le balcon pour prendre des photos

182
00:10:55,630 --> 00:10:58,270
et là, un type a dit : "Celui-là c'est un flic".

183
00:10:59,070 --> 00:11:00,370
Les gens ont commencé à crier.

184
00:11:00,370 --> 00:11:03,840
Un homme m'a demandé d’où je venais,

185
00:11:03,840 --> 00:11:05,260
si j’étais journaliste.

186
00:11:05,260 --> 00:11:06,120
J'ai répondu non.

187
00:11:06,120 --> 00:11:07,380
-“Alors qu’est-ce que tu fais là ?”

188
00:11:07,380 --> 00:11:09,900
J'ai répondu que j'étais juste un passionné de photos.

189
00:11:09,900 --> 00:11:11,500
Et les gens criaient de l’autre côté,

190
00:11:11,500 --> 00:11:15,840
d’autres lançaient des pierres sur le balcon.

191
00:11:15,840 --> 00:11:17,740
Le jeune homme a commencé à avoir peur.

192
00:11:18,390 --> 00:11:19,840
Il m'a demandé de partir.

193
00:11:23,960 --> 00:11:27,020
Ces deux secondes m'ont paru durer une éternité.

194
00:11:33,320 --> 00:11:36,200
Et d’un coup, on n’a plus entendu que “Allah akbar”.

195
00:11:36,200 --> 00:11:37,900
Le cortège funéraire est sorti.

196
00:11:40,940 --> 00:11:42,300
Plus personne ne s’occupait de moi.

197
00:11:42,300 --> 00:11:46,320
La foule en bas sortait avec le cortège.

198
00:11:46,320 --> 00:11:47,880
Donc j’ai pu prendre des photos

199
00:11:47,880 --> 00:11:50,680
que tout photographe rêve de prendre.

200
00:11:57,100 --> 00:11:58,940
Je ne vous raconte pas.

201
00:11:58,940 --> 00:12:00,630
Ce sont des photos extrêmement difficiles à prendre,

202
00:12:00,630 --> 00:12:02,870
parmi les plus difficiles que j’ai prises.

203
00:12:02,870 --> 00:12:05,220
Moi avant, je prenais des photos de portes de Sidi Bou Saïd,

204
00:12:05,220 --> 00:12:09,660
d’animaux du Belvédère, de tournois de tennis...

205
00:12:09,660 --> 00:12:11,570
Je ne couvrais pas l’actualité,

206
00:12:11,570 --> 00:12:14,780
et là, du jour au lendemain, c’était la grande actualité.

207
00:12:14,780 --> 00:12:17,260
C’est pas comme si j’avais commencé petit à petit.

208
00:12:17,260 --> 00:12:20,140
Du jour au lendemain, je photographie le ministère de l’Intérieur,

209
00:12:20,140 --> 00:12:23,500
“Game over”, “Dégage”,

210
00:12:23,500 --> 00:12:25,200
les militaires...

211
00:12:25,200 --> 00:12:26,580
et un enterrement.

212
00:12:27,160 --> 00:12:28,870
On ne peut pas photographier un enterrement,

213
00:12:28,870 --> 00:12:30,400
avec cette ambiance, sans être ému.

214
00:12:30,400 --> 00:12:32,260
Je prenais des photos et mes larmes coulaient.

215
00:12:32,260 --> 00:12:33,800
Quand j’y repense, ça me submerge encore.

216
00:12:38,740 --> 00:12:44,200
Des membres du club photo présents le 14 janvier,

217
00:12:44,200 --> 00:12:46,180
étaient tellement émus

218
00:12:46,180 --> 00:12:48,700
qu'ils ont même oublié de prendre des photos.

219
00:12:50,360 --> 00:12:52,240
Ce n'est pas du tout évident

220
00:12:52,240 --> 00:12:54,370
de prendre du recul.

221
00:12:54,370 --> 00:12:55,800
Si c’était un autre évènement,

222
00:12:55,800 --> 00:12:59,230
les manifestations à Hong Kong par exemple,

223
00:12:59,230 --> 00:13:01,230
ça n’aurait pas été pareil.

224
00:13:01,230 --> 00:13:04,780
Là je photographiais Alia, Selma, Fathi, Salah,

225
00:13:04,780 --> 00:13:06,300
des Tunisien·nes.

226
00:13:06,300 --> 00:13:08,070
Ça me touche directement.

227
00:13:08,070 --> 00:13:13,480
Car c’est différent, il s’agit de mon actualité.

228
00:13:25,140 --> 00:13:29,240
Au moment où ils allaient mettre le cercueil dans le camion,

229
00:13:29,820 --> 00:13:31,880
des gens ont décidé de le porter sur leurs épaules, à pieds,

230
00:13:31,880 --> 00:13:33,510
je les entendais d’en haut.

231
00:13:33,510 --> 00:13:40,320
Mais la mémoire de mon appareil photo était pleine.

232
00:13:40,320 --> 00:13:42,880
J’aurais pu effacer quelques photos

233
00:13:42,880 --> 00:13:44,580
et suivre l’enterrement,

234
00:13:44,580 --> 00:13:46,600
mais je ne pensais pas que le cortège allait rejoindre l'Avenue

235
00:13:46,600 --> 00:13:48,110
et que ça allait tout faire péter.

236
00:13:48,110 --> 00:13:49,690
Personne ne pouvait s’y attendre.

237
00:13:49,690 --> 00:13:51,540
D’ailleurs, aucun cortège funéraire n’est jamais passé par l’Avenue.

238
00:13:51,540 --> 00:13:55,140
Ni pour Essebsi, ni Brahmi, ni Chokri Belaïd.

239
00:13:55,140 --> 00:13:57,140
Ça doit être absolument interdit,

240
00:13:57,140 --> 00:13:58,690
je ne sais pas pourquoi.

241
00:14:11,100 --> 00:14:14,220
Le cortège est donc arrivé

242
00:14:14,220 --> 00:14:17,140
par la rue de Rome,

243
00:14:17,140 --> 00:14:20,910
avant d’entrer sur l’Avenue pour tourner avenue de Carthage.

244
00:14:20,910 --> 00:14:24,040
Comme ils criaient “Allah akbar”,

245
00:14:24,040 --> 00:14:25,620
les policiers n’ont pas compris ce qui se passait,

246
00:14:25,620 --> 00:14:27,040
ils ont commencé à frapper.

247
00:14:27,040 --> 00:14:30,870
C’est à partir de ce moment que ça a dégénéré.

248
00:14:30,870 --> 00:14:33,140
Il devait être 14h30 ou 15h.

249
00:14:35,880 --> 00:14:38,360
Moi je suis rentré à pieds à la maison.

250
00:14:41,600 --> 00:14:44,300
En gros, je suis sorti le matin pour prendre des photos,

251
00:14:44,300 --> 00:14:48,110
puis ce sont les événements qui ont pris le dessus.

252
00:14:48,110 --> 00:14:49,920
Je n'ai décidé de rien.

253
00:14:49,920 --> 00:14:52,400
je suis rentré chez moi au Bardo.

254
00:14:52,400 --> 00:14:54,310
J’ai visionné mes photos

255
00:14:54,660 --> 00:14:56,470
mais je n’ai publié que le lendemain

256
00:14:57,280 --> 00:14:58,840
parce que j’avais peur.

257
00:14:59,420 --> 00:15:03,200
Publier les photos de quelqu’un qui vient d’être tué,

258
00:15:03,200 --> 00:15:06,840
ça me faisait vraiment peur, je n’étais pas protégé.

259
00:15:06,840 --> 00:15:08,310
Je ne suis pas journaliste,

260
00:15:08,310 --> 00:15:11,550
ma page était à mon nom,

261
00:15:12,140 --> 00:15:16,570
si je publie ça, j’ai des problèmes dans l’heure.

262
00:15:16,840 --> 00:15:19,440
Rien n’était sûr à ce moment-là.

263
00:15:23,680 --> 00:15:26,240
Les gens étaient bloqués sur l’Avenue,

264
00:15:26,240 --> 00:15:28,240
dans les immeubles.

265
00:15:28,460 --> 00:15:32,640
Au Carlton, il y avait 20 personnes par chambre.

266
00:15:33,180 --> 00:15:36,380
Heureusement que le propriétaire de l’hôtel

267
00:15:37,020 --> 00:15:40,090
a empêché les policiers d’entrer.

268
00:15:40,090 --> 00:15:43,150
Les gens se réfugiaient partout.

269
00:15:43,150 --> 00:15:45,680
C’était vraiment la panique.

270
00:15:51,850 --> 00:15:53,390
Heureusement que j’ai vécu ça à 50 ans

271
00:15:53,390 --> 00:15:54,720
et pas à 20 ans.

272
00:15:54,720 --> 00:15:58,700
Parce qu’à 20 ans, j’aurais été frivole,

273
00:15:58,700 --> 00:16:00,700
je me serais enfui

274
00:16:00,700 --> 00:16:03,280
ou j'aurais fait une grasse matinée

275
00:16:03,280 --> 00:16:05,800
et je me serais réveillé à 17h.

276
00:16:05,800 --> 00:16:09,040
Je suis content aussi de l’avoir vécu à 50 ans

277
00:16:09,040 --> 00:16:10,990
et pas à mon âge actuel.

278
00:16:19,040 --> 00:16:20,920
Pour moi, le 14 janvier,

279
00:16:20,920 --> 00:16:24,090
a été tellement significatif.

280
00:16:24,480 --> 00:16:28,350
Ça coïncidait avec ma peur de la cinquantaine.

281
00:16:28,350 --> 00:16:30,000
Le 14,

282
00:16:30,270 --> 00:16:33,420
j’ai pu photographier le ministère de l’Intérieur à bout portant,

283
00:16:33,420 --> 00:16:37,530
photographier un policier de tellement près

284
00:16:37,530 --> 00:16:39,480
que je me voyais dans le reflet de son casque.

285
00:16:40,890 --> 00:16:43,020
Avant, c’était impossible,

286
00:16:43,020 --> 00:16:44,190
je n’osais pas m’en approcher

287
00:16:44,190 --> 00:16:46,890
sauf si c'était pour me demander mes papiers.

288
00:16:53,470 --> 00:16:57,020
Le 18 janvier, j’ai failli mourir,

289
00:16:57,020 --> 00:16:59,690
parce que j’ai pris la photo de trop.

290
00:17:01,690 --> 00:17:03,660
Il y a ce qu’on appelle la photo de trop

291
00:17:04,430 --> 00:17:07,050
pour laquelle beaucoup de photographes sont mort·es.

292
00:17:07,530 --> 00:17:09,610
Il faut savoir s’arrêter,

293
00:17:09,610 --> 00:17:12,330
se dire que ce qu’on a suffit,

294
00:17:12,330 --> 00:17:15,470
inutile de toujours en vouloir plus.

295
00:17:19,630 --> 00:17:21,370
C'était au Passage,

296
00:17:21,370 --> 00:17:23,600
lors d'une manifestation,

297
00:17:24,040 --> 00:17:25,180
je photographiais dans la rue,

298
00:17:25,180 --> 00:17:27,020
quand la police a chargé

299
00:17:27,020 --> 00:17:28,590
et tiré des gaz lacrymogènes.

300
00:17:30,940 --> 00:17:32,440
Les gens se sont enfuis

301
00:17:32,440 --> 00:17:35,390
et moi je suis monté sur un muret,

302
00:17:35,850 --> 00:17:39,160
collé à la station de métro du Passage.

303
00:17:39,500 --> 00:17:41,000
J’ai pris des photos

304
00:17:41,360 --> 00:17:42,720
puis je suis redescendu.

305
00:17:43,980 --> 00:17:45,720
Dans ces moments, il faut rester en éveil.

306
00:17:46,730 --> 00:17:49,440
La moindre distraction peut être mortelle.

307
00:17:49,980 --> 00:17:52,700
Mais dans un moment d’inattention,

308
00:17:52,700 --> 00:17:55,560
au milieu de la fumée des gaz lacrymogènes,

309
00:17:55,560 --> 00:17:59,310
un policier avait son lanceur de grenades

310
00:17:59,310 --> 00:18:00,990
pointé directement sur moi.

311
00:18:00,990 --> 00:18:03,340
Il s’était bouché les narines avec du papier mouchoir

312
00:18:03,340 --> 00:18:05,020
pour ne pas sentir le gaz.

313
00:18:05,720 --> 00:18:07,950
J’ai pris une photo, je me suis retourné et je me suis enfui.

314
00:18:07,950 --> 00:18:12,480
La grenade est passée à quelques centimètres de ma tête.

315
00:18:33,550 --> 00:18:36,810
Ces événements sont un concours de circonstances.

316
00:18:36,810 --> 00:18:39,420
Bien sûr, il y a eu des précédents,

317
00:18:39,420 --> 00:18:41,800
comme à Ben Guerdane, ou pendant les émeutes du pain,

318
00:18:41,800 --> 00:18:44,190
ou le 26 janvier, ou encore celles du Bassin minier

319
00:18:44,190 --> 00:18:45,470
et beaucoup d'autres

320
00:18:45,470 --> 00:18:46,430
dont on n’a pas entendu parler

321
00:18:46,430 --> 00:18:48,000
parce qu’ils ont dû être étouffés dans l’oeuf.

322
00:18:48,000 --> 00:18:52,410
C’est ce qui a fait que les gens ont saisi la première occasion

323
00:18:52,410 --> 00:18:53,930
pour renverser la table.

324
00:18:53,930 --> 00:18:56,920
Et rien ne dit que ça ne se répètera pas.

325
00:18:56,920 --> 00:18:59,980
“La liberté pour les Tunisien·nes”

326
00:19:02,030 --> 00:19:04,700
Je me souviens de la vidéo de l’avocat

327
00:19:04,700 --> 00:19:07,040
Abdennaceur Laouini.

328
00:19:11,290 --> 00:19:13,500
Comment l’oublier ?

329
00:19:14,080 --> 00:19:16,730
Ce sont des moments

330
00:19:16,730 --> 00:19:20,400
qui s’inscrivent dans la mémoire collective.

331
00:19:20,400 --> 00:19:21,950
Heureusement, il y a des images.

332
00:19:22,990 --> 00:19:25,280
Peut-être qu’on a oublié aujourd’hui,

333
00:19:25,280 --> 00:19:30,480
mais à l’époque on avait été chauffé·es à blanc

334
00:19:30,480 --> 00:19:33,850
par les images de Kasserine, prises avec des téléphones.

335
00:19:34,720 --> 00:19:37,840
Vous vous souvenez de ces personnes qui criaient à l’hôpital ?

336
00:19:37,840 --> 00:19:39,840
Imaginez si ces images n’avaient pas existé.

337
00:19:41,580 --> 00:19:43,310
Pourtant, il y a eu des événements avant,

338
00:19:43,310 --> 00:19:46,300
à Ben Guerdane ou à Redeyef,

339
00:19:46,300 --> 00:19:48,650
il n’y avait pas beaucoup d’images,

340
00:19:48,650 --> 00:19:50,240
pas de direct,

341
00:19:50,240 --> 00:19:52,010
tout le monde n’avait pas Facebook.

342
00:19:52,170 --> 00:19:54,860
C’est l’image

343
00:19:54,860 --> 00:19:58,250
qui a permis de prendre conscience de ce qui se passe.

344
00:19:58,250 --> 00:20:01,420
Car la première chose que fait un dictateur

345
00:20:02,560 --> 00:20:06,620
c’est contrôler la diffusion des images.

346
00:20:07,230 --> 00:20:08,620
S’il contrôle les images

347
00:20:08,620 --> 00:20:11,280
il peut montrer ce qu’il veut,

348
00:20:11,550 --> 00:20:14,560
dire que les gens vivent très bien dans son pays,

349
00:20:14,680 --> 00:20:16,510
que l’économie fonctionne,

350
00:20:16,510 --> 00:20:17,790
qu’il n’y a pas de pollution.

351
00:20:17,790 --> 00:20:20,600
Quand on a la possibilité de diffuser des images,

352
00:20:20,600 --> 00:20:24,040
on peut montrer la pollution, la pauvreté,

353
00:20:24,040 --> 00:20:26,970
les inégalités ou les massacres.

354
00:20:26,970 --> 00:20:30,490
Le dictateur ne peut plus tenir en place.

355
00:20:36,300 --> 00:20:39,280
Je dis toujours que 2011 est une année romantique,

356
00:20:39,280 --> 00:20:41,420
parce qu’il s’est passé des choses

357
00:20:41,420 --> 00:20:43,040
hors du commun.

358
00:20:43,580 --> 00:20:45,680
Par exemple, devant la Kasbah,

359
00:20:45,680 --> 00:20:48,540
j’ai photographié la plus petite manifestation.

360
00:20:49,100 --> 00:20:52,440
Une famille, une mère et ses trois enfants,

361
00:20:52,440 --> 00:20:53,740
brandissaient des pancartes,

362
00:20:53,740 --> 00:20:56,740
parce que le père a dû revenir malade de Libye

363
00:20:56,740 --> 00:21:00,420
et la famille n’a plus aucune source de revenus.

364
00:21:01,080 --> 00:21:03,780
Parfois, on oublie tout le chemin parcouru.

365
00:21:03,780 --> 00:21:07,840
Avant, dans les cafés,

366
00:21:07,840 --> 00:21:10,040
on ne pouvait parler que de foot,

367
00:21:10,040 --> 00:21:12,540
rien que le fait de dire que le prix du lait

368
00:21:12,540 --> 00:21:14,740
a augmenté de 200 millimes

369
00:21:14,740 --> 00:21:16,220
était mal vu.

370
00:21:29,180 --> 00:21:31,820
J'ai pu avoir la liberté de publier sur mon blog

371
00:21:31,820 --> 00:21:34,760
toutes mes photos.

372
00:21:35,120 --> 00:21:38,980
J'ai senti alors que je faisais partie du monde.

373
00:21:38,980 --> 00:21:42,020
Avant, ce n’était pas le cas, on se cachait.

374
00:21:42,580 --> 00:21:44,380
Maintenant, on ne peut plus rien cacher.

375
00:21:44,380 --> 00:21:45,960
Quand il y a un événement,

376
00:21:45,960 --> 00:21:47,960
tout le monde en parle.

377
00:21:48,680 --> 00:21:51,050
C’est une fierté d’avoir vécu ce moment.

378
00:21:51,050 --> 00:21:55,300
On pourra le raconter aux futures générations,

379
00:21:55,620 --> 00:21:58,960
comme les gens qui ont vécu Mai 68 en France, par exemple.

380
00:21:58,960 --> 00:22:00,780
Ce sont des événements à vivre.

381
00:22:00,780 --> 00:22:03,040
Il y a des événements qu’on peut te raconter,

382
00:22:03,040 --> 00:22:04,280
tu n’auras rien perdu,

383
00:22:04,280 --> 00:22:06,280
mais d’autres qu’il faut vivre.

384
00:22:09,820 --> 00:22:11,080
On a compris beaucoup de choses.

385
00:22:11,080 --> 00:22:14,400
Avant, on s’en foutait de la politique.

386
00:22:14,400 --> 00:22:19,480
À présent, la culture politique en Tunisie est exceptionnelle.

387
00:22:19,480 --> 00:22:22,180
Les Tunisien·nes suivent le Parlement, les député·es,

388
00:22:22,180 --> 00:22:24,880
parlent d’immunité ou de Constitution,

389
00:22:24,880 --> 00:22:27,140
on s’est énormément cultivés en 10 ans,

390
00:22:27,140 --> 00:22:30,500
y compris les enfants, à force d’écouter tout ça.

391
00:22:30,500 --> 00:22:34,840
Avant, on ne parlait que de foot, de Saint-Valentin,

392
00:22:34,840 --> 00:22:37,240
de l’Aïd, de l'achat du mouton ou des gâteaux,

393
00:22:37,240 --> 00:22:38,100
et c'est tout.

394
00:22:38,100 --> 00:22:40,760
Maintenant, tout ça est devenu accessoire.

395
00:22:40,760 --> 00:22:43,980
C’est la politique qui nous importe le plus,

396
00:22:44,600 --> 00:22:46,560
parce que c’est ce qui impacte nos vies.

397
00:22:48,560 --> 00:22:51,100
On est devenu·es adultes du jour au lendemain.

398
00:23:04,180 --> 00:23:06,380
Où étais-tu le 14 janvier ?

399
00:23:06,380 --> 00:23:08,980
Épisode 2 : Vues d’en haut

400
00:23:09,320 --> 00:23:10,680
Production

401
00:23:10,680 --> 00:23:12,420
Inkyfada

402
00:23:16,980 --> 00:23:20,180
Réalisation, montage, mixage

403
00:23:20,180 --> 00:23:23,380
Bochra Triki, Hazar Abidi

404
00:23:23,380 --> 00:23:26,360
Monia Ben Hamadi, Yassine Kawana

405
00:23:27,600 --> 00:23:28,960
Musique

406
00:23:28,960 --> 00:23:29,840
Oussema Gaidi

407
00:23:30,600 --> 00:23:31,980
Voix off

408
00:23:31,980 --> 00:23:33,400
Hazar Abidi

409
00:23:34,390 --> 00:23:36,820
Avec la collaboration de toute l'équipe d'Inkyfada
