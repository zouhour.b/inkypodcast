<?php 
/**
* Plugin Name: inkube dev mode
**/

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/vendor/symfony/var-dumper/Resources/functions/dump.php';
add_filter('wp_get_attachment_image_src', function($image){
	if (!$image) return $image;
	$image[0] = str_replace('.localhost', '.com', $image[0]);
	return $image;
});
