<?php 
/**
 * Plugin Name: Inkyfada Migration
 * 
 * 

 */

add_action('inkube_loaded', function(){
	include_once 'migrations/authors.php';
	include_once 'migrations/teamPosition.php';
	include_once 'migrations/nicenames.php';
	include_once 'migrations/webdocs.php';
	include_once 'migrations/themes-folder.php';
	include_once 'migrations/translations.php';
	include_once 'migrations/mapTerrorism.php';
	include_once 'migrations/formats.php';

});