<?php 

Inkube\Migration\MigrationModule::registerMigration('post_translations_relationships', 'Post Translations', '', function(){
	$ars = get_posts(['lang'=>'ar', 'numberposts'=>-1]);
	$table = [];
	$tr=0;
	foreach ($ars as $par){
		$row = ['ar'=>sprintf('<a href="/?p=%s" target="_blank">%s</a>', $par->ID, $par->post_title), 'fr'=>''];
		$frid = pll_get_post($par->ID, 'fr');
		if ($frid && $frid!=$par->ID) {
			$pfr =  get_post($frid);
			$row['fr'] = sprintf('<a href="/?p=%s" target="_blank">%s</a>', $pfr->ID, $pfr->post_title);
			$tr++;
		}
		$table[] = $row;
	}
	$html = '<h5>'.$tr.' articles traduits...</h5><hr>';
	foreach ($table as $row){
		$html .=sprintf('<div class="row mx-0 row-table"><div class="col-6 p-2">%s</div><div class="p-2 col-6 bl">%s</div></div>', $row['ar'], $row['fr']);
	}
	$html .= '<style>.row-table{border-bottom: 1px solid} .row-table .col-6.bl{border-left: 1px solid;}</style>';
	return $html;
});
