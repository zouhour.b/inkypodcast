<?php 

Inkube\Migration\MigrationModule::registerMigration('themes-folders', 'migrate Themes and folders', 'change les folders en thématique et les themes en dossiers', function(){
	
	global $wpdb;
	$wpdb->query("update $wpdb->term_taxonomy set taxonomy='thematique' where taxonomy='folder'");
	$wpdb->query("update $wpdb->term_taxonomy set taxonomy='dossier' where taxonomy='theme'");
});
