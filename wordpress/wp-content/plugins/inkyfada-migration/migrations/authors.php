<?php 

Inkube\Migration\MigrationModule::registerMigration('authorsMigration', 'Migrate Authors', 'divise la méta _contributors en deux meta : authors et contributors pour tous les post de type', function(){
	$posts = get_posts(['numberposts'=>-1, 'lang'=>'']);
	$posts_with_no_contributors = [] ;
	$posts_with_no_authors = [];
	$posts_with_authors = [];
	$posts_with_contributors = [];
	foreach ($posts as $post){
		$id = $post->ID;
		$author = $post->post_author;
		$_contributors = get_post_meta($id, "_contributors", true);
		if (!$_contributors){
			$posts_with_no_contributors[] = sprintf('<div><a href="%s">%s</a></div>', get_post_permalink($id), $post->post_title);
			continue;
		}
		$authors = $contributors = [];
		foreach ($_contributors as $contrib){
			if ($contrib['role']==="author"){
				$authors[] = (int)$contrib['user_id'];
			}
			else {
				$contributors[] = $contrib;
			}
		}

		if (!$authors){
			$posts_with_no_authors[] = sprintf('<div><a href="%s">%s</a></div>', get_post_permalink($id), $post->post_title);
			
		}
		else {
			$posts_with_authors[] = sprintf('<div><a href="%s">%s</a></div>', get_post_permalink($id), $post->post_title);
		}
		update_post_meta($id, 'authors', $authors);
		update_post_meta($id, 'contributions', $contributors);

		if (!$contributors){
			$posts_with_no_contributors[] = sprintf('<div><a href="%s">%s</a></div>', get_post_permalink($id), $post->post_title);

		}
		else{
			$posts_with_contributors[] = sprintf('<div><a href="%s">%s</a></div>', get_post_permalink($id), $post->post_title);

		}



	}
	return sprintf('
		<div><strong>Posts With authors: </strong> %s</div>
		<div><strong>Posts With contributors: </strong> %s</div>
		<div><strong>Post With no authors: </strong>%s</div>
		<div><strong>Post With no contributors: </strong>%s</div><hr style="border-color:white"/><h6>Articles sans contributeurs </h6>
		<div style="overflow-y:auto; max-height:250px">%s</div>
		', 
		count($posts_with_authors),
		count($posts_with_contributors), 
		count($posts_with_no_authors),
		count($posts_with_no_contributors),
		implode('',$posts_with_no_contributors)
	);


});
