<?php 

Inkube\Migration\MigrationModule::registerMigration('nicenames', 'migrate user nicenames', 'Protège les user_login en changeant le user_nicename à prenom-nom', function(){
	$users = get_users([
            "numberusers"=>-1,
	]);
	foreach ($users as $u){
		$id = $u->ID;
		$name = $u->data->display_name;
		$r = sanitize_title( $name, '', 'save' );
		$_u = ['ID'=>$id, 'user_nicename'=>$r];
		wp_update_user( $_u );

	}
	return 'done';



});
