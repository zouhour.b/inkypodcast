<?php 

Inkube\Migration\MigrationModule::registerMigration('positionMigration', 'Migrate Team Position', 'Traduit les position dans la team inkyfada', function(){
	$users = get_users([
            "numberusers"=>20,
            "orderby"=>"name",
            "order"=>"ASC",
            "meta_query"=>[
              ["key"=>"groups", "value"=>"team", "compare"=>"LIKE"]
          ]
	]);
	foreach ($users as $u){
		$id = $u->ID;
		$meta = get_user_meta($id, 'position', true);
		update_user_meta($id, 'position_ar', $meta.' (ar)' );
		update_user_meta($id, 'position_fr', $meta );

	}
	return 'done';



});
