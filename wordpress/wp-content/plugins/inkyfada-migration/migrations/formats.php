<?php 

Inkube\Migration\MigrationModule::registerMigration('post-types-to-format', 'migrate post types to format', 'change les post-types en formats', function(){
	
	global $wpdb;
	$wpdb->query("update $wpdb->term_taxonomy set taxonomy='formats' where taxonomy='post-type' or taxonomy='format'");
});
