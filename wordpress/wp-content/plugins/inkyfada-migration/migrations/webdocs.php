<?php 
function getOrCreateWebdoc($name){
	$post = get_page_by_title( $name, OBJECT, 'webdoc' );
	if ($post){
		return $post->ID;
	}
	$id = wp_insert_post( ['post_type'=>'webdoc', 'post_status'=>'publish',  'post_title'=>$name]);
	return $id;
}

Inkube\Migration\MigrationModule::registerMigration('webdocs', 'migrate webdocs', 'change le modèle des webdocs de taxonomy à posttype', function(){
	
	$webdocs = get_terms(['taxonomy'=>'webdoc', 'lang'=>'','hide_empty'=>false]);
	
	$html = '';
	foreach ($webdocs as $w){

		$id = getOrCreateWebdoc($w->name);
		if ($w->description){
				wp_update_post(['ID'=>$id, 'post_excerpt'=>$w->description]);

		}
		$m = get_term_meta($w->term_id);
		$str = '';
		foreach ($m as $k=>$v){
			if (!$v || !$v[0]) continue;
			switch ($k){
				case 'term_date':
				$str .= 'date migrated <br>';
				wp_update_post(['ID'=>$id, 'post_date'=>$v[0]]);
				break;
				case '_thumbnail_id':
				update_post_meta($id, '_thumbnail_id', $v[0]);
				break;
				case 'view_options':
				var_dump(get_term_meta($w->term_id, 'view_options',true));
				break;
				case 'wpcf-cover-video':
				$str .= 'migrated featuredVideo '.$v[0].'<br>';
				update_post_meta($id, 'featuredVideo', $v[0]);
				break;

			}	
			$posts = get_posts(['numberposts'=>10, 'tax_query'=>[
				['terms'=>[$w->term_id], 'taxonomy'=>'webdoc', 'field'=>'term_id']
			]]);
			$ids = [];
			foreach ($posts as $p){
				$ids[] = $p->ID;
				wp_update_post(['ID'=>$p->ID, 'post_parent'=>$id]);
			}
			if ($ids){
				$ids = array_unique($ids);
				update_post_meta($id, 'parts', $ids);
			}
		}
		

		$html .= sprintf('<div>%s - %s<hr>%s</div>', $id, $w->name, $str);
	}

	return $html;
});
