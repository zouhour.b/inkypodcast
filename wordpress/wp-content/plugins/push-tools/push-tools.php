<?php
/**
 * Plugin Name: Push Tools
 * Description: 
 * Plugin URI: http://example.org/
 * Author: nofearinc
 * Author URI: http://devwp.eu/
 * Version: 1.6
 * Text Domain: dx-sample-plugin
 * License: GPL2
*/

/**
 * Get some constants ready for paths when your plugin grows 
 * 
 */

Class StyleToRtl{
	public function __construct($input, $output){
		if (strpos(home_url(), '.dev')>0 || strpos(home_url(), 'localhost')>0) {

			$file 	= get_template_directory().'/assets/css/'.$input.'.css';
			$output = get_template_directory().'/assets/css/'.$output.'.css';
			if (!file_exists($file)){
				var_dump("FILE  $file not found!");
				die();
			}
			$css = file_get_contents($file);

			$css = str_replace('navbar-left', 'navbar-l', $css);
			$css = str_replace('navbar-right', 'navbar-r', $css);
			$css = str_replace('left', 'wasleft', $css);
			$css = str_replace('right', 'left', $css);
			$css = str_replace('wasleft', 'right', $css);
			$css = str_replace('navbar-l', 'navbar-left', $css);
			$css = str_replace('navbar-r', 'navbar-right', $css);
			$css .= ' body{direction: rtl; unicode-bidi: embed;}';
			file_put_contents($output, $css);
			$count = filesize($output)/1000;
			add_action('admin_notices', function(){
				echo '<div class="notice warning"><p>RTL converter is running...'.$count.'Ko written</p></div>';
			});

		}
	}
}

new StyleToRTL('main', 'main', 'main.rtl');


