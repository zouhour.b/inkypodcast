<?php 
/**
* plugin name: cookie
*/
add_action('wp_head' , function(){
?>
<script>
var myCookies = {};
var firsttimeuser = new Date(Date.now()).toString();
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return decodeURI(dc.substring(begin + prefix.length, end));
} 

function doSomething() {
    var myCookie = getCookie("userid");

    if (myCookie == null) {
    	var value = "userid="+firsttimeuser+";expires=Wed, 06 Nov 9019 14:20:43 GMT;"; 
		document.cookie = value;
    }
    else {
		console.log(myCookie);
    }
}

doSomething()
</script>
<?php 
});
